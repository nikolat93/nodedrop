﻿namespace Emerson.Interface {
    using UnityEngine;

    public class InterfaceInitialization : MonoBehaviour {

        private bool _landmarkDetected;
        public bool landmarkDetected 
        {
            get {
                return _landmarkDetected;
            }
            set {
                if (value) {
                    _landmarkDetectedDialog.SetActive(true);
                }else{
                    _landmarkDetectedDialog.SetActive(false);
                }

                _landmarkDetected = value;
            }
        }

        [SerializeField]
        private GameObject _landmarkDetectedDialog;

        private void Awake () {
            _landmarkDetectedDialog.SetActive(false);
            landmarkDetected = false;
        }

    }

}