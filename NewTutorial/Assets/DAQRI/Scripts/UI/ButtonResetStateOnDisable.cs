﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DAQRI
{
    /// <summary>
    /// This script prevents a bug where the button may briefly appear in the expanded state when re-enabling.
    /// </summary>
    public class ButtonResetStateOnDisable : MonoBehaviour
    {
        [SerializeField]
        private GameObject expandable;

        void OnDisable()
        {
            if (expandable != null)
            {
                expandable.SetActive(false);
            }
        }
    }
}
