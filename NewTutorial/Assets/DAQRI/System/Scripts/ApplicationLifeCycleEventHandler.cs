﻿//  Copyright © 2017 DAQRI, LLC and its affiliates.  All Rights Reserved.

using UnityEngine;

namespace DAQRI {
    using Events;

    /// <summary>
    /// Inherit Application Event Handler to override OnApplicationLoad, OnApplication Foreground, OnApplication Background, OnApplicationStop
    /// </summary>
    public class ApplicationEventHandler : ApplicationEventHandlerAbstractBehaviour 
    {
    
    }
}