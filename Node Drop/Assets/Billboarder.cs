﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DAQRI;

public class Billboarder : MonoBehaviour {
	public Quaternion defaultRotation;

	void Start () {
		defaultRotation = transform.rotation;
	}

	// Update is called once per frame
	void Update () {
		Vector3 toCam = transform.position - Camera.main.transform.position;
		toCam.y = 0f;

		Quaternion lookAt;
		lookAt = Quaternion.LookRotation(toCam, Vector3.up);

		gameObject.transform.rotation = lookAt;

		//transform.LookAt (new Vector3 (GameObject.Find ("Display").transform.position.x, transform.position.y, GameObject.Find ("Display").transform.position.z));
	}
}
