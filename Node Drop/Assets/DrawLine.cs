﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawLine : MonoBehaviour {

	public GameObject anchor;
	LineRenderer line;

	// Use this for initialization
	void Start () {
		line = this.GetComponent<LineRenderer> ();
	}
	
	// Update is called once per frame
	void LateUpdate () {
		line.SetPosition (0, this.transform.position);
		line.SetPosition (1, anchor.transform.position);
	}
}
