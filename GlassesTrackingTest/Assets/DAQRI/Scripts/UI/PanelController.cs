﻿//  Copyright © 2017 DAQRI, LLC and its affiliates.  All Rights Reserved.


using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using DAQRI.UI;

/// <summary>
/// Provides functionality for navigating between a series of panels.
/// See Panel for more information on individual panels.
/// </summary>
public class PanelController : PanelControllerAbstractBehaviour {
    
}
