﻿/****************************************************************************************************************************************
 * © 2016 Daqri International. All Rights Reserved.                                                                                     *
 *                                                                                                                                      *
 *     NOTICE:  All software code and related information contained herein is, and remains the property of DAQRI INTERNATIONAL and its  *
 * suppliers, if any.  The intellectual and technical concepts contained herein are proprietary to DAQRI INTERNATIONAL and its          *
 * suppliers and may be covered by U.S. and Foreign Patents, patents in process, and/or trade secret law, and the expression of         *
 * those concepts is protected by copyright law. Dissemination, reproduction, modification, public display, reverse engineering, or     *
 * decompiling of this material is strictly forbidden unless prior written permission is obtained from DAQRI INTERNATIONAL.             *
 *                                                                                                                                      *
 *                                                                                                                                      *
 *                                                                                                                                      *
 *     File Purpose:        Used by WorkInstructions Example                                                                            *
 *                                                                                                                                      *
 ****************************************************************************************************************************************/

using UnityEngine;
using System.Collections.Generic;

public class WorkInstructionsController : MonoBehaviour {

	public List<GameObject> steps;

	private int currentIndex;

	void Start()
	{
		currentIndex = 0;
		foreach (GameObject step in steps)
		{
			step.gameObject.SetActive(false);
		}
		steps[currentIndex].gameObject.SetActive(true);
	}

	public void GoToNextStep()
	{
		if (currentIndex <= steps.Count - 2)
		{
			steps[currentIndex].SetActive(false);
			currentIndex++;
			steps[currentIndex].SetActive(true);
			Debug.Log("Step " + currentIndex + " loaded");
		}
		else {
			Debug.Log("You're at the last step. No more steps to navigate to!");
		}
	}

	public void GoToPreviousStep()
	{
		if (currentIndex >= 1)
		{
			steps[currentIndex].SetActive(false);
			currentIndex--;
			steps[currentIndex].SetActive(true);
		}
	}

	public void Restart()
	{
		steps[currentIndex].SetActive(false);
		currentIndex = 0;
		steps[currentIndex].SetActive(true);
		Debug.Log("Step " + currentIndex + " loaded");
	}

	public void Quit()
	{
		Debug.Log ("Application Quit");
		Application.Quit();
	}
}
