﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeactivateAfterTime : MonoBehaviour {

    public float lifespan = 3;

    private void OnEnable () {
        StartCoroutine(DisableCoroutine(lifespan));
    }
    
    private IEnumerator DisableCoroutine (float delay) {
        yield return new WaitForSeconds(delay);
        gameObject.SetActive(false);
    }

}
