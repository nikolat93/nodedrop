﻿namespace Core
{
	using UnityEngine;

	public class TransformParentConstraint : MonoBehaviour 
	{
		public Transform target;

		[Header("Offsets")]
		public Vector3 positionOffset;
		public Vector3 rotationOffset;

		[Header("Constraints")]
		public bool constrainPosition = true;
		public bool constrainRotation = true;

		private void LateUpdate() 
		{
			if (constrainPosition)
				transform.position = target.TransformPoint(positionOffset);
			if (constrainRotation)
				transform.rotation = target.rotation * Quaternion.Euler(rotationOffset);
		}
	}	
}