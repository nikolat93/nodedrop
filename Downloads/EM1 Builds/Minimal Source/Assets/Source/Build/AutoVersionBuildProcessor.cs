﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.Build;
class AutoVersionBuildProcessor : IPreprocessBuild
{
    public int callbackOrder { get { return 0; } }
    public void OnPreprocessBuild(BuildTarget target, string path) {
        PlayerSettings.bundleVersion = System.DateTime.Now.ToString("MM/dd/yy HH:mm");
    }
}
#endif