﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ErrorScript : MonoBehaviour {

    public Transform target;
    public float magnitudeEndThreshold = 1;
    public float magnitudeStartThreshold = 2;

    public Transform indicator;
    public Transform Icon;
    public Transform InfoPanel;

    public string ErrorCodeText;
    public string ErrorDescriptionText;
    public string TaskText;

    public Text ErrorCodeTextBox;
    public Text ErrorDescriptionTextBox;
    public Text TaskTextBox;


    // Use this for initialization
    void Start()
    {
        InfoPanel.gameObject.SetActive(false);

        ErrorCodeTextBox.text = ErrorCodeText;
        ErrorDescriptionTextBox.text = ErrorDescriptionText;
        TaskTextBox.text = TaskText;
    }


    void Update()
    {
        float p = 0;

        // Rotate the camera every frame so it keeps looking at the target
        float magnitude = (transform.position - target.transform.position).magnitude;

        if (magnitude < magnitudeStartThreshold) {

            // Scale 
            float max = magnitudeStartThreshold;
            float min = magnitudeEndThreshold;

            // 0- 1
            float pm = (1-( (magnitude - min) / (max - min)))*0.9f;

            //------------------

            p += pm;
            //Debug.Log("pm "+ pm);
        }

        float angle = Vector3.Angle(target.transform.forward, transform.forward);
        //Debug.Log("angle: "+ angle);
        if (angle < 15.0f)
        {
            float angleMin = 5;
            float angleMax = 15;

            // 0- 1
            float pa = (1 - (angle - angleMin) / (angleMax - angleMin)) * 0.1f;
            //Debug.Log(pa);

            p += pa;
            //Debug.Log("pa "+pa);
        }


        //Debug.Log("p  "+p);


        if (p > .9f)
        {
            //Change state
            InfoPanel.gameObject.SetActive(true);
            Icon.gameObject.SetActive(false);
            //Debug.Log("PanelState");
        }
        else {
            InfoPanel.gameObject.SetActive(false);
            Icon.gameObject.SetActive(true);
            //0.5 - 1
            indicator.localScale = Vector3.one * ((p / 2) + 0.5f);
            //Debug.Log("IconState");
        }

    }
}
