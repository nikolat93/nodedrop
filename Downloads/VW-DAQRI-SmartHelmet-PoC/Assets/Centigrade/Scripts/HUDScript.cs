﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HUDScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}

    // Update is called once per frame
    float timeLeft = 4.0f;
    void Update()
    {
        timeLeft -= Time.deltaTime;
        if (timeLeft < 0.0f)
        {
            this.gameObject.SetActive(false);
        }
    }

    public void OnMarkerFound()
    {
        //this.gameObject.SetActive(false);
    }

    public void OnMarkerLost()
    {
        //this.gameObject.SetActive(true);
    }
}
