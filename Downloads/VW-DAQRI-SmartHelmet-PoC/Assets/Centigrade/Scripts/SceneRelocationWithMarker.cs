﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace DAQRI
{
    public class SceneRelocationWithMarker : MonoBehaviour
    {
        public GameObject SceneRoot;

        /*
	    // Use this for initialization
	    void Start () {
	    }
	
	    // Update is called once per frame
	    void Update () {
	    }
        */


        private bool wasFound = false;

        public void OnTrackedObjectFound()
        {
            wasFound = true;
            //Debug.Log("Repos");

            Transform markerTransform = this.transform;

            SceneRoot.transform.position = -markerTransform.position;// - PositionOffset;
            SceneRoot.transform.rotation = Quaternion.Euler( markerTransform.rotation.eulerAngles);

            /*
            // this is just one approach...
            foreach (Transform child in transform)
            {
                child.gameObject.SetActive(true);
                wasFound = true;
            }
            */
        }

        public void OnTrackedObjectLost()
        {
            wasFound = false;

            //Debug.Log("Lost");

            //Debug.Log(to.gameObject.name + " lost at [" + Time.time + "]");
            /*
            if (wasFound)
            {
                foreach (Transform child in transform)
                {
                    child.gameObject.SetActive(false);
                }
            }
            */
        }
    }
}


