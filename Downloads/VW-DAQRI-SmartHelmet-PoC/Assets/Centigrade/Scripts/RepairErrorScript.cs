﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class RepairErrorScript : MonoBehaviour {


    //public string[] steps;
	//public string stepText;

	//public string StepsText;
	public string DescriptionText;

    //public Text StepsTextBox;
    public Text DescriptionTextBox;

    public Button NextButton;
    public Button BackButton;
    public Button HelpButton;

    //public GameObject root;

	public int Case;

    int index = 0;

    // Use this for initialization
    void Start () {

        this.gameObject.SetActive(false);
        this.gameObject.transform.localPosition = Vector3.zero;
        //SetStep();

		//StepsTextBox.text = StepsText;
		//DescriptionTextBox.text = DescriptionText;

        //EverythingDoneButton.gameObject.SetActive(false);
        //BackButton.gameObject.SetActive(false);

        NextButton.onClick.AddListener(OnNextButton);
        BackButton.onClick.AddListener(OnBackButton);
		HelpButton.onClick.AddListener(OnHelpButton);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnBackButton() {

		ErrorPanelScript.FindObject(this.transform.parent.gameObject, "DetailedError").SetActive(true);
		this.gameObject.SetActive(false);

		/*
        if (index > 0) {
            index--;
            SetStep();

            StepsTextBox.gameObject.SetActive(true);
            DescriptionTextBox.gameObject.SetActive(true);
            NextButton.gameObject.SetActive(true);
            EverythingDoneButton.gameObject.SetActive(false);
        }

        if (index == 0)
        {
            ErrorPanelScript.FindObject(this.transform.parent.gameObject, "DetailedError").SetActive(true);
            this.gameObject.SetActive(false);
            //BackButton.gameObject.SetActive(false);
        }
        */
    }

    public void OnNextButton() {

		/*
        BackButton.gameObject.SetActive(true);

        if (index < steps.Length+1)
        {
            index++;
        }
        //Debug.Log(index);
        if (index < steps.Length)
        {
            SetStep();
        }

        if (index == steps.Length)
        {
            StepsTextBox.gameObject.SetActive(false);
            DescriptionTextBox.gameObject.SetActive(false);
            NextButton.gameObject.SetActive(false);
            EverythingDoneButton.gameObject.SetActive(true);
        }
        */


		//Debug.Log("Close");
		//this.gameObject.SetActive(false);

		// write data to permascript
		if (this.Case == 1){
			PlayerPrefs.SetInt("IsCase1Resolved", 1);
		}else if(this.Case == 2){
			PlayerPrefs.SetInt("IsCase2Resolved", 1);
		}else if(this.Case == 3){
			PlayerPrefs.SetInt("IsCase3Resolved", 1);
		}

		//root.SetActive(false);
		SceneManager.LoadScene("VW-Operations-Fault", LoadSceneMode.Single);
    }
		
    public void OnHelpButton()
    {
		ErrorPanelScript.FindObject(this.transform.parent.gameObject, "ServiceError").SetActive(true);
		this.gameObject.SetActive(false);
    }


    
	/*
    private void SetStep() {
        StepsTextBox.text = "Step " + (index + 1) + "/" + steps.Length;
        DescriptionTextBox.text = steps[index];
    }
    */


    /*
    public static GameObject FindObject(GameObject parent, string name)
    {
        Transform[] trs = parent.GetComponentsInChildren<Transform>(true);
        foreach (Transform t in trs)
        {
            if (t.name == name)
            {
                return t.gameObject;
            }
        }
        return null;
    }
    */
}
