﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}


	float w = 0.0f;
	// Update is called once per frame
	void Update () {

		w += 0.001f;

		Debug.Log (w);

		if(w > 1){
			w = 0;
		}
		float f = Mathf.Sin (Mathf.PI * 2 * w);

		float rot = f * 40;

        //transform.Rotate(Vector3.right * Time.deltaTime);

        // ...also rotate around the World's Y axis
		transform.rotation = Quaternion.Euler(new Vector3(0,rot,0));

    }
}
