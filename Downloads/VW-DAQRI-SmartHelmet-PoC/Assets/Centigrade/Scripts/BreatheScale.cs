﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreatheScale : MonoBehaviour
{
    Vector3 startPos;

    public float amplitude = 0.4f;
    public float period = 0.15f;

    protected void Start()
    {
        startPos = transform.localScale;
    }

    protected void Update()
    {
        float theta = Time.timeSinceLevelLoad / period;
        float distance = amplitude * Mathf.Sin(theta);
        transform.localScale = startPos + Vector3.one * distance;
    }
}