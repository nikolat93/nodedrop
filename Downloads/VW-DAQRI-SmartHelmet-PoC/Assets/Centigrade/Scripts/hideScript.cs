﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hideScript : MonoBehaviour {

    public GameObject source;
    public GameObject target;

    public GameObject d1;
    public GameObject d2;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if (Mathf.Abs((d1.transform.position - d2.transform.position).magnitude) < 1.0f) {
            if (source.activeSelf)
            {
                target.SetActive(true);
            }
            else
            {
                target.SetActive(false);
            }
        } else {
            target.SetActive(false);
        }
        Debug.Log(Mathf.Abs((d1.transform.position - d2.transform.position).magnitude));
    }
}
