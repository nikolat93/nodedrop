﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ServiceErrorScript : MonoBehaviour {


    
    public Button BackButton;

    public Button Person1Button;
    public Button Person2Button;
    public Button Person3Button;

    public int Case;

    int index = 0;

    // Use this for initialization
    void Start()
    {

        this.gameObject.SetActive(false);
        this.gameObject.transform.localPosition = Vector3.zero;
        //SetStep();

        //EverythingDoneButton.gameObject.SetActive(false);
        //BackButton.gameObject.SetActive(false);

        BackButton.onClick.AddListener(OnBackButton);
        Person1Button.onClick.AddListener(OnPerson1Button);
        Person2Button.onClick.AddListener(OnPerson2Button);
        Person3Button.onClick.AddListener(OnPerson3Button);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnBackButton()
    {
        ErrorPanelScript.FindObject(this.transform.parent.gameObject, "DetailedError").SetActive(true);
        this.gameObject.SetActive(false);
    }

    public void OnPerson1Button()
    {

        Finish();
    }

    public void OnPerson2Button()
    {

        Finish();
    }

    public void OnPerson3Button()
    {

        Finish();
    }



    public void Finish()
    {
        if (this.Case == 1)
        {
            PlayerPrefs.SetInt("IsCase1Resolved", 1);
        }
        else if (this.Case == 2)
        {
            PlayerPrefs.SetInt("IsCase2Resolved", 1);
        }
        else if (this.Case == 3)
        {
            PlayerPrefs.SetInt("IsCase3Resolved", 1);
        }

        SceneManager.LoadScene("VW-Operations-Fault", LoadSceneMode.Single);
    }


    /*
    public static GameObject FindObject(GameObject parent, string name)
    {
        Transform[] trs = parent.GetComponentsInChildren<Transform>(true);
        foreach (Transform t in trs)
        {
            if (t.name == name)
            {
                return t.gameObject;
            }
        }
        return null;
    }
    */
}
