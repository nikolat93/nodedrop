﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdjustCanvasDistscript : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        Vector3 offset = new Vector3(0.001f, .0f, .0f);

        if (Input.GetKey("r"))
            this.transform.localPosition = this.transform.localPosition + offset;

        if (Input.GetKey("t"))
            this.transform.localPosition = this.transform.localPosition - offset;

    }
}
