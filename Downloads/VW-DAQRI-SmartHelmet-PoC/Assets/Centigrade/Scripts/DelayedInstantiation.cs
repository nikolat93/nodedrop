﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DelayedInstantiation : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    float timeLeft = 2.0f;
    bool triggered = false;
    void Update()
    {
        if (triggered) {
            return;
        }
        timeLeft -= Time.deltaTime;
        //Debug.Log("timeLeft: " + timeLeft);
        if (timeLeft < 0.0f)
        {
            //Debug.Log("now");
            foreach (Transform t in transform)
            {
                t.gameObject.SetActive(true);
            }

            triggered = true;
        }
    }
}
