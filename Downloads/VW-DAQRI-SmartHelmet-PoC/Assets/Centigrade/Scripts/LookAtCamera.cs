﻿using UnityEngine;
using System.Collections;

public class LookAtCamera : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    public Transform target;

    void Update()
    {
        // Rotate the camera every frame so it keeps looking at the target
        transform.LookAt(2 * transform.position - target.position);
    }
}
