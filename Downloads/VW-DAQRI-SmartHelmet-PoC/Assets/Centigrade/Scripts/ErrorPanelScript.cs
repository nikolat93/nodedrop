﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ErrorPanelScript : MonoBehaviour {

    public GameObject Info;
    //public GameObject Task;

    //public GameObject AssociatedTakt;
    public GameObject Overview;
    //public GameObject TargetIndecator;

	public string ToBeLoadedSceneName;

	public int Case;

    // Use this for initialization
    void Start () {
        Info.SetActive(true);
        //Task.SetActive(false);

        try
        {
            if (this.Case == 1 && (PlayerPrefs.GetInt("IsCase1Resolved") == 1))
            {
                SceneManager.UnloadSceneAsync("VW-Operations-Fault_Case1");
            }
            if (this.Case == 2 && (PlayerPrefs.GetInt("IsCase2Resolved") == 1))
            {
                SceneManager.UnloadSceneAsync("VW-Operations-Fault_Case2");
            }
            if (this.Case == 3 && (PlayerPrefs.GetInt("IsCase3Resolved") == 1))
            {
                SceneManager.UnloadSceneAsync("VW-Operations-Fault_Case3");
            }
        }
        catch (Exception e) {

        }

        Debug.Log("SceneManager: "+SceneManager.sceneCount);
        

        if ((this.Case == 1 && (PlayerPrefs.GetInt("IsCase1Resolved") == 1)) || 
			(this.Case == 2 && (PlayerPrefs.GetInt("IsCase2Resolved") == 1)) || 
			(this.Case == 3 && (PlayerPrefs.GetInt("IsCase3Resolved") == 1))){
			this.gameObject.SetActive (false);
		}

        //AssociatedTakt.transform.Find("ServiceError").GetComponent<>();
    }

    // Update is called once per frame
    void Update () {
       
	}

	/*
    public void OnTaskButton() {
        Info.SetActive(false);
        Task.SetActive(true);

        //AssociatedTakt.SetActive(true);
    }
    */

    public void OnConfirmButton()
    {
        Overview.SetActive(false);
        //TargetIndecator.SetActive(false);

		// load othere Scene
		SceneManager.LoadScene(ToBeLoadedSceneName, LoadSceneMode.Single);

        /*
        GameObject go = FindObject(AssociatedTakt, "RepairError");
        RepairErrorScript sc = go.GetComponent(typeof(RepairErrorScript)) as RepairErrorScript;
        sc.AssociatedErrorIcon = this.gameObject;
        */
    }

	/*
    public void OnBackButton()
    {
        Info.SetActive(true);
        Task.SetActive(false);
    }
    */

	[RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
	static void OnBeforScenLaodRuntimeMethode(){
		PlayerPrefs.SetInt("IsCase1Resolved", 0);
		PlayerPrefs.SetInt("IsCase2Resolved", 0);	
		PlayerPrefs.SetInt("IsCase3Resolved", 0);
	}


    public static GameObject FindObject(GameObject parent, string name)
    {
        Transform[] trs = parent.GetComponentsInChildren<Transform>(true);
        foreach (Transform t in trs){
            if (t.name == name){
                return t.gameObject;
            }
        }
        return null;
    }
}
