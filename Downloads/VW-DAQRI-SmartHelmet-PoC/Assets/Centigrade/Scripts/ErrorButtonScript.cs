﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ErrorButtonScript : MonoBehaviour
{
    public Transform Icon;
    public Transform InfoPanel;

    public string ErrorCodeText;
    public Text ErrorCodeTextBox;

	public string GoToText;
	public Text GoToTextBox;

    // Use this for initialization
    void Start()
    {
        InfoPanel.gameObject.SetActive(false);
        ErrorCodeTextBox.text = ErrorCodeText;
		GoToTextBox.text = GoToText;
    }

    void Update()
    {
    }

    public void OpenError()
    {
        InfoPanel.gameObject.SetActive(true);
        Icon.gameObject.SetActive(false);

        //CloseOthers(this.transform.parent.gameObject);
    }

    public void CloseError()
    {
        InfoPanel.gameObject.SetActive(false);
        Icon.gameObject.SetActive(true);
    }


	/*
    public  GameObject CloseOthers(GameObject parent)
    {
        Transform[] trs = parent.GetComponentsInChildren<Transform>(true);
        foreach (Transform t in trs)
        {
            if (t.gameObject.GetComponent<ErrorButtonScript>() != null && 
                t.gameObject.GetComponent<ErrorButtonScript>() != this.gameObject.GetComponent<ErrorButtonScript>())
            {
                t.gameObject.GetComponent<ErrorButtonScript>().CloseError();
            }
        }
        return null;
    }
    */
}
