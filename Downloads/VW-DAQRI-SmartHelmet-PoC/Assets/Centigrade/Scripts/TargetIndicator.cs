﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



public class TargetIndicator : MonoBehaviour {

    public GameObject Target;
    public GameObject IndikatorOut;
    public GameObject IndikatorIn;

    public float Weight = 0.6f;

    //OnScreenDebugScript osds;

    //float screenWidth = 2736.0f;
    //float screenHeight = 768.0f;

    float screenWidthHelmet = 1210.0f;
    float screenHeightHelmet = 833.0f;

    float screenWidth;
    float screenHeight;

    float halfScreenWidth;
    float halfScreenHeight;

    // Use this for initialization
    void Start () {

        if (Application.isEditor)
        {
            screenWidth = Screen.width;
            screenHeight = Screen.height;
        }
        else {
            screenWidth = screenWidthHelmet;
            screenHeight = screenHeightHelmet;
        }

        //osds = (OnScreenDebugScript)GameObject.Find("DebugText").GetComponent("OnScreenDebugScript");

        halfScreenWidth = screenWidth / 2.0f;
        halfScreenHeight = screenHeight / 2.0f;
    }
	
	// Update is called once per frame
	void Update () {
        /*
       halfScreenWidth = screenWidth / 2.0f;
       halfScreenHeight = screenHeight / 2.0f;


       if (Input.GetKey("g"))
           screenWidth += 5;

       if (Input.GetKey("j"))
           screenWidth -= 5;

       if (Input.GetKey("y"))
           screenHeight += 5;

       if (Input.GetKey("n"))
           screenHeight -= 5;
       */

        /*
        if (osds != null) {
            osds.outputString = "w " + screenWidth.ToString() + " / " + " h " + screenHeight.ToString();
        }
        */
        

        Vector3 screenpos = Camera.main.WorldToScreenPoint(Target.transform.position);
        Vector3 screenCenter = new Vector3(halfScreenWidth, halfScreenHeight, 0);
        screenpos -= screenCenter;

       

        if (screenpos.z > 0 &&
            screenpos.x > -halfScreenWidth * Weight && screenpos.x < halfScreenWidth * Weight &&
            screenpos.y > -halfScreenHeight * Weight && screenpos.y < halfScreenHeight * Weight
            )
        { // OnScreen
            IndikatorOut.SetActive(false);
            IndikatorIn.SetActive(true);
            IndikatorIn.transform.localPosition = screenpos;
        }
        else
        { // Offscreen
            //Indikator.SetActive(true);
            if (screenpos.z < 0) {
                screenpos *= -1;
            }

            float angle = Mathf.Atan2(screenpos.y, screenpos.x);
            angle -= 90 * Mathf.Deg2Rad;

            float cos = Mathf.Cos(angle);
            float sin = -Mathf.Sin(angle);

            screenpos = screenCenter + new Vector3(sin * 150, cos * 150, 0);

            float m = cos / sin;
            Vector3 screenBounds = screenCenter * Weight;

            if (cos > 0) { //up
                screenpos = new Vector3(screenBounds.y/m, screenBounds.y, 0);
            }
            else {//down
                screenpos = new Vector3(-screenBounds.y/m, -screenBounds.y, 0);
            }

            if (screenpos.x > screenBounds.x) {
                screenpos = new Vector3(screenBounds.x, screenBounds.x * m, 0);

            } else if(screenpos.x < -screenBounds.x) {
                screenpos = new Vector3(-screenBounds.x, -screenBounds.x * m, 0);
            }

           
            IndikatorOut.SetActive(true);
            IndikatorIn.SetActive(false);
            
            IndikatorOut.transform.localPosition = screenpos;
            IndikatorOut.transform.localRotation = Quaternion.Euler(0, 0, angle * Mathf.Rad2Deg);
        }
        
    }

}
