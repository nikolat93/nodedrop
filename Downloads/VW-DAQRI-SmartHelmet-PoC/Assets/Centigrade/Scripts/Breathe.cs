﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Breathe : MonoBehaviour
{
    Vector3 startPos;

    float amplitude = 0.4f;
    float period = 0.15f;

    protected void Start()
    {
        startPos = transform.localPosition;
    }

    protected void Update()
    {
        float theta = Time.timeSinceLevelLoad / period;
        float distance = amplitude * Mathf.Sin(theta);
        transform.localPosition = startPos + Vector3.right * distance;
    }
}
