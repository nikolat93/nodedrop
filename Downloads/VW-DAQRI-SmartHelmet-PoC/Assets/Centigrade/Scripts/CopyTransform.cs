﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CopyTransform : MonoBehaviour {

    public Transform target;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void LateUpdate () {
        this.transform.position = target.position;
        this.transform.rotation = target.rotation;
        this.transform.localScale = target.localScale;
    }
}
