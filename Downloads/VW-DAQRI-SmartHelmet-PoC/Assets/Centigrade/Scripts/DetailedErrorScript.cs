﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DetailedErrorScript : MonoBehaviour
{
    public string ErrorName;
	public string ErrorCode;
    public string ErrorDetails;

    public Text ErrorNameTextBox;
	public Text ErrorCodeTextBox;
    public Text ErrorDetailsTextBox;

    public Button RepairButton;
    public Button ServiceButton;

    // Use this for initialization
    void Start()
    {
        ErrorNameTextBox.text = ErrorName;
		ErrorCodeTextBox.text = ErrorCode; 
        //ErrorDetailsTextBox.text = ErrorDetails;

        RepairButton.onClick.AddListener(OnRepairButton);
        ServiceButton.onClick.AddListener(OnServiceButton);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnRepairButton() {
        Debug.Log("OnRepairButton");


        ErrorPanelScript.FindObject(this.transform.parent.gameObject, "RepairError").SetActive(true);
        this.gameObject.SetActive(false);
    }

    public void OnServiceButton()
    {
        Debug.Log("OnServiceButton");
        ErrorPanelScript.FindObject(this.transform.parent.gameObject, "ServiceError").SetActive(true);
        this.gameObject.SetActive(false);
    }
}
