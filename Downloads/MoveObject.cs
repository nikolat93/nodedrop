﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

//*WARNING* Lots of placeholder code and hard-coded values
// Take whatever is needed, not intended as a model for implementation

namespace DAQRI 
{
	public class MoveObject : MonoBehaviour {

		public bool moveXNeg = false;
		public bool moveXPos = false;
		public bool moveYNeg = false;
		public bool moveYPos = false;
		public bool moveZNeg = false;
		public bool moveZPos = false;
		bool snapLock = false;
		public GameObject lockIcon;
		public GameObject rotateIcon;
		public GameObject translateIcon;
		public GameObject scaleIcon;
		public Sprite open;
		public Sprite close;
		public Sprite rotation;
		public Sprite translate;
		bool locked;
		bool rotate;
		bool tranzlate;
		bool sphereEnabled;
		bool scale; 
		bool rotLeft;
		bool rotRight;

		public GameObject rotater;
		public GameObject scaler; 

		GameObject arrows;
		Vector3 centerPos;
		Vector3 PositiveX;
		Vector3 NegativeX;
		Vector3 PositiveZ;
		Vector3 NegativeZ;
		Vector3 PositiveY;
		Vector3 NegativeY;

		public GameObject [] cones;
		public GameObject [] spheres;
		public GameObject [] cubes;

		float timeStamp;
		float timeToSpeedUp = 1.5f;
		float scaleFactor = .005f;
		float slowSpeed = .005f;
		float fastSpeed = .01f;

		bool grow;
		bool shrink;

		// Use this for initialization
		void Start () {
			Application.targetFrameRate = 90;
			arrows = GameObject.Find ("MoveableObject").transform.GetChild (1).gameObject;
			//SnapArrows ();

			tranzlate = true;
				
			rotater.SetActive (false);
			arrows.SetActive (false);
			this.GetComponent<Collider> ().enabled = false;
			locked = true;
		} 

		public void RotateLeft () {
			rotLeft = true;
		}

		public void RotateRight () {
			rotRight = true;
		}

		void Update () {
			//custom coordinate system of vectors relative to each directional control
			if (GameObject.Find ("Center") != null) 
				centerPos = GameObject.Find ("Center").transform.position;
			if (GameObject.Find ("PositiveX") != null)
				PositiveX = GameObject.Find ("PositiveX").transform.position - centerPos;
			if (GameObject.Find ("NegativeX") != null)
				NegativeX = GameObject.Find ("NegativeX").transform.position - centerPos;
			if (GameObject.Find ("PositiveZ") != null)
				PositiveZ = GameObject.Find ("PositiveZ").transform.position - centerPos;
			if (GameObject.Find ("NegativeZ") != null)
				NegativeZ = GameObject.Find ("NegativeZ").transform.position - centerPos;
			if (GameObject.Find ("PositiveY") != null)
				PositiveY = GameObject.Find ("PositiveY").transform.position - centerPos;
			if (GameObject.Find ("NegativeY") != null)
				NegativeY = GameObject.Find ("NegativeY").transform.position - centerPos;

			//simple distance check so that the object collider doesn't interfere with arrow colliders
			//needs to be reworked to prioritize arrow colliders when overlapping with the object
			if (arrows != null) {
				float distanceBetween = Vector3.Distance (this.transform.position, arrows.transform.position);
				if (distanceBetween >= .5f)
					this.GetComponent<Collider> ().enabled = true;
			}

			//movement bools for each respective direction 
			//scale factor used to adjust for frame rate
			//snapLock prevents a control from being activate unintentionally when the controls are repositioned 
			if (moveXNeg && !snapLock) {
				if (Time.time < timeStamp)
					scaleFactor = slowSpeed;
				else
					scaleFactor = fastSpeed;

				transform.position = (transform.position + (NegativeX.normalized * scaleFactor));
			}
			if (moveXPos && !snapLock) {
				if (Time.time < timeStamp)
					scaleFactor = slowSpeed;
				else
					scaleFactor = fastSpeed;

				transform.position = (transform.position + (PositiveX.normalized * scaleFactor));
			}
			if (moveYNeg && !snapLock) {
				if (Time.time < timeStamp)
					scaleFactor = slowSpeed;
				else
					scaleFactor = fastSpeed;

				transform.position = (transform.position + (NegativeY.normalized * scaleFactor));
			}
			if (moveYPos && !snapLock) {
				if (Time.time < timeStamp)
					scaleFactor = slowSpeed;
				else
					scaleFactor = fastSpeed;

				transform.position = (transform.position + (PositiveY.normalized * scaleFactor));
			}
			if (moveZNeg && !snapLock) {
					if (Time.time < timeStamp)
						scaleFactor = slowSpeed;
					else
						scaleFactor = fastSpeed;
					
					transform.position = (transform.position + (NegativeZ.normalized * scaleFactor));
			}
			if (moveZPos && !snapLock) {
				if (Time.time < timeStamp)
					scaleFactor = slowSpeed;
				else
					scaleFactor = fastSpeed;

				transform.position = (transform.position + (PositiveZ.normalized * scaleFactor));
			}

			//code for spherical rotation control
			// 1.) Get the ray of the user's gaze  
			// 2.) Get the normal of the gaze ray's intersection point with the sphere
			// 3.) Get the cross product of these two vectors to get the rotational axis
			if (sphereEnabled && rotate && !snapLock) {
				Vector3 gazeDirection = DisplayManager.Instance.transform.forward;
				Vector3 surfaceNormal = Vector3.zero;
				RaycastHit hit;

				if (Physics.Raycast(DisplayManager.Instance.transform.position, gazeDirection, out hit))
					surfaceNormal = hit.normal;
					
				Vector3 perpendicularAxis = Vector3.Cross (surfaceNormal, gazeDirection);

				transform.RotateAround(this.transform.position, perpendicularAxis, 20 * Time.deltaTime);
				GameObject.Find("Center (1)").transform.RotateAround(GameObject.Find("Center (1)").transform.position, perpendicularAxis, 20 * Time.deltaTime);
			}

			//code for basic rotation of the translation controls
			if (!snapLock && rotLeft) {
				arrows.transform.RotateAround(arrows.transform.position, arrows.transform.up, 20 * Time.deltaTime);
			}

			if (!snapLock && rotRight) {
				arrows.transform.RotateAround(arrows.transform.position, -arrows.transform.up, 20 * Time.deltaTime);
			}

			//code for scaling (should probably use time.DeltaTime to be framerate independent)
			if (grow && !snapLock) {
				transform.localScale *= 1.01f;
			}

			if (shrink && !snapLock) {
				transform.localScale /= 1.01f;
			}
		}

		void OnTriggerEnter (Collider c) {
			if (c.gameObject.tag == "Arrow")
				this.GetComponent<Collider> ().enabled = false;
		}

		//called on pointer exit for scaler control
		public void ResetScaling () {
			grow = false;
			shrink = false;
			snapLock = false;
		}

		//called on pointer exit for translation (arrows) rotation control
		public void ResetArrowRot () {
			rotLeft = false;
			rotRight = false;
		}

		//called on pointer exit for each respective translation control
		public void ResetXNeg () {
			GameObject NegX = GameObject.Find ("NegativeX");
			if (!snapLock)
				NegX.transform.localScale = NegX.transform.localScale / 1.2f;
			moveXNeg = false;
			snapLock = false;
		}

		public void ResetXPos () {
			GameObject PosX = GameObject.Find ("PositiveX");
			if (!snapLock)
				PosX.transform.localScale = PosX.transform.localScale / 1.2f;
			moveXPos = false;
			snapLock = false;
		}

		public void ResetYNeg () {
			GameObject NegY = GameObject.Find ("NegativeY");
			if (!snapLock)
				NegY.transform.localScale = NegY.transform.localScale / 1.2f;
			moveYNeg = false;
			snapLock = false;
		}

		public void ResetYPos () {
			GameObject PosY = GameObject.Find ("PositiveY");
			if (!snapLock)
				PosY.transform.localScale = PosY.transform.localScale / 1.2f;
			moveYPos = false;
			snapLock = false;
		}

		public void ResetZNeg () {
			GameObject NegZ = GameObject.Find ("NegativeZ");
			if (!snapLock)
				NegZ.transform.localScale = NegZ.transform.localScale / 1.2f;
			moveZNeg = false;
			snapLock = false;
		}

		public void ResetZPos () {
			GameObject PosZ = GameObject.Find ("PositiveZ");
			if (!snapLock)
				PosZ.transform.localScale = PosZ.transform.localScale / 1.2f;
			moveZPos = false;
			snapLock = false;
		}

		//called on pointer exit for rotation control
		public void ResetRotater () {
			GameObject rotater = GameObject.Find ("Rotater");
			if (!snapLock) {
				rotater.transform.DetachChildren ();
				rotater.transform.localScale = GameObject.Find ("Rotater").transform.localScale / 1.2f;
				GameObject.Find ("UI").transform.parent = rotater.transform;
				GameObject.Find ("Center (1)").transform.parent = rotater.transform;
			}
			sphereEnabled = false;
			snapLock = false;
		}

		//hard-coded reset methods
		//called when:
		//1.) that respective button is pressed while already toggled 
		//for instance - dwell once to toggle the translation controls, dwell again to reset its position
		//2.) the Oshit button is dwelled on
		public void ResetPosition () {
			this.transform.position = arrows.transform.position;
			snapLock = true;
		}

		public void ResetRotation () {
			this.transform.rotation = new Quaternion (0, 0, 0, 0);
			snapLock = true;
		}

		public void ResetScale () {
			this.transform.localScale = new Vector3 (.7f, .7f, .7f);
			snapLock = true;
		}

		//moves the UI and control elemets to the position of the object when it's dwelled on
		//for larger models and scalability's sake, it would probably better to use a generic sphere collider
		public void SnapArrows () {
			snapLock = true;

			if (arrows.activeInHierarchy) {
				GameObject.Find ("UI").transform.parent = arrows.transform;
				GameObject.Find ("UI").transform.localPosition = new Vector3 (0, 1.4f, 0);
			}
				
			if (rotater.activeInHierarchy) {
				GameObject.Find ("UI").transform.parent = rotater.transform;
				GameObject.Find ("UI").transform.localPosition = new Vector3 (0, .5f, 0);
			}

			arrows.transform.position = this.transform.position;
			rotater.transform.position = this.transform.position;
			scaler.transform.position = this.transform.position;
		}

		//body space button used to move an object in front of the user and reset all transform
		public void Oshit () {
			GameObject.Find("MoveableObject").transform.position = DisplayManager.Instance.transform.position + (DisplayManager.Instance.transform.forward * 2.5f);
			ResetPosition ();
			ResetRotation ();
			ResetScale ();
		}


		//code for the lock button
		public void ToggleLock () {
			locked = !locked;

			if (locked) {
				this.GetComponent<Collider> ().enabled = false;
				rotateIcon.transform.position = (this.transform.position + (Vector3.up * .35f));
				lockIcon.transform.position = (this.transform.position + (Vector3.up * .35f));
				translateIcon.transform.position = (this.transform.position + (Vector3.up * .35f));
				scaleIcon.transform.position = (this.transform.position + (Vector3.up * .35f));

				GameObject.Find ("UI").transform.parent = transform;

				arrows.SetActive (false);
				rotater.SetActive (false);
				scaler.SetActive (false);
				GameObject.Find ("lock_yellow").GetComponent<SpriteRenderer> ().sprite = close;
				rotateIcon.transform.position = lockIcon.transform.position;
				translateIcon.transform.position = lockIcon.transform.position;
				scaleIcon.transform.position = lockIcon.transform.position;
				rotateIcon.SetActive (false);
				scaleIcon.SetActive (false);
				translateIcon.SetActive (false);
			} else {
				GameObject.Find ("Top").GetComponent<Animation> ().Play ();

				//probably a less messy way to do this
				if (tranzlate) {
					scale = false;
					rotate = false;
					rotater.SetActive (false);
					arrows.SetActive (true);
					scaler.SetActive (false);
				}

				if (rotate) {
					scale = false;
					tranzlate = false;
					arrows.SetActive (false);
					scaler.SetActive (false);
					rotater.SetActive (true);
				}

				if (scale) {
					tranzlate = false;
					rotate = false;
					rotater.SetActive (false);
					scaler.SetActive (true);
					arrows.SetActive (false);
				}
										
				SnapArrows ();
				snapLock = true; 
				GameObject.Find ("lock_yellow").GetComponent<SpriteRenderer> ().sprite = open;
				rotateIcon.SetActive (true);
				translateIcon.SetActive (true);
				scaleIcon.SetActive (true);

				//relativeX, as in right relative to wherever the user is facing 
				//i'd like to keep most of the controls in world space
				Vector3 relativeX = DisplayManager.Instance.transform.right;

				//arranging the icons for the UI
				rotateIcon.transform.position = (GameObject.Find("UI").transform.position + (relativeX * -.35f) + (Vector3.up * .2f));
				lockIcon.transform.position = (GameObject.Find("UI").transform.position + (relativeX * .35f) + (Vector3.up * .2f));
				translateIcon.transform.position = (GameObject.Find("UI").transform.position + (relativeX * -.135f) + (Vector3.up * .4f));
				scaleIcon.transform.position = (GameObject.Find("UI").transform.position + (relativeX * .135f) + (Vector3.up * .4f));
			}
		}

		//enable rotation, reset rotation if already toggled
		public void ToggleRotate () {
			if (rotate) {
				ResetRotation ();
			} else {
				tranzlate = false;
				scale = false;
				rotate = true;

				GameObject.Find ("UI").transform.parent = rotater.transform;

				scaler.SetActive (false);
				rotater.SetActive (true);
				arrows.SetActive (false);
				translateIcon.transform.GetChild (1).gameObject.SetActive (false);
				scaleIcon.transform.GetChild (1).gameObject.SetActive (false);
				rotateIcon.transform.GetChild (1).gameObject.SetActive (true);

			}
		}

		//enable translation, reset position if already toggled
		public void ToggleTranslate () {
			if (tranzlate) {
				ResetPosition ();
			} else {
				tranzlate = true;
				scale = false;
				rotate = false;

				GameObject.Find ("UI").transform.parent = arrows.transform;

				scaler.SetActive (false);
				rotater.SetActive (false);
				arrows.SetActive (true);

				translateIcon.transform.GetChild (1).gameObject.SetActive (true);
				scaleIcon.transform.GetChild (1).gameObject.SetActive (false);
				rotateIcon.transform.GetChild (1).gameObject.SetActive (false);

				GameObject.Find ("UI").transform.parent = null;
			}
		}

		//enable scale, reset scale if already toggled
		public void ToggleScale () {
			if (scale) {
				ResetScale ();
			} else {
				tranzlate = false;
				scale = true;
				rotate = false;

				//assigning parent to scaler distorts UI icons
				//manual UI position assignment
				GameObject.Find ("UI").transform.parent = null;
				Vector3 relativeX = DisplayManager.Instance.transform.right;
				rotateIcon.transform.position = (GameObject.Find("UI").transform.position + (relativeX * -.35f) + (Vector3.up * .2f));
				lockIcon.transform.position = (GameObject.Find("UI").transform.position + (relativeX * .35f) + (Vector3.up * .2f));
				translateIcon.transform.position = (GameObject.Find("UI").transform.position + (relativeX * -.135f) + (Vector3.up * .4f));
				scaleIcon.transform.position = (GameObject.Find("UI").transform.position + (relativeX * .135f) + (Vector3.up * .4f));

				scaler.SetActive (true);
				rotater.SetActive (false);
				arrows.SetActive (false);

				translateIcon.transform.GetChild (1).gameObject.SetActive (false);
				scaleIcon.transform.GetChild (1).gameObject.SetActive (true);
				rotateIcon.transform.GetChild (1).gameObject.SetActive (false);
			}
		}
			
		//used with the scaler control
		public void Grow () {
			shrink = false;
			grow = true;
		}

		public void Shrink () {
			grow = false;
			shrink = true;
		}

		//movement methods assigned to each arrow button in the inspector
		public void MoveXPositive () {
			timeStamp = Time.time + timeToSpeedUp;
			moveXPos = true;
			if (!snapLock)
				GameObject.Find ("PositiveX").transform.localScale = GameObject.Find ("PositiveX").transform.localScale * 1.2f;
		}

		public void MoveXNegative () {
			timeStamp = Time.time + timeToSpeedUp;
			moveXNeg = true;
			if (!snapLock)
				GameObject.Find ("NegativeX").transform.localScale = GameObject.Find ("NegativeX").transform.localScale * 1.2f;
		}

		public void MoveYPositive () {
			timeStamp = Time.time + timeToSpeedUp;
			moveYPos = true;
			if (!snapLock)
				GameObject.Find ("PositiveY").transform.localScale = GameObject.Find ("PositiveY").transform.localScale * 1.2f;
		}

		public void MoveYNegative () {
			timeStamp = Time.time + timeToSpeedUp;
			moveYNeg = true;
			if (!snapLock)
				GameObject.Find ("NegativeY").transform.localScale = GameObject.Find ("NegativeY").transform.localScale * 1.2f;
		}

		public void MoveZPositive () {
			timeStamp = Time.time + timeToSpeedUp;
			moveZPos = true;
			if (!snapLock)
				GameObject.Find ("PositiveZ").transform.localScale = GameObject.Find ("PositiveZ").transform.localScale * 1.2f;
		}

		public void MoveZNegative () {
			timeStamp = Time.time + timeToSpeedUp;
			moveZNeg = true;
			if (!snapLock)
				GameObject.Find ("NegativeZ").transform.localScale = GameObject.Find ("NegativeZ").transform.localScale * 1.2f;
		}

		public void EnableSphere () {
			timeStamp = Time.time + timeToSpeedUp;
			sphereEnabled = true;
			if (!snapLock) {
				rotater.transform.DetachChildren ();
				rotater.transform.localScale = GameObject.Find ("Rotater").transform.localScale * 1.2f;
				GameObject.Find ("UI").transform.parent = rotater.transform;
				GameObject.Find ("Center (1)").transform.parent = rotater.transform;
			}
		}
	}
}
