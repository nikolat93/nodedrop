﻿#if UNITY_EDITOR

//AttributesBaseEditor
using UnityEngine;
using UnityEditor;
using System.Collections;

using L7.Tools;
using L7.VideoStreamer;

[CustomEditor (typeof(VideoStreamerAttributes))]
public class AttributesBaseEditor : Editor {

	public override void OnInspectorGUI ()
	{

		DrawDefaultInspector ();
		VideoStreamerAttributes attributes = (VideoStreamerAttributes)target;

		EditorGUILayout.BeginHorizontal ();
		if (GUILayout.Button ("Load")) {
			attributes.Load ();
		}

		if (GUILayout.Button ("Save")) {
			attributes.Save ();
		}
		EditorGUILayout.EndHorizontal ();

		EditorGUILayout.BeginHorizontal ();
		if (GUILayout.Button ("Verify Data")) {
			attributes.Verify ();
		}

		EditorGUILayout.EndHorizontal ();

	}

}

#endif