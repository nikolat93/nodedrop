﻿/****************************************************************************************************************************************
 * © 2016 Daqri International. All Rights Reserved.                                                                                    	*
 *                                                                                                                                     	*
 *     NOTICE:  All software code and related information contained herein is, and remains the property of DAQRI INTERNATIONAL and its 	*
 * suppliers, if any.  The intellectual and technical concepts contained herein are proprietary to DAQRI INTERNATIONAL and its         	*
 * suppliers and may be covered by U.S. and Foreign Patents, patents in process, and/or trade secret law, and the expression of			*
 * those concepts is protected by copyright law. Dissemination, reproduction, modification, public display, reverse engineering, or 	*
 * decompiling of this material is strictly forbidden unless prior written permission is obtained from DAQRI INTERNATIONAL.            	*
 *																																		*
 *																																		*
 *																																		*
 *     Created by:        	Jan Bajana																				*                                
 *     Created on:        	Sep 2 2016																									*                
 *     File Purpose:		Class to drive all experience																				*                                                                                    
 *																																		*
 *     Guide:               <TBD>																										*
 *																																		*
 ****************************************************************************************************************************************/

#define DSH_PLUGIN

using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Collections;
using System.Collections.Generic;

namespace DAQRI.VideoStreamer
{
	
	public class CamAccesor : MediaSource {

		public float _cameraFPS = 30f;
		public float _facusDelay = 1f;
		public RawImage _photoPreview;
		public RawImage _camPreview;

		public CameraParams _videoParams;
		private CameraParams _photoParams;

		static CamAccesor _instance = null;
		public static CamAccesor Instance 
		{
			get {
				if(_instance == null)
				{
					_instance = GameObject.FindObjectOfType<CamAccesor>();

					if(_instance == null) {
						GameObject obj = new GameObject("CamAccesor");
						_instance = obj.AddComponent<CamAccesor>();
					}
					Debug.LogWarning("CamAccesor: Create Instance");
				}
				return _instance; 
			}
		}

		// Use this for  firstinitialization
		void Awake(){

			if (_instance == null) {
				_instance = this;
			}

			string name = CamAccesor.Instance.name;
			Debug.Log ("CamAccesor: Auto Instance: " + name);

			if (_photoParams == null) {
				_photoParams = new CameraParams (1280, 720);
			}

#if DSH_PLUGIN

			#else
			Debug.LogError("CamAccesor: DSH_PLUGIN in not define, please define it");
#endif

		}

#if DSH_PLUGIN

		private float _timeSinceBackFrameCaptured;

		#region MonoBehavior Events

		// Use this for initialization
		void Start () {

			ServiceManager.Instance.RegisterVideoTextureUser (this);

			//cameraParams = new CameraOptions ();

			if (_autoStart == true) {
				InitCam (CameraParams.CameraMode.VIDEO);
			} else {
				InitCam (CameraParams.CameraMode.NONE);
			}

			OnVideoFrameEvent += OnVideoFrameTaken;
			OnPhotoFrameEvent += OnPhotoFrameTaken;
		}
		
		// Update is called once per frame
		void Update () {

			if (_videoParams.GetCameraMode == CameraParams.CameraMode.VIDEO) {

				if(Time.time - _timeSinceBackFrameCaptured > (1.0f/_cameraFPS)) {
					StartCoroutine (UpdateVideoFrame(_videoParams));
					_timeSinceBackFrameCaptured = Time.time;

				}
			}
		}

		#endregion

		#region Public Methods

		public override void CapturePhoto()
		{
			Debug.Log ("CamAccesor: Play");
			StartCoroutine (UpdatePhotoFrame(_photoParams, _facusDelay));
		}

		protected override void Play()
		{
			Debug.Log ("CamAccesor: Play");
		}

		protected override void Stop()
		{
			Debug.Log ("CamAccesor: Stop");
		}

		public void InitCam(CameraParams.CameraMode mode){
			Texture2D tmpText = ServiceManager.Instance.GetColorCameraTexture ();

			if (tmpText != null) {
				_videoParams.InitFromExternal (tmpText, mode);
				//_photoParams.InitFromExternal (tmpText, mode);
			} else {
				Texture2D _text = LoadDefaulTexture();
				_videoParams.InitFromExternal (_text, mode);
				//_photoParams.InitFromExternal (_text, mode);
			}

			_photoParams.Init (_videoParams);
		}

		public void StopCam(){
			_videoParams.Deinit ();
		}

		#endregion

		#region Private Methods

		public static Texture2D LoadDefaulTexture() {

			Texture2D tex = null;
			string path = Application.streamingAssetsPath + "/logo.jpg";
			byte[] fileData;

			if (File.Exists(path))     {
				fileData = File.ReadAllBytes(path);
				tex = new Texture2D(1280, 720, TextureFormat.RGB24, false);
				tex.LoadImage(fileData); //..this will auto-resize the texture dimensions.
			}
			return tex;
		}

		private IEnumerator UpdateVideoFrame(CameraParams cameraParams)
		{
			yield return null;

			OnNewVideoFrame (cameraParams);
		}

		private IEnumerator UpdatePhotoFrame(CameraParams cameraParams, float seconds)
		{
			yield return new WaitForSeconds(seconds);

			//if (!cameraParams.ApplyFromColorCamera ()) {
			//cameraParams.ApplyFrom (_videoParams);
		//	}

			cameraParams.CopyFrom (_videoParams);
			//cameraParams.ApplyFromColorCamera ();
			OnNewPhotoFrame (cameraParams);
		}
			
		private void OnVideoFrameTaken(CameraParams options)
		{
				if (_camPreview != null) {
					_camPreview.texture = options.GetGrabedTexture;
					_camPreview.color = Color.white;
				}
		}

		private void OnPhotoFrameTaken(CameraParams options)
		{

			if (_photoPreview != null) {
				_photoPreview.texture = options.GetGrabedTexture;
				_photoPreview.color = Color.white;
			}
				
		}

		#endregion

#endif

	}

}