﻿/****************************************************************************************************************************************
 * © 2016 Daqri International. All Rights Reserved.                                                                                    	*
 *                                                                                                                                     	*
 *     NOTICE:  All software code and related information contained herein is, and remains the property of DAQRI INTERNATIONAL and its 	*
 * suppliers, if any.  The intellectual and technical concepts contained herein are proprietary to DAQRI INTERNATIONAL and its         	*
 * suppliers and may be covered by U.S. and Foreign Patents, patents in process, and/or trade secret law, and the expression of			*
 * those concepts is protected by copyright law. Dissemination, reproduction, modification, public display, reverse engineering, or 	*
 * decompiling of this material is strictly forbidden unless prior written permission is obtained from DAQRI INTERNATIONAL.            	*
 *																																		*
 *																																		*
 *																																		*
 *     Created by:        	Jan Bajana																				*                                
 *     Created on:        	Sep 2 2016																									*                
 *     File Purpose:		Class to drive all experience																				*                                                                                    
 *																																		*
 *     Guide:               <TBD>																										*
 *																																		*
 ****************************************************************************************************************************************/

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using DAQRI.VideoStreamer;
using L7.VideoStreamer;

namespace DAQRI.VideoStreamer
{
	/// <summary>
	/// Virtual cam accesor. Provide you video data rendered to GPU canvas for video and photo processing on CPU including VR content overlapping camera background.
	/// </summary>
	public class VirtualCamAccesor : MediaSource {

		private Camera _DSHTargetCamera;
		public Camera _virtualCamera;

		public bool _disableContentOnPhoto = true;
		public string _cameraLayer = "Video Streamer";

		public RawImage _camPreview;
		public RawImage _photoPreview;

		private CamPostRender _camPostRender;
		private CameraParams _photoData;

		private bool _opticalSeeThrough = false;
		private GameObject _videoBackdrop;
		private string _cameraName;
		private int _originCullingMask = 0;

		static VirtualCamAccesor _instance = null;
		public static VirtualCamAccesor Instance 
		{
			get {
				if(_instance == null)
				{
					_instance = GameObject.FindObjectOfType<VirtualCamAccesor>();

					if(_instance == null) {
						GameObject obj = new GameObject("VirtualCamAccesor");
						_instance = obj.AddComponent<VirtualCamAccesor>();
					}
					Debug.LogWarning("VirtualCamAccesor: Create Instance");
				}
				return _instance; 
			}
		}

		#region MonoBehavior Events

		void Awake(){

			if (_instance == null) {
				_instance = this;
			} else if (_instance != this) {
				Destroy (gameObject);   
			}

			if (!Application.isEditor) {
				_opticalSeeThrough = true;
			}

			SetCameraParams ();
		}

		// Use this for initialization
		void Start () {
			OnVideoFrameEvent += OnVideoTaken;
			OnPhotoFrameEvent += OnPhotoTaken;

			ServiceManager.Instance.VisionParametersAvailable += ResizeCamera;
			DisplayManager.Instance.ScreenSizeChanged += ResizeCamera;
		
		}

		#endregion

		#region Public Methods

		public override void CapturePhoto()
		{
			StartCoroutine (UpdatePhotoFrame(_photoData, 0.5f));
		}

		public void DisableContent()
		{
			if(_virtualCamera != null){
				_virtualCamera.cullingMask = 1 << LayerMask.NameToLayer (_cameraLayer);
			}
		}

		public void EnableContent()
		{
			if(_virtualCamera != null){
				_virtualCamera.cullingMask = _originCullingMask;
			}
		}

		/// <summary>
		/// This will int RGB Camera on HW layer if ou are first user.
		/// </summary>
		protected override void Play()
		{
			Debug.Log ("VirtualCamAccesor: Play");
			StartCoroutine(StartVideoWithDellay ());
		}

		/// <summary>
		/// This will deinit RGB Camera on HW ayer if you are last user.
		/// </summary>
		protected override void Stop()
		{
			Debug.Log ("VirtualCamAccesor: Stop");
			//_videoBackdrop will enable of disable camera on HW layer if there i no more camera users.
			if (_videoBackdrop.activeInHierarchy) {
				_videoBackdrop.SetActive (false);
			}

			_camPostRender.Stop ();
			_camPostRender.OnRenderFrameEvent -= OnNewVideoFrame;
		}

		#endregion

		#region Private Methods

		private IEnumerator StartVideoWithDellay()
		{
			yield return new WaitForSeconds(0.2f);
			//_videoBackdrop.SetActive (true);
			if (!_videoBackdrop.activeInHierarchy) {
				_videoBackdrop.SetActive (true);
			}

			_camPostRender.Play ();
			_camPostRender.OnRenderFrameEvent += OnNewVideoFrame;
		}
			
		private IEnumerator UpdateVideoFrame(CameraParams cameraParams)
		{
			yield return new WaitForEndOfFrame();
			RenderTexture.active = cameraParams.GetRenderTexture;
			cameraParams.Apply();
			OnNewVideoFrame (cameraParams);
		}

		private IEnumerator UpdatePhotoFrame(CameraParams cameraParams, float seconds)
		{
			if (_disableContentOnPhoto) {
				DisableContent ();
			}

			yield return new WaitForSeconds(seconds);
			yield return new WaitForEndOfFrame();
			cameraParams.CopyFrom (_camPostRender.CameraData);
			OnNewPhotoFrame (cameraParams);
		
			if (_disableContentOnPhoto) {
				EnableContent ();
			}
		}

		private void SetCameraParams()
		{
			if (_virtualCamera == null) {
				_virtualCamera = GetComponentInChildren<Camera>(true);
			}

			if (_photoData == null) {
				_photoData = new CameraParams (1280, 720);
			}

			_photoData.Init(CameraParams.CameraMode.PHOTO);

			if (_opticalSeeThrough) {
				_cameraName = "Left Camera";
			} else {
				_cameraName = "Mono Camera";
			}
		}

		private void CreateCameraBackdrop()
		{
			Debug.Log("VirtualCamAccesor: GetVideoBackgroundCamera creating camera dependebcies");
			_videoBackdrop = GameObject.CreatePrimitive (PrimitiveType.Quad);
			_videoBackdrop.name = "Video Backdrop";
			_videoBackdrop.layer = LayerMask.NameToLayer (_cameraLayer);
			_videoBackdrop.transform.SetParent (DisplayManager.Instance.transform, false);
			GameObject.Destroy (_videoBackdrop.GetComponent<Collider> ());
			_videoBackdrop.AddComponent<VideoBackdrop> ();
			_videoBackdrop.SetActive (false);
			//_videoBackdrop.SetActive(true);

			Camera[] cameras = DisplayManager.Instance.GetComponentsInChildren<Camera> ();

			foreach(Camera tmpCamera in cameras)
			{
				Debug.Log("VirtualCamAccesor: tCamera: " + tmpCamera.name);
				tmpCamera.cullingMask &=  ~(1 << LayerMask.NameToLayer(_cameraLayer));
			}

		}

		private void InstantiateCamera()
		{
			Debug.Log ("VirtualCamAccesor: InstantiateCamera");

			_DSHTargetCamera = FindCamera (_cameraName);

			if (_DSHTargetCamera == null) {
				return;
			}

			if (_virtualCamera == null) {
				//_virtualCamera = Instantiate<Camera> (_DSHTargetCamera);
				//_virtualCamera.name = "VideoCamera (Clone)";
				//_virtualCamera.tag = "Untagged";
				_virtualCamera = this.GetComponentInChildren<Camera>(true);
			}

			_camPostRender = _virtualCamera.GetComponent<CamPostRender> ();
			_camPostRender.enabled = true;
			_camPostRender.Init ();

			PhysicsRaycaster raycaster = _virtualCamera.GetComponent<PhysicsRaycaster> ();
			if (raycaster != null) {
				raycaster.enabled = false;
			}

			CreateCameraBackdrop ();
		}

		private void ResizeCamera()
		{
			if (_DSHTargetCamera == null || _virtualCamera == null) {
				InstantiateCamera ();
			}

			Debug.Log ("Message: VirtualCamAccesor: Resize Camera");

			_camPostRender.Resize (_DSHTargetCamera);

			_virtualCamera.cullingMask |= 1 << LayerMask.NameToLayer (_cameraLayer);
			_originCullingMask = _virtualCamera.cullingMask;

			if (_autoStart == true) {
				_autoStart = true;
				RegisterDeviceUser (this);
			}

		}

		private Camera FindCamera(string name)
		{
			GameObject cameraObject = GameObject.Find(name); //returns null if no background visible initially
			if (cameraObject != null) {
				return cameraObject.GetComponent<Camera> ();
			} else {
				return null;
			}
		}

		/// <summary>
		/// Raises the screenshot taken events. Fill RawImage data for video preview in UI.
		/// </summary>
		private void OnVideoTaken(CameraParams options)
		{
			if (options.GetCameraMode == CameraParams.CameraMode.VIDEO) {
				if (_camPreview != null) {
					_camPreview.texture = options.GetGrabedTexture;;
					_camPreview.color = Color.white;
				}
			}
		}

		/// <summary>
		/// Raises the photo taken event. Fill RawImage data for photo preview in UI.
		/// </summary>
		/// <param name="options">Options.</param>
		private void OnPhotoTaken(CameraParams options)
		{
			if (options.GetCameraMode == CameraParams.CameraMode.PHOTO) {
				//Debug.Log ("VirtualCameraAccesor: PHOTO TAKEN");
				if (_photoPreview != null) {
					_photoPreview.texture = options.GetGrabedTexture;;
					_photoPreview.color = Color.white;
				}
			}
		}

		#endregion

	}

}
