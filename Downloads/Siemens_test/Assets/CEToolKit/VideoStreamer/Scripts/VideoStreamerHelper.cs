﻿/****************************************************************************************************************************************
 * © 2016 Daqri International. All Rights Reserved.                                                                                    	*
 *                                                                                                                                     	*
 *     NOTICE:  All software code and related information contained herein is, and remains the property of DAQRI INTERNATIONAL and its 	*
 * suppliers, if any.  The intellectual and technical concepts contained herein are proprietary to DAQRI INTERNATIONAL and its         	*
 * suppliers and may be covered by U.S. and Foreign Patents, patents in process, and/or trade secret law, and the expression of			*
 * those concepts is protected by copyright law. Dissemination, reproduction, modification, public display, reverse engineering, or 	*
 * decompiling of this material is strictly forbidden unless prior written permission is obtained from DAQRI INTERNATIONAL.            	*
 *																																		*
 *																																		*
 *																																		*
 *     Created by:        	Jan Bajana, Chris Fairclough																				*                                
 *     Created on:        	May 25 2016																									*                
 *     File Purpose:		Class to drive all experience																				*                                                                                    
 *																																		*
 *     Guide:               <TBD>																										*
 *																																		*
 ****************************************************************************************************************************************/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace DAQRI.VideoStreamer
	{
	
	public class VideoStreamerHelper : MonoBehaviour {
		
		//public bool _autoStartStreaming = false;
		public bool _showUI = false;

		public MediaSource _cameraDevice;

		private string _lastPictureSaved = string.Empty;
		private VideoEncoder _videoStreamer; //Maybe singleton
		private string _videoPluginMuxInfo = "Streaming: Stop";

		#region MonoBehavior Events

		void Start()
		{
			_videoStreamer = GetComponent<VideoEncoder> ();

			ImageBrowser.OnImageSaved += OnScreenshotSaved;	
			_cameraDevice.OnPhotoFrameEvent += OnPhotoCaptured;

			//if (_autoStartStreaming == true) {
				//Debug.LogWarning ("Warning: VideoStreamerHelper: Camera device auto Init. Be sure you do not int camera if you do not need!!!");

				//StartStreaming ();
			//}
		}

		void OnApplicationQuit() {
			Debug.Log ("VideoStreamerHelper: OnApplicationQuit : ");
			StopStreaming ();
		}

		void OnDestroy() {
			Debug.Log ("VideoStreamerHelper: OnDestroy : ");
		}
		
		// Update is called once per frame
		void Update () 
		{

			if (Input.GetKeyDown (KeyCode.Escape)) {
				Debug.Log("QuitApp");
				Application.Quit ();
			}
				
			if(Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.C))
			{
				_showUI = !_showUI;
			}

			if (Input.GetKeyDown (KeyCode.P)) {
				CapturePhoto ();
			}

			if (Input.GetKeyDown (KeyCode.I)) {
				StartStreaming ();
			}

			if (Input.GetKeyDown (KeyCode.O)) {
				StopStreaming ();
			}
				
		}

		#endregion

		#region Public Methods

		public void CapturePhoto()
		{
			if (_cameraDevice != null) {
				StartCoroutine (CapturePhotoCoroutine ());
			}
		}

		public void StartStreaming()
		{
			StartCoroutine (StartStreamingCoroutine());
		}

		public void StopStreaming()
		{
			if(_videoStreamer != null)
			{
				_cameraDevice.UnregisterDeviceUser (this);
				_videoStreamer.Stop ();
				_videoPluginMuxInfo = "Streaming: Stop"; 
			}
		}

		public bool IsStreaming()
		{
			if(_videoStreamer != null)
			{
				return _videoStreamer.IsStreaming;
			}
			return false;
		}

		#endregion

		#region Public Methods

		private IEnumerator CapturePhotoCoroutine(){

			_cameraDevice.RegisterDeviceUser (this);
			yield return new WaitForSeconds(1.0f);
			_cameraDevice.CapturePhoto ();

		}

		private IEnumerator StartStreamingCoroutine()
		{
			yield return new WaitForSeconds(1f);
			if(_videoStreamer != null)
			{
				_cameraDevice.RegisterDeviceUser (this);
				_videoStreamer.Play ();
				_videoPluginMuxInfo = "Streaming: " + _videoStreamer.GetMuxInfo ();
			}

		}
			
		void OnScreenshotSaved(string path)
		{
			_lastPictureSaved = path;
			Debug.Log ("VideoStreamerHelper: ScreenshotSaved: " + path);
		}

		void OnPhotoCaptured(CameraParams camParams)
		{
			if (!_videoStreamer.IsStreaming) {
				_cameraDevice.UnregisterDeviceUser (this);
				Debug.Log ("VideoStreamerHelper: Unregister: ");

			}
			Debug.Log ("VideoStreamerHelper: OnPhotoCaptured: ");
		}

		#endregion

		#region MonoBehavior UI Events

		/// <summary>
		/// Scale UI elements with constant physical pixel size
		/// </summary>
		/// <returns>The DPI scale.</returns>
		public float GetDPIScale()
		{
			float dpi = (Screen.dpi == 0 ? 160f : Screen.dpi);
			return dpi / 160f;
		}

		/// <summary>
		/// This will scale your UI similar to Canvas settings "Scall With Screen Size" including DPI value
		/// You can setup UI size in units not in pixels.
		/// </summary>
		/// <returns>The scale with scree size.</returns>
		public float GetScaleWithScreeSize()
		{
			float resolutionScale = Screen.width / 640f;
			return (GetDPIScale () + resolutionScale)/2f;
		}
			
		void OnGUI()
		{
			
			if (!_showUI) {
				return;
			}

			float scale = GetScaleWithScreeSize ();

			float possition = 50 * scale;
			float heigth = 25f * scale;
			float width = 120f * scale;
			
			List<string> screenShotList = ImageBrowser.Instance._capturedScreenshots;
			
			GUI.Box(new Rect(Screen.width - 300f,0f, 300f ,screenShotList.Count*heigth), "");
			for (int i = 0; i < screenShotList.Count; i++) {
				if (GUI.Button(new Rect(Screen.width - 300f, (heigth*i), 300f, heigth), Path.GetFileName(screenShotList[i])))
				{
					string loadingPath = screenShotList[i];
					//Debug.Log("loading: " + loadingPath);
					ImageBrowser.Instance.LoadImage (loadingPath);

				}
			}

			if (!string.IsNullOrEmpty(_lastPictureSaved)) {
				GUI.Button(new Rect(Screen.width - 750f, Screen.height - heigth, 750f, heigth), _lastPictureSaved);
			}
			
			GUI.Box(new Rect(10,possition, width ,heigth*5f), "");
			if (GUI.Button(new Rect(10f, possition, width, heigth), "Capture Photo"))
			{
				CapturePhoto ();
			}

			if (_videoStreamer != null) {
				
				possition += heigth;
				if (GUI.Button(new Rect(10f, possition, width, heigth), "Start Streamer"))
				{
					StartStreaming ();
				}

				possition += heigth;
				if (GUI.Button(new Rect(10f, possition, width, heigth), "Stop Streamer"))
				{
					StopStreaming ();
				}

				possition += (heigth + 1f);
				width = width * 2.2f;
				heigth = 50f * scale;
				GUI.Box(new Rect(10, possition, width ,heigth), "");
				GUI.Label (new Rect (10f, possition, width, heigth), _videoPluginMuxInfo);
			}
			
		}
		#endregion

	}
}