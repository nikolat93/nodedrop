﻿/****************************************************************************************************************************************
 * © 2016 Daqri International. All Rights Reserved.                                                                                    	*
 *                                                                                                                                     	*
 *     NOTICE:  All software code and related information contained herein is, and remains the property of DAQRI INTERNATIONAL and its 	*
 * suppliers, if any.  The intellectual and technical concepts contained herein are proprietary to DAQRI INTERNATIONAL and its         	*
 * suppliers and may be covered by U.S. and Foreign Patents, patents in process, and/or trade secret law, and the expression of			*
 * those concepts is protected by copyright law. Dissemination, reproduction, modification, public display, reverse engineering, or 	*
 * decompiling of this material is strictly forbidden unless prior written permission is obtained from DAQRI INTERNATIONAL.            	*
 *																																		*
 *																																		*
 *																																		*
 *     Created by:        	Jan Bajana																				*                                
 *     Created on:        	Sep 2 2016																									*                
 *     File Purpose:		Class to drive all experience																				*                                                                                    
 *																																		*
 *     Guide:               <TBD>																										*
 *																																		*
 ****************************************************************************************************************************************/

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using DAQRI.VideoStreamer;
using DAQRI;
using L7.Tools;

//Camera.Render: https://docs.unity3d.com/ScriptReference/Camera.Render.html
//OnPostRender: https://docs.unity3d.com/ScriptReference/Camera.Render.html
//Camera.OnPostRender: https://docs.unity3d.com/ScriptReference/Camera-onPostRender.html
//Native glpixels: http://answers.unity3d.com/questions/465409/reading-from-a-rendertexture-is-slow-how-to-improv.html

namespace L7.VideoStreamer {

	public class CamPostRender : MonoBehaviour {

		public event Action<CameraParams> OnRenderFrameEvent;

		public float _cameraFPS = 24f;
		public float _countedFPS = 0f;

		private float _timeSinceBackFrameCaptured;

		[SerializeField]
		private bool _isPlaying = false;
		private bool _isInit = false;

		private Camera _camera;
		private CameraParams _cameraData;
		private FPSCounter _FPSCounter;

		public CameraParams CameraData
		{
			get {return _cameraData;}
			set {_cameraData = value;}
		}

		public Camera RenderCamera
		{
			get {return _camera;}
			set {_camera = value;}
		}

		#region MonoBehavior Events

		void Awake(){
			if (!_isInit) {
				Init ();
			}
		}

		public void Init ()
		{
			if (_cameraData != null) {
				Debug.LogWarning ("Warning: Camera data aldeady Init");
				return;
			}

			if (_cameraData == null) {
				_cameraData = new CameraParams (1280, 720);
			}

			_cameraData.Init(CameraParams.CameraMode.VIDEO);

			_camera = GetComponent<Camera> ();
			_camera.transform.SetParent (DisplayManager.Instance.transform);

			_camera.targetTexture = _cameraData.GetRenderTexture;
			_camera.rect = new Rect(0f, 0f, 1f, 1f);

			_FPSCounter = new FPSCounter ();

			Stop ();
			_isInit = true;
		}

		public void Resize (Camera mainCam)
		{
			_camera.CopyFrom (mainCam);
			_camera.clearFlags = CameraClearFlags.SolidColor;
			_camera.rect = new Rect(0f, 0f, 1f, 1f);
			_camera.backgroundColor = new Color (1.0f, 0.5f, 0.5f, 0.5f);
			_camera.targetTexture = _cameraData.GetRenderTexture;

		}

		//void OnPreRender() {
		//
		//}
		/*
		void OnPostRender () {

			if (_isPlaying) {
				if (Time.time - _timeSinceBackFrameCaptured > (1.0f / _cameraFPS)) {
					
					_cameraData.Apply();

					if (OnRenderFrameEvent != null) {
						OnRenderFrameEvent (_cameraData);
					}
				
					_FPSCounter.Update ();
					_countedFPS = _FPSCounter.GetValue;
					_timeSinceBackFrameCaptured = Time.time;
				}
			}
		}
		*/
		void LateUpdate () {

			if (_isPlaying) {
				if (Time.time - _timeSinceBackFrameCaptured > (1.0f / _cameraFPS)) {
					StartCoroutine(CaptureVideoCoroutine());
					_timeSinceBackFrameCaptured = Time.time;
				}

			}
		}

		private IEnumerator CaptureVideoCoroutine()
		{
			yield return new WaitForEndOfFrame();
			_camera.Render ();
			_cameraData.Apply();

			if (OnRenderFrameEvent != null) {
				OnRenderFrameEvent (_cameraData);
			}
			_FPSCounter.Update ();
			_countedFPS = _FPSCounter.GetValue;
		}

		#endregion

		#region Public Methods

		public void Play()
		{
			_isPlaying = true;
			_camera.gameObject.SetActive (true);
			_camera.enabled = false;
			_FPSCounter.Reset ();
		}

		public void Stop()
		{
			_isPlaying = false;
			_camera.gameObject.SetActive (false);
			_camera.enabled = false;

		}

		public float GetFps()
		{
			if (_FPSCounter != null) {
				return _FPSCounter.GetValue;
			}
			return 0f;
		}

		#endregion

	}

}