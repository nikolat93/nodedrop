﻿/****************************************************************************************************************************************
 * © 2016 Daqri International. All Rights Reserved.                                                                                    	*
 *                                                                                                                                     	*
 *     NOTICE:  All software code and related information contained herein is, and remains the property of DAQRI INTERNATIONAL and its 	*
 * suppliers, if any.  The intellectual and technical concepts contained herein are proprietary to DAQRI INTERNATIONAL and its         	*
 * suppliers and may be covered by U.S. and Foreign Patents, patents in process, and/or trade secret law, and the expression of			*
 * those concepts is protected by copyright law. Dissemination, reproduction, modification, public display, reverse engineering, or 	*
 * decompiling of this material is strictly forbidden unless prior written permission is obtained from DAQRI INTERNATIONAL.            	*
 *																																		*
 *																																		*
 *																																		*
 *     Created by:        	Jan Bajana																				*                                
 *     Created on:        	Jan 9 2016																									*                
 *     File Purpose:		Class to drive all experience																				*                                                                                    
 *																																		*
 *     Guide:               <TBD>																										*
 *																																		*
 ****************************************************************************************************************************************/

using UnityEngine;
using UnityEngine.EventSystems;

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

using L7.Tools;
using DAQRI.Tools;

namespace L7.VideoStreamer
{
	
	public class VideoStreamerAttributes : AttributesBase {

		[SerializeField]
		private VideoAttributes _attributes;

		static VideoStreamerAttributes _instance = null;
		public static VideoStreamerAttributes Instance 
		{
			get {
				if(_instance == null)
				{
					_instance = GameObject.FindObjectOfType<VideoStreamerAttributes>();

					if(_instance == null) {
						GameObject obj = new GameObject("VideoStreamerAttributes");
						_instance = obj.AddComponent<VideoStreamerAttributes>();
					}
					Debug.LogWarning("VideoStreamerAttributes: Create Instance");
				}
				return _instance; 
			}
		}

		public VideoAttributes Attributes
		{
			get {return _attributes;}
			set { _attributes = value; }
		}

		void Awake(){
			if (_instance == null) {
				_instance = this;
			} else if (_instance != this) {
				Destroy (gameObject);   
			}

			Load ();
		}

		public void Save()
		{
			base.Save (_attributes);
		}

		public void Load()
		{
			VideoAttributes tmpAttributes = base.Load<VideoAttributes> ();

			if (tmpAttributes != null) {
				_attributes = tmpAttributes;
			}
		}

	}

	[Serializable]
	public class VideoAttributes
	{
		//String veriables
		public string streamingDestination = "rtp://239.255.10.12:54009";
		public string gStreamerDestination = "rtp rtp://239.255.10.12:54009";
		//Bool variables
		public bool autoActiveMonitor = true;

		//Int variables

		//Float variables

	}

}