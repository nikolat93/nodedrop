﻿/****************************************************************************************************************************************
 * © 2016 Daqri International. All Rights Reserved.                                                                                    	*
 *                                                                                                                                     	*
 *     NOTICE:  All software code and related information contained herein is, and remains the property of DAQRI INTERNATIONAL and its 	*
 * suppliers, if any.  The intellectual and technical concepts contained herein are proprietary to DAQRI INTERNATIONAL and its         	*
 * suppliers and may be covered by U.S. and Foreign Patents, patents in process, and/or trade secret law, and the expression of			*
 * those concepts is protected by copyright law. Dissemination, reproduction, modification, public display, reverse engineering, or 	*
 * decompiling of this material is strictly forbidden unless prior written permission is obtained from DAQRI INTERNATIONAL.            	*
 *																																		*
 *																																		*
 *																																		*
 *     Created by:        	Jan Bajana, Chris Fairclough																				*                                
 *     Created on:        	May 25 2016																									*                
 *     File Purpose:		Class to drive all experience																				*                                                                                    
 *																																		*
 *     Guide:               <TBD>																										*
 *																																		*
 ****************************************************************************************************************************************/

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using L7.VideoStreamer;

namespace DAQRI.VideoStreamer
{

	public class VideoEncoder : MediaDestination {

		//public delegate void OnNewFrameCallback(Texture2D obj, CameraParams.CameraMode mode);
		//public static OnNewFrameCallback OnNewFrame;// = (byte[] frame) => {}; //Initialize it to something, so += works

		#region Action Events

		private Action<CameraParams> _onNewFrameCallback = (CameraParams options) => {

			if (options.GetCameraMode != CameraParams.CameraMode.VIDEO) {
				return;
			}

			if(options.Width != VideoEncoder.Instance._encoderParams.Width)
			{
				Debug.Log("VideoEncoder: source is in different resolution" + VideoEncoder.Instance._encoderParams.Width);
				return;
			}

			byte[] frameData = options.GetGrabedTexture.GetRawTextureData ();
			int streamOk = VideoStreamerNativePlugin.SendFrame (frameData);
			if(streamOk != 0)
			{
				Debug.LogWarning ("FFMpeg StreamFrame error : " + streamOk);
			}

		};
		#endregion

		public OnStartStopEvent _OnStartStop;

		private VideoStreamerNativePlugin.MuxPixelFormat _inputPixelFormat;

		[SerializeField]
		private VideoStreamerNativePlugin.MuxVideoCodec _videoCodec;

		//[SerializeField]
		private VideoStreamerNativePlugin.VideoResolution _videoResolution = VideoStreamerNativePlugin.VideoResolution.RES_16x9_1280x720;

		[SerializeField]
		private VideoStreamerNativePlugin.VideoQuality _videoQuality;

		//public string _destination = "rtp://239.255.10.12:54009";

		[SerializeField]
		private bool _enableCustomParams = false;

		public VideoEncoderParams _encoderParams;

		private bool _isStreaming = false;
		private string _videoPluginVersion = string.Empty;

		public bool IsStreaming
		{
			get {return _isStreaming;}
		}

		#region MonoBehavior Events

		static VideoEncoder _instance = null;
		public static VideoEncoder Instance 
		{
			get {
				if(_instance == null)
				{
					_instance = GameObject.FindObjectOfType<VideoEncoder>();

					if(_instance == null) {
						GameObject obj = new GameObject("VideoEncoder");
						_instance = obj.AddComponent<VideoEncoder>();
					}
					Debug.LogWarning("VideoEncoder: Create Instance");
				}
				return _instance; 
			}
		}

		void Awake(){

			if (_instance == null) {
				_instance = this;
			}
		}

		// Use this for initialization
		void Start () {

			//setup native log callback
			VideoStreamerNativePlugin.RegisterNativeLogCallback ((msg) => {
				Debug.Log (">NativeLog: " + msg);
			});

			_videoPluginVersion = string.Empty + VideoStreamerNativePlugin.GetPluginParamIntM (VideoStreamerNativePlugin.VideoPluginParams.VIDEO_PLUGIN_VERSION_MAJOR) + "." +
				VideoStreamerNativePlugin.GetPluginParamIntM (VideoStreamerNativePlugin.VideoPluginParams.VIDEO_PLUGIN_VERSION_MINOR) + "." +
				VideoStreamerNativePlugin.GetPluginParamIntM (VideoStreamerNativePlugin.VideoPluginParams.VIDEO_PLUGIN_VERSION_MICRO);
		}

		#endregion

		#region Public Methods

		public void Play()
		{
			if (_isStreaming == true) {
				return;
			}
				
			if (_enableCustomParams) {
				_encoderParams.SetFromCustomParams ();
			} else {
				_encoderParams.SetAutoParams (_videoResolution, _videoQuality);
			}

			int width = _encoderParams.Width;
			int height = _encoderParams.Height;

			_inputPixelFormat = VideoStreamerNativePlugin.MuxPixelFormat.VIDEO_PIX_FORMAT_RGB24;
			string destination = VideoStreamerAttributes.Instance.Attributes.streamingDestination;
			int initFF = VideoStreamerNativePlugin.SetStreamingOptionsCustomM (width, height, destination, _inputPixelFormat, _videoCodec);

			if (initFF != 0) {
				Debug.LogError ("VideoEncoder: FFMpeg init failed : " + initFF);
			}

			if(initFF == 0)
			{
				_isStreaming = true;
				//CameraScreencastManager.OnVideoFrameEvent += StreamFrame;
				_input.OnVideoFrameEvent += _onNewFrameCallback;
				_encoderParams.RefreshLocalParams();
				Debug.Log ("VideoStreamer: " + GetMuxInfo());

				if (_OnStartStop != null) {
					_OnStartStop.Invoke (true);
				}

			} else {
				Debug.LogError ("FFMpeg init error : " + initFF);
			}
		}

		public void Stop()
		{

			if (_isStreaming == false) {
				return;
			}
				
			_input.OnVideoFrameEvent -= _onNewFrameCallback;
			int finishStreamOk = VideoStreamerNativePlugin.FinishStream();
			if (finishStreamOk != 0) {
				Debug.LogError ("CameraScreencastHelper: FFMpeg FinishStream error : " + finishStreamOk);
			} else {
				_isStreaming = false;

				if (_OnStartStop != null) {
					_OnStartStop.Invoke (false);
				}
				string destination = VideoStreamerAttributes.Instance.Attributes.streamingDestination;
				Debug.Log ("CameraScreencastHelper: FFMpeg FinishStream: " + destination);
			}
		}

		#endregion

		/// <summary>
		/// It will get some mux encoder information. DO not call in runtime. It is enoght to call after StartStreaming ones.
		/// </summary>
		/// <returns>The mux info.</returns>
		public string GetMuxInfo()
		{
			string destination = VideoStreamerAttributes.Instance.Attributes.streamingDestination;
			return (string.Empty + "| ver: " + _videoPluginVersion + " | des: " + destination + _encoderParams.GetMuxInfo());
		}
	}

}