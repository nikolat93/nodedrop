﻿/****************************************************************************************************************************************
 * © 2016 Daqri International. All Rights Reserved.                                                                                    	*
 *                                                                                                                                     	*
 *     NOTICE:  All software code and related information contained herein is, and remains the property of DAQRI INTERNATIONAL and its 	*
 * suppliers, if any.  The intellectual and technical concepts contained herein are proprietary to DAQRI INTERNATIONAL and its         	*
 * suppliers and may be covered by U.S. and Foreign Patents, patents in process, and/or trade secret law, and the expression of			*
 * those concepts is protected by copyright law. Dissemination, reproduction, modification, public display, reverse engineering, or 	*
 * decompiling of this material is strictly forbidden unless prior written permission is obtained from DAQRI INTERNATIONAL.            	*
 *																																		*
 *																																		*
 *																																		*
 *     Created by:        	Jan Bajana, Chris Fairclough																				*                                
 *     Created on:        	JAN 19 2016																									*                
 *     File Purpose:		Class to drive all experience																				*                                                                                    
 *																																		*
 *     Guide:               <TBD>																										*
 *																																		*
 ****************************************************************************************************************************************/

using UnityEngine;
using System.Collections;
using System.Diagnostics;
using System;

namespace L7.VideoStreamer
{
	public abstract class SystemProcess : MonoBehaviour {

		protected Process _process;

		public abstract void StartProcess ();

		public virtual void KillProcess ()
		{
			if (_process != null) {
				UnityEngine.Debug.Log("Killing process");

				_process.Kill ();
				_process = null;

			}
		}

		protected void StartProcess (string process, params string[] args)
		{
			if(_process != null){
				UnityEngine.Debug.LogWarning ("SystemProcess - Process is already running.");
				return;
			}

			/*
			if (!extDisplayAvailable) {
				CreateVirtualDisplay ();
			}
			*/

			string tmpArgs = string.Empty;
			foreach (string arg in args) {
				tmpArgs += (arg + " ");
			}
				
			try {
				UnityEngine.Debug.Log("SystemProcess starting process");
				_process = new Process();
				//myProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
				//myProcess.StartInfo.CreateNoWindow = true;
				_process.StartInfo.UseShellExecute = false;
				_process.StartInfo.FileName = process;
				_process.StartInfo.Arguments = tmpArgs;
				_process.EnableRaisingEvents = true;
				_process.Start();

				//myProcess.WaitForExit();
				int ExitCode = _process.ExitCode;
				UnityEngine.Debug.Log("GenericVideoStreamer" + ExitCode);
			} catch (Exception e){
				//UnityEngine.Debug.LogError("GenericVideoStreamer" + e);
			}

		}

		protected void CreateVirtualDisplay()
		{
			UnityEngine.Debug.Log ("Screen.width" + Screen.width);

			try {
				UnityEngine.Debug.Log("SystemProcess starting process");

				Process displyProcess = new Process();
				//myProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
				//myProcess.StartInfo.CreateNoWindow = true;
				displyProcess.StartInfo.UseShellExecute = false;
				displyProcess.EnableRaisingEvents = true;
				displyProcess.StartInfo.FileName = "vs-displaymode";
				//displyProcess.StartInfo.Arguments = "-o \"rtp rtp://10.0.20.82:54009\" -m x11grab";
				displyProcess.Start();
				displyProcess.WaitForExit();
				int ExitCode = displyProcess.ExitCode;
				UnityEngine.Debug.Log("GenericVideoStreamer" + ExitCode);
			} catch (Exception e){
				//UnityEngine.Debug.LogError("GenericVideoStreamer" + e);
			}
		}

		public static int FindNewProcess(string processName, Process[] proceses)
		{
			int foundID = -1;
			Process[] newProceses = null;
			newProceses = Process.GetProcessesByName (processName);

			foreach (Process newProcess in newProceses) {
				bool foundMatch = false;

				foreach (Process process in proceses) {
					if (process.Id == newProcess.Id) {
						foundMatch = true;
						break;
					}
				}

				if (!foundMatch) {
					foundID = newProcess.Id;
					break;
				}
			}
			return foundID;
		}

		public static void KillAllProcessesByName(string processName)
		{
			Process[] proceses = null;
			proceses = Process.GetProcessesByName (processName);

			foreach (Process process in proceses) {
				process.Kill ();
			}
		}

	}
}
