﻿/****************************************************************************************************************************************
 * © 2016 Daqri International. All Rights Reserved.                                                                                    	*
 *                                                                                                                                     	*
 *     NOTICE:  All software code and related information contained herein is, and remains the property of DAQRI INTERNATIONAL and its 	*
 * suppliers, if any.  The intellectual and technical concepts contained herein are proprietary to DAQRI INTERNATIONAL and its         	*
 * suppliers and may be covered by U.S. and Foreign Patents, patents in process, and/or trade secret law, and the expression of			*
 * those concepts is protected by copyright law. Dissemination, reproduction, modification, public display, reverse engineering, or 	*
 * decompiling of this material is strictly forbidden unless prior written permission is obtained from DAQRI INTERNATIONAL.            	*
 *																																		*
 *																																		*
 *																																		*
 *     Created by:        	Jan Bajana, Chris Fairclough																				*                                
 *     Created on:        	JAN 19 2016																									*                
 *     File Purpose:		Class to drive all experience																				*                                                                                    
 *																																		*
 *     Guide:               <TBD>																										*
 *																																		*
 ****************************************************************************************************************************************/

using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Diagnostics;
using System;

namespace L7.VideoStreamer
{
	public class GenericVideoRecorder : SystemProcess {

		[SerializeField]
		private int _ffmpegProcessID = -1;

		public Text _pid;
		// Use this for initialization
		void Start () {
		
		}

		public void StartKillProcess(bool toggle)
		{
			if (toggle) {
				StartProcess ();
			} else {
				KillProcess ();
			}
		}

		public override void StartProcess()
		{
			if(_process != null){
				UnityEngine.Debug.LogWarning ("SystemProcess - Process is already running.");
				return;
			}

			Process[] proceses = null;
			proceses = Process.GetProcessesByName ("ffmpeg");

			string[] args = new string[3];
			args[0] = "-o POV_" + System.DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss");
			args[1] = "-m x11virtual";
			args[2] = "-v libx264";

			if (Application.isEditor) {
				args[1] = "-m x11grab";
			}

			base.StartProcess ("videorecorder", args);
			StartCoroutine (FindIfNewProcess("ffmpeg", proceses));

		}

		public override void KillProcess()
		{
			if (_process != null) {
				UnityEngine.Debug.Log("Video Recorder killing process");

				Process proceses = Process.GetProcessById (_ffmpegProcessID);

				if (proceses != null) {
					proceses.Kill ();
					_ffmpegProcessID = -1;
				}

				base.KillProcess ();

				if (_pid != null) {
					_pid.text = "-1";
				}
			}
		}

		void OnApplicationQuit() {
			//KillProcess ();
			//SystemProcess.KillProcessesByName("ffmpeg");
			KillProcess();


		}

		IEnumerator FindIfNewProcess(string processName, Process[] proceses)
		{
			bool searching = true;
			int counter = 0;

			while (searching) {
				yield return new WaitForSeconds(0.5f);

				int PID = SystemProcess.FindNewProcess (processName, proceses);

				if (PID >= 0 || counter > 5f) {
					_ffmpegProcessID = PID;
					searching = false;

					if (_pid != null) {
						_pid.text = string.Empty + PID;
					}

					UnityEngine.Debug.Log ("Video Recorder FFmpeg ID: " + _ffmpegProcessID);
				}
				counter++;
			}
		}

	}
}