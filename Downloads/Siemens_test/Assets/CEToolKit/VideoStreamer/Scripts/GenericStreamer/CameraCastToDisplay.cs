﻿/****************************************************************************************************************************************
 * © 2016 Daqri International. All Rights Reserved.                                                                                    	*
 *                                                                                                                                     	*
 *     NOTICE:  All software code and related information contained herein is, and remains the property of DAQRI INTERNATIONAL and its 	*
 * suppliers, if any.  The intellectual and technical concepts contained herein are proprietary to DAQRI INTERNATIONAL and its         	*
 * suppliers and may be covered by U.S. and Foreign Patents, patents in process, and/or trade secret law, and the expression of			*
 * those concepts is protected by copyright law. Dissemination, reproduction, modification, public display, reverse engineering, or 	*
 * decompiling of this material is strictly forbidden unless prior written permission is obtained from DAQRI INTERNATIONAL.            	*
 *																																		*
 *																																		*
 *																																		*
 *     Created by:        	Jan Bajana																									*                                
 *     Created on:        	JAN 11 2016																									*                
 *     File Purpose:		Class to drive all experience																				*                                                                                    
 *																																		*
 *     Guide:               <TBD>																										*
 *																																		*
 ****************************************************************************************************************************************/

using UnityEngine;
using System.Collections;
using DAQRI;
using UnityEngine.EventSystems;

namespace L7.VideoStreamer
{
	/// <summary>
	/// Camera cast to display.
	/// Warning: Class require that you created new sorting Layer called "Video Streamer". Use "Add Layer" in Unity Editor.
	/// </summary>
	public class CameraCastToDisplay : MonoBehaviour {

		private Camera _extCam;
		private Camera _cameraMain;

		private GameObject _videoBackdrop;
		private string _cameraLayer = "Video Streamer";

		bool _extDisplayConnected = false;
		bool _extDisplayDisconected = false;

		bool _extDisplayActive = false;
		bool _extDisplayPviousState = false;

		private const float _refreshTime = 2.0f;
		private float timer = _refreshTime;
			
		public bool extDisplayAvailable {
			get{
				if (Application.isEditor) {
					return true;
				} else {
					return Display.displays.Length > 1; 
				}
			}
		}

		void Start () {

			ServiceManager.Instance.VisionParametersAvailable += ResizeCamera;
			DisplayManager.Instance.ScreenSizeChanged += ResizeCamera;
			_extDisplayPviousState = !extDisplayAvailable;
		}

		void Update()
		{
			timer += Time.deltaTime;
			if(timer >= _refreshTime && _extCam != null){
				DetectDisplay ();
				ActiveDeactiveIfDetected ();
				timer = 0.0f;
			}

			if (Input.GetKeyDown (KeyCode.M)) {
				//Active();
			}

			if (Input.GetKeyDown (KeyCode.N)) {
				//Deactive ();
			}
		}

		public void Active()
		{

			if (Display.displays.Length > 1 && !_extDisplayActive) {
				ResizeCamera ();
				Display.displays [1].Activate ();
				_extDisplayActive = true;
			}
				
			bool active = true;

			if (_extCam != null) {
				_extCam.enabled = active;
				_extCam.gameObject.SetActive(active);
			}

			if (_videoBackdrop != null) {
				_videoBackdrop.SetActive (active);
			}

		}

		public void Deactive()
		{
				
			if (_extCam != null) {
				_extCam.enabled = false;
				_extCam.gameObject.SetActive(false);
			}
				
			if (_videoBackdrop != null) {
				_videoBackdrop.SetActive (false);
			}
		}

		void ActiveDeactiveIfDetected()
		{
			if(VideoStreamerAttributes.Instance.Attributes.autoActiveMonitor){
				if (_extDisplayConnected) {
					Active();
				}

				if (_extDisplayDisconected) {
					Deactive ();
				}	
			}
		}

		/// <summary>
		/// Detects the display can detect if display is Plug And Play in runtime.
		/// </summary>
		void DetectDisplay()
		{

			if (!extDisplayAvailable) {
				_extDisplayActive = false;
			}

			_extDisplayConnected = false;
			_extDisplayDisconected = false;

			if (_extDisplayPviousState != extDisplayAvailable) {
				if (extDisplayAvailable) {
					_extDisplayConnected =true;
				} else {
					_extDisplayDisconected = true;
				}
				_extDisplayPviousState = extDisplayAvailable;
			}
		}

		void CloneCamera(){

			string cameraName = string.Empty;
			if (Application.isEditor) {
				cameraName = "Mono Camera";
			} else {
				cameraName = "Left Camera";
			}

			_cameraMain = FindCamera (cameraName);
				
			GameObject clonedCameraObject = Instantiate(_cameraMain.gameObject);
			_extCam = clonedCameraObject.GetComponent<Camera> ();

			clonedCameraObject.SetActive (false);
			_extCam.enabled = false;

			clonedCameraObject.transform.SetParent (DisplayManager.Instance.transform);
			_extCam.targetDisplay = 1;

			PhysicsRaycaster raycaster = clonedCameraObject.GetComponent<PhysicsRaycaster> ();
			if (raycaster != null) {
				raycaster.enabled = false;
			}

			CreateCameraBackdrop ();

			Deactive ();

		}

		private void ResizeCamera()
		{

			if (!extDisplayAvailable) {
				return;
			}

			if (_extCam == null) {
				CloneCamera ();
			}

			Debug.Log ("Message: VirtualCamAccesor: Resize Camera");
			_extCam.CopyFrom (_cameraMain);
			_extCam.clearFlags = CameraClearFlags.SolidColor;
			_extCam.rect = new Rect(0f, 0f, 1f, 1f);
			_extCam.backgroundColor = new Color (0.1f, 0.0f, 0.0f, 1.0f);
			_extCam.targetDisplay = 1;
			_extCam.cullingMask |= 1 << LayerMask.NameToLayer (_cameraLayer);

		}

		private Camera FindCamera(string name)
		{
			GameObject cameraObject = GameObject.Find(name); //returns null if no background visible initially
			if (cameraObject != null) {
				return cameraObject.GetComponent<Camera> ();
			} else {
				return null;
			}
		}

		private void CreateCameraBackdrop()
		{
			Debug.Log("CloneCameraToDisplay: GetVideoBackgroundCamera creating camera dependebcies");
			_videoBackdrop = GameObject.CreatePrimitive (PrimitiveType.Quad);
			_videoBackdrop.name = "Video Backdrop Display";
			_videoBackdrop.layer = LayerMask.NameToLayer (_cameraLayer);
			_videoBackdrop.transform.SetParent (DisplayManager.Instance.transform, false);
			GameObject.Destroy (_videoBackdrop.GetComponent<Collider> ());
			_videoBackdrop.AddComponent<VideoBackdrop> ();
			_videoBackdrop.SetActive (false);

			Camera[] cameras = DisplayManager.Instance.GetComponentsInChildren<Camera> ();

			foreach(Camera tmpCamera in cameras)
			{
				//TODO: This will disable camera background layer for all cameras.
				tmpCamera.cullingMask &=  ~(1 << LayerMask.NameToLayer(_cameraLayer));
			}

		}

	}

}