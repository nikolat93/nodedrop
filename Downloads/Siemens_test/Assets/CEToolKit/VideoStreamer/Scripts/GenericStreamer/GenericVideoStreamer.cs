﻿/****************************************************************************************************************************************
 * © 2016 Daqri International. All Rights Reserved.                                                                                    	*
 *                                                                                                                                     	*
 *     NOTICE:  All software code and related information contained herein is, and remains the property of DAQRI INTERNATIONAL and its 	*
 * suppliers, if any.  The intellectual and technical concepts contained herein are proprietary to DAQRI INTERNATIONAL and its         	*
 * suppliers and may be covered by U.S. and Foreign Patents, patents in process, and/or trade secret law, and the expression of			*
 * those concepts is protected by copyright law. Dissemination, reproduction, modification, public display, reverse engineering, or 	*
 * decompiling of this material is strictly forbidden unless prior written permission is obtained from DAQRI INTERNATIONAL.            	*
 *																																		*
 *																																		*
 *																																		*
 *     Created by:        	Jan Bajana, Chris Fairclough																				*                                
 *     Created on:        	JAN 19 2016																									*                
 *     File Purpose:		Class to drive all experience																				*                                                                                    
 *																																		*
 *     Guide:               <TBD>																										*
 *																																		*
 ****************************************************************************************************************************************/

using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Diagnostics;
using System;

namespace L7.VideoStreamer
{
	public class GenericVideoStreamer : SystemProcess {

		//public CameraCastToDisplay _cameraCastToDisplay;
		//Process _myProcess;
		[SerializeField]
		private int _ffmpegProcessID = -1;

		public Text _pid;

		public bool extDisplayAvailable {
			get{
				if (Application.isEditor) {
					return true;
				} else {
					return Display.displays.Length > 1; 
				}
			}
		}

		// Use this for initialization
		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () {
			if (Input.GetKeyDown (KeyCode.H)) {
				//StartProcess ();
			}

			if (Input.GetKeyDown (KeyCode.J)) {
				//KillProcess ();
			}
		}

		void OnApplicationQuit() {
			KillProcess ();
		}

		public void StartKillProcess(bool toggle)
		{
			if (toggle) {
				StartProcess ();
			} else {
				KillProcess ();
			}
		}

		public override void StartProcess()
		{
			if(_process != null){
				UnityEngine.Debug.LogWarning ("SystemProcess - Process is already running.");
				return;
			}

			/*
			if (!extDisplayAvailable) {
				CreateVirtualDisplay ();
			}
			*/

			Process[] proceses = null;
			proceses = Process.GetProcessesByName ("ffmpeg");

			//"-o \"rtp rtp://10.0.20.82:54009\""
			string[] args = new string[2];
			args[0] = "-o \"" + VideoStreamerAttributes.Instance.Attributes.gStreamerDestination + "\"";
			args[1] = "-m x11virtual";

			if (Application.isEditor) {
				args[1] = "-m x11grab";
			}

			base.StartProcess ("videostreamer", args);
			StartCoroutine (FindIfNewProcess("ffmpeg", proceses));

		}

		public override void KillProcess()
		{
			if (_process != null) {
				UnityEngine.Debug.Log("Video Streamer killing process");

				Process proceses = Process.GetProcessById (_ffmpegProcessID);
				if (proceses != null) {
					proceses.Kill ();
					_ffmpegProcessID = -1;
				}

				base.KillProcess ();

				if (_pid != null) {
					_pid.text = "-1";
				}
			}

		}

		public bool IsStreaming()
		{
			return _process != null;
		}

		IEnumerator FindIfNewProcess(string processName, Process[] proceses)
		{
			bool searching = true;
			int counter = 0;

			while (searching) {
				yield return new WaitForSeconds(0.5f);

				int PID = SystemProcess.FindNewProcess (processName, proceses);

				if (PID >= 0 || counter > 5f) {
					_ffmpegProcessID = PID;
					searching = false;

					if (_pid != null) {
						_pid.text = string.Empty + PID;
					}

					UnityEngine.Debug.Log ("Video Recorder FFmpeg ID: " + _ffmpegProcessID);
				}
				counter++;
			}
		}

	}
}