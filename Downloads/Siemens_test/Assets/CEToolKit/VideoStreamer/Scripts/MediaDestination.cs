﻿/****************************************************************************************************************************************
 * © 2016 Daqri International. All Rights Reserved.                                                                                    	*
 *                                                                                                                                     	*
 *     NOTICE:  All software code and related information contained herein is, and remains the property of DAQRI INTERNATIONAL and its 	*
 * suppliers, if any.  The intellectual and technical concepts contained herein are proprietary to DAQRI INTERNATIONAL and its         	*
 * suppliers and may be covered by U.S. and Foreign Patents, patents in process, and/or trade secret law, and the expression of			*
 * those concepts is protected by copyright law. Dissemination, reproduction, modification, public display, reverse engineering, or 	*
 * decompiling of this material is strictly forbidden unless prior written permission is obtained from DAQRI INTERNATIONAL.            	*
 *																																		*
 *																																		*
 *																																		*
 *     Created by:        	Jan Bajana																				*                                
 *     Created on:        	Sep 2 2016																									*                
 *     File Purpose:		Class to drive all experience																				*                                                                                    
 *																																		*
 *     Guide:               <TBD>																										*
 *																																		*
 ****************************************************************************************************************************************/

using UnityEngine;
using UnityEngine.Events;
using System.Collections;

using System;
using System.Collections;
using System.Collections.Generic;

namespace DAQRI.VideoStreamer
{

	[System.Serializable]
	public class OnStartStopEvent : UnityEvent<bool>
	{

	}

	public abstract class MediaDestination : MonoBehaviour {

		[SerializeField]
		protected MediaSource _input;

		//[SerializeField]
		protected bool _autoStartOnIntput = false;

		protected bool _autoDetectIntput = false;

		public bool PlayOnStart
		{
			get { return _autoStartOnIntput; }
			set { _autoStartOnIntput = value; }
		}

	}

}