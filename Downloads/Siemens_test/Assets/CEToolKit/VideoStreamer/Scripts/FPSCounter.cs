﻿/****************************************************************************************************************************************
 * © 2016 Daqri International. All Rights Reserved.                                                                                    	*
 *                                                                                                                                     	*
 *     NOTICE:  All software code and related information contained herein is, and remains the property of DAQRI INTERNATIONAL and its 	*
 * suppliers, if any.  The intellectual and technical concepts contained herein are proprietary to DAQRI INTERNATIONAL and its         	*
 * suppliers and may be covered by U.S. and Foreign Patents, patents in process, and/or trade secret law, and the expression of			*
 * those concepts is protected by copyright law. Dissemination, reproduction, modification, public display, reverse engineering, or 	*
 * decompiling of this material is strictly forbidden unless prior written permission is obtained from DAQRI INTERNATIONAL.            	*
 *																																		*
 *																																		*
 *																																		*
 *     Created by:        	Jan Bajana																							*                                
 *     Created on:        	May 25 2016																									*                
 *     File Purpose:		Class to drive all experience																			*                                                                                    
 *																																		*
 *     Guide:               <TBD>																										*
 *																																		*
 ****************************************************************************************************************************************/

using UnityEngine;
using System.Collections;

namespace L7.Tools
{
	public class FPSCounter {

		int _frames = 0;
		float _startTime = 0;
		float _fps = 0.0f;

		public FPSCounter()
		{
			Reset();
		}

		public float GetValue {
			get {
				return _fps;
			}
		}

		public void Reset()
		{
			_frames = 0;
			_startTime = Time.time;
		}

		public void Update() {
			_frames++;
			float difTime = Time.time - _startTime;
			if (difTime > 1.0f) {
				_fps = ((float) _frames) / difTime;
				_startTime = Time.time;
				_frames = 0;
			}
		}

	}
}