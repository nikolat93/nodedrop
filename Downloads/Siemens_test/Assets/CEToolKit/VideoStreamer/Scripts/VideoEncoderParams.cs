﻿/****************************************************************************************************************************************
 * © 2016 Daqri International. All Rights Reserved.                                                                                    	*
 *                                                                                                                                     	*
 *     NOTICE:  All software code and related information contained herein is, and remains the property of DAQRI INTERNATIONAL and its 	*
 * suppliers, if any.  The intellectual and technical concepts contained herein are proprietary to DAQRI INTERNATIONAL and its         	*
 * suppliers and may be covered by U.S. and Foreign Patents, patents in process, and/or trade secret law, and the expression of			*
 * those concepts is protected by copyright law. Dissemination, reproduction, modification, public display, reverse engineering, or 	*
 * decompiling of this material is strictly forbidden unless prior written permission is obtained from DAQRI INTERNATIONAL.            	*
 *																																		*
 *																																		*
 *																																		*
 *     Created by:        	Jan Bajana                  																				*                                
 *     Created on:        	June 28 2016																								*                
 *     File Purpose:		Class to drive all experience																				*                                                                                    
 *																																		*
 *     Guide:               <TBD>																										*
 *																																		*
 ****************************************************************************************************************************************/

using UnityEngine;
using System.Collections;

namespace DAQRI.VideoStreamer 
{

	using NativeVideoResolution = VideoStreamerNativePlugin.VideoResolution;
	using NativeVideoQuality = VideoStreamerNativePlugin.VideoQuality;

	[System.Serializable]
	public class VideoEncoderParams : System.Object{

		[SerializeField]
		private int _width = 848;

		[SerializeField]
		private int _height = 480;

		[SerializeField]
		private int _bitRate = 2560000;

		[SerializeField]
		private int _bFameStrategy = 1;

		[SerializeField]
		private int _bFamesMax = 2;

		[SerializeField]
		private int _minGOP = 4;

		[SerializeField]
		private int _maxGOP = 16;

		[SerializeField]
		private int _qMin = 10;

		[SerializeField]
		private int _qMax = 26;

		public int Width
		{
			get { return _width; }
			private set { _width = value; }
		}

		public int Height
		{
			get { return _height; }
			private set { _height = value; }
		}

		public int BitRate
		{
			get { return _bitRate; }
			private set { _bitRate = value; }
		}

		public int GOPLenght
		{
			get { return _maxGOP; }
			private set { _maxGOP = value; }
		}

		public int GOPMin
		{
			get { return _minGOP; }
			private set { _minGOP = value; }
		}

		public int BFameStrategy
		{
			get { return _bFameStrategy; }
			private set { _bFameStrategy = value; }
		}

		public int BFamesMax
		{
			get { return _bFamesMax; }
			private set { _bFamesMax = value; }
		}

		public int qMin
		{
			get { return _qMin; }
			private set { _qMin = value; }
		}

		public int qMax
		{
			get { return _qMax; }
			private set { _qMax = value; }
		}

		public void SetFromCustomParams()
		{
			VideoStreamerNativePlugin.SetMuxCodecM (VideoStreamerNativePlugin.MuxCodecInfo.BIT_RATE, BitRate);
			VideoStreamerNativePlugin.SetMuxCodecM (VideoStreamerNativePlugin.MuxCodecInfo.B_FRAMES_MAX, _bFamesMax);
			VideoStreamerNativePlugin.SetMuxCodecM (VideoStreamerNativePlugin.MuxCodecInfo.B_FRAMES_STRATEGY, _bFameStrategy);
			VideoStreamerNativePlugin.SetMuxCodecM (VideoStreamerNativePlugin.MuxCodecInfo.GOP_MIN_LENGHT, _minGOP);
			VideoStreamerNativePlugin.SetMuxCodecM (VideoStreamerNativePlugin.MuxCodecInfo.GOP_LENGHT, _maxGOP);
			VideoStreamerNativePlugin.SetMuxCodecM (VideoStreamerNativePlugin.MuxCodecInfo.QUANTIZER_MAX, _qMax);
			VideoStreamerNativePlugin.SetMuxCodecM (VideoStreamerNativePlugin.MuxCodecInfo.QUANTIZER_MIN, _qMin);
		}

		public void SetAutoParams(NativeVideoResolution resolution, NativeVideoQuality quality)
		{

			if (resolution == NativeVideoResolution.RES_16x9_1280x720 && quality == NativeVideoQuality.HIGH_QUALITY_HQ) {
				_width = 1280;
				_height = 720;
				_bitRate = 3560000;
				_bFameStrategy = 1;
				_bFamesMax = 2;
				_minGOP = 4;
				_maxGOP = 16;
				_qMin = 10;
				_qMax = 23;

			}else if (resolution == NativeVideoResolution.RES_16x9_1280x720 && quality == NativeVideoQuality.LOW_QUALITY_LQ) {
				_width = 1280;
				_height = 720;
				_bitRate = 2560000;
				_bFameStrategy = 1;
				_bFamesMax = 2;
				_minGOP = 4;
				_maxGOP = 16;
				_qMin = 10;
				_qMax = 26;

			}else if (resolution == NativeVideoResolution.RES_16x9_848x480 && quality == NativeVideoQuality.HIGH_QUALITY_HQ) {
				_width = 848;
				_height = 480;
				_bitRate = 1600000;
				_bFameStrategy = 1;
				_bFamesMax = 2;
				_minGOP = 4;
				_maxGOP = 16;
				_qMin = 10;
				_qMax = 23;

			}else if (resolution == NativeVideoResolution.RES_16x9_848x480 && quality == NativeVideoQuality.LOW_QUALITY_LQ) {
				_width = 848;
				_height = 480;
				_bitRate = 1280000;
				_bFameStrategy = 1;
				_bFamesMax = 2;
				_minGOP = 4;
				_maxGOP = 16;
				_qMin = 10;
				_qMax = 26;

			}else if (resolution == NativeVideoResolution.RES_4x3_640x480 && quality == NativeVideoQuality.HIGH_QUALITY_HQ) {
				_width = 640;
				_height = 480;
				_bitRate = 1600000;
				_bFameStrategy = 1;
				_bFamesMax = 2;
				_minGOP = 4;
				_maxGOP = 16;
				_qMin = 10;
				_qMax = 23;

			}else if (resolution == NativeVideoResolution.RES_4x3_640x480 && quality == NativeVideoQuality.LOW_QUALITY_LQ) {
				_width = 640;
				_height = 480;
				_bitRate = 1280000;
				_bFameStrategy = 1;
				_bFamesMax = 2;
				_minGOP = 4;
				_maxGOP = 16;
				_qMin = 10;
				_qMax = 26;

			}

			SetFromCustomParams();

		}

		public void RefreshLocalParams()
		{
			_bitRate = VideoStreamerNativePlugin.GetMuxCodecM (VideoStreamerNativePlugin.MuxCodecInfo.BIT_RATE);
			_bFamesMax = VideoStreamerNativePlugin.GetMuxCodecM (VideoStreamerNativePlugin.MuxCodecInfo.B_FRAMES_MAX);
			_bFameStrategy = VideoStreamerNativePlugin.GetMuxCodecM (VideoStreamerNativePlugin.MuxCodecInfo.B_FRAMES_STRATEGY);
			_minGOP = VideoStreamerNativePlugin.GetMuxCodecM (VideoStreamerNativePlugin.MuxCodecInfo.GOP_MIN_LENGHT);
			_maxGOP = VideoStreamerNativePlugin.GetMuxCodecM (VideoStreamerNativePlugin.MuxCodecInfo.GOP_LENGHT);
			_qMax = VideoStreamerNativePlugin.GetMuxCodecM (VideoStreamerNativePlugin.MuxCodecInfo.QUANTIZER_MAX);
			_qMin = VideoStreamerNativePlugin.GetMuxCodecM (VideoStreamerNativePlugin.MuxCodecInfo.QUANTIZER_MIN);
		}

		public string GetMuxInfo()
		{
			return (string.Empty + " |res: " +_width + "x" + _height + " |bitRate: " + (_bitRate/1000f) + " Kb/s" + " |GOPmin: " + _minGOP + " |GOPmax: " + _maxGOP + " |qMax: " + _qMax);
		}

	}

}