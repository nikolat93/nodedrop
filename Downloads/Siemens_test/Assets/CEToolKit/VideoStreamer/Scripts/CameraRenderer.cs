﻿/****************************************************************************************************************************************
 * © 2016 Daqri International. All Rights Reserved.                                                                                    	*
 *                                                                                                                                     	*
 *     NOTICE:  All software code and related information contained herein is, and remains the property of DAQRI INTERNATIONAL and its 	*
 * suppliers, if any.  The intellectual and technical concepts contained herein are proprietary to DAQRI INTERNATIONAL and its         	*
 * suppliers and may be covered by U.S. and Foreign Patents, patents in process, and/or trade secret law, and the expression of			*
 * those concepts is protected by copyright law. Dissemination, reproduction, modification, public display, reverse engineering, or 	*
 * decompiling of this material is strictly forbidden unless prior written permission is obtained from DAQRI INTERNATIONAL.            	*
 *																																		*
 *																																		*
 *																																		*
 *     Created by:        	Jan Bajana, Chris Fairclough																				*                                
 *     Created on:        	May 25 2016																									*                
 *     File Purpose:		Class to drive all experience																				*                                                                                    
 *																																		*
 *     Guide:               <TBD>																										*
 *																																		*
 ****************************************************************************************************************************************/

using UnityEngine;
using UnityEngine.UI;

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;

namespace DAQRI.VideoStreamer
{
	[System.Obsolete("CameraRenderer class is obsolete. Use VirtuealCameraAccesor instead")]
	public class CameraRenderer : MediaSource 
	{

		public string _cullingMask = "Video Streamer";
		public float _cameraFPS = 30f;

		[SerializeField]
		private List<Camera> _foregroundCameras;

		//vars for enabling camera background, streaming it, and then disabling it again
		public bool _backgrundCameraByName = false;
		public string _backgroundCameraName = string.Empty;
		public Camera _backgroundCamera = null;

		//public bool _toogleCullingMaskForCamera = false;

		public CameraParams _videoOptions;
		public CameraParams _photoOptions;

		//private variabils
		private CameraParams.CameraMode _videoMode = CameraParams.CameraMode.NONE;

		private bool _sceneConfigured = false;
		private Rect _cameraMonoViewPort;

		Camera clearingCamera;

		private float _timeSinceBackFrameCaptured;

		#region MonoBehavior Events

		protected virtual void Awake ()
		{
			_videoOptions.Init(CameraParams.CameraMode.VIDEO);
			_photoOptions.Init(CameraParams.CameraMode.PHOTO);
		}

		protected virtual void OnEnable ()
		{
			OnVideoFrameEvent += OnVideoTaken;
			OnPhotoFrameEvent += OnPhotoTaken;
		}
		
		protected virtual void OnDisable ()
		{
			OnVideoFrameEvent -= OnVideoTaken;
			OnPhotoFrameEvent -= OnPhotoTaken;
		}
			
		protected virtual void LateUpdate () 
		{

			if (_videoMode == CameraParams.CameraMode.VIDEO) {
				if(Time.time - _timeSinceBackFrameCaptured > (1.0f/_cameraFPS)) {
					StartCoroutine(CaptureVideoCoroutine (_videoOptions));
					_timeSinceBackFrameCaptured = Time.time;
				}
			}

		}

		#endregion

		#region Public Methods

		public void CapturePhoto()
		{
			//StartCoroutine (CaptureWithTimerForPhotoProcessing (RenderFromCameraList (_photoOptions)));
			StartCoroutine(CapturePhotoCoroutine (_photoOptions));
		}

		public void Play()
		{
			Debug.Log ("CameraRenderer: Start Play");
			StartCoroutine(StartVideoWithDellay ());
		}

		public void Stop()
		{
			Debug.Log ("CameraRenderer: Stop CaptureVideoFromGPUCanvas");
			_videoMode = CameraParams.CameraMode.NONE;
		}
			
		#endregion

		#region Private Methods

		private Camera FindCamera(string name)
		{
			Debug.Log("CameraRenderer: FindCamera from name: " + name);

			GameObject cameraObject = GameObject.Find(name); //returns null if no background visible initially
			if (cameraObject != null) {
				return cameraObject.GetComponent<Camera> ();
			} else {
				return null;
			}
		}

		private void ConfigureScene()
		{	
			Debug.Log("CameraRenderer: Configure Scene");

			Int32 layer = LayerMask.NameToLayer(_cullingMask);
			if (layer==-1){
				Debug.LogError ("You have to add new layer in Editor->Project Settings-> Tags & Layers: " + _cullingMask);
			}

			_cameraMonoViewPort = new Rect(0f, 0f, 1f, 1f);
			if(_backgrundCameraByName){
				_backgroundCamera = FindCamera (_backgroundCameraName);
			}

			CreateClearingCamera();
		}

		void CreateClearingCamera()
		{
			clearingCamera = this.gameObject.GetComponent<Camera>();
			if (clearingCamera == null) {
				clearingCamera = this.gameObject.AddComponent<Camera>();    
			}

			// First camera to render, don't render any layers.
			clearingCamera.depth = 0;
			clearingCamera.cullingMask = 0;

			// Clear color to black.
			clearingCamera.clearFlags = CameraClearFlags.SolidColor;
			//clearingCamera.clearFlags = CameraClearFlags.Color;
			//clearingCamera.targetTexture = ...;
			clearingCamera.backgroundColor = new Color (0.5f, 0.5f, 0.5f, 1.0f);
			clearingCamera.enabled = false;
		}
			
		/// <summary>
		/// Renders from camera list. New function. Use instead of GrabFromCameraList
		/// </summary>
		private void RenderFromCameraList(CameraParams cameraParams)
		{
			if (!_sceneConfigured) {
				ConfigureScene();	
				_sceneConfigured = true;
			}

			//yield return new WaitForEndOfFrame();

			RenderTexture tmpRenderTexture = cameraParams.GetRenderTexture;
			Rect tmpPixelRect = _cameraMonoViewPort;

			if(_backgroundCamera != null)
			{
				tmpPixelRect = _backgroundCamera.rect;
			}

			//Debug.Log ("Writer: " + (int) _activeRenderTexture.GetNativeTexturePtr());

			List<Rect> previousCameraRects = new List<Rect> ();

			try{
				RenderTexture.active = tmpRenderTexture;

				if(clearingCamera != null)
				{
					clearingCamera.targetTexture = tmpRenderTexture;
					clearingCamera.Render();
				}
					
				if(_backgroundCamera != null)
				{

					_backgroundCamera.cullingMask |= 1 << LayerMask.NameToLayer(_cullingMask);
					_backgroundCamera.targetTexture = tmpRenderTexture;
					_backgroundCamera.rect = _cameraMonoViewPort;
					_backgroundCamera.Render();
				
				}

				//Render foreground layers containing AR content like UI and 3D objects.
				foreach (Camera tmpCamera in _foregroundCameras) {
					if (tmpCamera != null) {

						CameraClearFlags oldClearingFlag = tmpCamera.clearFlags;
						tmpCamera.clearFlags = CameraClearFlags.Nothing;

						previousCameraRects.Add(tmpCamera.pixelRect);
						tmpCamera.rect = _cameraMonoViewPort;
						tmpCamera.targetTexture = tmpRenderTexture;
						tmpCamera.Render ();
						tmpCamera.clearFlags = oldClearingFlag;
					}
				}

				cameraParams.Apply();
				//RenderTexture.active = null;

			}catch (System.IO.IOException e)
			{
				Debug.LogError("ScreenshotManager: Exception while rendering: " + e.Message);
			}finally
			{
				int lastCamRectIndex = previousCameraRects.Count - 1;
				foreach (Camera tmpCamera in _foregroundCameras) {
					if (tmpCamera != null) {
						tmpCamera.targetTexture = null;
						tmpCamera.rect = previousCameraRects[lastCamRectIndex];
						lastCamRectIndex--;
					}
				}

				if(_backgroundCamera != null)
				{
					_backgroundCamera.targetTexture = null;
					_backgroundCamera.rect = tmpPixelRect;
					_backgroundCamera.cullingMask &=  ~(1 << LayerMask.NameToLayer(_cullingMask));
				}

				OnNewVideoFrame (cameraParams);

			}

		}

		/// <summary>
		/// Initiates the with timer.
		/// This is need if you have disabled camera background but you need take screen shot with background.
		/// Optimalized for Helmet.
		/// </summary>
		private IEnumerator CaptureVideoCoroutine(CameraParams camParams)
		{
			yield return new WaitForEndOfFrame();
			RenderFromCameraList (camParams);
			OnNewVideoFrame (camParams);
		}

		private IEnumerator CapturePhotoCoroutine(CameraParams camParams)
		{
			//yield return new WaitForSeconds(DelaySeconds);
			yield return new WaitForEndOfFrame();
			RenderFromCameraList (camParams);
			OnNewPhotoFrame (camParams);
		}

		private IEnumerator StartVideoWithDellay()
		{
			yield return new WaitForSeconds(0.5f);
			_videoMode = CameraParams.CameraMode.VIDEO;
		}

		/// <summary>
		/// Raises the screenshot taken events.
		/// </summary>
		void OnVideoTaken(CameraParams options)
		{
			if (options.GetCameraMode == CameraParams.CameraMode.VIDEO) {
				//_videoOptions.RefreshCameraPreview ();
			}
		}

		void OnPhotoTaken(CameraParams options)
		{
			if (options.GetCameraMode == CameraParams.CameraMode.PHOTO) {
				Debug.Log ("CameraRenderer: PHOTO TAKEN");
				//_photoOptions.RefreshCameraPreview ();
			}
		}

		#endregion

	}

}
