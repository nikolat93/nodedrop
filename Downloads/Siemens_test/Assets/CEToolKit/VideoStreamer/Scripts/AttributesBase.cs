﻿/****************************************************************************************************************************************
 * © 2016 Daqri International. All Rights Reserved.                                                                                    	*
 *                                                                                                                                     	*
 *     NOTICE:  All software code and related information contained herein is, and remains the property of DAQRI INTERNATIONAL and its 	*
 * suppliers, if any.  The intellectual and technical concepts contained herein are proprietary to DAQRI INTERNATIONAL and its         	*
 * suppliers and may be covered by U.S. and Foreign Patents, patents in process, and/or trade secret law, and the expression of			*
 * those concepts is protected by copyright law. Dissemination, reproduction, modification, public display, reverse engineering, or 	*
 * decompiling of this material is strictly forbidden unless prior written permission is obtained from DAQRI INTERNATIONAL.            	*
 *																																		*
 *																																		*
 *																																		*
 *     Created by:        	Jan Bajana																				*                                
 *     Created on:        	Jan 9 2016																									*                
 *     File Purpose:		Class to drive all experience																				*                                                                                    
 *																																		*
 *     Guide:               <TBD>																										*
 *																																		*
 ****************************************************************************************************************************************/

using UnityEngine;

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

using DAQRI.Tools;

namespace L7.Tools
{
	
	public abstract class AttributesBase : MonoBehaviour {

		public string localFolder = "Attributes";
		public string fileName = "VideoSteamer.json";

		protected void Save(object obj)
		{
			if (obj == null) {
				return;
			}

			string path = Path.Combine(Application.streamingAssetsPath, Path.Combine(localFolder, fileName));
			SaveToJSON (obj, path);
			Debug.Log ("AttributesBase - Saving attributes: " + path);
		}

		protected T Load<T>()
		{
			if (!Verify ()) {
				return default(T);
			}

			string path = Path.Combine(Application.streamingAssetsPath, Path.Combine(localFolder, fileName));
			Debug.Log ("AttributesBase - Loading attributes: " + path);

			return LoadFromJSON<T> (path);
		}

		public bool Verify()
		{
			bool returnVal = false;

			string path = Path.Combine(Application.streamingAssetsPath, Path.Combine(localFolder, fileName));

			if (!FileSystem.IsExistFolder (path)) {
				Debug.Log ("AttributesBase - Attributes verified: " + path);
				//FileSystem.DirectoryCopy (mainStreamingDataPath, wIDataFolder, true);
				returnVal = true;
			}else {
				Debug.LogWarning ("AttributesBase - Attributes dont axist: " + path);
			}

			return returnVal;
		}

		protected void SaveToJSON(object obj, string path)
		{
			if (obj == null) {
				return;
			}
			string json = JsonUtility.ToJson(obj, true);
			FileSystem.WriteText (path, json);
		}

		protected T LoadFromJSON<T>(string path)
		{
			string json = FileSystem.ReadText (path);
			return JsonUtility.FromJson<T> (json);
		}

	}

}
