﻿/****************************************************************************************************************************************
 * © 2016 Daqri International. All Rights Reserved.                                                                                    	*
 *                                                                                                                                     	*
 *     NOTICE:  All software code and related information contained herein is, and remains the property of DAQRI INTERNATIONAL and its 	*
 * suppliers, if any.  The intellectual and technical concepts contained herein are proprietary to DAQRI INTERNATIONAL and its         	*
 * suppliers and may be covered by U.S. and Foreign Patents, patents in process, and/or trade secret law, and the expression of			*
 * those concepts is protected by copyright law. Dissemination, reproduction, modification, public display, reverse engineering, or 	*
 * decompiling of this material is strictly forbidden unless prior written permission is obtained from DAQRI INTERNATIONAL.            	*
 *																																		*
 *																																		*
 *																																		*
 *     Created by:        	Jan Bajana																				*                                
 *     Created on:        	Sep 2 2016																									*                
 *     File Purpose:		Class to drive all experience																				*                                                                                    
 *																																		*
 *     Guide:               <TBD>																										*
 *																																		*
 ****************************************************************************************************************************************/

using UnityEngine;
using UnityEngine.Events;

using System;
using System.Collections;
using System.Collections.Generic;

namespace DAQRI.VideoStreamer
{

	public abstract class MediaSource : MonoBehaviour {

		[SerializeField]
		protected bool _autoStart = false;

		public event Action<CameraParams> OnVideoFrameEvent;
		public event Action<CameraParams> OnPhotoFrameEvent;

		protected HashSet<System.Object> videoDeviceUsers = new HashSet<System.Object> ();

		public bool AutoStart
		{
			get { return _autoStart; }
			set { _autoStart = value; }
		}

		public virtual void CapturePhoto()
		{
			Debug.LogWarning ("MediaSource: CapturePhoto is abstract");
		}

		protected virtual void Play()
		{
			Debug.LogWarning ("MediaSource: Play is abstract");
		}

		protected virtual void Stop()
		{
			Debug.LogWarning ("MediaSource: Stop is abstract");
		}

		/// <summary>
		/// Registers a device user and starts the device if required.
		/// </summary>
		/// <param name="user">The object using this service (e.g. "this")</param>
		public void RegisterDeviceUser (System.Object user) {
			if (user == null) {
				return;
			}

			if (videoDeviceUsers.Count == 0) {
				Play();
			}

			if(!videoDeviceUsers.Contains(user))
			{
				videoDeviceUsers.Add (user);
			}
		}

		/// <summary>
		/// Unregisters a device user and stop the device if required.
		/// </summary>
		/// <param name="user">The object using this service (e.g. "this")</param>
		public void UnregisterDeviceUser (System.Object user) {
			if (user == null) {
				//Debug.Log ("UnregisterVideoTextureUser: requires instance of an object");
				return;
			}
			videoDeviceUsers.Remove (user);
			if (videoDeviceUsers.Count == 0) {
				Stop ();
			}
		}

		protected virtual void OnNewVideoFrame(CameraParams options)
		{
			// Make a temporary copy of the event to avoid possibility of
			// a race condition if the last subscriber unsubscribes
			// immediately after the null check and before the event is raised.

			Action<CameraParams> tmpEvent = OnVideoFrameEvent;
			if (tmpEvent != null)
			{
				tmpEvent(options);
			}
		}

		protected virtual void OnNewPhotoFrame(CameraParams options)
		{
			// Make a temporary copy of the event to avoid possibility of
			// a race condition if the last subscriber unsubscribes
			// immediately after the null check and before the event is raised.

			Action<CameraParams> tmpEvent = OnPhotoFrameEvent;
			if (tmpEvent != null)
			{
				tmpEvent(options);
			}
		}

		protected bool IsVideoEventActive()
		{
			return (OnVideoFrameEvent != null);
		}

		protected bool IsPhotoEventActive()
		{
			return (OnPhotoFrameEvent != null);
		}

	}

}