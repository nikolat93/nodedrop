using System;
using UnityEngine;
using UnityEngine.UI;

public class DebugInfo : MonoBehaviour
{
	public Text _textFPS;
	int numberOfFrames = 0;
	float fps;
	Time time;
	float samplingInterval;
	float maxSamplingInterval = 1.0f;
	public bool bShowDebugInfo = false;
	public float top = 10.0f;
	public float topMargin = 15.0f;
	public float left = 10.0f;
	public float leftMargin = 10.0f;

	//private DSHCamera dshCamera = null;

	void Awake() {
		samplingInterval = 0.0f;
		numberOfFrames = 0;
		//dshCamera = gameObject.GetComponent<DSHCamera> ();
	}

	void OnGUI() {
		if (bShowDebugInfo) {
			GUI.Label (new Rect (left, top, Screen.width, Screen.height), "FPS = " + fps.ToString ("F5"));
			//if (dshCamera) {
				//GUI.Label (new Rect (left, top + topMargin, Screen.width, Screen.height), "Tracking = " + dshCamera.EnableTracking);
			//}
		}
	}

	void Update() {
		if ((Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl)) && Input.GetKeyDown (KeyCode.D)) {
			bShowDebugInfo = !bShowDebugInfo;
		}
		samplingInterval += Time.deltaTime;
		//Calculating Frames per second
		numberOfFrames++;
		if (samplingInterval >= maxSamplingInterval) {
			fps = ((float)numberOfFrames * 1.0f/samplingInterval);
			samplingInterval = 0.0f;
			numberOfFrames = 0;

			if(_textFPS != null)
			{
				_textFPS.text = "" + fps;
			}

		}
	}
		
}


