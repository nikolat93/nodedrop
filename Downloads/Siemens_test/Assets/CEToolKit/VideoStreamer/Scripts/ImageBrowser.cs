/****************************************************************************************************************************************
 * © 2016 Daqri International. All Rights Reserved.                                                                                    	*
 *                                                                                                                                     	*
 *     NOTICE:  All software code and related information contained herein is, and remains the property of DAQRI INTERNATIONAL and its 	*
 * suppliers, if any.  The intellectual and technical concepts contained herein are proprietary to DAQRI INTERNATIONAL and its         	*
 * suppliers and may be covered by U.S. and Foreign Patents, patents in process, and/or trade secret law, and the expression of			*
 * those concepts is protected by copyright law. Dissemination, reproduction, modification, public display, reverse engineering, or 	*
 * decompiling of this material is strictly forbidden unless prior written permission is obtained from DAQRI INTERNATIONAL.            	*
 *																																		*
 *																																		*
 *																																		*
 *     Created by:        	Jan Bajana																				*                                
 *     Created on:        	May 25 2016																									*                
 *     File Purpose:		Class to drive all experience																				*                                                                                    
 *																																		*
 *     Guide:               <TBD>																										*
 *																																		*
 ****************************************************************************************************************************************/

using UnityEngine;
using UnityEngine.UI;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace DAQRI.VideoStreamer
{
	public class ImageBrowser : MediaDestination {

		enum SaveStatus { NOTSAVED, SAVED, DENIED, TIMEOUT };

		private RawImage _loadedImagePreview;//remove

		//public bool _enableConfirmation = false;
		//private Texture2D _postponeImage = null;

		public string _imagePrefix = "Img";
		public string _albumDirectory = "PhotoAlbum"; 
		public string _fileType = "jpg";

		public List<string> _capturedScreenshots = new List<string>();

		public static event Action<string> OnImageSaved;
		public static event Action<Texture2D> OnImageLoaded;

		private Action<CameraParams> onNewInput = (CameraParams options) => {

			if (options.GetCameraMode != CameraParams.CameraMode.PHOTO) {
				//return;
			}

			ImageBrowser.Instance.SaveImageWithTimestamp(options.GetGrabedTexture);

		};
			
		static ImageBrowser _instance = null;
		public static ImageBrowser Instance 
		{
			get {
				if(_instance == null)
				{
					_instance = GameObject.FindObjectOfType(typeof(ImageBrowser)) as ImageBrowser;
					
					if(_instance == null) {
						GameObject obj = new GameObject("ImageBrowser");
						_instance = obj.AddComponent<ImageBrowser>();
					}
					Debug.Log("ImageBrowser: Create Instance");
				}
				return _instance; 
			}
		}
		
		void Awake() 
		{
			Debug.Log("ImageBrowser: Create Instance from Awake");

			if (_instance == null) {
				_instance = this;
			}

			string name = ImageBrowser.Instance.name;
			Debug.Log ("ImageBrowser: Auto Instance: " + name);

			if(_input != null)
			{
				_input.OnPhotoFrameEvent += onNewInput;
				//_input.OnVideoFrameEvent += onNewInput;
				//ImageBrowser.OnImageLoaded += OnImageLoadedEvent;
			}
		}

		public void SaveImageWithTimestamp(Texture2D texture)
		{

			string imagePrefix = _imagePrefix;
			string albumName = _albumDirectory;
			string fileType = _fileType;

			byte[] bytes;
			string fileExt;
			
			if(fileType == "png")
			{
				bytes = texture.EncodeToPNG();
				fileExt = ".png";
			}
			else
			{
				bytes = texture.EncodeToJPG();
				fileExt = ".jpg";
			}

			string date = System.DateTime.Now.ToString("hh-mm-ss_dd-MM-yy");
			string screenshotFilename = imagePrefix + "_" + date + fileExt;
			string path = Application.persistentDataPath + "/" + screenshotFilename;

			if(albumName != "None")
			{
				path = Path.Combine(Application.persistentDataPath, Path.Combine(albumName, screenshotFilename));
				string pathonly = Path.GetDirectoryName(path);

				if (!Directory.Exists (pathonly)) {
					Directory.CreateDirectory (pathonly);
					Debug.Log (pathonly);
				}
			}

			StartCoroutine(Save(bytes, path));

		}

		/// <summary>
		/// Saves the image.
		/// </summary>

		public void SaveImage(Texture2D texture, string fileName, string fileType = "jpg")
		{
			Debug.Log("Save image to gallery " + fileName);

			byte[] bytes;
			string fileExt;
			
			if(fileType == "png")
			{
				bytes = texture.EncodeToPNG();
				fileExt = ".png";
			}
			else
			{
				bytes = texture.EncodeToJPG();
				fileExt = ".jpg";
			}

			string path = Application.persistentDataPath + "/" + fileName + fileExt;
			StartCoroutine(Save(bytes, path));
		}

		public void LoadImage(string path)
		{
			try
			{
				if (System.IO.File.Exists(path)) 
				{
					string loadFile = "file://" + path;
					Instance.StartCoroutine(Instance.Load(loadFile));

				}else
				{
					Debug.LogWarning ("CaptureScreenShot: can not load: " + path);
				}
			}
			catch (System.IO.IOException e)
			{
				Debug.LogError("CaptureScreenShot: " + e.Message);
			}

		}

		IEnumerator Load(string path)
		{
			WWW www = new WWW(path);
			yield return www;

			//TODO: if problems with RAM check also: www.LoadImageIntoTexture((Texture2D)texture);
			if(OnImageLoaded != null) 
				OnImageLoaded(www.texture);
		}

		IEnumerator Save(byte[] bytes, string path)
		{
			int count = 0;
			SaveStatus saved = SaveStatus.NOTSAVED;

			System.IO.File.WriteAllBytes(path, bytes);
			saved = SaveStatus.SAVED;
			yield return null;

			switch(saved)
			{
			case SaveStatus.DENIED:
				path = "DENIED";
				break;

			case SaveStatus.TIMEOUT:
				path = "TIMEOUT";
			break;
			}
			
			if(OnImageSaved != null)
			{
				if(!_capturedScreenshots.Contains(path))
				{
					_capturedScreenshots.Add(path);
				}
				Debug.Log ("ImageBrowser: Image Saved: " + path);
				OnImageSaved(path);
			}
		}

		IEnumerator Wait(float delay)
		{
			float pauseTarget = Time.realtimeSinceStartup + delay;
			
			while(Time.realtimeSinceStartup < pauseTarget)
			{
				yield return null;	
			}
		}

		Texture2D _oldTexture;
		void OnImageLoadedEvent(Texture2D image)
		{
			if (_oldTexture != null) {
				Destroy (_oldTexture);
			}
			_oldTexture = image;
			if (_loadedImagePreview != null) {
				_loadedImagePreview.texture = image;
				_loadedImagePreview.color = Color.white;
				Debug.Log ("ImageBrowser OnImageTaken: Width = " + image.width + " Height = " + image.height);
			}

		}

	}
}
