Video Streamer Unity extension documentation.

==CameraCastToDisplay==
Camera cast to display is tool which will clone stereo camera in Helmet into mono mode and render this camera to external HDMI monitor connected to Helmet.
You use this tool when you want to make POV of your app on external monitor in mono mode including camera background while Helmet app runs in default stereo mode without camera background.
A HDMI monitor is connected like extended and use default Helmet params.
