﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace DAQRI.Tools
{

	public static class FileSystem {
		
		public static bool IsExistFolder(string path)
		{
			try
			{
				if (System.IO.Directory.Exists(path)) 
				{
					return true;
				}
			}
			catch (System.IO.IOException e)
			{
				Debug.LogError("FileSystem: " + e.Message + e.StackTrace);
			}

			return false;
		}

		public static bool IsExistFile(string path)
		{
			try
			{
				if (System.IO.File.Exists(path)) 
				{					
					return true;
				}
			}
			catch (System.IO.IOException e)
			{
				Debug.LogError("FileSystem: " + e.Message);
			}
			return false;
		}

		public static bool WriteText(string path, string content)
		{
			File.WriteAllText(path, content);
			return true;
		}

		public static bool AppendText(string path, string content)
		{
			File.AppendAllText(path, content);
			return true;
		}

		public static string ReadText(string path)
		{
			if (FileSystem.IsExistFile (path)) {
				string content = File.ReadAllText (path);
				return content;
			} else {
				return null;
			}
		}
			
		public static void CreateFolder(string path)
		{
			try
			{
				Directory.CreateDirectory(path);
			}
			catch (System.IO.IOException e)
			{
				Debug.LogError("FileSystem: " + e.Message);
			}
		}

		public static void DeleteFolder(string path)
		{
			try
			{
				Directory.Delete(path, true);
			}
			catch (System.IO.IOException e)
			{
				Debug.LogError("FileSystem: " + e.Message);
			}
		}

		public static void DeleteFile(string path)
		{
			try
			{
				File.Delete(path);
			}
			catch (System.IO.IOException e)
			{
				Debug.LogError("FileSystem: " + e.Message);
			}
		}

		public static void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs)
		{
			// Get the subdirectories for the specified directory.
			DirectoryInfo dir = new DirectoryInfo(sourceDirName);

			if (!dir.Exists)
			{
				throw new DirectoryNotFoundException(
					"Source directory does not exist or could not be found: "
					+ sourceDirName);
			}

			DirectoryInfo[] dirs = dir.GetDirectories();
			// If the destination directory doesn't exist, create it.
			if (!Directory.Exists(destDirName))
			{
				Directory.CreateDirectory(destDirName);
			}

			// Get the files in the directory and copy them to the new location.
			FileInfo[] files = dir.GetFiles();
			foreach (FileInfo file in files)
			{
				string temppath = Path.Combine(destDirName, file.Name);
				file.CopyTo(temppath, false);
			}

			// If copying subdirectories, copy them and their contents to new location.
			if (copySubDirs)
			{
				foreach (DirectoryInfo subdir in dirs)
				{
					string temppath = Path.Combine(destDirName, subdir.Name);
					DirectoryCopy(subdir.FullName, temppath, copySubDirs);
				}
			}
		}


	}

}