﻿/****************************************************************************************************************************************
 * © 2016 Daqri International. All Rights Reserved.                                                                                    	*
 *                                                                                                                                     	*
 *     NOTICE:  All software code and related information contained herein is, and remains the property of DAQRI INTERNATIONAL and its 	*
 * suppliers, if any.  The intellectual and technical concepts contained herein are proprietary to DAQRI INTERNATIONAL and its         	*
 * suppliers and may be covered by U.S. and Foreign Patents, patents in process, and/or trade secret law, and the expression of			*
 * those concepts is protected by copyright law. Dissemination, reproduction, modification, public display, reverse engineering, or 	*
 * decompiling of this material is strictly forbidden unless prior written permission is obtained from DAQRI INTERNATIONAL.            	*
 *																																		*
 *																																		*
 *																																		*
 *     Created by:        	Jan Bajana																								*                                
 *     Created on:        	Sep 14 2016																								*                
 *     File Purpose:		Class to drive all experience																				*                                                                                    
 *																																		*
 *     Guide:               <TBD>																										*
 *																																		*
 ****************************************************************************************************************************************/

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace DAQRI.Tools
{

	public class TransformerBehaviour : MonoBehaviour {

		// Control speed of leerp
		public float _speed = 10f;

		// Controls pan speed.
		public float _panMultiplier = 3f;

		private float _localDeltaPan;
		private float _localDeltaRotate;
		private float _localDeltaScale;

		private Vector3 _localPositionToGo, _localScaleToGo;
		private Vector3 _lastLocalPosition, _lastLocalScale;

		private Quaternion _localRotationToGo;
		private Quaternion _lastLocalRotation;

		// Use this for initialization
		void Start () {
			SetDefaults ();
		}
		
		// Update is called once per frame
		void Update () {

			float fraction = _speed * Time.deltaTime;

			//Debug.Log ("GestureControlTest: PanGesture: " + fraction);

			// changed by someone else
			if (!Mathf.Approximately(transform.localPosition.x, _lastLocalPosition.x))
				_localPositionToGo.x = transform.localPosition.x;
			if (!Mathf.Approximately(transform.localPosition.y, _lastLocalPosition.y))
				_localPositionToGo.y = transform.localPosition.y;
			if (!Mathf.Approximately(transform.localPosition.z, _lastLocalPosition.z))
				_localPositionToGo.z = transform.localPosition.z;
			transform.localPosition = _lastLocalPosition = Vector3.Lerp(transform.localPosition, _localPositionToGo, fraction);

			// changed by someone else
			if (!Mathf.Approximately(transform.localScale.x, _lastLocalScale.x))
				_localScaleToGo.x = transform.localScale.x;
			if (!Mathf.Approximately(transform.localScale.y, _lastLocalScale.y))
				_localScaleToGo.y = transform.localScale.y;
			if (!Mathf.Approximately(transform.localScale.z, _lastLocalScale.z))
				_localScaleToGo.z = transform.localScale.z;
			transform.localScale = _lastLocalScale = Vector3.Lerp(transform.localScale, _localScaleToGo, fraction);

			// changed by someone else

			if(transform.localRotation != _localRotationToGo)
			{
				if (transform.localRotation != _lastLocalRotation) {
					_localRotationToGo = transform.localRotation;
				}
				transform.localRotation = _lastLocalRotation = Quaternion.Lerp(transform.localRotation, _localRotationToGo, fraction);
			}
				
			//transform.Rotate(gesture.RotationAxis * (fraction * gesture.LocalDeltaRotation), Space.World);

		}

		private void SetDefaults()
		{

			_localPositionToGo = _lastLocalPosition = transform.localPosition;
			_localRotationToGo = _lastLocalRotation = transform.localRotation;
			_localScaleToGo = _lastLocalScale = transform.localScale;

		}

		public void GesturePanHandler(object sender, EventArgs e)
		{
			
		}

		public void GestureScaleHandler(object sender, EventArgs e)
		{
			var gesture = (SimpleGesture)sender;

			if (Math.Abs(gesture.LocalDeltaScale - 1) > 0.00001)
			{
				_localScaleToGo *= gesture.LocalDeltaScale;
			}
		}

		public void GestureRotateHandler(object sender, EventArgs e)
		{
			//Debug.Log ("GestureControlTest: Rotate delta: " + gesture.LocalDeltaRotation + ", axis: " + gesture.RotationAxis);

			var gesture = (SimpleGesture)sender;
			Vector3 projectionPlane = gesture.RotationAxis;
			Space tmpSpace = Space.World;

			if (gesture.Projection == SimpleGesture.ProjectionSpace.Local) {
				projectionPlane = transform.up;
				tmpSpace = Space.Self;
			} else	if (gesture.Projection == SimpleGesture.ProjectionSpace.Camera) {
				projectionPlane = Camera.main.transform.right;
				tmpSpace = Space.Self;
			}
				
			if (!Mathf.Approximately (gesture.LocalDeltaRotation, 0f)) {
				//_localRotationToGo = Quaternion.AngleAxis (gesture.LocalDeltaRotation, projectionPlane) * _localRotationToGo;
			}

			transform.Rotate(gesture.RotationAxis * (4f * _speed * Time.deltaTime * gesture.LocalDeltaRotation), tmpSpace);


		}


	}

}