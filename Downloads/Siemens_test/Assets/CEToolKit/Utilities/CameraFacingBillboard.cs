﻿/****************************************************************************************************************************************
 * © 2016 Daqri International. All Rights Reserved.                                                                                    	*
 *                                                                                                                                     	*
 *     NOTICE:  All software code and related information contained herein is, and remains the property of DAQRI INTERNATIONAL and its 	*
 * suppliers, if any.  The intellectual and technical concepts contained herein are proprietary to DAQRI INTERNATIONAL and its         	*
 * suppliers and may be covered by U.S. and Foreign Patents, patents in process, and/or trade secret law, and the expression of			*
 * those concepts is protected by copyright law. Dissemination, reproduction, modification, public display, reverse engineering, or 	*
 * decompiling of this material is strictly forbidden unless prior written permission is obtained from DAQRI INTERNATIONAL.            	*
 *																																		*
 *																																		*
 *																																		*
 *     Created by:        	Jan Bajana																				*                                
 *     Created on:        	Sep 15 2016																									*                
 *     File Purpose:		Class to drive all experience																				*                                                                                    
 *																																		*
 *     Guide:               <TBD>																										*
 *																																		*
 ****************************************************************************************************************************************/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace DAQRI.Tools
{
	
	public class CameraFacingBillboard : MonoBehaviour {

		private Camera _camera;
		public List<Transform> _targets;

		// Update is called once per frame
		void LateUpdate () {

			if (_camera == null) {
				_camera = Camera.main;
			}

			if(_targets.Count == 0){
				transform.LookAt (transform.position + _camera.transform.rotation * Vector3.forward,
					_camera.transform.rotation * Vector3.up);
			}

			foreach (Transform tragetTransform in _targets) {
				if (tragetTransform != null) {
					tragetTransform.LookAt (tragetTransform.position + _camera.transform.rotation * Vector3.forward,
						_camera.transform.rotation * Vector3.up);
				}
			}
				
		}
	}

}