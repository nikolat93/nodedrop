﻿/****************************************************************************************************************************************
 * © 2016 Daqri International. All Rights Reserved.                                                                                    	*
 *                                                                                                                                     	*
 *     NOTICE:  All software code and related information contained herein is, and remains the property of DAQRI INTERNATIONAL and its 	*
 * suppliers, if any.  The intellectual and technical concepts contained herein are proprietary to DAQRI INTERNATIONAL and its         	*
 * suppliers and may be covered by U.S. and Foreign Patents, patents in process, and/or trade secret law, and the expression of			*
 * those concepts is protected by copyright law. Dissemination, reproduction, modification, public display, reverse engineering, or 	*
 * decompiling of this material is strictly forbidden unless prior written permission is obtained from DAQRI INTERNATIONAL.            	*
 *																																		*
 *																																		*
 *																																		*
 *     Created by:        	Jan Bajana																								*                                
 *     Created on:        	Sep 14 2016																								*                
 *     File Purpose:		Class to drive all experience																				*                                                                                    
 *																																		*
 *     Guide:               <TBD>																										*
 *																																		*
 ****************************************************************************************************************************************/

using UnityEngine;
using System.Collections;

namespace DAQRI.Tools
{
	
	public class SimpleGesture : MonoBehaviour {

		public enum ProjectionSpace
		{
			Camera,
			Local,
			Global,
		}

		[SerializeField]
		private ProjectionSpace _projection = ProjectionSpace.Global;

		[SerializeField]
		private Vector3 _projectionNormal = new Vector3 (0f, 1f, 0f);

		private float _deltaPosition = 0f;
		private float _deltaRotation = 0f;
		private float _deltaScale = 0f;

		private TransformerBehaviour _transformerBehaviour;

		//properties
		public ProjectionSpace Projection
		{
			get { return _projection; }
			set
			{
				if (_projection == value) return;
				_projection = value;
			}
		}

		public float LocalDeltaPosition {
			get { return _deltaPosition; }
			private set { _deltaPosition = value; }
		}

		public float LocalDeltaRotation {
			get { return _deltaRotation; }
			private set { _deltaRotation = value; }
		}

		public float LocalDeltaScale {
			get { return _deltaScale; }
			private set { _deltaScale = value; }
		}
			
		public Vector3 RotationAxis {
			get { return _projectionNormal; }
			set { _projectionNormal = value; }
		}

		// Use this for initialization
		void Start () {
		
			if (GetComponent<TransformerBehaviour> () != null) {
				_transformerBehaviour = GetComponent<TransformerBehaviour> ();
			}

		}

		public void Rotate(float delta)
		{
			_deltaRotation = delta;
			_projection = ProjectionSpace.Local;
			_projectionNormal = new Vector3 (0f, 1f, 0f);
			_transformerBehaviour.GestureRotateHandler (this, null);
		}

		public void Tumble(float delta)
		{
			_deltaRotation = delta;
			_projection = ProjectionSpace.Local;
			_projectionNormal = new Vector3 (1f, 0f, 0f);
			_transformerBehaviour.GestureRotateHandler (this, null);
		}

		public void Zoom(float delta)
		{
			_deltaScale = delta;
			_transformerBehaviour.GestureScaleHandler (this, null);
		}

	}

}