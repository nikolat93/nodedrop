(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['homepage.hbs'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, stack2, functionType="function", escapeExpression=this.escapeExpression, self=this, helperMissing=helpers.helperMissing;

function program1(depth0,data,depth1) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "\n          <span class=\"orgcon\" ";
  stack1 = helpers.unless.call(depth0, depth1.manyOrgs, {hash:{},inverse:self.noop,fn:self.program(2, program2, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += ">\n            <span class=\"dorg dorg_";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.sanitize_id || depth0.sanitize_id),stack1 ? stack1.call(depth0, depth0.id, options) : helperMissing.call(depth0, "sanitize_id", depth0.id, options)))
    + "\">\n              ";
  stack2 = helpers['if'].call(depth0, depth1.manyOrgs, {hash:{},inverse:self.noop,fn:self.program(4, program4, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n              <span class=\"orglogo\">";
  stack2 = helpers['if'].call(depth0, depth0.icon, {hash:{},inverse:self.program(8, program8, data),fn:self.program(6, program6, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "</span>\n              <span class=\"orgname\">";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.truncateOrg || depth0.truncateOrg),stack1 ? stack1.call(depth0, depth0.name, options) : helperMissing.call(depth0, "truncateOrg", depth0.name, options)))
    + "</span>\n            </span>\n          </span>\n        ";
  return buffer;
  }
function program2(depth0,data) {
  
  
  return "id=\"single_org\"";
  }

function program4(depth0,data) {
  
  
  return "\n              <i class=\"icon-caret-down\"></i>\n              ";
  }

function program6(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "<img src=\"";
  if (stack1 = helpers.icon) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.icon; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" alt=\"";
  if (stack1 = helpers.name) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.name; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\">";
  return buffer;
  }

function program8(depth0,data) {
  
  
  return "<i class=\"icon-globe\"></i>";
  }

function program10(depth0,data) {
  
  var buffer = "", stack1, options;
  buffer += "\n          <li class=\"gotoOrg\" data-org_id=\"";
  if (stack1 = helpers.id) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.id; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\">\n            <span class=\"orglogo\">";
  stack1 = helpers['if'].call(depth0, depth0.icon, {hash:{},inverse:self.program(8, program8, data),fn:self.program(6, program6, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</span>\n            <span class=\"orgname\">";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.truncateOrg || depth0.truncateOrg),stack1 ? stack1.call(depth0, depth0.name, options) : helperMissing.call(depth0, "truncateOrg", depth0.name, options)))
    + "</span>\n          </li>\n        ";
  return buffer;
  }

function program12(depth0,data) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "\n      <div class=\"dorg dorg_";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.sanitize_id || depth0.sanitize_id),stack1 ? stack1.call(depth0, depth0.id, options) : helperMissing.call(depth0, "sanitize_id", depth0.id, options)))
    + "\">\n        <ul class=\"item_ul item_ul_app\">\n          ";
  stack2 = helpers.each.call(depth0, depth0.subscriptions, {hash:{},inverse:self.noop,fn:self.program(13, program13, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n        </ul>\n        <div style=\"clear: both\"></div>\n      </div>\n      ";
  return buffer;
  }
function program13(depth0,data) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "\n          <li class=\"li_app\">\n            <div class=\"app\">\n              <a href=\"";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.fixUrl || depth0.fixUrl),stack1 ? stack1.call(depth0, depth0.url, options) : helperMissing.call(depth0, "fixUrl", depth0.url, options)))
    + "\">\n                ";
  stack2 = helpers['if'].call(depth0, depth0.bookmarks, {hash:{},inverse:self.noop,fn:self.program(14, program14, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n                <span class=\"applogo\"><img src=\"";
  if (stack2 = helpers.icon) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.icon; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\" alt=\"";
  if (stack2 = helpers.name) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.name; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\"></span>\n                <span class=\"forfilter\">";
  if (stack2 = helpers.name) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.name; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "</span>\n              </a>\n            </div>\n            ";
  stack2 = helpers['if'].call(depth0, depth0.bookmarks, {hash:{},inverse:self.noop,fn:self.program(16, program16, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n          </li>\n          ";
  return buffer;
  }
function program14(depth0,data) {
  
  var buffer = "", stack1, options;
  buffer += "\n                <span class=\"appbm\">"
    + escapeExpression(((stack1 = ((stack1 = depth0.bookmarks),stack1 == null || stack1 === false ? stack1 : stack1.length)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + " bookmark";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.pluralize || depth0.pluralize),stack1 ? stack1.call(depth0, ((stack1 = depth0.bookmarks),stack1 == null || stack1 === false ? stack1 : stack1.length), options) : helperMissing.call(depth0, "pluralize", ((stack1 = depth0.bookmarks),stack1 == null || stack1 === false ? stack1 : stack1.length), options)))
    + "</span>\n                ";
  return buffer;
  }

function program16(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\n            <div class=\"bm\">\n              <ul>\n                ";
  stack1 = helpers.each.call(depth0, depth0.bookmarks, {hash:{},inverse:self.noop,fn:self.program(17, program17, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n              </ul>\n            </div>\n            ";
  return buffer;
  }
function program17(depth0,data) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "\n                <li class=\"li_bm\">\n                  <a href=\"";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.fixUrl || depth0.fixUrl),stack1 ? stack1.call(depth0, depth0.url, options) : helperMissing.call(depth0, "fixUrl", depth0.url, options)))
    + "\"><i class=\"icon-star\"></i> <span class=\"forfilter\">";
  if (stack2 = helpers.name) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.name; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "</span></a>\n                </li>\n                ";
  return buffer;
  }

  buffer += "<div class=\"homepage_contentwrap\">\n  <div class=\"homepage_head\">\n    <span class=\"closer\"><i class=\"icon-remove-sign\"></i></span>\n    <h1><a href=\"";
  stack2 = ((stack1 = ((stack1 = depth0.settings),stack1 == null || stack1 === false ? stack1 : stack1.home)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1);
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\">";
  stack2 = ((stack1 = ((stack1 = depth0.settings),stack1 == null || stack1 === false ? stack1 : stack1.name)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1);
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "</a></h1>\n    <p>This new tab screen from Bitium makes it easy to access your Apps.  Click on any App to log in quickly and securely with Bitium.</p>\n  </div>\n  <div class=\"homepage_topbar\">\n    <span class=\"whatsthis\">What's this?</span>\n    <span class=\"defaultpagetoggle\">Want the default page back?</span>\n  </div>\n  <div class=\"homepage_wrap\">\n    <div class=\"searchbar\">\n      <div class=\"orglist\">\n        ";
  stack2 = helpers.each.call(depth0, depth0.orgs, {hash:{},inverse:self.noop,fn:self.programWithDepth(1, program1, data, depth0),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n        <ul>\n        ";
  stack2 = helpers.each.call(depth0, depth0.orgs, {hash:{},inverse:self.noop,fn:self.program(10, program10, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n        </ul>\n      </div>\n      <form action=\"/\" method=\"get\" class=\"hide_org_list\">\n        <input type=\"text\" name=\"search\" id=\"search\" placeholder=\"Search Apps or Bookmarks\" />\n      </form>\n    </div>\n    <div class=\"apps\">\n      ";
  stack2 = helpers.each.call(depth0, depth0.orgs, {hash:{},inverse:self.noop,fn:self.program(12, program12, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n    </div>\n  </div>\n</div>\n<footer>\n  <a href=\"";
  stack2 = ((stack1 = ((stack1 = depth0.settings),stack1 == null || stack1 === false ? stack1 : stack1.home)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1);
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\">";
  stack2 = ((stack1 = ((stack1 = depth0.settings),stack1 == null || stack1 === false ? stack1 : stack1.name)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1);
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "</a>\n</footer>";
  return buffer;
  });
})();