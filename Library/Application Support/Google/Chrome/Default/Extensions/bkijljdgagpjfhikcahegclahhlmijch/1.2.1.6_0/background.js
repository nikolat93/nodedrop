// Generated by CoffeeScript 1.7.1

/*
 *
 * This file contains all of the chrome listeners for processing various events
 *
 */

(function() {
  trackPageView("background.html");

  getBrowser().runtime.onMessage.addListener(function(request, sender, nativeSendResponse) {
    var sendResponse;
    sendResponse = function(obj) {
      var allowedFromAnywhere, fromPlugin, hasObj, hasResponseURL, responseUrl, senderUrl;
      fromPlugin = /^(chrome|moz)-extension:\/\/.*/.test(sender.url);
      hasObj = !!obj;
      hasResponseURL = hasObj && obj.hasOwnProperty('response') && obj.response.hasOwnProperty('url');
      allowedFromAnywhere = ['bitium:plugin:clipboard:copy', 'bitium:plugin:cookie', 'bitium:plugin:fetch:subscriptions', 'bitium:plugin:formfiller:fill_in', 'bitium:plugin:formfiller:get:itemsforurl', 'bitium:plugin:formfiller:user'];
      if (fromPlugin || !hasObj || allowedFromAnywhere.includes(request.type)) {
        return nativeSendResponse(obj);
      }
      if (hasObj && !hasResponseURL) {
        console.log("" + request.type + " response doesn't include a URL");
        return;
      }
      senderUrl = new URL(sender.url);
      responseUrl = new URL(obj.response.url);
      if (senderUrl.hostname !== responseUrl.hostname) {
        console.log("" + request.type + " response URL doesn't match request URL");
        return;
      }
      return nativeSendResponse(obj);
    };
    switch (request.type) {
      case "bitium:plugin:cookie":
        verify(request) && cookie(request, sendResponse, sender);
        break;
      case "bitium:plugin:subscription":
        verify({
          payload: request.data.subscriptions
        }) && store(request.data, sendResponse, sender);
        break;
      case "bitium:plugin:fetch:subscriptions":
        fetch_subscriptions(request, sendResponse, sender);
        break;
      case "bitium:plugin:fetch:organizations":
        fetch_organizations(request.data, sendResponse, sender);
        break;
      case "bitium:plugin:fetch:password":
        fetch_password(request, sendResponse, sender);
        break;
      case "bitium:plugin:validate:password":
        validate_password(request, sendResponse, sender);
        break;
      case "bitium:plugin:auth":
        auth(request, sendResponse, sender);
        break;
      case "bitium:plugin:check":
        check_auth(request, sendResponse, sender);
        break;
      case "bitium:plugin:open":
        open_new_tab(request, sendResponse, sender);
        break;
      case "bitium:plugin:close:self":
        tabsClose(sender, sendResponse);
        break;
      case "bitium:plugin:auth:complete":
        auth_complete(request, sendResponse, sender);
        break;
      case "bitium:plugin:auth:error":
        auth_error(request, sendResponse, sender);
        break;
      case "bitium:auth:error:debug":
        auth_error_complete(request, sendResponse, sender);
        break;
      case "bitium:plugin:auth:move:tab":
        auth_tab_move(request, sendResponse, sender);
        break;
      case "bitium:auth:state:next:step":
        scriptManager(request, sendResponse, sender);
        break;
      case "bitium:auth:state:page:unload":
        scriptManager(request, sendResponse, sender);
        break;
      case "bitium:auth:state:page:load":
        scriptManager(request, sendResponse, sender);
        break;
      case "bitium:auth:state:complete":
        scriptManager(request, sendResponse, sender);
        break;
      case "bitium:plugin:subscription:list":
        list_subscriptions(request, sendResponse, sender);
        break;
      case "bitium:plugin:subscription:details":
        subscription_info(request, sendResponse);
        break;
      case "bitium:plugin:debug":
        console.log(request.message);
        break;
      case "bitium:plugin:api":
        api(request, sendResponse, sender);
        break;
      case "bitium:plugin:login":
        bitiumAuthenticationLogin(request, sendResponse, sender);
        break;
      case "bitium:plugin:logout":
        logoutPlugin(request, sendResponse, sender);
        break;
      case "bitium:plugin:notify":
        showNotification(request, sendResponse, sender);
        break;
      case "bitium:plugin:launch_timeout":
        removeBackgroundWindow(request, sendResponse, sender);
        break;
      case "bitium:plugin:set:current:org":
        setCurrentOrg(request.data, sendResponse, sender);
        break;
      case "bitium:plugin:store:set":
        localStorageSet(request.data, sendResponse);
        break;
      case "bitium:plugin:store:get":
        localStorageGet(request.data, sendResponse);
        break;
      case "bitium:plugin:clipboard:copy":
        copyToClipboard(request, sendResponse);
        break;
      case "bitium:plugin:formfiller:authtab:store":
        storeAuthTab(request, sendResponse, sender);
        break;
      case "bitium:plugin:formfiller:authtab:post":
        postAuthTab(request, sendResponse, sender);
        break;
      case "bitium:plugin:formfiller:authtab:close":
        closeAuthTab(request, sendResponse, sender);
        break;
      case "bitium:plugin:formfiller:authtab:blacklist":
        blacklistAuthTab(request, sendResponse, sender);
        break;
      case "bitium:plugin:formfiller:detection:continue":
        form_filler_capture_listener_continue(request, sendResponse, sender);
        break;
      case "bitium:plugin:formfiller:list:rules":
        list_formfiller_rules(request, sendResponse, sender);
        break;
      case "bitium:plugin:formfiller:data":
        form_filler_data(request, sendResponse, sender);
        break;
      case "bitium:plugin:formfiller:user":
        form_filler_user(request, sendResponse, sender);
        break;
      case "bitium:plugin:formfiller:get:itemsforurl":
        items_for_url(request, sendResponse, sender);
        break;
      case "bitium:plugin:formfiller:authenticate":
        authenticate(request, sendResponse, sender);
        break;
      case "bitium:plugin:formfiller:fill_in":
        fetch_fill_in_form(request, sendResponse, sender);
        break;
      case "bitium:plugin:track:pageview":
        trackPageView(request.data.pageName);
        break;
      case "bitium:plugin:track:event":
        trackEvent(request.data.eventCategory, request.data.eventAction);
        break;
      case "bitium:plugin:track:audit_log":
        trackEventBitium(request.data);
    }
    return true;
  });

  getBrowser().webRequest.onBeforeSendHeaders.addListener(function(details) {
    var all, browserName, pluginVersoin;
    all = details.requestHeaders;
    browserName = getBrowserData().name;
    pluginVersoin = getBrowser().runtime.getManifest().version;
    all.push({
      name: 'X-Bitium-Extension',
      value: "" + browserName + ":" + pluginVersoin
    });
    all.push({
      name: 'X-Bitium-App-Protocol',
      value: '2.0'
    });
    return {
      requestHeaders: all
    };
  }, {
    urls: Settings.home_domains,
    types: ["main_frame", "xmlhttprequest"]
  }, ["blocking", "requestHeaders"]);

  getBrowser().webRequest.onHeadersReceived.addListener(function(info) {
    var header, headers, itr, _i, _len;
    headers = info.responseHeaders;
    for (_i = 0, _len = headers.length; _i < _len; _i++) {
      itr = headers[_i];
      if (itr && itr.name) {
        header = itr.name.toLowerCase();
        console.log("header: " + header);
        if (header === 'x-frame-options' || header === 'frame-options') {
          console.log("matched!");
          headers.splice(headers.indexOf(itr), 1);
        }
      }
    }
    return {
      responseHeaders: headers
    };
  }, {
    urls: Settings.iframe_whitelist,
    types: ['sub_frame']
  }, ['blocking', 'responseHeaders']);

  subscriptions(function(stored) {
    return subscriptionListener(stored);
  });

  webRequestOnCompletedRemoveListener(auto_login_listener);

  webRequestOnCompletedAddListener(form_filler_capture_listener, {
    urls: ["http://*/*", "https://*/*"],
    types: ["main_frame"]
  });

  getBrowser().runtime.onInstalled.addListener(function(details) {
    console.log("extension on installed");
    if (details.reason === 'update') {
      console.log("previus version: " + details.previousVersion);
      return console.log("current version: " + getBrowser().runtime.getManifest().version);
    }
  });

}).call(this);
