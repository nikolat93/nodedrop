(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['options.hbs'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression;


  buffer += "<form class=\"options\">\n  <h1>"
    + escapeExpression(((stack1 = ((stack1 = depth0.settings),stack1 == null || stack1 === false ? stack1 : stack1.name)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + " Browser Extension Options</h1>\n  <div class=\"success\"></div>\n  <div class=\"field\">\n    <div class=\"lbl\">Secure Auto-Fill</div>\n    <p>When enabled, Secure Auto-Fill will recognize when you land on a login page of an app that you have added to "
    + escapeExpression(((stack1 = ((stack1 = depth0.settings),stack1 == null || stack1 === false ? stack1 : stack1.name)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + ". You will be able to log into a selected installation of the app without manually entering your username and password.</p>\n    <label><input type=\"checkbox\" name=\"form_filler\" value=\"1\" id=\"form_filler_on\"> Secure Auto-Fill</label><br><br>\n    <p>Enable the “Show Automatically” feature to have your Auto-Fill options displayed when you \n    land on a login page. This eliminates the need to click on the <img src=\"images/ic_16.png\"/> icon to show your accounts.</p>\n    <label><input type=\"checkbox\" name=\"form_filler_auto_show\" value=\"0\" id=\"form_filler_auto_show\"> Show Automatically</label><br><br>\n    <p>Enable “Capture” if you want to save credentials directly from websites into "
    + escapeExpression(((stack1 = ((stack1 = depth0.settings),stack1 == null || stack1 === false ? stack1 : stack1.name)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + ".</p>\n    <label><input type=\"checkbox\" name=\"form_filler_capture\" value=\"0\" id=\"form_filler_capture\"> Capture [Beta]</label><br>\n  </div>\n\n  <a href=\"#\" id=\"save\" class=\"btn\">Save Settings</a>\n</form>";
  return buffer;
  });
})();