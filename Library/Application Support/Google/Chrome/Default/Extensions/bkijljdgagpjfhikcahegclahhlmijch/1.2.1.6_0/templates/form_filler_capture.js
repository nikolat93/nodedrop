(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['form_filler_capture.hbs'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\n						<option value=\"";
  if (stack1 = helpers.id) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.id; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\" ";
  stack1 = helpers['if'].call(depth0, depth0.current, {hash:{},inverse:self.noop,fn:self.program(2, program2, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += " >";
  if (stack1 = helpers.name) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.name; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</option>\n					";
  return buffer;
  }
function program2(depth0,data) {
  
  
  return "selected";
  }

  buffer += "<div id=\"";
  if (stack1 = helpers.popup_id) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.popup_id; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\" class=\"bitium-ffc\">\n	<div class=\"ffc-head\">\n		<div class=\"ffc-logo\"></div>\n		<div class=\"ffc-close\"></div>\n	</div>\n	<div class=\"ffc-close-prompt\">\n		<div class=\"ffc-block-prompt\">\n			Don't ask me again\n		</div>\n		<div class=\"ffc-btn-close\">\n			Close\n		</div>\n	</div>\n	<div class=\"ffc-prompt\">\n		<div class=\"ffc-prompt-text\">\n			Would you like to add <span class=\"ffc-app-name\">";
  if (stack1 = helpers.appName) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.appName; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</span>?\n		</div>\n	</div>\n	<div class=\"ffc-notification ffc-notification-success\">\n		<div class=\"ffc-prompt-text\">\n			<span class=\"ffc-app-name\">";
  if (stack1 = helpers.appName) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.appName; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</span> was added succesfully.\n		</div>\n	</div>\n	<div class=\"ffc-notification ffc-notification-error\">\n		<div class=\"ffc-prompt-text\">\n			There was an error adding <span class=\"ffc-app-name\">";
  if (stack1 = helpers.appName) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.appName; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</span>. Please try again later or contact support.\n		</div>\n	</div>\n	<div class=\"ffc-details-prompt\">\n		<div class=\"ffc-section ffc-application-type\">\n			<div class=\"ffc-title\">\n				Type\n			</div>\n			<div class=\"ffc-content ffc-content-buttons\">\n				<div class=\"ffc-small-button ffc-btn-company ffc-selected\">\n					Company\n				</div>\n				<div class=\"ffc-small-button ffc-btn-personal\">\n					Personal\n				</div>\n			</div>\n		</div>\n		<div class=\"ffc-section ffc-org\">\n			<div class=\"ffc-title\">\n				Organization\n			</div>\n			<div class=\"ffc-content\">\n				<select id=\"ffc-org-select\">\n					";
  stack1 = helpers.each.call(depth0, depth0.orgs, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n				</select>\n			</div>\n		</div>\n		<div class=\"ffc-section ffc-display-name\">\n			<div class=\"ffc-title\">\n				Name\n			</div>\n			<div class=\"ffc-content\">\n				<input id=\"ffc-app-display-name\" placeHolder=\"";
  if (stack1 = helpers.appName) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.appName; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\"></input>\n			</div>\n		</div>\n	</div>\n	<div class=\"ffc-buttons ffc-save-btns\">\n		<div class=\"ffc-btn ffc-save-subscription ffc-btn-pos\"><span class=\"ffc-default\">Yes, add it!</span><span class=\"ffc-loading\">Saving...</span></div>\n		<div class=\"ffc-btn ffc-btn-neg\">Not now</div>\n		<div class=\"ffc-btn ffc-btn-neg ffc-notification-close\">Close</div>\n	</div>\n</div>";
  return buffer;
  });
})();