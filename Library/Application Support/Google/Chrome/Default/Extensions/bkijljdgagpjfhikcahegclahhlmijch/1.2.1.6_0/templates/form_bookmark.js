(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['form_bookmark.hbs'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression;


  buffer += "<div class=\"formwrap\">\n  <form action=\"/api/v2/organizations/";
  if (stack1 = helpers.organization) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.organization; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "/subscriptions/";
  if (stack1 = helpers.subscription) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.subscription; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "/bookmarks.json\" method=\"post\" data-token=\"";
  if (stack1 = helpers.token) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.token; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\">\n    <h3>Bookmark this page</h3>\n    <ol>\n      <li>\n        <input type=\"text\" name=\"name\" id=\"bookmark_name\" value=\"";
  if (stack1 = helpers.title) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.title; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\"/>\n      </li>\n      <li>\n        <input type=\"text\"   name=\"url\"  id=\"bookmark_url\"  value=\"";
  if (stack1 = helpers.url) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.url; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\" disabled=\"disabled\" />\n        <input type=\"hidden\" name=\"url\" id=\"bookmark_path\" value=\"";
  if (stack1 = helpers.url) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.url; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\" />\n      </li>\n      <li>\n        <input type=\"hidden\" name=\"default_page\" value=\"0\" />\n        <label><input type=\"checkbox\" name=\"default_page\" id=\"bookmark_default_page\" value=\"1\" /> Make default page on launch</label>\n      </li>\n    </ol>\n    <div class=\"actions\">\n      <input type=\"submit\" class=\"btn btn-green\" value=\"Done\" />\n      <input type=\"reset\" class=\"btn gotoAddBm\" value=\"Cancel\" />\n    </div>\n  </form>\n</div>";
  return buffer;
  });
})();