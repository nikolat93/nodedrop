// Generated by CoffeeScript 1.7.1
(function() {
  window.api = function(params, callback) {
    return getBrowser().runtime.sendMessage({
      type: 'bitium:plugin:api',
      params: params
    }, callback);
  };

}).call(this);
