(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['debug.hbs'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression;


  buffer += "<form class=\"options\">\n  <h1>"
    + escapeExpression(((stack1 = ((stack1 = depth0.settings),stack1 == null || stack1 === false ? stack1 : stack1.name)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + " Browser Extension Debug Options</h1>\n  <div class=\"success\"></div>\n  <div class=\"field\">\n    <div class=\"lbl\">Debug Mode</div>\n    <p>Enables debugging output for the "
    + escapeExpression(((stack1 = ((stack1 = depth0.settings),stack1 == null || stack1 === false ? stack1 : stack1.name)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + " extension</p>\n    <label><input type=\"checkbox\" name=\"debug\" value=\"1\" id=\"debug_on\"> Enable Debug</label>\n  </div>\n  <div class=\"field\">\n    <div class=\"lbl\">Debug Form Filler</div>\n    <p>Turns on debug functions for form filler</p>\n    <label><input type=\"checkbox\" name=\"debug_form_filler\" value=\"1\" id=\"debug_form_filler_on\"> Enable Form Filler Debug</label>\n  </div>\n\n  \n  <a href=\"#\" id=\"save\" class=\"btn\">Save Settings</a>\n</form>";
  return buffer;
  });
})();