(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['auto_login_iframe.hbs'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, stack2, functionType="function", escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  
  return "\n  <div class=\"pagination off\" id=\"prev\"><a href=\"#\"><i class=\"icon-chevron-sign-left icon-2x\"></i></a></div>\n  ";
  }

function program3(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\n    <ul class=\"clearfix";
  stack1 = helpers['if'].call(depth0, depth0.listview, {hash:{},inverse:self.program(6, program6, data),fn:self.program(4, program4, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  stack1 = helpers['if'].call(depth0, depth0.first_page, {hash:{},inverse:self.noop,fn:self.program(8, program8, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\">\n      ";
  stack1 = helpers.each.call(depth0, depth0.subscriptions, {hash:{},inverse:self.noop,fn:self.programWithDepth(10, program10, data, depth0),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n    </ul>\n  ";
  return buffer;
  }
function program4(depth0,data) {
  
  
  return " list";
  }

function program6(depth0,data) {
  
  
  return " icon";
  }

function program8(depth0,data) {
  
  
  return " active";
  }

function program10(depth0,data,depth1) {
  
  var buffer = "", stack1;
  buffer += "\n      <li><a class=\"applink\" href=\"";
  if (stack1 = helpers.url) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.url; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\" target=\"_top\">\n        ";
  stack1 = helpers['if'].call(depth0, depth1.listview, {hash:{},inverse:self.program(13, program13, data),fn:self.program(11, program11, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n      </a></li>\n      ";
  return buffer;
  }
function program11(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\n        <span class=\"applogo\"><img src=\"";
  if (stack1 = helpers.icon) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.icon; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\" alt=\"";
  if (stack1 = helpers.name) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.name; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\"></span><p><span class=\"appname\">";
  if (stack1 = helpers.name) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.name; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</span><span class=\"account\">";
  if (stack1 = helpers.login) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.login; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</span></p>\n        ";
  return buffer;
  }

function program13(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\n        <p><span class=\"applogo\"><img src=\"";
  if (stack1 = helpers.icon_large) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.icon_large; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\" alt=\"";
  if (stack1 = helpers.name) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.name; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\"></span></p><p>";
  if (stack1 = helpers.name) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.name; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</p><p class=\"account\">";
  if (stack1 = helpers.login) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.login; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</p>\n        ";
  return buffer;
  }

function program15(depth0,data) {
  
  
  return "\n  <div class=\"pagination\" id=\"next\"><a href=\"#\"><i class=\"icon-chevron-sign-right icon-2x\"></i></a></div>\n  ";
  }

  buffer += "<style>\n  @font-face {\n    font-family: 'OS';\n    src: url('";
  if (stack1 = helpers.font_path) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.font_path; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "/OpenSans-Regular-webfont.eot');\n    src: url('";
  if (stack1 = helpers.font_path) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.font_path; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "/OpenSans-Regular-webfont.eot?#iefix') format('eot'),\n    url('";
  if (stack1 = helpers.font_path) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.font_path; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "/OpenSans-Regular-webfont.woff') format('woff'),\n    url('";
  if (stack1 = helpers.font_path) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.font_path; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "/OpenSans-Regular-webfont.ttf') format('truetype'),\n    url('";
  if (stack1 = helpers.font_path) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.font_path; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "/OpenSans-Regular-webfont.svg#OpenSans') format('svg');\n    font-weight: normal;\n    font-style: normal;\n  }\n\n\n  body {\n    border: 0;\n    padding: 0;\n    margin: 0;\n    color: #333;\n    font-family: \"OS\", Arial, Sans-serif;\n    font-size: 12px;\n    background-color:#fff;\n  }\n  \n  body > div {\n    background-color:#fff;\n  }\n\n  div.b-center {\n    text-align: center;\n  }\n  \n  h1 {\n    font-family: \"OS\", Arial, Sans-serif;\n    font-size: 25px;\n    margin-top: 65px;\n    margin-bottom: 0;\n  }\n  \n  h2 {\n    font-family: \"OS\", Arial, Sans-serif;\n    font-size: 14px;\n    margin-top: 5px;\n    margin-bottom: 20px;\n    color: #333;\n  }\n    \n  .clearfix:before,.clearfix:after {\n    content: \" \";\n    display: table;\n  }\n    \n  .clearfix:after {\n    clear: both;\n  }\n  \n  a,a:hover,a:active {\n    color: #333;\n    text-decoration: none;\n    font-family: \"OS\", Arial, Sans-serif;\n  }\n  \n  img {\n    border: none;\n  }\n  \n  ul a span.applogo {\n    display: inline-block;\n    padding: 0;\n    margin: 0;\n    border: 1px solid #E1E6EB;\n  }\n  \n  ul.list a span.applogo {\n    -moz-border-radius: 3px;\n    border-radius: 3px;\n    height: 34px;\n    width: 34px;\n    background: white;\n  }\n  \n  ul {\n    list-style-type: none;\n    padding: 0;\n    margin: 0;\n    border: 0;\n    margin-top: 20px;\n  }\n  \n  li {\n    float: left;\n    display: block;\n  }\n  \n  ul.list {\n    display: inline-block;\n    width: 520px;\n  }\n  \n  ul.icon {\n    display: inline-block;\n  }\n  \n  ul.list li {\n    margin: 0;\n    padding: 0;\n    border: 0;    \n  }\n  \n  ul.list li span.applogo {\n    float: left;\n  }\n  \n  ul.list li p {\n    float: left;\n    text-align: left;\n    margin-left: 5px;\n    line-height: 16px;\n  }\n  \n  ul.list li a {\n    display: inline-block;\n    padding: 5px;\n    margin: 0;\n    border: 0;\n    width: 200px;\n    overflow: hidden;\n  }\n  \n  ul.icon li a {\n    display: inline-block;\n    padding: 5px;\n    margin-right: 15px;\n    margin-left: 15px;\n  }\n  \n  ul.list li a:hover {\n    background: #FFFBD5;\n  }\n  \n  ul.icon a span.applogo {\n    -moz-border-radius: 6px;\n    border-radius: 6px;\n    height: 100px;\n    width: 100px;\n    margin-bottom: 5px;\n  }\n  \n  ul.icon p img {\n    margin-bottom: 5px;\n  }\n  \n  div#logo {\n    position: absolute;\n    left: 15px;\n    top: 15px;\n  }\n\n  div#no_thanks {\n    position: absolute;\n    font-family: \"OS\", Arial, Sans-serif;\n    font-size: 12px;\n    bottom: 15px;\n    right: 15px;\n  }\n  \n  div#no_thanks a {\n    color: #333;\n  }\n  \n  p {\n    font-size: 12px;\n    margin: 0;\n    padding: 0;\n    border: 0;\n    line-height: 16px;\n  }\n\n  .appname, .account {\n    display: block;\n    white-space: nowrap;\n    overflow: hidden;\n    text-overflow: ellipsis;\n    max-width: 150px;\n  }\n  \n  .account {\n    font-size: 11px;\n    color: #AAA;\n  }\n  \n  a:hover,div#no_thanks a:hover {\n    color: #111;\n  }\n  \n  a:hover span.account {\n    color: #888;\n  }\n  \n  ul.icon > li > a:hover span.applogo {\n    border-color: #3B6F8D;\n  }\n  \n  ul.icon > li > a:hover p {\n    color: #111;\n  }\n  \n  ul.icon a:hover p.account {\n    color: #888;\n  }\n  \n  ul.list {\n    position: absolute;\n    top: 130px;\n    right: -100%;\n    transition: all 0.2s ease;\n  }\n  \n  ul.list.active {\n    right: 20px;\n  }\n  \n  div.pagination {\n    position: absolute;\n    top: 200px;\n  }\n  \n  div.pagination a {\n    color: #3B6F8D;\n  }\n  \n  div.pagination a:hover,div.pagination a:active {\n    color: #333;\n  }\n  \n  div#prev {\n    left: 45px;\n  }\n  \n  div#next {\n    right: 45px;\n  }\n  \n  div.off {\n    display: none;\n  }\n</style>\n\n<link rel=\"stylesheet\" href=\"";
  if (stack1 = helpers.font_awesome_url) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.font_awesome_url; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "/font-awesome.min.css\">\n\n<div id=\"logo\">\n  <a href=\"";
  stack2 = ((stack1 = ((stack1 = depth0.settings),stack1 == null || stack1 === false ? stack1 : stack1.home)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1);
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\"><img src=\"";
  if (stack2 = helpers.logo_path) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.logo_path; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "/ic_logo_98x28.png\"></a>\n</div>\n\n<div class=\"b-center\">\n  <h1>Hello there. Would you like me to log you in?</h1>\n  <h2>Click the account you want to use</h2>\n \n  ";
  stack2 = helpers['if'].call(depth0, depth0.listview, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n  \n  ";
  stack2 = helpers.each.call(depth0, depth0.pages, {hash:{},inverse:self.noop,fn:self.program(3, program3, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n  \n  ";
  stack2 = helpers['if'].call(depth0, depth0.listview, {hash:{},inverse:self.noop,fn:self.program(15, program15, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n</div>\n\n<div id=\"no_thanks\">\n  <a href=\"#\">skip</a>\n</div>";
  return buffer;
  });
})();