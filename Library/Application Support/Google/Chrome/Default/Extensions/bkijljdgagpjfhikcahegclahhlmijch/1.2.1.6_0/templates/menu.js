(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['menu.hbs'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, stack2, options, functionType="function", escapeExpression=this.escapeExpression, helperMissing=helpers.helperMissing, self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "\n        <li class=\"dorg dorg_";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.sanitize_id || depth0.sanitize_id),stack1 ? stack1.call(depth0, depth0.id, options) : helperMissing.call(depth0, "sanitize_id", depth0.id, options)))
    + "\">\n          <div class=\"icons\">\n            <a href=\"";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.fixUrl || depth0.fixUrl),stack1 ? stack1.call(depth0, depth0.url, options) : helperMissing.call(depth0, "fixUrl", depth0.url, options)))
    + "\" class=\"hbtn icon_dash\" title=\"Go to ";
  if (stack2 = helpers.name) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.name; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "'s Dashboard\">Go to Dashboard</a>\n            <span class=\"hbtn icon_off\" title=\"Logout\">Logout</span>\n          </div>\n          <span class=\"back fl backbtn backbtn-handle\">\n            <span class=\"hbtn\"><span class=\"icon_back\">Back</span></span>\n          </span>\n          <div class=\"org\">\n            <a href=\"";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.fixUrl || depth0.fixUrl),stack1 ? stack1.call(depth0, depth0.url, options) : helperMissing.call(depth0, "fixUrl", depth0.url, options)))
    + "\">\n              <span class=\"orglogo\">";
  stack2 = helpers['if'].call(depth0, depth0.icon, {hash:{},inverse:self.program(4, program4, data),fn:self.program(2, program2, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "</span>\n              ";
  if (stack2 = helpers.name) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.name; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\n            </a>\n          </div>\n        </li>\n        ";
  return buffer;
  }
function program2(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "<img src=\"";
  if (stack1 = helpers.icon) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.icon; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" alt=\"";
  if (stack1 = helpers.name) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.name; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\">";
  return buffer;
  }

function program4(depth0,data) {
  
  
  return "<i class=\"icon-globe\"></i>";
  }

function program6(depth0,data) {
  
  var buffer = "", stack1, options;
  buffer += "\n              <li class=\"item item_org ";
  stack1 = helpers['if'].call(depth0, depth0.trial_expired, {hash:{},inverse:self.noop,fn:self.program(7, program7, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\">\n                <div class=\"org\">\n                  <span class=\"fl gotoOrg\" data-org_id=\"";
  if (stack1 = helpers.id) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.id; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\">\n                    <span class=\"arrow\">\n                      ";
  stack1 = helpers['if'].call(depth0, depth0.trial_expired, {hash:{},inverse:self.program(11, program11, data),fn:self.program(9, program9, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n                    </span>\n                    <span class=\"orglogo\">";
  stack1 = helpers['if'].call(depth0, depth0.icon, {hash:{},inverse:self.program(4, program4, data),fn:self.program(2, program2, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</span>\n                    ";
  if (stack1 = helpers.name) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.name; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + " ("
    + escapeExpression(((stack1 = ((stack1 = depth0.subscriptions),stack1 == null || stack1 === false ? stack1 : stack1.length)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + " App";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.pluralize || depth0.pluralize),stack1 ? stack1.call(depth0, ((stack1 = depth0.subscriptions),stack1 == null || stack1 === false ? stack1 : stack1.length), options) : helperMissing.call(depth0, "pluralize", ((stack1 = depth0.subscriptions),stack1 == null || stack1 === false ? stack1 : stack1.length), options)))
    + ")\n                  </span>\n                </div>\n              </li>\n              ";
  return buffer;
  }
function program7(depth0,data) {
  
  
  return "org_trial_expired";
  }

function program9(depth0,data) {
  
  
  return "\n                        <span class=\"dd_expired\">Trial Expired</span>\n                      ";
  }

function program11(depth0,data) {
  
  
  return "\n                        <i class=\"icon-chevron-right\"></i>\n                      ";
  }

function program13(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\n      <div class=\"credits\"><a href=\"";
  if (stack1 = helpers.bitium_host) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.bitium_host; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "/site/credits/plugin\">Credits</a></div>\n      ";
  return buffer;
  }

function program15(depth0,data) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "\n              <li class=\"dorg dorg_";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.sanitize_id || depth0.sanitize_id),stack1 ? stack1.call(depth0, depth0.id, options) : helperMissing.call(depth0, "sanitize_id", depth0.id, options)))
    + "\">\n                <ul class=\"item_ul item_ul_app\">\n                  ";
  stack2 = helpers.each.call(depth0, depth0.subscriptions, {hash:{},inverse:self.noop,fn:self.programWithDepth(16, program16, data, depth0),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n                </ul>\n              </li>\n              ";
  return buffer;
  }
function program16(depth0,data,depth1) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "\n                  <li class=\"item item_app\">\n                    <div class=\"app\">\n                      ";
  stack1 = helpers['if'].call(depth0, depth0.bookmarks, {hash:{},inverse:self.noop,fn:self.programWithDepth(17, program17, data, depth0, depth1),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n                      <a href=\"";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.fixUrl || depth0.fixUrl),stack1 ? stack1.call(depth0, depth0.launch_url, options) : helperMissing.call(depth0, "fixUrl", depth0.launch_url, options)))
    + "\"><span class=\"applogo\">\n                        <img src=\"";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.iconUrl || depth0.iconUrl),stack1 ? stack1.call(depth0, depth0, options) : helperMissing.call(depth0, "iconUrl", depth0, options)))
    + "\" data-ltr-size=\"34\" alt=\"";
  if (stack2 = helpers.name) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.name; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\">\n                      </span> <div class=\"app-name\">";
  if (stack2 = helpers.name) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.name; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + " <span class=\"app-login\">(";
  if (stack2 = helpers.login) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.login; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + ")</span></div></a>\n                    </div>\n                  </li>\n                  ";
  return buffer;
  }
function program17(depth0,data,depth1,depth2) {
  
  var buffer = "", stack1, options;
  buffer += "\n                      <span class=\"arrow gotoBookmarks\" data-sub_id=\""
    + escapeExpression(((stack1 = depth1.id),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\" data-org_id=\""
    + escapeExpression(((stack1 = depth2.id),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\">\n                        <span class=\"bookmarks_count_text\">"
    + escapeExpression(((stack1 = ((stack1 = depth0.bookmarks),stack1 == null || stack1 === false ? stack1 : stack1.length)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + " bookmark";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.pluralize || depth0.pluralize),stack1 ? stack1.call(depth0, ((stack1 = depth0.bookmarks),stack1 == null || stack1 === false ? stack1 : stack1.length), options) : helperMissing.call(depth0, "pluralize", ((stack1 = depth0.bookmarks),stack1 == null || stack1 === false ? stack1 : stack1.length), options)))
    + "</span>\n                        <i class=\"icon-chevron-right\"></i>\n                      </span>\n                      ";
  return buffer;
  }

function program19(depth0,data) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "\n              <li class=\"dorg dorg_";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.sanitize_id || depth0.sanitize_id),stack1 ? stack1.call(depth0, depth0.id, options) : helperMissing.call(depth0, "sanitize_id", depth0.id, options)))
    + "\" org_id=\"";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.sanitize_id || depth0.sanitize_id),stack1 ? stack1.call(depth0, depth0.id, options) : helperMissing.call(depth0, "sanitize_id", depth0.id, options)))
    + "\">\n                <ul>\n                  ";
  stack2 = helpers.each.call(depth0, depth0.subscriptions, {hash:{},inverse:self.noop,fn:self.program(20, program20, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n                </ul>\n              </li>\n              ";
  return buffer;
  }
function program20(depth0,data) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "\n                  <li class=\"dsub dsub_";
  if (stack1 = helpers.id) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.id; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" sub_id=\"";
  if (stack1 = helpers.id) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.id; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\">\n                    <div class=\"bookmark_title\">\n                      <span class=\"applogo\"><img src=\"";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.iconUrl || depth0.iconUrl),stack1 ? stack1.call(depth0, depth0, options) : helperMissing.call(depth0, "iconUrl", depth0, options)))
    + "\" data-ltr-size=\"34\" alt=\"";
  if (stack2 = helpers.name) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.name; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\"></span> ";
  if (stack2 = helpers.name) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.name; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\n                    </div>\n                    <h3>Bookmarks (<span class=\"bookmarks_count\">"
    + escapeExpression(((stack1 = ((stack1 = depth0.bookmarks),stack1 == null || stack1 === false ? stack1 : stack1.length)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</span>)</h3>\n                    <ul class=\"item_ul item_ul_bm\">\n                      ";
  stack2 = helpers.each.call(depth0, depth0.bookmarks, {hash:{},inverse:self.noop,fn:self.program(21, program21, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n                    </ul>\n                  </li>\n                  ";
  return buffer;
  }
function program21(depth0,data) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "\n                      <li class=\"item item_bm\"><div class=\"bookmark-item\"><a href=\"";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.fixUrl || depth0.fixUrl),stack1 ? stack1.call(depth0, depth0.launch_url, options) : helperMissing.call(depth0, "fixUrl", depth0.launch_url, options)))
    + "\">";
  if (stack2 = helpers.name) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.name; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "</a><i class=\"icon-remove remove-bookmark\" bookmark_id=\"";
  if (stack2 = helpers.bookmark_id) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.bookmark_id; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\"></i></div></li>\n                      ";
  return buffer;
  }

function program23(depth0,data) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "\n              <li class=\"dorg dorg_";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.sanitize_id || depth0.sanitize_id),stack1 ? stack1.call(depth0, depth0.id, options) : helperMissing.call(depth0, "sanitize_id", depth0.id, options)))
    + "\">\n                <ul class=\"item_ul\">\n                  <li class=\"item_title\">Apps (<span class=\"sr_apps_count\"></span>)</li>\n                  ";
  stack2 = helpers.each.call(depth0, depth0.subscriptions, {hash:{},inverse:self.noop,fn:self.program(24, program24, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n                  <li class=\"item_title\">Bookmarks (<span class=\"sr_bm_count\"></span>)</li>\n                  ";
  stack2 = helpers.each.call(depth0, depth0.subscriptions, {hash:{},inverse:self.noop,fn:self.program(26, program26, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n                </ul>\n              </li>\n              ";
  return buffer;
  }
function program24(depth0,data) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "\n                    <li class=\"item sr_app\">\n                      <div class=\"app\">\n                        <a href=\"";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.fixUrl || depth0.fixUrl),stack1 ? stack1.call(depth0, depth0.launch_url, options) : helperMissing.call(depth0, "fixUrl", depth0.launch_url, options)))
    + "\"><span class=\"applogo\"><img src=\"";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.iconUrl || depth0.iconUrl),stack1 ? stack1.call(depth0, depth0, options) : helperMissing.call(depth0, "iconUrl", depth0, options)))
    + "\" data-ltr-size=\"34\" alt=\"";
  if (stack2 = helpers.name) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.name; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\"></span> <div class=\"forfilter app-name\" bitium-sub=\"";
  if (stack2 = helpers.name) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.name; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\" bitium-extra=\""
    + escapeExpression(((stack1 = ((stack1 = ((stack1 = depth0.installation),stack1 == null || stack1 === false ? stack1 : stack1.configuration)),stack1 == null || stack1 === false ? stack1 : stack1.extra)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\" bitium-install=\""
    + escapeExpression(((stack1 = ((stack1 = depth0.installation),stack1 == null || stack1 === false ? stack1 : stack1.name)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\" bitium-slug=\""
    + escapeExpression(((stack1 = ((stack1 = ((stack1 = depth0.installation),stack1 == null || stack1 === false ? stack1 : stack1.provider)),stack1 == null || stack1 === false ? stack1 : stack1.name)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\" bitium-labels=\"";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.print_array || depth0.print_array),stack1 ? stack1.call(depth0, ((stack1 = depth0.installation),stack1 == null || stack1 === false ? stack1 : stack1.labels), options) : helperMissing.call(depth0, "print_array", ((stack1 = depth0.installation),stack1 == null || stack1 === false ? stack1 : stack1.labels), options)))
    + "\">";
  if (stack2 = helpers.name) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.name; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + " <span class=\"app-login\">(";
  if (stack2 = helpers.login) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.login; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + ")</span></div></a>\n                      </div>\n                    </li>\n                  ";
  return buffer;
  }

function program26(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\n                    ";
  stack1 = helpers.each.call(depth0, depth0.bookmarks, {hash:{},inverse:self.noop,fn:self.programWithDepth(27, program27, data, depth0),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n                  ";
  return buffer;
  }
function program27(depth0,data,depth1) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "\n                    <li class=\"item sr_bm\">\n                      <div class=\"app\">\n                        <a href=\"";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.fixUrl || depth0.fixUrl),stack1 ? stack1.call(depth0, depth0.launch_url, options) : helperMissing.call(depth0, "fixUrl", depth0.launch_url, options)))
    + "\"><span class=\"applogo\"><img src=\"";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.iconUrl || depth0.iconUrl),stack1 ? stack1.call(depth0, depth0, options) : helperMissing.call(depth0, "iconUrl", depth0, options)))
    + "\" data-ltr-size=\"34\" alt=\"";
  if (stack2 = helpers.name) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.name; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\"></span> <div class=\"forfilter app-name\" bitium-slug=\""
    + escapeExpression(((stack1 = ((stack1 = ((stack1 = depth1.installation),stack1 == null || stack1 === false ? stack1 : stack1.provider)),stack1 == null || stack1 === false ? stack1 : stack1.name)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\">";
  if (stack2 = helpers.name) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.name; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + " <span class=\"app-login\">("
    + escapeExpression(((stack1 = depth1.login),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + ")</span></div></a>\n                      </div>\n                    </li>\n                    ";
  return buffer;
  }

function program29(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\n    <span class=\"settings-link\"><a href=\"";
  if (stack1 = helpers.options_url) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.options_url; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\">Settings <i class=\"icon-reorder\"></i></a></span>\n  ";
  return buffer;
  }

  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.debug || depth0.debug),stack1 ? stack1.call(depth0, "Render Menu", options) : helperMissing.call(depth0, "debug", "Render Menu", options)))
    + "\n\n<!-- <Header> -->\n<div class=\"header\" id=\"header\">\n  <div class=\"headwrap\">\n    <div class=\"show_org_list org\">\n      Organizations\n    </div>\n    <div class=\"hide_org_list\">\n      <ul>\n\n        ";
  stack2 = helpers.each.call(depth0, depth0.orgs, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n      </ul>\n    </div>\n  </div>\n</div>\n<!-- </Header> -->\n<!-- <Search bar> -->\n<div class=\"bar\">\n  <div class=\"show_pw_gen\">\n    <div class=\"pw-gen-title\">\n      <div class=\"pwgen-large\"></div><div>Password Generator</div>\n    </div>\n    <div class=\"pw-gen-close\">\n      <div class=\"close-x\"></div>\n    </div>\n  </div>\n  <div class=\"show_org_list\">\n    Select an Organization\n  </div>\n  <form action=\"/\" method=\"get\" class=\"hide_org_list\">\n    <input type=\"text\" name=\"search\" id=\"search\" placeholder=\"Search\" tabindex=\"1\" autocomplete=\"off\" />\n  </form>\n</div>\n<!-- </Search bar> -->\n<div class=\"content\">\n  <div id=\"mover\">\n    <!-- <Org List> -->\n    <div id=\"orgs\" class=\"page\">\n      <div class=\"scrolly hasscroll\">\n        <div class=\"scrollbar\"><div class=\"track\"><div class=\"thumb\"><div class=\"end\"></div></div></div></div>\n        <div class=\"viewport\">\n          <div class=\"overview\">\n            <ul class=\"item_ul item_ul_org\">\n              ";
  stack2 = helpers.each.call(depth0, depth0.orgs, {hash:{},inverse:self.noop,fn:self.program(6, program6, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n            </ul>\n          </div>\n        </div>\n      </div>\n      ";
  stack2 = helpers['if'].call(depth0, depth0.credits, {hash:{},inverse:self.noop,fn:self.program(13, program13, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n    </div>\n    <!-- </Org List> -->\n    <!-- <Org's App List -->\n    <div id=\"applist\" class=\"page\">\n      <div class=\"scrolly hasscroll\">\n        <div class=\"scrollbar\"><div class=\"track\"><div class=\"thumb\"><div class=\"end\"></div></div></div></div>\n        <div class=\"viewport\">\n          <div class=\"overview\">\n            <ul>\n              ";
  stack2 = helpers.each.call(depth0, depth0.orgs, {hash:{},inverse:self.noop,fn:self.program(15, program15, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n            </ul>\n          </div>\n        </div>\n      </div>\n    </div>\n    <!-- </Org's App List> -->\n    <!-- <Org's App Bookmarks List> -->\n    <div id=\"bookmarks\" class=\"page\">\n      <div class=\"scrolly hasscroll\">\n        <div class=\"scrollbar\"><div class=\"track\"><div class=\"thumb\"><div class=\"end\"></div></div></div></div>\n        <div class=\"viewport\">\n          <div class=\"overview\">\n            <ul>\n              ";
  stack2 = helpers.each.call(depth0, depth0.orgs, {hash:{},inverse:self.noop,fn:self.program(19, program19, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n            </ul>\n          </div>\n        </div>\n      </div>\n      <div class=\"bmbar\">\n        <div class=\"active\"><i class=\"icon-star\"></i></div>\n      </div>\n    </div>\n    <!-- </Org's App Bookmarks List> -->\n    <!-- <Results List> -->\n    <div id=\"results\" class=\"page\">\n      <div class=\"scrolly hasscroll\">\n        <div class=\"scrollbar\"><div class=\"track\"><div class=\"thumb\"><div class=\"end\"></div></div></div></div>\n        <div class=\"viewport\">\n          <div class=\"overview\">\n            <ul>\n              ";
  stack2 = helpers.each.call(depth0, depth0.orgs, {hash:{},inverse:self.noop,fn:self.program(23, program23, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n            </ul>\n          </div>\n        </div>\n      </div>\n    </div>\n    <!-- </Results List> -->\n  </div>\n  <!-- <Password Generator> -->\n  <div id=\"pw_gen\" class=\"page\">\n    <div class=\"overview frame\">\n      <!-- <iframe id=\"pw_gen_frame\" frameBorder=\"0\" src=\"";
  if (stack2 = helpers.bitium_host) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.bitium_host; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "/site/password/plugin/\"></iframe> -->\n    </div>\n  </div>\n  <!-- <Password Generator> -->\n</div>\n<!-- <Share Form> -->\n<div id=\"share\" class=\"page pagebar_form\">\n</div>\n<!-- </Share Form> -->\n<!-- <Bookmark Form> -->\n<div id=\"addbm\" class=\"page pagebar_form\">\n</div>\n<!-- </Bookmark Form> -->\n\n\n<div class=\"settings_bar\">\n  <span class=\"pw-gen-link gotoPWGen\"><i class=\"icon-pwgen\"></i><span class=\"pw-gen-link-title\">Password Generator</span></span>\n  ";
  stack2 = helpers['if'].call(depth0, depth0.settings, {hash:{},inverse:self.noop,fn:self.program(29, program29, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n</div>\n";
  return buffer;
  });
})();