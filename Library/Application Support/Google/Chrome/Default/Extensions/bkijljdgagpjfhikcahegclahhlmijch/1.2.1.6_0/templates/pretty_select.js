(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['pretty_select.hbs'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression, self=this, helperMissing=helpers.helperMissing;

function program1(depth0,data,depth1) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "\n          <li><a href=\"#\" data-val=\"";
  if (stack1 = helpers.val) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.val; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" data-select-id=\""
    + escapeExpression(((stack1 = depth1.select_id),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\" ";
  options = {hash:{
    'operator': ("==")
  },inverse:self.noop,fn:self.program(2, program2, data),data:data};
  stack2 = ((stack1 = helpers.compare || depth0.compare),stack1 ? stack1.call(depth0, depth0.val, depth1.selected, options) : helperMissing.call(depth0, "compare", depth0.val, depth1.selected, options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += ">";
  if (stack2 = helpers.text) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.text; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "</a></li>\n        ";
  return buffer;
  }
function program2(depth0,data) {
  
  
  return "class=\"active\"";
  }

  buffer += "<div class=\"ffc-app-select-wrap\">\n  <div class=\"ffc-app-selected\">\n    <a data-val=\"";
  if (stack1 = helpers.selected_val) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.selected_val; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" href=\"#\">";
  if (stack1 = helpers.selected_text) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.selected_text; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</a>\n  </div>\n  <div class=\"ffc-app-select scrolly\">\n    <div class=\"scrollbar\">\n      <div class=\"track\">\n        <div class=\"thumb\">\n          <div class=\"end\"></div>\n        </div>\n      </div>\n    </div>\n    <div class=\"viewport\">\n      <div class=\"overview\">\n        <ul class=\"ffc-app-select-list\">\n        ";
  stack1 = helpers.each.call(depth0, depth0.items, {hash:{},inverse:self.noop,fn:self.programWithDepth(1, program1, data, depth0),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n        </ul>\n      </div>\n    </div>\n  </div>\n</div>";
  return buffer;
  });
})();