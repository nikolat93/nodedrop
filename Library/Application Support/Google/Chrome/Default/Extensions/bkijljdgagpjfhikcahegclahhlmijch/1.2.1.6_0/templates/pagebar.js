(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['pagebar.hbs'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, stack2, self=this, functionType="function", escapeExpression=this.escapeExpression;

function program1(depth0,data) {
  
  
  return " is_bookmarked";
  }

  buffer += "<!-- <PageBar> -->\n<div class=\"pagebar\" id=\"pagebar\">\n  <div class=\"pagebar_btns\">\n    <span class=\"gotoAddBm ";
  stack1 = helpers['if'].call(depth0, depth0.is_bookmarked, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\"><i class=\"icon-star\"></i> Bookmark page</span>\n  </div>\n  <span class=\"pagebar_logo\">\n    <img src=\"";
  stack2 = ((stack1 = ((stack1 = depth0.icons),stack1 == null || stack1 === false ? stack1 : stack1.small)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1);
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\" alt=\"";
  if (stack2 = helpers.name) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.name; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\">\n  </span>\n  <div class=\"pagebar_desc\">\n    <h2>";
  if (stack2 = helpers.name) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.name; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "</h2>\n    <small>";
  if (stack2 = helpers.login) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.login; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "</small>\n  </div>\n</div>\n<!-- </PageBar> -->";
  return buffer;
  });
})();