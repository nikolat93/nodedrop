(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['signin_wrap.hbs'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, stack2, functionType="function", escapeExpression=this.escapeExpression;


  buffer += "<div class=\"header\" id=\"header\">\n  <div class=\"headwrap\">\n      <div class=\"bitlogo\"><img src=\"images/ic_16.png\" alt=\"\" />"
    + escapeExpression(((stack1 = ((stack1 = depth0.settings),stack1 == null || stack1 === false ? stack1 : stack1.name)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</div>\n  </div>\n</div>\n<div class=\"content\">\n    <div class=\"signin\">\n        <a href=\"";
  stack2 = ((stack1 = ((stack1 = depth0.settings),stack1 == null || stack1 === false ? stack1 : stack1.login_url)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1);
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\">\n            <span class=\"signin_icon\"><i class=\"icon-lock\"></i></span>\n            <span class=\"signin_text\">"
    + escapeExpression(((stack1 = ((stack1 = ((stack1 = ((stack1 = depth0.settings),stack1 == null || stack1 === false ? stack1 : stack1.lang)),stack1 == null || stack1 === false ? stack1 : stack1.en)),stack1 == null || stack1 === false ? stack1 : stack1.login)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</span>\n        </a>\n    </div>\n</div>";
  return buffer;
  });
})();