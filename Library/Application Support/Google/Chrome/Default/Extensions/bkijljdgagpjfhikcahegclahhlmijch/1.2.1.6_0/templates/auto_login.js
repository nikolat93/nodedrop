(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['auto_login.hbs'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function";


  buffer += "<div id=\"";
  if (stack1 = helpers.overlay_id) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.overlay_id; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\">&nbsp;</div>\n\n<div id=\"";
  if (stack1 = helpers.bitium_id) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.bitium_id; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\">    \n  <style>\n    div#";
  if (stack1 = helpers.overlay_id) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.overlay_id; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += " {\n      background: none;\n      border: 0;\n      padding: 0;\n      margin: 0;\n      position: fixed;\n      top: 0;\n      left: 0;\n      height: 100%;\n      width: 100%;\n      opacity: 0;\n      background-color: #000;\n    }\n  \n    body.";
  if (stack1 = helpers.bitium_id) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.bitium_id; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += " > div#";
  if (stack1 = helpers.overlay_id) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.overlay_id; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += " {\n      opacity: 0.2;\n      z-index: 9999999;      \n    }\n    \n    div#";
  if (stack1 = helpers.bitium_id) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.bitium_id; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += " {\n      border: 0;\n      margin: 0;\n      padding: 0;\n      background: none;\n      \n      width: 700px;\n      height: 400px;\n      \n      position: fixed;\n      top: -150%;\n      left: 30%;\n      \n      opacity: 0;\n      z-index: 10000000;\n    }\n    \n    div#";
  if (stack1 = helpers.bitium_id) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.bitium_id; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += " > iframe, #auto-login-frame {\n      border: 0;\n      /*overflow: hidden;*/\n      margin: 0;\n      padding: 0;\n      border: 0;\n      width:100%;\n      height:400px;\n      display: block;\n      border: 1px solid #CCC;\n      -moz-border-radius: 15px;\n      border-radius: 15px;\n      box-shadow: 0px 0px 20px 2px rgba( 0, 0, 0, 0.3 );\n    }\n\n    #auto-login-frame {\n      background-color: white;\n    }\n\n    body.";
  if (stack1 = helpers.bitium_id) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.bitium_id; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += " > div#";
  if (stack1 = helpers.bitium_id) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.bitium_id; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += " {\n      opacity: 1;\n      top: 18%;\n    }\n        \n  </style>\n\n</div>";
  return buffer;
  });
})();