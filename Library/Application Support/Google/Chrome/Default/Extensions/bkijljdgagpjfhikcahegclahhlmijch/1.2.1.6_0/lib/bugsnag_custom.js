var BugsnagExtension = {};
BugsnagExtension.apiKey = '7ffb4f9d3c4312a44e2203a839936b16';
BugsnagExtension.notifyURL = 'https://notify.bugsnag.com';
BugsnagExtension.reportEvent = function (browser,version,context,exceptions,extraData,someMoreData){
    context = context || "default_extension_context";
    extraData = extraData || {};
    json = {
        apiKey: this.apiKey,
        notifier: {
            name: "Bugsnag Bitium Extension",
            version: "0.0.1",
            url: "https://bitium.com"
        },
        events: [{
            userId: "",
            appVersion: version,
            osVersion: browser,
            releaseStage: "development",
            context: context,
            exceptions: exceptions,
            metaData: {
                someData: extraData,
                someMoreData: someMoreData
            }
        }]
    }
    if (browser == 'chrome' || browser == 'safari')
        this.sendRequest(json);
    else if (browser == 'firefox')
        this.sendRequestFirefox(json);
    else
        console.log("Browser not supported for error");
}

BugsnagExtension.sendRequest = function(data){
    $.ajax({
        url:BugsnagExtension.notifyURL,
        type:"POST",
        data:JSON.stringify(data),
        contentType:"application/json",
        dataType:"json",
        success: function(){
            console.log ("Submitted to Bugsnag");
        }
    });
}

BugsnagExtension.sendRequestFirefox = function(data){
    var Request = require("sdk/request").Request;
    var bugsnagRequest = Request({
        url: BugsnagExtension.notifyURL,
        headers: {
            "content-type":"application/json"
        },
        content: JSON.stringify(data),
        onComplete: function (response) {
            console.log("Submitted to Bugsnag");
        }
    });
    bugsnagRequest.post();
}


