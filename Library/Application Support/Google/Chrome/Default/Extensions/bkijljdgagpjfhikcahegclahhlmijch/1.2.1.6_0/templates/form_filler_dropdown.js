(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['form_filler_dropdown.hbs'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, stack2, options, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  
  return "\n	<div class=\"dd_search_subs\">\n		<div>\n			<input type='text' class='dd_search_field' placeholder=\"Search subscriptions...\"></input>\n		</div>\n	</div>\n";
  }

function program3(depth0,data) {
  
  
  return "dd_no_search";
  }

function program5(depth0,data,depth1) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "\n		      	\n			      "
    + "\n\n			      <div class=\"dd_item dd_subscription ";
  options = {hash:{
    'operator': (">")
  },inverse:self.noop,fn:self.program(6, program6, data),data:data};
  stack2 = ((stack1 = helpers.compare || depth0.compare),stack1 ? stack1.call(depth0, depth0.index, 2, options) : helperMissing.call(depth0, "compare", depth0.index, 2, options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += " ";
  stack2 = helpers['if'].call(depth0, depth0.org_trial_expired, {hash:{},inverse:self.noop,fn:self.program(8, program8, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\" bitium_id=\"";
  if (stack2 = helpers.id) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.id; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\" org_id=\"";
  if (stack2 = helpers.orgId) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.orgId; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\" installation_id=\""
    + escapeExpression(((stack1 = ((stack1 = depth0.installation),stack1 == null || stack1 === false ? stack1 : stack1.id)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\">\n			        ";
  options = {hash:{},inverse:self.program(19, program19, data),fn:self.programWithDepth(10, program10, data, depth1),data:data};
  stack2 = ((stack1 = helpers.permisionViewCondition || depth0.permisionViewCondition),stack1 ? stack1.call(depth0, ((stack1 = depth0.permissions),stack1 == null || stack1 === false ? stack1 : stack1.can_view_password), ((stack1 = ((stack1 = depth0.installation),stack1 == null || stack1 === false ? stack1 : stack1.configuration)),stack1 == null || stack1 === false ? stack1 : stack1.saml), options) : helperMissing.call(depth0, "permisionViewCondition", ((stack1 = depth0.permissions),stack1 == null || stack1 === false ? stack1 : stack1.can_view_password), ((stack1 = ((stack1 = depth0.installation),stack1 == null || stack1 === false ? stack1 : stack1.configuration)),stack1 == null || stack1 === false ? stack1 : stack1.saml), options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n			      </div>\n			    ";
  return buffer;
  }
function program6(depth0,data) {
  
  
  return "dd_hide";
  }

function program8(depth0,data) {
  
  
  return "dd_org_trial_expired";
  }

function program10(depth0,data,depth2) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "\n			        		<div class=\"dd_item_header\">\n				        		<span class=\"dd_applogo\"><img src=\"";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.iconUrl || depth0.iconUrl),stack1 ? stack1.call(depth0, depth0, options) : helperMissing.call(depth0, "iconUrl", depth0, options)))
    + "\" data-ltr-size=\"34\" alt=\"";
  if (stack2 = helpers.name) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.name; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\"></span>\n					        	<p class=\"dd_view\">\n					        	<span class=\"dd_appname dd_appname_hide\">";
  if (stack2 = helpers.name) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.name; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "</span>\n\n					        	<span class=\"dd_appname_view_permission\">\n						        	";
  stack2 = helpers['if'].call(depth0, depth0.org_trial_expired, {hash:{},inverse:self.programWithDepth(13, program13, data, depth0),fn:self.program(11, program11, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n					        	</span>\n\n					        	<span class=\"dd_account\">";
  if (stack2 = helpers.login) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.login; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "</span>\n					        	</p>\n					        	<span class=\"dd_more_options\">\n					        		";
  stack2 = helpers['if'].call(depth0, depth0.org_trial_expired, {hash:{},inverse:self.programWithDepth(17, program17, data, depth2),fn:self.program(15, program15, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n					        	</span>\n					        	<span class=\"dd_more_options_hover\">\n					        			";
  stack2 = helpers['if'].call(depth0, depth0.org_trial_expired, {hash:{},inverse:self.programWithDepth(17, program17, data, depth2),fn:self.program(15, program15, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n					        	</span>\n				        	</div>\n				        	<div class=\"dd_item_extra\">\n				        		<div class=\"dd_item_extra_container\"> 	\n				        			<span class=\"dd_btn dd_fill\">Fill Form</span>\n				        			<span class=\"dd_btn copy-user\">Copy Username</span>\n				        			<span class=\"dd_btn copy-pwd\">Copy Password</span>\n				        			<div class=\"dd_copy_loading dd_msg\">Copying...</div>\n				        			<div class=\"dd_copied_message dd_msg\"></div>\n				        			<div class=\"dd_copy_sign_in dd_msg\">";
  if (stack2 = helpers.login_prompt) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.login_prompt; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "</div>\n				        			<div class=\"dd_verify_password\">\n				        				<input type=\"password\" placeholder=\"Verify your password...\" class=\"dd_verify_input\"></input>\n				        				<span class=\"dd_btn dd_verify_pwd dd_msg\">Verify</span>\n				        			</div>\n				        			<div class=\"dd_verify_pwd_loading dd_msg\">Verifying...</div>\n				        			<div class=\"dd_verify_pwd_error dd_msg\">Error validating password</div>\n				        		</div>\n				        	</div>\n			        ";
  return buffer;
  }
function program11(depth0,data) {
  
  
  return "\n						        		<span class=\"dd_fill_submit\">Upgrade Account</span>\n						        	";
  }

function program13(depth0,data,depth1) {
  
  var buffer = "", stack1, stack2;
  buffer += "\n						        		<span class=\"dd_fill_submit\">Log Into ";
  stack2 = ((stack1 = depth1.name),typeof stack1 === functionType ? stack1.apply(depth0) : stack1);
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "</span>\n						        	";
  return buffer;
  }

function program15(depth0,data) {
  
  
  return "\n					        			<span class=\"dd_expired\">Trial Expired</span>\n					        		";
  }

function program17(depth0,data,depth3) {
  
  var buffer = "", stack1, stack2;
  buffer += "\n					        			<img src=\"";
  stack2 = ((stack1 = depth3.dir_images),typeof stack1 === functionType ? stack1.apply(depth0) : stack1);
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "pf-ico-dots@2x.png\"></img>\n					        		";
  return buffer;
  }

function program19(depth0,data) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "\n			        	<div class=\"dd_item_header_no_perm\">\n			        		<span class=\"dd_applogo\"><img src=\"";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.iconUrl || depth0.iconUrl),stack1 ? stack1.call(depth0, depth0, options) : helperMissing.call(depth0, "iconUrl", depth0, options)))
    + "\" data-ltr-size=\"34\" alt=\"";
  if (stack2 = helpers.name) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.name; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\"></span>\n				        	<p class=\"dd_no_view\">\n				        	<span class=\"dd_appname\">";
  if (stack2 = helpers.name) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.name; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "</span>\n				        	<span class=\"dd_account\">";
  if (stack2 = helpers.login) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.login; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "</span>\n				        	</p>\n			        	</div>\n			        ";
  return buffer;
  }

function program21(depth0,data) {
  
  var buffer = "", stack1, options;
  buffer += "\n	      	<div class=\"dd_item dd_expand_subs\">\n	      		<span>Show ";
  if (stack1 = helpers.subscriptionsHidden) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.subscriptionsHidden; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + " more subscription";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.pluralize || depth0.pluralize),stack1 ? stack1.call(depth0, depth0.subscriptionsHidden, options) : helperMissing.call(depth0, "pluralize", depth0.subscriptionsHidden, options)))
    + "</span>\n	      	</div>\n	      	";
  return buffer;
  }

  options = {hash:{
    'operator': (">")
  },inverse:self.noop,fn:self.program(1, program1, data),data:data};
  stack2 = ((stack1 = helpers.compare || depth0.compare),stack1 ? stack1.call(depth0, depth0.subscriptionsHidden, 0, options) : helperMissing.call(depth0, "compare", depth0.subscriptionsHidden, 0, options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n<div class=\"dd_items_list_container ";
  options = {hash:{
    'operator': ("==")
  },inverse:self.noop,fn:self.program(3, program3, data),data:data};
  stack2 = ((stack1 = helpers.compare || depth0.compare),stack1 ? stack1.call(depth0, depth0.subscriptionsHidden, 0, options) : helperMissing.call(depth0, "compare", depth0.subscriptionsHidden, 0, options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\">\n	<div class=\"dd_items_list scrolly\">\n			<div class=\"scrollbar\">\n				<div class=\"track\">\n					<div class=\"thumb\">\n						<div class=\"end\"></div>\n					</div>\n				</div>\n			</div>\n			<div class=\"viewport\">\n				<div class=\"overview\">\n		      ";
  options = {hash:{},inverse:self.noop,fn:self.programWithDepth(5, program5, data, depth0),data:data};
  stack2 = ((stack1 = helpers.each_with_index || depth0.each_with_index),stack1 ? stack1.call(depth0, depth0.subscriptions, options) : helperMissing.call(depth0, "each_with_index", depth0.subscriptions, options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n		      ";
  options = {hash:{
    'operator': (">")
  },inverse:self.noop,fn:self.program(21, program21, data),data:data};
  stack2 = ((stack1 = helpers.compare || depth0.compare),stack1 ? stack1.call(depth0, depth0.subscriptionsHidden, 0, options) : helperMissing.call(depth0, "compare", depth0.subscriptionsHidden, 0, options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n      	</div>\n      </div>\n    </div>\n  <div class=\"dd_close_footer\">close</div>\n </div>";
  return buffer;
  });
})();