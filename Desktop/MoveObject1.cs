﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace DAQRI 
{
	public class MoveObject : MonoBehaviour {

		public bool moveXNeg = false;
		public bool moveXPos = false;
		public bool moveYNeg = false;
		public bool moveYPos = false;
		public bool moveZNeg = false;
		public bool moveZPos = false;
		bool snapLock = false;
		public GameObject lockIcon;
		public GameObject rotateIcon;
		public GameObject translateIcon;
		public GameObject scaleIcon;
		public Sprite open;
		public Sprite close;
		public Sprite rotation;
		public Sprite translate;
		bool locked;
		bool rotate;
		bool tranzlate;
		bool scale; 

		GameObject arrows;
		Vector3 centerPos;
		Vector3 PositiveX;
		Vector3 NegativeX;
		Vector3 PositiveZ;
		Vector3 NegativeZ;
		Vector3 PositiveY;
		Vector3 NegativeY;

		public GameObject [] cones;
		public GameObject [] spheres;
		public GameObject [] cubes;

		float timeStamp;
		float timeToSpeedUp = 1.5f;
		float scaleFactor = .005f;
		float slowSpeed = .005f;
		float fastSpeed = .01f;

		Quaternion defaultRotation;

		// Use this for initialization
		void Start () {
			Application.targetFrameRate = 90;
			arrows = GameObject.Find ("MoveableObject").transform.GetChild (0).gameObject;
			//SnapArrows ();
		
//			cones = GameObject.FindGameObjectsWithTag ("Cone");
//			spheres = GameObject.FindGameObjectsWithTag ("Sphere");
//			cubes = GameObject.FindGameObjectsWithTag ("Cube");

			foreach (GameObject s in spheres) {
				s.SetActive (false);
			}

			foreach (GameObject c in cubes) {
				c.SetActive (false);
			}
				
			defaultRotation = transform.rotation;

			arrows.SetActive (false);
			this.GetComponent<Collider> ().enabled = false;
			locked = true;
		} 

		void Update () {
			if (GameObject.Find ("Center") != null) 
				centerPos = GameObject.Find ("Center").transform.position;
			if (GameObject.Find ("PositiveX") != null)
				PositiveX = GameObject.Find ("PositiveX").transform.position - centerPos;
			if (GameObject.Find ("NegativeX") != null)
				NegativeX = GameObject.Find ("NegativeX").transform.position - centerPos;
			if (GameObject.Find ("PositiveZ") != null)
				PositiveZ = GameObject.Find ("PositiveZ").transform.position - centerPos;
			if (GameObject.Find ("NegativeZ") != null)
				NegativeZ = GameObject.Find ("NegativeZ").transform.position - centerPos;
			if (GameObject.Find ("PositiveY") != null)
				PositiveY = GameObject.Find ("PositiveY").transform.position - centerPos;
			if (GameObject.Find ("NegativeY") != null)
				NegativeY = GameObject.Find ("NegativeY").transform.position - centerPos;

			if (arrows != null) {
				float distanceBetween = Vector3.Distance (this.transform.position, arrows.transform.position);
				if (distanceBetween >= .5f)
					this.GetComponent<Collider> ().enabled = true;
			}
				
			if (moveXNeg && !snapLock) {
				if (rotate) {
					Vector3 gazeDirection = GameObject.Find ("NegativeX").transform.position - DisplayManager.Instance.transform.position;
					Vector3 perpendicularAxis = Vector3.Cross (NegativeX, gazeDirection);

					transform.RotateAround(this.transform.position, perpendicularAxis, 20 * Time.deltaTime);
 
				} else {
					if (Time.time < timeStamp)
						scaleFactor = slowSpeed;
					else
						scaleFactor = fastSpeed;

					transform.position = (transform.position + (NegativeX.normalized * scaleFactor));
				}
			}
			if (moveXPos && !snapLock) {
				if (rotate) {
					Vector3 gazeDirection = GameObject.Find ("PositiveX").transform.position - DisplayManager.Instance.transform.position;
					Vector3 perpendicularAxis = Vector3.Cross (PositiveX, gazeDirection);

					transform.RotateAround(this.transform.position, perpendicularAxis, 20 * Time.deltaTime);
 
				} else {
					if (Time.time < timeStamp)
						scaleFactor = slowSpeed;
					else
						scaleFactor = fastSpeed;

					transform.position = (transform.position + (PositiveX.normalized * scaleFactor));
				}
			}
			if (moveYNeg && !snapLock) {
				if (rotate) {
					Vector3 gazeDirection = GameObject.Find ("NegativeY").transform.position - DisplayManager.Instance.transform.position;
					Vector3 perpendicularAxis = Vector3.Cross (gazeDirection, Vector3.up);

					transform.RotateAround(this.transform.position, perpendicularAxis, 20 * Time.deltaTime);
 
				} else {
					if (Time.time < timeStamp)
						scaleFactor = slowSpeed;
					else
						scaleFactor = fastSpeed;

					transform.position = (transform.position + (NegativeY.normalized * scaleFactor));
				}
			}
//			if (moveYPos && !snapLock) {
//				if (rotate) {
//					Vector3 gazeDirection = GameObject.Find ("PositiveY").transform.position - DisplayManager.Instance.transform.position;
//					Vector3 perpendicularAxis = Vector3.Cross (gazeDirection, Vector3.down);
//
//					transform.RotateAround(this.transform.position, perpendicularAxis, 20 * Time.deltaTime);
//
//				} else {
//					if (Time.time < timeStamp)
//						scaleFactor = slowSpeed;
//					else
//						scaleFactor = fastSpeed;
//
//					transform.position = (transform.position + (PositiveY.normalized * scaleFactor));
//				}
//			}
			if (moveZNeg && !snapLock) {
				if (rotate) {
					Vector3 gazeDirection = GameObject.Find ("NegativeZ").transform.position - DisplayManager.Instance.transform.position;
					Vector3 perpendicularAxis = Vector3.Cross (NegativeZ, gazeDirection);

					transform.RotateAround(this.transform.position, perpendicularAxis, 20 * Time.deltaTime);
 
				} else {
					if (Time.time < timeStamp)
						scaleFactor = slowSpeed;
					else
						scaleFactor = fastSpeed;
					
					transform.position = (transform.position + (NegativeZ.normalized * scaleFactor));
				}
			}
			if (moveZPos && !snapLock) {
				if (rotate) {
					Vector3 gazeDirection = GameObject.Find ("PositiveZ").transform.position - DisplayManager.Instance.transform.position;
					Vector3 perpendicularAxis = Vector3.Cross (PositiveZ, gazeDirection);

					transform.RotateAround(this.transform.position, perpendicularAxis, 20 * Time.deltaTime);
 
				} else {
					if (Time.time < timeStamp)
						scaleFactor = slowSpeed;
					else
						scaleFactor = fastSpeed;

					transform.position = (transform.position + (PositiveZ.normalized * scaleFactor));
				}
			}

			if (moveYPos && rotate && !snapLock) {
				Vector3 gazeDirection = DisplayManager.Instance.transform.forward;
				Vector3 surfaceNormal = Vector3.zero;
				RaycastHit hit;

				if (Physics.Raycast(DisplayManager.Instance.transform.position, gazeDirection, out hit))
					surfaceNormal = hit.normal;
					
				Vector3 perpendicularAxis = Vector3.Cross (gazeDirection, -surfaceNormal);

				transform.RotateAround(this.transform.position, perpendicularAxis, 20 * Time.deltaTime);
			}

		}

		void OnTriggerEnter (Collider c) {
			if (c.gameObject.tag == "Arrow")
				this.GetComponent<Collider> ().enabled = false;
		}

		public void ResetXNeg () {
			GameObject NegX = GameObject.Find ("NegativeX");
			if (!snapLock)
				NegX.transform.localScale = NegX.transform.localScale / 1.2f;
			moveXNeg = false;
			snapLock = false;
		}

		public void ResetXPos () {
			GameObject PosX = GameObject.Find ("PositiveX");
			if (!snapLock)
				PosX.transform.localScale = PosX.transform.localScale / 1.2f;
			moveXPos = false;
			snapLock = false;
		}

		public void ResetYNeg () {
			GameObject NegY = GameObject.Find ("NegativeY");
			if (!snapLock)
				NegY.transform.localScale = NegY.transform.localScale / 1.2f;
			moveYNeg = false;
			snapLock = false;
		}

		public void ResetYPos () {
			GameObject PosY = GameObject.Find ("PositiveY");
			//if (!snapLock)
				//PosY.transform.localScale = PosY.transform.localScale / 1.2f;
			moveYPos = false;
			snapLock = false;
		}

		public void ResetZNeg () {
			GameObject NegZ = GameObject.Find ("NegativeZ");
			if (!snapLock)
				NegZ.transform.localScale = NegZ.transform.localScale / 1.2f;
			moveZNeg = false;
			snapLock = false;
		}

		public void ResetZPos () {
			GameObject PosZ = GameObject.Find ("PositiveZ");
			if (!snapLock)
				PosZ.transform.localScale = PosZ.transform.localScale / 1.2f;
			moveZPos = false;
			snapLock = false;
		}

		public void ResetPosition () {
			this.transform.position = arrows.transform.position;
			snapLock = true;
		}

		public void ResetRotation () {
			this.transform.rotation = arrows.transform.rotation;
			snapLock = true;
		}

		public void SnapArrows () {
			snapLock = true;
			GameObject.Find ("UI").transform.parent = arrows.transform;

			if (arrows != null)
				arrows.transform.position = new Vector3 (this.transform.position.x, this.transform.position.y, this.transform.position.z);
		}

		public void ToggleLock () {
			locked = !locked;

			if (locked) {
				this.GetComponent<Collider> ().enabled = false;
				rotateIcon.transform.position = (this.transform.position + (Vector3.up * .35f));
				lockIcon.transform.position = (this.transform.position + (Vector3.up * .35f));
				translateIcon.transform.position = (this.transform.position + (Vector3.up * .35f));
				scaleIcon.transform.position = (this.transform.position + (Vector3.up * .35f));

				foreach (GameObject c in cones) {
					c.SetActive (false);
				}

				foreach (GameObject s in spheres) {
					s.SetActive (false);
				}

				foreach (GameObject c in cubes) {
					c.SetActive (false);
				}

				GameObject.Find ("UI").transform.parent = transform;

				arrows.SetActive (false);
				GameObject.Find ("lock_yellow").GetComponent<SpriteRenderer> ().sprite = close;
				rotateIcon.transform.position = lockIcon.transform.position;
				translateIcon.transform.position = lockIcon.transform.position;
				scaleIcon.transform.position = lockIcon.transform.position;
				rotateIcon.SetActive (false);
				scaleIcon.SetActive (false);
				translateIcon.SetActive (false);
			} else {
				GameObject.Find ("Top").GetComponent<Animation> ().Play ();
				arrows.SetActive (true);

				foreach (GameObject c in cones) {
					c.SetActive (false);
				}

				foreach (GameObject s in spheres) {
					s.SetActive (false);
				}

				foreach (GameObject c in cubes) {
					c.SetActive (false);
				}

				if (!rotate) {
					foreach (GameObject c in cones) {
						c.SetActive (true);
					}
				}

				if (rotate) {
					foreach (GameObject s in spheres) {
						s.SetActive (true);
					}
				}

				GameObject.Find ("UI").transform.parent = arrows.transform;
					
				SnapArrows ();
				snapLock = true;
				GameObject.Find ("lock_yellow").GetComponent<SpriteRenderer> ().sprite = open;
				rotateIcon.SetActive (true);
				translateIcon.SetActive (true);
				scaleIcon.SetActive (true);

				Vector3 relativeX = DisplayManager.Instance.transform.right;

				rotateIcon.transform.position = (GameObject.Find("UI").transform.position + (relativeX * -.35f) + (Vector3.up * .2f));
				lockIcon.transform.position = (GameObject.Find("UI").transform.position + (relativeX * .35f) + (Vector3.up * .2f));
				translateIcon.transform.position = (GameObject.Find("UI").transform.position + (relativeX * -.135f) + (Vector3.up * .4f));
				scaleIcon.transform.position = (GameObject.Find("UI").transform.position + (relativeX * .135f) + (Vector3.up * .4f));
			}
		}

		public void ToggleRotate () {
			if (rotate) {
				ResetRotation ();
			} else {
				tranzlate = false;
				scale = false;
				rotate = true;

				translateIcon.transform.GetChild (1).gameObject.SetActive (false);
				rotateIcon.transform.GetChild (1).gameObject.SetActive (true);

				foreach (GameObject a in cones) {
					a.SetActive (false);
				}

				foreach (GameObject b in cubes) {
					b.SetActive (false);
				}

				foreach (GameObject c in spheres) {
					c.SetActive (true);
				}

				GameObject.Find ("UI").transform.parent = null;
			}
		}

		public void ToggleTranslate () {
			if (tranzlate) {
				ResetPosition ();
			} else {
				tranzlate = true;
				scale = false;
				rotate = false;

				rotateIcon.transform.GetChild (1).gameObject.SetActive (false);
				translateIcon.transform.GetChild (1).gameObject.SetActive (true);

				foreach (GameObject a in spheres) {
					a.SetActive (false);
				}

				foreach (GameObject b in cubes) {
					b.SetActive (false);
				}

				foreach (GameObject o in cones) {
					o.SetActive (true);
				}

				GameObject.Find ("UI").transform.parent = null;
			}
		}

		public void MoveXPositive () {
			timeStamp = Time.time + timeToSpeedUp;
			moveXPos = true;
			if (!snapLock)
				GameObject.Find ("PositiveX").transform.localScale = GameObject.Find ("PositiveX").transform.localScale * 1.2f;
		}

		public void MoveXNegative () {
			timeStamp = Time.time + timeToSpeedUp;
			moveXNeg = true;
			if (!snapLock)
				GameObject.Find ("NegativeX").transform.localScale = GameObject.Find ("NegativeX").transform.localScale * 1.2f;
		}

		public void MoveYPositive () {
			timeStamp = Time.time + timeToSpeedUp;
			moveYPos = true;
			//if (!snapLock)
				//GameObject.Find ("PositiveY").transform.localScale = GameObject.Find ("PositiveY").transform.localScale * 1.2f;
		}

		public void MoveYNegative () {
			timeStamp = Time.time + timeToSpeedUp;
			moveYNeg = true;
			if (!snapLock)
				GameObject.Find ("NegativeY").transform.localScale = GameObject.Find ("NegativeY").transform.localScale * 1.2f;
		}

		public void MoveZPositive () {
			timeStamp = Time.time + timeToSpeedUp;
			moveZPos = true;
			if (!snapLock)
				GameObject.Find ("PositiveZ").transform.localScale = GameObject.Find ("PositiveZ").transform.localScale * 1.2f;
		}

		public void MoveZNegative () {
			timeStamp = Time.time + timeToSpeedUp;
			moveZNeg = true;
			if (!snapLock)
				GameObject.Find ("NegativeZ").transform.localScale = GameObject.Find ("NegativeZ").transform.localScale * 1.2f;
		}
	}
}
