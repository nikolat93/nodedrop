﻿using UnityEngine;
using UnityEditor;
using System.IO;
using ProdTools;

/// <summary>
/// runs methods that help set up whatever is needed during Unity startup.
/// </summary>
/// 
[InitializeOnLoad]
public class PROD_UnityStartup
{
    // any startup methods are called in here
    static PROD_UnityStartup()
    {
        CreateUserWorkFolder();
        PROD_RecentScenes recentScenes = new PROD_RecentScenes();
        recentScenes.CreateRecentScenesScriptableObject();
    }


    static void CreateUserWorkFolder ()
    {
        // set up paths
        string fullWorkFolder = PROD_User.GetWorkPath();
        
        // create user work folder if it doesn't exist
        if(!Directory.Exists(fullWorkFolder))
        {
            Directory.CreateDirectory(fullWorkFolder);
        }
    }
}
