﻿using UnityEngine;
using UnityEditor;

namespace DaqriProd {
    
    [CustomEditor(typeof(KeyboardPressTrigger))]
    public class CustomKeyPresskpentInspector : Editor {

        // Reference to the KeyPressEvent class being used by KeyboardPressTrigger
        KeyboardPressTrigger kp;

        void OnEnable ()
        {
            kp = (KeyboardPressTrigger)target;
        }

        // Draw the new inspector for KeyboardPressTrigger
        public override void OnInspectorGUI ()
        {
            // Redraws the defaul inspector so we don't have to created it again from scratch
            DrawDefaultInspector();

            // Adds a popup field to the default inspector
            kp.keyPressEvent.mappedKeyIndex = EditorGUILayout.Popup("Input", kp.keyPressEvent.mappedKeyIndex, kp.keyPressEvent.mappedKey);

            switch(kp.keyPressEvent.mappedKeyIndex)
            {
            case 0:
                kp.keyPressEvent.keyToPress = "Fire1";
                break;
            case 1:
                kp.keyPressEvent.keyToPress= "Fire2";
                break;
            case 2:
                kp.keyPressEvent.keyToPress = "Fire3";
                break;
            case 3:
                kp.keyPressEvent.keyToPress = "Jump";
                break;
            case 4:
                kp.keyPressEvent.keyToPress = "Submit";
                break;
            case 5:
                kp.keyPressEvent.keyToPress = "Cancel";
                break;
            }
        }
    }
}