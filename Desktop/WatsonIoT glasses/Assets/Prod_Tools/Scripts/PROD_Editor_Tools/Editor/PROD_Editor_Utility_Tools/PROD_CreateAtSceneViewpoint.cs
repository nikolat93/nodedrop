﻿using UnityEngine;
using UnityEditor;
using ProdTools;

public class PROD_CreateAtSceneViewpoint : EditorWindow 
{
    const string MENUPATH = PROD_Menu.PRODTOOLSMENUPATH + "PROD_CreateAtSceneViewpoint";
    static PROD_CreateAtSceneViewpoint createAtCameraPosWindow;

    // Used to do a case switch for creating different types of lights using a Popup in the editor window
    static string[] createLightOfType = new string[] {"Directional Light", "Point Light", "Spotlight"};
    static int lightOfTypeIndex = 0;

    // Used to cache the scene viewport camera when launching the editor window
    static Camera sceneCamera;
    static GameObject newCamera;

    static bool tagAsMainCamera;
    static bool anchorToWorldOrigin;
    static bool followCamRotation;

    [MenuItem (MENUPATH, false, 300)]
    static void Init()
    {
        createAtCameraPosWindow = (PROD_CreateAtSceneViewpoint)EditorWindow.GetWindow(typeof(PROD_CreateAtSceneViewpoint));
        createAtCameraPosWindow.Show();
        // Grab the first Scene Window if there's more than one
        if(SceneView.sceneViews.Count > 0)
        {
            SceneView sceneView = (SceneView)SceneView.sceneViews[0];
            sceneView.Focus();
            sceneCamera = Camera.current;
        }
    }

    //-------------------------------------------------------------------------------------------------
    // Block for versioning up all of the objects created at the scene viewport
    static string DoesCamNameExist()
    {
        int iterator = 1;
        string iteratorString = string.Format ("{0:000}", iterator);

        while (GameObject.Find("Camera_" + iteratorString)) {
            iterator++;
            iteratorString = string.Format ("{0:000}", iterator);
        }

        return "Camera_" + iteratorString;

    }

    static string DoesAnchorNameExist()
    {
        int iterator = 1;
        string iteratorString = string.Format ("{0:000}", iterator);

        while (GameObject.Find("camera_Anchor_" + iteratorString)) {
            iterator++;
            iteratorString = string.Format ("{0:000}", iterator);
        }

        return "camera_Anchor_" + iteratorString;

    }

    // TODO: Consolidate all light versioning into one case switch 'static string DoesLightNameExist()'
    static string DoesDirectionalLightNameExist()
    {
        int iteratorDirectional = 1;
        string directionalString = string.Format ("{0:000}", iteratorDirectional);

        while (GameObject.Find("light_directional_" + directionalString)) {
            iteratorDirectional++;
            directionalString = string.Format ("{0:000}", iteratorDirectional);
        }

        return "light_directional_" + directionalString;
    }

    static string DoesPointLightNameExist()
    {
        int iteratorPoint = 1;
        string pointString = string.Format ("{0:000}", iteratorPoint);

        while (GameObject.Find("light_point_" + pointString)) {
            iteratorPoint++;
            pointString = string.Format ("{0:000}", iteratorPoint);
        }

        return "light_point_" + pointString;
    }

    static string DoesSpotLightNameExist()
    {
        int iteratorSpot = 1;
        string spotString = string.Format ("{0:000}", iteratorSpot);

        while (GameObject.Find("light_spot_" + spotString)) {
            iteratorSpot++;
            spotString = string.Format ("{0:000}", iteratorSpot);
        }

        return "light_spot_" + spotString;
    }

    static string DoesLocatorNameExist()
    {
        int iteratorLocator = 1;
        string locatorString = string.Format ("{0:000}", iteratorLocator);

        while (GameObject.Find("locator_" + locatorString)) {
            iteratorLocator++;
            locatorString = string.Format ("{0:000}", iteratorLocator);
        }

        return "locator_" + locatorString;
    }
    //-------------------------------------------------------------------------------------------------

    void OnGUI()
    {
        GUILayout.Label("Create At Scene Viewpoint", EditorStyles.boldLabel);

        // Create Camera Options
        GUILayout.Label("Camera Options", EditorStyles.boldLabel);
        tagAsMainCamera = EditorGUILayout.Toggle("Tag As Main Camera", tagAsMainCamera);
        anchorToWorldOrigin = EditorGUILayout.Toggle("Anchor To World Origin", anchorToWorldOrigin);
        if(GUILayout.Button("Create Camera"))
        {
            if(sceneCamera != null)
            {
                // Create a camera and set up necessary components and values
                if(anchorToWorldOrigin)
                {
                    newCamera = new GameObject ("Camera");
                }
                else
                {
                    string newCameraName = DoesCamNameExist();
                    newCamera = new GameObject (newCameraName);
                }

                newCamera.transform.position = sceneCamera.transform.position;
                newCamera.transform.rotation = sceneCamera.transform.rotation;
                newCamera.AddComponent<Camera>().depth = -1;
                newCamera.AddComponent<GUILayer>();
                newCamera.AddComponent<FlareLayer>();
                newCamera.AddComponent<AudioListener>();

                // Tag this object as MainCamera if the option is toggled on
                if(tagAsMainCamera)
                {
                    newCamera.tag = "MainCamera";
                }

                // Anchor the camera to the world origin if the option is toggled on
                // Select the cameraAnchor after creating it
                if(anchorToWorldOrigin)
                {
                    string newAnchorName = DoesAnchorNameExist();
                    GameObject cameraAnchor = new GameObject (newAnchorName);
                    newCamera.transform.parent = cameraAnchor.transform;
                    Selection.activeGameObject = cameraAnchor.gameObject;
                }
                else
                {
                    // Select the newly created camera
                    Selection.activeGameObject = newCamera.gameObject;
                }
            }
            else
            {
                EditorUtility.DisplayDialog("Scene Camera Not Found","Close the 'PROD_CreateAtSceneCameraPosition' Editor Window and relaunch it from the Prod_Tools dropdown menu","OK");
            }
        }

        // Create Light Options
        GUILayout.Label("Light Options", EditorStyles.boldLabel);

        lightOfTypeIndex = EditorGUILayout.Popup("Type of Light", lightOfTypeIndex, createLightOfType);

        if(GUILayout.Button("Create Light"))
        {
            if(sceneCamera != null)
            {
                switch (lightOfTypeIndex)
                {
                case 0:
                    string newDirectionalLightName = DoesDirectionalLightNameExist();
                    GameObject directionalLight = new GameObject(newDirectionalLightName);
                    directionalLight.AddComponent<Light>().type = LightType.Directional;
                    directionalLight.transform.position = sceneCamera.transform.position;
                    directionalLight.transform.rotation = sceneCamera.transform.rotation;
                    Selection.activeGameObject = directionalLight.gameObject;
                    break;
                case 1:
                    string newPointLightName = DoesPointLightNameExist();
                    GameObject pointLight = new GameObject(newPointLightName);
                    pointLight.AddComponent<Light>().type = LightType.Point;
                    pointLight.GetComponent<Light>().bounceIntensity = 0;
                    pointLight.transform.position = sceneCamera.transform.position;
                    Selection.activeGameObject = pointLight.gameObject;
                    break;
                case 2:
                    string newSpotLightName = DoesSpotLightNameExist();
                    GameObject spotLight = new GameObject(newSpotLightName);
                    spotLight.AddComponent<Light>().type = LightType.Spot;
                    spotLight.GetComponent<Light>().bounceIntensity = 0;
                    spotLight.transform.position = sceneCamera.transform.position;
                    spotLight.transform.rotation = sceneCamera.transform.rotation;
                    Selection.activeGameObject = spotLight.gameObject;
                    break;
                }
            }
            else
            {
                EditorUtility.DisplayDialog("Scene Camera Not Found","Close the 'PROD_CreateAtSceneCameraPosition' Editor Window and relaunch it from the Prod_Tools dropdown menu","OK");
            }
        }

        GUILayout.Label("Locator Options", EditorStyles.boldLabel);

        followCamRotation = EditorGUILayout.Toggle("Use Camera Rotation", followCamRotation);

        // Create "Locator" Options
        if(GUILayout.Button("Create Locator"))
        {
            if(sceneCamera != null)
            {
                string newLocatorName = DoesLocatorNameExist();
                GameObject emptyGO = new GameObject (newLocatorName);
                emptyGO.transform.position = sceneCamera.transform.position;

                if(followCamRotation)
                {
                    emptyGO.transform.rotation = sceneCamera.transform.rotation;
                }

                Selection.activeGameObject = emptyGO.gameObject;
            }
            else
            {
                EditorUtility.DisplayDialog("Scene Camera Not Found","Close the 'PROD_CreateAtSceneCameraPosition' Editor Window and relaunch it from the Prod_Tools dropdown menu","OK");
            }
        }
    }
}