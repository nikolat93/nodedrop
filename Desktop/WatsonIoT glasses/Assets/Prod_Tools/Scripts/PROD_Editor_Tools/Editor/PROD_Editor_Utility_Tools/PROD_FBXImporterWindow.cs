﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using ProdTools;

public class PROD_FBXImporterWindow : EditorWindow
{
    const string MENUPATH = PROD_Menu.PRODTOOLSMENUPATH + "PROD_ReimportFBXWindow";
    static PROD_FBXImporterWindow fbxImporterWindow;

    //Float values used for the FloatField in the EditorWindow
    static float scaleFactor = 1.0f;
    static float smoothingAngle = 70.0f;

    static Object[] selectObjs;
    
    static List <GameObject> rootObjs = new List<GameObject>();
    static List <AnimationClip> animClips = new List<AnimationClip>();
    
    static bool multipleClips;

    static bool toggleImportMaterials = false;
    static bool importMaterialsOption = true;
    static bool toggleImportAnimation = false;
    static bool importAnimationOption = true;

    static string[] animationSelectionType = new string[] {"Off", "Keyframe Reduction", "Optimal"};
    static int animationSelectionIndex = 1;

    [MenuItem (MENUPATH, false, 200)]
    static void Init ()
    {
        // Get existing open window or if none, make a new one
        fbxImporterWindow = (PROD_FBXImporterWindow)EditorWindow.GetWindow (typeof(PROD_FBXImporterWindow));
        fbxImporterWindow.Show ();
    }

    void OnGUI ()
    {
        GUILayout.Label ("FBX Importer | Global Settings", EditorStyles.boldLabel);
        //Float Field for the Scale Factor parameter in the Inspector
        scaleFactor = EditorGUILayout.FloatField ("Scale Factor", scaleFactor);

        //Float Field and Slider for the Normal Smoothing Angle parameter in the Inspector
        smoothingAngle = EditorGUILayout.Slider ("Normal Smoothing Angle", smoothingAngle, 0, 180);


        //Imports the FBX Animation Only
        GUILayout.Label ("FBX Importer | Animation", EditorStyles.boldLabel);

        //Popup menu for selecting the type of animation compression when reimporting the animation clip(s)
        animationSelectionIndex = EditorGUILayout.Popup ("Animation Compression:", animationSelectionIndex, animationSelectionType);
        
        //Runs through the Import FBX Anim functions
        if (GUILayout.Button ("Import FBX Animation Only")) 
        {
            ExecuteReimportAnimOnly();
            ValidateExecuteReimportAnimOnly();
        }

        //Imports the FBX Model Only with Selected Toggled Settings for Animation and Material Import Options
        GUILayout.Label ("FBX Importer | Model", EditorStyles.boldLabel);

        //Toggles the Import Materials option on or off in the Import Settings for FBX
        toggleImportMaterials = EditorGUILayout.Toggle ("Import Materials", toggleImportMaterials);
        
        if (toggleImportMaterials == true)
        {
            importMaterialsOption = true;
        }
        else
        {
            importMaterialsOption = false;
        }

        //Toggles the Import Animation option on or off in the Import Settings for FBX
        toggleImportAnimation = EditorGUILayout.Toggle ("Import Animation", toggleImportAnimation);

        if (toggleImportAnimation == true)
        {
            importAnimationOption = true;
        }
        else
        {
            importAnimationOption = false;
        }

        //Runs through the Import FBX Model functions
        if (GUILayout.Button ("Import FBX Model Only"))
        {
            ExecuteReimportFBXModel();
        }
    }

    //---------------------------------------------------------------------------------------------------------
    //Only Imports the FBX Animation Clips
    //---------------------------------------------------------------------------------------------------------
    static void ExecuteReimportAnimOnly () 
    {
        selectObjs = Selection.GetFiltered(typeof(Object),SelectionMode.DeepAssets);

        animClips.Clear();
        rootObjs.Clear();
        
        List <string> deletionPaths = new List<string>();
        
        bool doAll = false;
        bool doSave = false;
        
        // Editor GUI Popup
        int caseSwitch;
        caseSwitch = EditorUtility.DisplayDialogComplex("Duplicate Animation, Delete FBX's and Materials?", "Duplicates animation clips, " +
                                                        "then deletes fbx and materials found within selected or folder hierarchy \n","Ok", "Ok without Deleting", "Cancel");
        
        switch (caseSwitch)
        {
        case 0:
            doAll = true;
            break;
        case 1:
            doSave = true;
            break;
        case 2:
            Debug.Log("Reimport FBX for Animation Has Been Canceled!");
            break;
        }
        
        
        if(doAll == false && doSave == false) return;
        
        
        for(int i=0; i<selectObjs.Length; i++)
        {
            
            string path = AssetDatabase.GetAssetPath(selectObjs[i]).ToLower();
            
            if(path.Substring(path.Length - 4, 4)==".fbx" || path.Substring(path.Length - 3, 3)==".ma")
            {
                // Iterate thru all childs in selected folder and delete extraneous fbx objects
                string pathToDelete = AssetDatabase.GetAssetPath(selectObjs[i]);
                deletionPaths.Add(pathToDelete);
                
                object[] objects = AssetDatabase.LoadAllAssetsAtPath(path);
                for(int j=0; j<objects.Length; j++)
                {
                    AnimationClip clip = objects[j] as AnimationClip;
                    if (clip != null)
                    {
                        animClips.Add(clip);
                        rootObjs.Add(selectObjs[i] as GameObject);
                    }
                }
                //If there are two anim clips, they are prob Take 001 and __preview__Take 001 (minions)
                //so we are copying the Take 001 anim and renaming to the root fbx name (ex. page_08_pirate_v06)
                //Otherwise we are assuming that there are several embedded anim takes in the fbx already named
                bool multiClips = false;
                if(animClips.Count > 2) multiClips = true;
                ProcessAnimClips(multiClips);
            }

            // Add materials folder to deletion path
            if(selectObjs[i].name == "Materials")
            {
                string pathToDelete = AssetDatabase.GetAssetPath(selectObjs[i]); 
                deletionPaths.Add(pathToDelete);
            }
        }

        if(doAll)
        {
            // Iterate thru deletionPaths list and delete fbx's and materials
            for(int k=0; k<deletionPaths.Count; k++)
            {
                AssetDatabase.DeleteAsset(deletionPaths[k]);
                Debug.Log(deletionPaths[k]+"  <--- Has Been Deleted from Project!");
            }
        }
    }
    
    static void ProcessAnimClips (bool multiClips)
    {
        multipleClips = multiClips;
        
        for(int i=0; i<animClips.Count; i++)
        {
            if(!animClips[i].name.Contains("__preview__Take "))
                _DupeAnimationClip(animClips[i], rootObjs[i]);
        }

        animClips.Clear();
        rootObjs.Clear();
    }
    
    
    static void _DupeAnimationClip (AnimationClip sourceClip, GameObject root)
    {
        Debug.Log(" Duplicating sourceClip:    "+sourceClip);
        
        if (sourceClip != null)
        {
            // Get fbx and set its import settings
            string rootPath = AssetDatabase.GetAssetPath(root);
            ModelImporter importer = AssetImporter.GetAtPath(rootPath) as ModelImporter;

            switch (animationSelectionIndex)
            {
            case 0: importer.animationCompression = ModelImporterAnimationCompression.Off;
                Debug.Log ("Animation Compression Set to Off");
                break;
            case 1: importer.animationCompression = ModelImporterAnimationCompression.KeyframeReduction;
                Debug.Log ("Animation Compression Set to Keyframe Reduction");
                break;
            case 2: importer.animationCompression = ModelImporterAnimationCompression.Optimal;
                Debug.Log ("Animation Compression Set to Optimal");
                break;
            }

            importer.importMaterials = false;
            importer.globalScale = scaleFactor;
            importer.animationRotationError = 0.05f;
            importer.animationPositionError = 0.5f;
            importer.animationScaleError = 0.0f;
            importer.animationType = ModelImporterAnimationType.Generic;
            AssetDatabase.ImportAsset(rootPath);
            
            // Create duplicate animation clip
            string path = AssetDatabase.GetAssetPath(sourceClip);
            // Check if there are multiple anim clips and use their names or use the root obj name
            path = Path.Combine(Path.GetDirectoryName(path), multipleClips ? sourceClip.name : root.name) + ".anim";

            string newPath = AssetDatabase.GenerateUniqueAssetPath (path);
            AnimationClip newClip = new AnimationClip();
            EditorUtility.CopySerialized(sourceClip, newClip);
            AssetDatabase.CreateAsset(newClip, newPath);
        }
    }

    //---------------------------------------------------------------------------------------------------------
    //Only Imports the FBX Model | Options for importing it with Animation or Materials
    //---------------------------------------------------------------------------------------------------------
    static void ExecuteReimportFBXModel()
    {
        Object[] selectObjs = Selection.GetFiltered(typeof(Object),SelectionMode.DeepAssets);
        
        for(int i=0; i<selectObjs.Length; i++)
        {
            
            string path = AssetDatabase.GetAssetPath(selectObjs[i]).ToLower();
            
            if(path.Substring(path.Length - 4, 4)==".fbx" || path.Substring(path.Length - 3, 3)==".ma")
            {
                // Get fbx path and set its import settings
                string rootPath = AssetDatabase.GetAssetPath(selectObjs[i]);
                ModelImporter importer = AssetImporter.GetAtPath(rootPath) as ModelImporter;
                
                Debug.Log(" ---  Reimporting model : "+selectObjs[i].name);
                
                // Model import settings here
                importer.globalScale = scaleFactor;
                importer.meshCompression = ModelImporterMeshCompression.Off;
                importer.optimizeMesh = false;
                importer.normalSmoothingAngle = smoothingAngle;
                importer.importNormals = ModelImporterNormals.Calculate;
                importer.importMaterials = importMaterialsOption;
                importer.animationType = ModelImporterAnimationType.Generic;
                importer.importAnimation = importAnimationOption;
                AssetDatabase.ImportAsset(rootPath);
            }
        }
    }

    //The EditorWindow can only be opened if the activeObject is a valid selection
    [MenuItem ( MENUPATH, true, 200)]
    static bool ValidateExecuteReimportAnimOnly ()
    {
        return Selection.activeObject != null;
    }

    static bool ValidateExecuteReimportModel () 
    {
        return Selection.activeObject != null;
    }
}
