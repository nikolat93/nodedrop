﻿namespace UnityEngine
{

    // This can be called from anywhere to return a nameSpace (ie from Maya)
    public static class PROD_GetNameSpace
    {

        public static string GetNameSpace (Transform trns)
        {
            string[] selSplit = trns.name.Split(":"[0]);
            if (selSplit.Length > 1) 
            {
                return selSplit [0] + ":";
            }
            else
            {
                //Search whole hierarchy
                foreach (Transform child in trns)
                {
                    // if there is a gameobject with namespace inside the hierarchy
                    if (child.name.Split(":"[0]).Length > 1)
                    {
                        return child.name.Split(":"[0])[0];
                    }
                }
            }
            return "No namespace found";
        }

    }

}
