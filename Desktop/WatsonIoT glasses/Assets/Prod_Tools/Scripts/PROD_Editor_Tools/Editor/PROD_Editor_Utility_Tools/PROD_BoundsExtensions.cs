﻿namespace UnityEngine
{
    using System.Collections.Generic;
    using System.Linq;

    // This can be called from anywhere to return the average mesh bounds of a hierarchy of meshes
    public static class BoundsExtensions
    {
        public static Bounds EncapsulatedBounds(this IEnumerable<Renderer> renderers)
        {
            return renderers.Select(renderer => renderer.bounds).Encapsulation();
        }
        
        public static Bounds EncapsulatedBounds(this IEnumerable<Mesh> meshes)
        {
            return meshes.Select(mesh => mesh.bounds).Encapsulation();
        }
        
        public static Bounds Encapsulation(this IEnumerable<Bounds> bounds)
        {
            return bounds.Aggregate((encapsulation, next) =>
            {
                encapsulation.Encapsulate(next);
                return encapsulation;
            });
        }
    }
}
