﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ProdTools;

namespace ProdTools
{
    public class PROD_ApplyMaterialToSelection : EditorWindow
    {
    
        const string MENUPATH = PROD_Menu.PRODTOOLSMENUPATH + "PROD_ApplyMaterialToSelection";
    
        static GameObject[] thisSelection;
        static Object[] thisMaterialSelection;
        static PROD_ApplyMaterialToSelection prod_applyMaterialsToSelection;
        
        
        [MenuItem (MENUPATH, false, 100)]
        static void Init ()
        {
            thisSelection = Selection.gameObjects;
            thisMaterialSelection = Selection.GetFiltered (typeof(Material), SelectionMode.Assets);
            Material myMat;
    
            // check if a material is also selected... and force only one.
            if (thisMaterialSelection.Count () > 0) {
                myMat = thisMaterialSelection [0] as Material;
    
                // apply material to selection
                foreach (GameObject reference in thisSelection) {
                    MeshRenderer thisMR = reference.GetComponent<MeshRenderer> ();
                    if (thisMR) {
                        if (thisMR.sharedMaterials.Length > 1) {
                            Material[] thisMat = thisMR.sharedMaterials;
                            for (int i = 0; i < thisMat.Length; i++) {
                                thisMat [i] = myMat;
                            }
                            thisMR.sharedMaterials = thisMat;
                        } else {
                            thisMR.sharedMaterial = myMat;
                        }
                    }
    
                }
    
            }
            
            // Get existing open window or if none, make a new one:
            //      prod_applyMaterialsToSelection = (PROD_ApplyMaterialToSelection)EditorWindow.GetWindow (typeof (PROD_ApplyMaterialToSelection));
        }
    
    }
}
