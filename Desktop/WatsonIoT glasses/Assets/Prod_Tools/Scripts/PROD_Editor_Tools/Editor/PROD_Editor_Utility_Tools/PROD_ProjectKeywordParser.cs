﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using ProdTools;

namespace ProdTools
{
    public static class PROD_ProjectKeywordParser
    {
        // *****************************************************************************************************
        // Parses a text file and returns a list of keywords per row
        // GetListFrom() returns a list of keywords for something like 
        // 'Character_Setup.cs' to use ('burp,attention,snap,wave')
        // 
        // Arguments:
        // keywordsFilePath = path to text file to parse "Assets/Folder/textFile.txt"
        // groupName = all lines of a project's keywords are grouped into blocks by this ('namespace')
        // projectName = name of project associated with these keywords ('min', 'avg', 'fzn')
        // projectKey = group name of keywords at the start of lines of key words ('keyWordIdles')
        // 
        // *****************************************************************************************************
    
        static List<string> keywordsFileLines = new List<string> ();
        static Dictionary<string, List<string>> keywordsDict = new Dictionary<string, List<string>> ();

        static float keywordsFloat;
    
    
        public static List<string> GetListFrom (string keywordsFilePath, string groupName, string projectName, string projectKey)
        {
            keywordsFileLines.Clear ();
            keywordsDict.Clear ();
            
            // read the file to parse
            try {
                keywordsFileLines = PROD_FileUtils.ReadTextFileAt (keywordsFilePath);
            } catch (System.Exception ex) {
                Debug.Log ("No File At Given Path! exc: "+ex.Message);
            }
            
            // build a list from the project name
            List<string> keywordList = new List<string> ();
            bool buildListSwitch = false;
    
            // iterate through each line of the text file after 'projectName'
            // and add to the list until next namespace or end of file
            for (int i = 0; i < keywordsFileLines.Count; i++) {

                // Do not read in text file commented lines ('//')
                if(keywordsFileLines[i].StartsWith("//")) continue;

                string thisLine = keywordsFileLines [i];
    
                if (!buildListSwitch && thisLine.Contains (groupName) && thisLine.Contains (projectName)) {
                    buildListSwitch = true;
                    continue;
                }
    
                if (buildListSwitch && thisLine.Contains (projectKey)) {
                    string[] keyList = thisLine.Split (',');
                    for (int n = 1; n < keyList.Length; n++) {
                        keywordList.Add (keyList [n]);
                    }
                }
    
                if (buildListSwitch && thisLine.Contains (groupName))
                    break;
    
            }
    
            if (keywordList.Count > 0) {
                return keywordList;
            }
    
            Debug.Log ("could not find keyword : "+projectKey);
            return keywordList;
    
        }

        // String return type  
        public static string GetStringFrom (string keywordsFilePath, string groupName, string projectName, string projectKey)
        {
            keywordsFileLines.Clear ();
            keywordsDict.Clear ();
            
            // read the file to parse
            try {
                keywordsFileLines = PROD_FileUtils.ReadTextFileAt (keywordsFilePath);
            } catch (System.Exception ex) {
                Debug.Log ("No File At Given Path! exc:"+ex.Message);
            }
            
            // return boolean
            string keyString = "";
            
            bool buildListSwitch = false;
            
            // iterate through each line of the text file after 'projectName'
            // and add to the list until next namespace or end of file
            for (int i = 0; i < keywordsFileLines.Count; i++) {
                
                // Do not read in text file commented lines ('//')
                if(keywordsFileLines[i].StartsWith("//")) continue;
                
                string thisLine = keywordsFileLines [i];
                
                if (!buildListSwitch && thisLine.Contains (groupName) && thisLine.Contains (projectName)) {
                    buildListSwitch = true;
                    continue;
                }
                
                if (buildListSwitch && thisLine.Contains (projectKey)) {
                    string[] keyList = thisLine.Split (',');
                    // just grab the string after the first comma
                    if(keyList.Length < 2) Debug.Log("No string found after keyword "+keyList[0]);
                    else keyString = keyList[1];

                }
                
                if (buildListSwitch && thisLine.Contains (groupName))
                    break;
                
            }
            
            if(!String.IsNullOrEmpty(keyString)) return keyString;
            else {
                Debug.Log ("could not find keyword : "+projectKey);
            }
            return null;
            
        }


        // Nullable float return type
        public static float? GetFloatFrom (string keywordsFilePath, string groupName, string projectName, string projectKey)
        {
            keywordsFileLines.Clear ();
            keywordsDict.Clear ();
            
            // read the file to parse
            try {
                keywordsFileLines = PROD_FileUtils.ReadTextFileAt (keywordsFilePath);
            } catch (System.Exception ex) {
                Debug.Log ("No File At Given Path!  exc: "+ex.Message);
            }
            // return float number
            float? number = null;

            bool buildListSwitch = false;
            
            // iterate through each line of the text file after 'projectName'
            // and add to the list until next namespace or end of file
            for (int i = 0; i < keywordsFileLines.Count; i++) {
                
                // Do not read in text file commented lines ('//')
                if(keywordsFileLines[i].StartsWith("//")) continue;
                
                string thisLine = keywordsFileLines [i];

                if (!buildListSwitch && thisLine.Contains (groupName) && thisLine.Contains (projectName)) {
                    buildListSwitch = true;
                    continue;
                }
                
                if (buildListSwitch && thisLine.Contains (projectKey)) {
                    string[] keyList = thisLine.Split (',');
                    for (int n = 0; n < keyList.Length; n++) {
                        number = Convert.ToSingle( keyList[1] );
                    }

                }
                
                if (buildListSwitch && thisLine.Contains (groupName))
                    break;
                
            }

            if(number.HasValue) return number;
            else {
                Debug.Log ("could not find keyword : "+projectKey+"     value = "+number);
            }
            return null;

        }

        // Nullable bool return type
        public static bool? GetBoolFrom (string keywordsFilePath, string groupName, string projectName, string projectKey)
        {
            keywordsFileLines.Clear ();
            keywordsDict.Clear ();

            // read the file to parse
            try {
                keywordsFileLines = PROD_FileUtils.ReadTextFileAt (keywordsFilePath);
            } catch (System.Exception ex) {
                Debug.Log ("No File At Given Path! exc:"+ex.Message);
            }
            
            // return boolean
            bool? keyBool = null;
            
            bool buildListSwitch = false;
            
            // iterate through each line of the text file after 'projectName'
            // and add to the list until next namespace or end of file
            for (int i = 0; i < keywordsFileLines.Count; i++) {
                
                // Do not read in text file commented lines ('//')
                if(keywordsFileLines[i].StartsWith("//")) continue;
                
                string thisLine = keywordsFileLines [i];
                
                if (!buildListSwitch && thisLine.Contains (groupName) && thisLine.Contains (projectName)) {
                    buildListSwitch = true;
                    continue;
                }
                
                if (buildListSwitch && thisLine.Contains (projectKey)) {
                    string[] keyList = thisLine.Split (',');
                    for (int n = 0; n < keyList.Length; n++) {
                        if(String.Equals(keyList[1].ToLower(),"true") || String.Equals(keyList[1].ToLower(),"false") 
                           || String.Equals(keyList[1].ToLower(),"1") || String.Equals(keyList[1].ToLower(),"0"))
                        {   keyBool = Convert.ToBoolean(keyList[1]);  }
                        else
                            Debug.Log(" No boolean text value ('true','false','1','0') detected on key: "+projectKey); 
                    }
                    
                }
                
                if (buildListSwitch && thisLine.Contains (groupName))
                    break;
                
            }

            if(keyBool.HasValue) return keyBool;
            else {
                Debug.Log ("could not find keyword : "+projectKey);
            }
            return null;

        }
    
    }


}






