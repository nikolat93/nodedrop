﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace ProdTools
{
    public class PROD_Unparent : EditorWindow
    {

        const string MENUPATH = PROD_Menu.PRODTOOLSMENUPATH + "PROD_Unparent #p";

        static GameObject[] thisSelection;


        [MenuItem (MENUPATH, false, 100)]
        static void Init ()
        {
            thisSelection = Selection.gameObjects;

            // iterate through the selection and set parent to null within the undo stack
            foreach (GameObject go in thisSelection) {
                if (go is GameObject) {
                    Undo.SetTransformParent (go.transform, null, go.name + " unparented.");
                }
            }

        }


        [MenuItem ( MENUPATH, true, 100)]
        static bool ValidateMenuTest ()
        {
            return Selection.activeTransform != null;
        }

    }

}
