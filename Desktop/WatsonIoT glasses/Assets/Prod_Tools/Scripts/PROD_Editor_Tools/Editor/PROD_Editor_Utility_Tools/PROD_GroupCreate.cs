﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using ProdTools;

namespace ProdTools
{
    public class PROD_GroupCreate : MonoBehaviour
    {
    
        const string MENUPATH = PROD_Menu.PRODTOOLSMENUPATH + "PROD_GroupCreate %g";
    
        [MenuItem (MENUPATH, false, 100)]
        static void GroupSelection ()
        {
            List<GameObject> parentsList = new List<GameObject> ();
    
            // get user selection
            Transform[] thisSelection = Selection.transforms;
    
            // create a new group GO and set up the undo stack
            string newGroupName = DoesNameExist ();
    
            GameObject thisNewGroup = new GameObject (newGroupName);
            Undo.RegisterCreatedObjectUndo (thisNewGroup, "new PROD_GroupCreate");
    
            // parent each GO to newly created group GO and set up the undo stack
            foreach (Transform transform in thisSelection) {
                // fill a List of parents
                Transform transformParent = transform.parent;
                if (transformParent) {
                    parentsList.Add (transformParent.gameObject);
                }
    
                // parent them with undo
                Undo.SetTransformParent (transform, thisNewGroup.transform, "parented to PROD_Group");
    
            }
            // if the parents list is only one item then parent the new group to the same parent
            if (parentsList.Count == 1) {
                thisNewGroup.transform.parent = parentsList [0].gameObject.transform;
            }
    
    
            // select the newly created group GO
            Selection.activeGameObject = thisNewGroup.gameObject;
    
        }
    
    
        [MenuItem ( MENUPATH, true, 100)]
        static bool ValidateMenuTest ()
        {
            return Selection.activeTransform != null;
        }
    
    
        static string DoesNameExist ()
        {
            int iter = 1;
            string iterString = string.Format ("{0:000}", iter);
    
            while (GameObject.Find("PROD_Group_" + iterString)) {
                iter++;
                iterString = string.Format ("{0:000}", iter);
            }
    
            return "PROD_Group_" + iterString;
    
        }
    }
}
