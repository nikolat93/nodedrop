﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace ProdTools
{
    public class PROD_Parent : EditorWindow
    {
    
        const string MENUPATH = PROD_Menu.PRODTOOLSMENUPATH + "PROD_Parent _p";
    
        static GameObject[] thisSelection;
        static List<int> goReferences = new List<int> ();
        static PROD_Parent prod_parent_window;
        string errMssg = "";
    
    
        [MenuItem (MENUPATH, false, 100)]
        static void Init ()
        {
            thisSelection = Selection.gameObjects;
    
            // clear the static list from previous useage
            goReferences.Clear ();
            
            // store the old selection
            foreach (GameObject reference in thisSelection) {
                if (reference is GameObject)
                    goReferences.Add (reference.GetInstanceID ());
            }
    
            // Get existing open window or if none, make a new one:
            prod_parent_window = (PROD_Parent)EditorWindow.GetWindow (typeof(PROD_Parent));
        }
    
    
        [MenuItem ( MENUPATH, true, 100)]
        static bool ValidateMenuTest ()
        {
            return Selection.activeTransform != null;
        }
    
    
    
        void OnSelectionChange ()
        {
            GameObject thisSelectedGameObject = Selection.activeGameObject;
    
            // check if there's a selection, and check to see if it's already in the previously selected list
            if (thisSelectedGameObject && (!thisSelection.Contains (thisSelectedGameObject))) {
                foreach (var item in thisSelection) {
                    Undo.SetTransformParent (item.transform, thisSelectedGameObject.transform, "parented to " + thisSelectedGameObject.name);
                }
                prod_parent_window.Close ();
            }
    
            // give a human-readable error
            errMssg = "Select an object to parent your\nprevious selection to! Try again!";
    
            // refocus the editor window
            prod_parent_window.Focus ();
    
    
        }
    
    
    
        void OnGUI ()
        {
            GUILayout.Label ("Select a game object to parent to!", EditorStyles.boldLabel);
    
            GUILayout.Label (errMssg, EditorStyles.boldLabel);
    
            // retrieve the old selection
            Selection.instanceIDs = goReferences.ToArray ();
        }
    
    }
}
