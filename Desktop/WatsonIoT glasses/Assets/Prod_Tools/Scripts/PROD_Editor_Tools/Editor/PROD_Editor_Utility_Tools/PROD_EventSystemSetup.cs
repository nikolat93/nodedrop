﻿using UnityEngine;
using UnityEditor;
using UnityEngine.EventSystems;
using System.Collections;

namespace ProdTools
{
    public class PROD_EventSystemSetup : MonoBehaviour {

    const string MENUPATH = PROD_Menu.PRODTOOLSMENUPATH + "PROD_EventSystemSetup";

        [MenuItem (MENUPATH, false, 200)]
        static void Init()
        {
            GameObject eventSystem;
            GameObject mainCamera;

            // Look for a camera tagged as MainCamera
            mainCamera = GameObject.FindGameObjectWithTag("MainCamera");

            // If no object tagged as 'MainCamera' can be found throw up a warning
            if(mainCamera == null)
            {
                EditorUtility.DisplayDialog("MainCamera Not Found", "An object tagged as 'MainCamera' could not be found. Please tag a camera as the 'MainCamera; and try again.", "OK");
            }
            else
            {
                // If the MainCamera doesn't have a Physics Raycaster add one
                if(mainCamera.GetComponent<PhysicsRaycaster>() == null)
                {
                    mainCamera.AddComponent<PhysicsRaycaster>();
                }

            }

            // Look for an object named EventSystem. If one exists, throw up a warning
            eventSystem = GameObject.Find("EventSystem");
            if(eventSystem != null)
            {
                EditorUtility.DisplayDialog ("Cannot Create EventSystem", "An object named 'EventSystem' already exists in the scene.", "OK");
                return;
            }
            else
            {
                // If an object named 'EventSystem' doesn't exist, create one add the necessary components to work with Unity's Event System
                if(eventSystem == null)
                {
                    eventSystem = new GameObject ("EventSystem");
                    eventSystem.AddComponent<EventSystem>();
                    eventSystem.AddComponent<StandaloneInputModule>();
                }
            }
        }
    }
}