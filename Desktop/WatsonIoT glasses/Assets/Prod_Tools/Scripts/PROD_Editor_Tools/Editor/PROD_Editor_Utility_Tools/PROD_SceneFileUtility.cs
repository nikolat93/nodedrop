﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using UnityEditor;
using UnityEditor.SceneManagement;
using System.Text.RegularExpressions;
using ProdTools;

//TODO Add a feature where if a folder is selected in the project view, save into that folder.

namespace ProdTools
{
    [Serializable]
    public class PROD_SceneFileUtility : EditorWindow
    {   
        const string MENUPATH = PROD_Menu.PRODTOOLSMENUPATH + "PROD_SceneFileUtility";
        
        static string thisUserName = PROD_User.MyPreferredUserName();
    
        static PROD_RecentScenes recentScenes;
        
        static List<string> sceneList;  
    
        
        [MenuItem (MENUPATH, false, 24)]
        public static void Init ()
        {   
            EditorWindow.GetWindow<PROD_SceneFileUtility>();
        }
        
        
        void OnEnable ()
        {   
            recentScenes = new PROD_RecentScenes();
            recentScenes.CreateRecentScenesScriptableObject();

            if (!Directory.Exists (PROD_User.GetWorkPath() + "/Scenes")) {
                Directory.CreateDirectory (PROD_User.GetWorkPath() + "/Scenes");
            }

            sceneList = recentScenes.GetList();
        }
    
    
        private string GetCurrentScene ()
        {           
            return EditorSceneManager.GetActiveScene().path;
        }
    
    
        private void OpenThisScene (string thisScene)
        {
            if (!EditorSceneManager.GetActiveScene().isDirty) {
                EditorSceneManager.OpenScene(thisScene);
            }
            else if (EditorUtility.DisplayDialog("You have unsaved changes!", "Are you sure you want to load?", "Yes, load the scene.", "DO NOT LOAD!")) {
                EditorSceneManager.OpenScene(thisScene);
                Debug.Log("Ok... you chose to throw out the previous scene changes. Hope they didn't matter!");
            }           
        }
    
    
        private void SaveThisScene (string thisScene)
        {           
            EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene(), thisScene, false);            
        }
    
    
        void OnGUI ()
        {   
            GUIStyle style = new GUIStyle(GUI.skin.GetStyle("label"));
            style.alignment = TextAnchor.MiddleCenter;
            style.normal.textColor = Color.white;
    
            GUIStyle styleButtons = new GUIStyle(GUI.skin.GetStyle("Button"));
            styleButtons.alignment = TextAnchor.MiddleCenter;
            styleButtons.normal.textColor = new Color(.75f, .75f, .75f, 1f);

            GUIStyle styleButtons2 = new GUIStyle(GUI.skin.GetStyle("Button"));
            styleButtons2.alignment = TextAnchor.MiddleCenter;
            styleButtons2.normal.textColor = new Color(.75f, .75f, .75f, 1f);

            GUIStyle styleXButton = new GUIStyle(GUI.skin.GetStyle("Button"));
            styleXButton.alignment = TextAnchor.MiddleCenter;
            styleXButton.normal.textColor = new Color(.75f, .75f, .75f, 1f);
    
            float elementWidthMin = 150f;
    
    
            EditorGUILayout.BeginVertical();
            GUILayout.Space(10f);
            
            EditorGUILayout.BeginHorizontal();
            GUILayout.Label("SCENE FILE UTILITY", style);
            EditorGUILayout.EndHorizontal();
    
    
            EditorGUILayout.BeginHorizontal();
            GUILayout.Label("OPEN", style, GUILayout.MinWidth(elementWidthMin), GUILayout.MaxHeight(24.0f));
    
            GUILayout.Label("SAVE", style, GUILayout.MinWidth(elementWidthMin), GUILayout.MaxHeight(24.0f));
            EditorGUILayout.EndHorizontal();
    
    
            // =============================================== "Open Latest Scene" and "Scene Version Up" BUTTONS
            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("Open Latest Favorite", styleButtons2, GUILayout.MinWidth(elementWidthMin), GUILayout.MinHeight(30.0f)))
            {
                if (File.Exists(recentScenes.GetMostRecent()))
                {
                    OpenThisScene(recentScenes.GetMostRecent());
                }
                else
                {
                    Debug.Log("Couldn't Open Last Saved.");
                }
            }
            if (GUILayout.Button("Save Version Up", styleButtons2, GUILayout.MinWidth(elementWidthMin), GUILayout.MinHeight(30.0f)))
            {
                PROD_SceneVersionUpClass sceneVersionUp = new PROD_SceneVersionUpClass();
                sceneVersionUp.VersionUp();
            }
            EditorGUILayout.EndHorizontal();
    
            GUILayout.Space(10f);
    
    
            // =============================================== "Browse Current Path" BUTTONS
            EditorGUILayout.BeginHorizontal();
            GUILayout.Space(5f);
            if (GUILayout.Button("Open From Current Path", styleButtons2, GUILayout.MinWidth(elementWidthMin), GUILayout.MinHeight(22.0f)))
            {
                string thisScene = EditorUtility.OpenFilePanel("Open From Current Scene Location", 
                                                               Path.GetDirectoryName(GetCurrentScene()), 
                                                               "unity");
                
                if (thisScene != "")
                {
                    OpenThisScene (thisScene);
                }
            }
            if (GUILayout.Button("Save As to Current Path", styleButtons2, GUILayout.MinWidth(elementWidthMin), GUILayout.MinHeight(22.0f)))
            {
                string thisScene = EditorUtility.SaveFilePanel("Save To Current Scene Location",
                                                                Path.GetDirectoryName (GetCurrentScene()),
                                                                Path.GetFileName (GetCurrentScene()),
                                                                "unity");
                if (thisScene != "")
                {
                    SaveThisScene(PROD_FileUtils.FullPathToRelativePath(thisScene));
                }
            }
            EditorGUILayout.EndHorizontal();
            
            
            // =============================================== WORK BUTTONS
            EditorGUILayout.BeginHorizontal();
            GUILayout.Space(5f);
            if (GUILayout.Button("Open From Work/" + thisUserName, styleButtons2, GUILayout.MinWidth(elementWidthMin), GUILayout.MinHeight(22.0f)))
            {
                string thisScene = EditorUtility.OpenFilePanel("Open From Your Work Path", 
                                                               PROD_User.GetWorkPath() + "/Scenes", 
                                                               "unity");
                
                if (thisScene != "")
                {
                    OpenThisScene(thisScene);
                }
            }
            if (GUILayout.Button("Save As to Work/" + thisUserName, styleButtons2, GUILayout.MinWidth(elementWidthMin), GUILayout.MinHeight(22.0f)))
            {
                string thisScene = EditorUtility.SaveFilePanel("Save To Your Work Path", 
                                                               PROD_User.GetWorkPath() + "/Scenes", 
                                                               Path.GetFileName (GetCurrentScene()), 
                                                               "unity");
                if (thisScene != "")
                {
                    SaveThisScene(PROD_FileUtils.FullPathToRelativePath(thisScene));
                }
            }
            EditorGUILayout.EndHorizontal();
    
            GUILayout.Space(10f);
    
    
            // =============================================== "Recent Scenes", "Recent Folders" and "+" BUTTONS
            EditorGUILayout.BeginHorizontal();
            GUILayout.Space(5f);
            style.alignment = TextAnchor.MiddleCenter;
            GUILayout.Label("FAVORITES LIST", style, GUILayout.MinWidth(elementWidthMin), GUILayout.MinHeight(22.0f));
            EditorGUILayout.EndHorizontal();
    
    
            // =============================================== "Recent Scenes", "Recent Folders" and "+" BUTTONS
            EditorGUILayout.BeginHorizontal();
            GUILayout.Space(5f);
            GUILayout.Label("");
            if (GUILayout.Button ("Add Current & Refresh", GUILayout.MinWidth (elementWidthMin), GUILayout.MaxWidth(elementWidthMin + 30f), 
                                 GUILayout.MinHeight(22.0f)))
            {
                recentScenes.AddToList(GetCurrentScene());
                sceneList = recentScenes.GetList();
            }
            GUILayout.Label("");
    
            EditorGUILayout.EndHorizontal();
    
            EditorGUILayout.BeginVertical();
            BeginWindows();
            for (int i = 0; i < sceneList.Count; i++)
            {
                string[] sceneNameSplit = sceneList[i].Split('/');
                string sceneName = sceneNameSplit[sceneNameSplit.Length-1];
                EditorGUILayout.BeginHorizontal();
                if (GUILayout.Button(new GUIContent(sceneName, sceneList[i]), styleButtons, GUILayout.MinWidth(200f) ))
                {
                    if (File.Exists(sceneList[i]))
                    {
                        OpenThisScene(sceneList[i]);
                    }
                    else
                    {
                        Debug.Log("Scene No Longer Exists. Deleting Entry: " + sceneList [i]);
                        recentScenes.RemoveFromList(sceneList[i]);
                        sceneList = recentScenes.GetList();
                    }
                }
                if (GUILayout.Button("X", styleXButton, GUILayout.MinWidth(22f), GUILayout.MaxWidth(22f)))
                {
                    recentScenes.RemoveFromList(sceneList[i]);
                    sceneList = recentScenes.GetList();
                }
                EditorGUILayout.EndHorizontal();
            }
            EndWindows();
            EditorGUILayout.EndVertical();
    
            EditorGUILayout.EndVertical();  
        }       
    }
}
