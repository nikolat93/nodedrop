﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Linq;
using ProdTools;

/// <summary>
/// Assigns the selected material to the global Skybox in the Lighting tab.
/// </summary>
public class PROD_AssignSkybox : MonoBehaviour {

    const string MENUPATH = PROD_Menu.PRODTOOLSMENUPATH + "PROD_AssignSkybox &s";

    // Using an array to filter the selection as a Material
    static Object[] thisMaterialSelection;

    [MenuItem (MENUPATH, false, 100)]
    static void Init ()
    {
        thisMaterialSelection = Selection.GetFiltered(typeof(Material), SelectionMode.Assets);
        Material skyboxMat;

        // Checks if we have more than one material selected. If we do, throw up a debug warning
        if (thisMaterialSelection.Count () > 1)
        {
            Debug.Log("More than one material selected. Select a single material and try again.");
            return;
        }

        // If we have exactly one material selected, find out what shader it's using. If it's using a Skybox/Cubemap shader, assign it to the global skybox. If it isn't throw up a debug warning
        if(thisMaterialSelection.Count () == 1)
        {
            skyboxMat = thisMaterialSelection [0] as Material;

            if(skyboxMat.shader == Shader.Find("Skybox/Cubemap"))
            {
                RenderSettings.skybox = skyboxMat;
            }
            else
            {
                Debug.Log("The Selected Material is not using a Skybox/Cubemap shader. To Assign a Material to the Global Skybox a Skybox Material must be selected.");
            }
        }
    }
}
