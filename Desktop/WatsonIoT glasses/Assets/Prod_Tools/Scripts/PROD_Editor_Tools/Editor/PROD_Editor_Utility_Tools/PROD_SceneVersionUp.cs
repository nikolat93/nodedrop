﻿using UnityEngine;
using UnityEditor;
using ProdTools;

namespace ProdTools
{
    public class PROD_SceneVersionUp
    {
    
        const string MENUPATH = PROD_Menu.PRODTOOLSMENUPATH + "PROD_SceneVersionUp #&s";
    
        [MenuItem (MENUPATH, false, 25)]
        static void SceneVersionUp ()
        {
    
            PROD_SceneVersionUpClass sceneVersionUp = new PROD_SceneVersionUpClass ();
            sceneVersionUp.VersionUp ();
    
        }
    
    }
}
