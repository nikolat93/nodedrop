﻿using UnityEngine;
using UnityEditor;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using ProdTools;

public class PROD_CreateShowPath : EditorWindow {

    const string MENUPATH = PROD_Menu.PRODTOOLSMENUPATH + "PROD_CreateShowPath";
    
    static PROD_CreateShowPath showSetupWindow;

    // The string used in the text field for PROD_CreateShowPath
    public string showName = "";

    // The user's work path
    static string userWorkPath = PROD_User.GetWorkPath();
    // The Assets folder in Unity
    static string assetPath = "Assets";
    // This string changes with the useWorkPath bool
    static string subfolderRootPath;
    // When true the show is created under the user's work folder, otherwise it's created under the Assets folder
    static bool useWorkPath;

    [MenuItem (MENUPATH, false, 300)]
    static void Init ()
    {
        
        // Get existing open window or if none, make a new one
        showSetupWindow = (PROD_CreateShowPath)EditorWindow.GetWindow (typeof(PROD_CreateShowPath));
        showSetupWindow.Show ();
    }

    void OnGUI ()
    {
        GUILayout.Label ("Create Show Folder Structure Setup", EditorStyles.boldLabel);
        
        // Text Field for creating a custom path
        showName = EditorGUILayout.TextField ("Enter Show Name", showName);
        // Toggleable bool used to create subfolders under the user _Work folder or under Assets
        useWorkPath = EditorGUILayout.Toggle("Use Work Path", useWorkPath);
        // Changes the subfolderRootPath depending on the toggled option
        if(useWorkPath)
            subfolderRootPath = userWorkPath;
        else
            subfolderRootPath = assetPath;
        
        // Checks to see if the path exists. If it doesn't then create the path with subfolder structure
        if (GUILayout.Button ("Confirm")) 
        {
            if(showName == "")
            {
                EditorUtility.DisplayDialog ("Invalid Name", "The text field is empty. Please enter a name and try again.", "OK");
            }
            else
            {
                CheckShowPath();
            }
        }
    }

    // Creates the Show Path with subfolder setup if it doesn't already exist
    void CheckShowPath()
    {
        if(Directory.Exists(subfolderRootPath + "/" + showName))
        {
            ExistingPath();
            return;
        }
        else
        {
            CreateFolders ();
        }
    }

    // Dialog box telling the user that the show name written in the text box is an existing path in the project
    void ExistingPath()
    {
        EditorUtility.DisplayDialog ("The Path Already Exists!", "The selected Show Name is already an existing path. Input a different path name or use the existing one.", "OK");
    }

    // Creates the root folder and all of the subfolders under it
    void CreateFolders() 
    {
        AssetDatabase.CreateFolder (subfolderRootPath, showName);
        AssetDatabase.CreateFolder (subfolderRootPath + "/" + showName, "Animation");
        AssetDatabase.CreateFolder (subfolderRootPath + "/" + showName, "Animator_Controller");
        AssetDatabase.CreateFolder (subfolderRootPath + "/" + showName, "Audio");
        AssetDatabase.CreateFolder (subfolderRootPath + "/" + showName, "Materials");
        AssetDatabase.CreateFolder (subfolderRootPath + "/" + showName, "Mesh");
        AssetDatabase.CreateFolder (subfolderRootPath + "/" + showName, "Prefabs");
        AssetDatabase.CreateFolder (subfolderRootPath + "/" + showName, "Rig");
        AssetDatabase.CreateFolder (subfolderRootPath + "/" + showName, "Scenes");
        AssetDatabase.CreateFolder (subfolderRootPath + "/" + showName, "Scripts");
        AssetDatabase.CreateFolder (subfolderRootPath + "/" + showName, "Shaders");
        AssetDatabase.CreateFolder (subfolderRootPath + "/" + showName, "Textures");
    }
}
