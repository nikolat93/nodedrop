﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using UnityEditor;
using System.Text.RegularExpressions;
using ProdTools;
using Object=UnityEngine.Object;


namespace ProdTools
{
    public class PROD_PrefabVersionUp : MonoBehaviour
    {
    
        const string MENUPATH = PROD_Menu.PRODTOOLSMENUPATH + "PROD_PrefabVersionUp #&p";
        const string MENUPATHROLLBACK = PROD_Menu.PRODTOOLSMENUPATH + "PROD_PrefabRollBack #&r";
        const string MENUPATHROLLFORWARD = PROD_Menu.PRODTOOLSMENUPATH + "PROD_PrefabRollForward #&f";

    
        [MenuItem (MENUPATH, false, 50)]
        static void PrefabVersionUp ()
        {
            PrefabVersioner prefabVersioner = new PrefabVersioner();

            prefabVersioner.PrefabVersionUp();

        }



        [MenuItem (MENUPATHROLLBACK, false, 50)]
        static void PrefabRollBack ()
        {
            PrefabVersioner prefabVersioner = new PrefabVersioner();

            prefabVersioner.PrefabChangeVersion (false);

        }



        [MenuItem (MENUPATHROLLFORWARD, false, 50)]
        static void PrefabRollForward ()
        {
            PrefabVersioner prefabVersioner = new PrefabVersioner();

            prefabVersioner.PrefabChangeVersion (true);

        }
    

    
        [MenuItem ( MENUPATH, true, 50)]
        static bool ValidateVersionUp ()
        {
            return Selection.activeTransform != null;
        }

        [MenuItem ( MENUPATHROLLBACK, true, 50)]
        static bool ValidateRollBack ()
        {
            return Selection.activeTransform != null;
        }

        [MenuItem ( MENUPATHROLLFORWARD, true, 50)]
        static bool ValidateRollForward ()
        {
            return Selection.activeTransform != null;
        }
    
    }

    public class PrefabVersioner : Object
    {

        char sep = Path.DirectorySeparatorChar;
        // default path where prefabs are saved
        string relPath = "";

        GameObject thisSelectedGO;
        GameObject thisSelectedRootGO;
        string thisSelectedType = "";


        public void PrefabVersionUp () {
            
            ProcessRelPath ();

            // create folders if they don't exist
            if (!Directory.Exists (relPath)) {
                Directory.CreateDirectory (relPath);
            }

            // check folder for all versions of this prefab and create a list.
            List<string> filesList = new List<string> ();

            filesList = BuildList ();

            // create new prefab name and save prefab
            string thisNewPrefabName = PROD_VersionUp.NewVersion (thisSelectedRootGO.name, ".prefab", filesList);

            // save prefab with version number
            GameObject newPrefabObject = PrefabUtility.CreatePrefab (relPath + thisNewPrefabName + ".prefab", thisSelectedRootGO);

            PrefabUtility.ReplacePrefab (thisSelectedRootGO, newPrefabObject, ReplacePrefabOptions.ConnectToPrefab);

        }



        public void PrefabChangeVersion ( bool direction ) {

            ProcessRelPath ();

            // check folder for all versions of this prefab and create a list.
            List<string> filesList = new List<string> ();

            filesList = BuildList ();

            // fetch the next lowest version
            string verString = PROD_FileUtils.FullPathToRelativePath(GetVersion ( filesList, direction ));

            GameObject Prefab = (GameObject) AssetDatabase.LoadAssetAtPath(verString, typeof(GameObject));
            GameObject go = thisSelectedGO;
            GameObject newObject = (GameObject)PrefabUtility.InstantiatePrefab(Prefab);

            newObject.transform.SetParent(go.transform.parent, true);
            newObject.transform.localPosition = go.transform.localPosition;
            newObject.transform.localRotation = go.transform.localRotation;
            newObject.transform.localScale = go.transform.localScale;
            newObject.transform.name = go.transform.name;
            DestroyImmediate(go);
            Selection.activeGameObject = newObject;

        }



        public List<string> BuildList () {

            // list to build all versions of our prefab into
            List<string> filesList = new List<string> ();

            DirectoryInfo dir = new DirectoryInfo (relPath);
            FileInfo[] myFiles = dir.GetFiles ("*.prefab");

            // get a list of all versions of this prefab
            foreach (var item in myFiles) {
                if (Regex.IsMatch (item.ToString (), thisSelectedRootGO.name)) {
                    filesList.Add (item.ToString ());
                }
            }

            return filesList;

        }


        public string GetVersion ( List<string> thisList, bool direction ) {

            string verString = "";
            int curIndex = 0;

            NavigationList<string> n = new NavigationList<string>();

            n.AddRange(thisList);

            GameObject gamePrefab = PrefabUtility.FindPrefabRoot(Selection.activeGameObject);

            Object prefabParent =  PrefabUtility.GetPrefabParent(gamePrefab);

            verString = prefabParent.name;

            foreach (string item in thisList) {
                if ( item.Contains(verString) ) {
                    curIndex = thisList.IndexOf(item);
                    n.CurrentIndex = curIndex;
                }
            }

            // if direction is true, roll forward
            if (direction) {
                verString = n.MoveNext;
                Debug.Log("rolled forward to " + GetNewVersionNumberFromString(verString));
            }

            // if direction is false, roll back
            if (!direction) {
                verString = n.MovePrevious;
                Debug.Log("rolled back to " + GetNewVersionNumberFromString(verString));
            }

            return verString;
        }


        public void ProcessRelPath () {

            // figures out the relative path of the selection

            thisSelectedGO = Selection.activeGameObject;
            // get the root of the selection in case
            thisSelectedRootGO = thisSelectedGO.transform.root.gameObject;
            thisSelectedRootGO = thisSelectedGO;
            // prefab type
            thisSelectedType = PrefabUtility.GetPrefabType (thisSelectedRootGO).ToString ();

            // if selection is a prefab, check the path location
            if (thisSelectedType != "None") {
                // change default path to wherever existing prefab parent lives
                relPath = Path.GetDirectoryName (AssetDatabase.GetAssetPath (PrefabUtility.GetPrefabParent (thisSelectedRootGO))) + sep;
            } else {
                relPath = "Assets" + sep + "Prefabs"  + sep + thisSelectedRootGO.name + sep;
            }

        }


        public string GetNewVersionNumberFromString ( string thisString ) {
            
            return Regex.Match (thisString, @"_v\d{3}").ToString();

        }

    }


    public class NavigationList<T> : List<T>
    {
        private int _currentIndex = 0;
        public int CurrentIndex
        {
            get
            {
                if (_currentIndex > Count - 1) { _currentIndex = Count - 1; }
                if (_currentIndex < 0) { _currentIndex = 0; }
                return _currentIndex;
            }
            set { _currentIndex = value; }
        }

        public T MoveNext
        {
            get { _currentIndex++; return this[CurrentIndex]; }
        }

        public T MovePrevious
        {
            get { _currentIndex--; return this[CurrentIndex]; }
        }

        public T Current
        {
            get { return this[CurrentIndex]; }
        }
    }

}
