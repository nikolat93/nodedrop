﻿using UnityEngine;
using UnityEditor;
using UnityEditor.Animations;
using System;
using System.IO;
using System.Text.RegularExpressions;

namespace ProdTools
{
    public class PROD_ApplyAnimatorSetup : Editor
    {
        
        const string MENUPATH = PROD_Menu.PRODTOOLSMENUPATH + "PROD_ApplyAnimatorSetup";
    
        [MenuItem ( MENUPATH, false, 200)]
        static void ApplySetup ()
        {
            // get list of selected items
            GameObject[] thisGOSelection = Selection.gameObjects;
    
    
            // set up paths
            char sep = Path.DirectorySeparatorChar;
            string animationFolder = "Assets" + sep + "Animations" + sep + "PROD_AnimatorSetups" + sep;
            CreateFolder (animationFolder);
    
    
            // go through each item
            foreach (GameObject n in thisGOSelection) {
                // get name
                string assetNameGO = n.name;
    
                // create "Assets/Animation/<assetname>" folder
                string controllerPath = animationFolder + assetNameGO + sep;
                CreateFolder (controllerPath);
    
    
                // add Animator component if none exists
                Animator animator;
    
                if (!n.GetComponent<Animator> ()) {
                    animator = n.AddComponent<Animator> () as Animator;
                } else {
                    Debug.Log (assetNameGO + " already has an Animator component! Please remove it and try again.");
                    break;
                }
    
    
                // create Animation "idle" clip with prefix name inside "Assets/Animation/<assetname>" folder
                AnimationClip idleAnimationClip = new AnimationClip ();
                idleAnimationClip.name = assetNameGO + "_Idle_clip";
                AssetDatabase.CreateAsset ((AnimationClip)idleAnimationClip, controllerPath + assetNameGO + "_Idle_clip.anim");
                
                // create Animation "active" clip with prefix name inside "Assets/Animation/<assetname>" folder
                AnimationClip activeAnimationClip = new AnimationClip ();
                activeAnimationClip.name = assetNameGO + "_Active_clip";
                AssetDatabase.CreateAsset ((AnimationClip)activeAnimationClip, controllerPath + assetNameGO + "_Active_clip.anim");
    
    
                // add Animation clips into AnimatorController (idle is default clip)
                string controllerName = assetNameGO + "_ctrl";
                AnimatorController animatorCtrl = AnimatorController.CreateAnimatorControllerAtPath (controllerPath + controllerName + ".controller");
                animatorCtrl.name = controllerName;
    
                animator.runtimeAnimatorController = animatorCtrl;
                AnimatorStateMachine rootStateMachine = animatorCtrl.layers [0].stateMachine;
    
                AnimatorState idleState = rootStateMachine.AddState (idleAnimationClip.name);
                idleState.motion = idleAnimationClip;
    
                AnimatorState activeState = rootStateMachine.AddState (activeAnimationClip.name);
                activeState.motion = activeAnimationClip;
                
    
                // this requires PROD_AnimationClipSettings class to access it's serialized properties ==============================================
                SerializedObject serializedClip = new SerializedObject (idleAnimationClip);
                PROD_AnimationClipSettings clipSettings = new PROD_AnimationClipSettings (serializedClip.FindProperty ("m_AnimationClipSettings"));
                
                clipSettings.loopTime = true;
                
                serializedClip.ApplyModifiedProperties ();
                // ================================================================================================================================
    
                
                // add parameter "bool" type with "<assetname>_active_trigger"
                animatorCtrl.AddParameter (assetNameGO + "_active_trigger", AnimatorControllerParameterType.Trigger);
    
                // make transition from idle to active and set it's "Conditions" with "<assetname>_active_trigger" = true
                AnimatorStateTransition idleTransition = idleState.AddTransition (activeState);
                idleTransition.AddCondition (UnityEditor.Animations.AnimatorConditionMode.If, 0, assetNameGO + "_active_trigger");
                idleTransition.duration = 0.0f;
    
                // make transition from active to idle
                AnimatorStateTransition activeTransition = activeState.AddTransition (idleState);
                activeTransition.duration = 0.1f;
                activeTransition.hasExitTime = true;
    
            }
    
        }
    
    
        [MenuItem ( MENUPATH, true, 200)]
        static bool ValidateMenuTest ()
        {
            return Selection.activeTransform != null;
        }
    
    
    
        static void CreateFolder (string myPath)
        {
            
            // create Animation folder if it doesn't exist
            if (!Directory.Exists (myPath)) {
                Directory.CreateDirectory (myPath);
            }
            
        }
    
    }
}
