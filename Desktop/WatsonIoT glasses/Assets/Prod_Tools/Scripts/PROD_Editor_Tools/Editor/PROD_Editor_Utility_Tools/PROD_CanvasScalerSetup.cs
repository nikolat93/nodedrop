﻿using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using ProdTools;

public class PROD_CanvasScalerSetup : MonoBehaviour {

    const string MENUPATH = PROD_Menu.PRODTOOLSMENUPATH + "PROD_CanvasScalerSetup";
    static PROD_CanvasScalerSetup canvasScalerSetup;

    [MenuItem (MENUPATH, false, 200)]
    static void Init()
    {
        CanvasScaler uiCanvasScaler;

        uiCanvasScaler = FindObjectOfType<CanvasScaler>();

        if(uiCanvasScaler != null)
        {
            uiCanvasScaler.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
            uiCanvasScaler.referenceResolution = new Vector2 (1242.0f, 2208.0f);
            uiCanvasScaler.screenMatchMode = CanvasScaler.ScreenMatchMode.MatchWidthOrHeight;
            uiCanvasScaler.matchWidthOrHeight = 0.5f;
            uiCanvasScaler.referencePixelsPerUnit = 100f;
            Debug.Log("Canvas Scaler Settings Updated");
        }
        else
        {
            // If no UI Canvas exists in the scene, give us the option to create one and then update the settings after it has been created
            if(EditorUtility.DisplayDialog("Canvas Scaler Not Found in the Scene", "Do you want to create a UI Canvas now?", "Yes", "No"))
            {
                GameObject uiCanvas = new GameObject("Canvas");

                uiCanvas.AddComponent<Canvas>().renderMode = RenderMode.ScreenSpaceOverlay;
                uiCanvas.AddComponent<CanvasScaler>();
                uiCanvas.AddComponent<GraphicRaycaster>();
                uiCanvas.layer = 5;

                uiCanvasScaler = uiCanvas.GetComponent<CanvasScaler>();

                uiCanvasScaler.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
                uiCanvasScaler.referenceResolution = new Vector2 (1242.0f, 2208.0f);
                uiCanvasScaler.screenMatchMode = CanvasScaler.ScreenMatchMode.MatchWidthOrHeight;
                uiCanvasScaler.matchWidthOrHeight = 0.5f;
                uiCanvasScaler.referencePixelsPerUnit = 100f;

                Selection.activeGameObject = uiCanvas;
            }
        }
    }
}
