﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using ProdTools;

namespace ProdTools
{   
    
    public class PROD_RenameSelection : EditorWindow
    {
    
        const string MENUPATH = PROD_Menu.PRODTOOLSMENUPATH + "PROD_RenameSelection";
    
        static PROD_RenameSelection dcRenameSelectionWindow;
    
        string mySearchPattern = "";
        string myReplacePattern = "";
        string myPrefix = "";
        string mySuffix = "";
        bool selectAllChildren = true;
    
    
        [MenuItem (MENUPATH, false, 150)]
        static void Init ()
        {
    
            // Get existing open window or if none, make a new one:
            dcRenameSelectionWindow = (PROD_RenameSelection)EditorWindow.GetWindow (typeof(PROD_RenameSelection));
            dcRenameSelectionWindow.Show ();
    
        }
    
    
        [MenuItem ( MENUPATH, true, 150)]
        static bool ValidateMenuTest ()
        {
            return Selection.activeTransform != null;
        }
    
        [SerializeField]
        void OnGUI ()
        {
    
            GUILayout.Label ("Search and replace patterns in names", EditorStyles.boldLabel);
            mySearchPattern = EditorGUILayout.TextField ("Search", mySearchPattern);
            myReplacePattern = EditorGUILayout.TextField ("Replace", myReplacePattern);
            myPrefix = EditorGUILayout.TextField ("Prefix", myPrefix);
            mySuffix = EditorGUILayout.TextField ("Suffix", mySuffix);
            selectAllChildren = EditorGUILayout.Toggle ("Include All Children", selectAllChildren);
    
            if (GUILayout.Button ("OK")) {
                if (mySearchPattern == "" && myPrefix == "" && mySuffix == "") {
                    Debug.Log ("No search pattern, prefix nor suffix entered!");
                } else {
                    RenameStart (mySearchPattern, myReplacePattern);
                }
            }
    
        }
    

        void RenameStart (string searchString, string replaceString)
        {
    
            GameObject[] thisGOSelection = Selection.gameObjects;
    
            if (thisGOSelection.Length > 0) {
                foreach (var n in thisGOSelection) {
                    if (selectAllChildren) {
                        Transform[] childTransforms = n.GetComponentsInChildren<Transform> (true);
    
                        foreach (var child in childTransforms) {
                            Rename (child.gameObject, searchString, replaceString);
                            if (myPrefix != "") {
                                string tempString = myPrefix + "_" + child.name;
                                child.name = tempString;
                            }
                            if (mySuffix != "") {
                                string tempString = child.name + "_" + mySuffix;
                                child.name = tempString;
                            }
                        }
                    } else {
                        Rename (n, searchString, replaceString);
                        if (myPrefix != "") {
                            string tempString = myPrefix + "_" + n.name;
                            n.name = tempString;
                        }
                        if (mySuffix != "") {
                            string tempString = n.name + "_" + mySuffix;
                            n.name = tempString;
                        }
                    }
    
                }
                //dcRenameSelectionWindow.Close ();
            } else
                Debug.Log ("Select something to rename!");
    
        }
    
    
        void Rename (GameObject thisGO, string searchString, string replaceString)
        {
            
            // rename game object name if there's a match
            if (Regex.IsMatch (thisGO.name, searchString)) {
                thisGO.name = Regex.Replace (thisGO.name, searchString, replaceString);
            }
    
        }
    
    }
}
