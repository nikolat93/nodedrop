﻿using UnityEngine;
using System.Collections;

namespace ProdTools
{
    public static class PROD_Menu
    {
    
        public const string ROOTMENUPATH = "Prod_Tools/";
        
        public const string PRODTOOLSMENUPATH = ROOTMENUPATH + "PROD Editor Tools/";
        
        public const string PRODCHARTOOLSMENUPATH = ROOTMENUPATH + "PROD Character Tools/";
    
    }
}
