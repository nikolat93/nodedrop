﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System;
using ProdTools;


public class PROD_User
{
    public const string CFGDATA = "Configdata"; // name of folder for storing useful configuration data for editor scripts, etc.
    
    public static string MyPreferredUserName ()
    {
        // create a dictionary to correct OS usernames with preferred usernames
        Dictionary<string, string> usernamesDict = new Dictionary<string, string>();

        usernamesDict.Add("matthiaswittman",    "matthiaswittmann");
        usernamesDict.Add("daqri206",           "joshmorris");
        usernamesDict.Add("Dave",               "davidcarlson");    // TODO this temporarily solves multiple computer login issue but there might be a better solution with Unity login

        // key the value and return it
        string thisUser;
        if (usernamesDict.TryGetValue(Environment.UserName, out thisUser))
        {
            return thisUser;
        }
        else return MyUserName ();
    }


    public static string MyUserName ()
    {
        return Environment.UserName;
    }


    public static string GetWorkPath ()
    {
        return "Assets/_Work/" + MyPreferredUserName ();
    }


    public static string GetUserDesktopPath ()
    {
        return Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
    }


    public static string GetProjectName()
    {
        string[] s = Application.dataPath.Split('/');
        string projectName = s[s.Length - 2];
        return projectName;
    }
}
