﻿using UnityEngine;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using ProdTools;

namespace ProdTools
{
    public static class PROD_FileUtils
    {
        
        public static char Sep  // OS path delimiter for convenience
        {
            get
            {
                return Path.DirectorySeparatorChar;
            }
        }

        //temp
        public static string FullPathToRelativePath (string thisPath)
        {
            string[] splitPath = Regex.Split (thisPath, @"/Assets/");
            return "Assets/" + splitPath [1];
        }
        
        
        public static void CreateTextFileAt (string thisScenePath)
        {
            // create a file with entry and save to folder
            try {
                using (StreamWriter writer = new StreamWriter(thisScenePath)) {
                    writer.Write ("");
                }
            } catch (IOException) {
                Debug.Log ("Couldn't write " + thisScenePath);
            }
        }
        
        
        public static List<string> ReadTextFileAt (string txtFile)
        {
            List<string> list = new List<string> ();
            using (StreamReader reader = new StreamReader(txtFile)) {
                string line;
                while ((line = reader.ReadLine()) != null) {
                    list.Add (line); // Add to list.
                }
            }
            return list;
        }
    }
}
