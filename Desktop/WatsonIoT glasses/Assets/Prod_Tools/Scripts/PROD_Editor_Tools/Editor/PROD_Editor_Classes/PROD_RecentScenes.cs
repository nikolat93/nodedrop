﻿using UnityEngine;
using UnityEditor;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using ProdTools;

public class PROD_RecentScenes
{

    public List<string> pathsList;

    int maxListSize = 15;
    char sep = PROD_FileUtils.Sep;
    PROD_RecentSceneItemList asset;


    void OnEnable ()
    {
        asset = ReadScriptableObject();
    }


    private string CFGFolderPath
    {
        get
        {
            return PROD_User.GetWorkPath() + sep + PROD_User.CFGDATA;
        }
    }


    private string CFGAssetName
    {
        get
        {
            return PROD_User.MyPreferredUserName() + "_RecentScenesList.asset";
        }
    }


    public void AddToList ( string sceneName )
    {
        // read the file
        pathsList = GetList();

        if ( pathsList.Contains (sceneName) )
        {
            // if it already exists in list remove it
            pathsList.Remove (sceneName);
        }

        // insert at the top of the list
        pathsList.Insert (0, sceneName);

        // drop the oldest list member if the list is at maximum size
        if ( pathsList.Count > maxListSize )
        {
            pathsList.RemoveAt (pathsList.Count-1);
        }

        // update the file
        UpdateList(pathsList);
    }


    public void RemoveFromList ( string sceneName )
    {
        // read the file
        pathsList = GetList();

        pathsList.Remove (sceneName);

        // update the file
        UpdateList(pathsList);
    }


    public void UpdateList ( List<string> pathsList )
    {
        // load the scriptable object file, update the contents, then save

        asset.recentSceneItemList.Clear();

        foreach (var n in pathsList)
        {
            // only add existing files
            if (File.Exists(n))
            {
                asset.recentSceneItemList.Add(new PROD_RecentSceneItem());
                asset.recentSceneItemList[asset.recentSceneItemList.Count-1].recentScenePath = n;
            }
        }
        // save the data
        AssetDatabase.Refresh();
        EditorUtility.SetDirty(asset);
        AssetDatabase.SaveAssets();
        asset = null;
    }
        

    public List<string> GetList()
    {
        // build a list of whatever's there
        List<string> list = new List<string>();

        asset = ReadScriptableObject();

        foreach (var n in asset.recentSceneItemList)
        {
            list.Add(n.recentScenePath);
        }

        return list;
    }


    public string GetMostRecent()
    {
        List<string> thisList = GetList();

        if (thisList.Count > 0)
        {
            return thisList [0];
        }
        else
        {
            Debug.Log ("No data!");
            return "";
        }
    }


    private PROD_RecentSceneItemList ReadScriptableObject ()
    {
        // build user work config data path
        string assetName = CFGFolderPath + sep + CFGAssetName;

        // load the file into memory
        asset = AssetDatabase.LoadAssetAtPath (assetName, typeof(PROD_RecentSceneItemList)) as PROD_RecentSceneItemList;

        // if the file doesn't contain anything, make an entry into it's list
        if (asset == null)
        {
            asset = ScriptableObject.CreateInstance<PROD_RecentSceneItemList>();
            asset.recentSceneItemList = new List<PROD_RecentSceneItem>();
            PROD_RecentSceneItem rsItem = new PROD_RecentSceneItem();
            rsItem.recentScenePath = "";
            asset.recentSceneItemList.Add(rsItem);
        }

        return asset;
    }


    public void CreateRecentScenesScriptableObject ()
    {
        // create user work config data folder
        string assetName = CFGFolderPath + sep + CFGAssetName;

        if(!Directory.Exists(CFGFolderPath))
        {
            Directory.CreateDirectory(CFGFolderPath);
        }

        // if file doesn't exist, create it
        asset = ReadScriptableObject();

        if (!File.Exists(assetName))
        {
            AssetDatabase.CreateAsset(asset, assetName);
            AssetDatabase.SaveAssets();
        }
        asset = null;
    }
}
