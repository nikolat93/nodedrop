﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using UnityEditor;
using UnityEditor.SceneManagement;
using System.Text.RegularExpressions;
using ProdTools;

public class PROD_SceneVersionUpClass {

    public void VersionUp() {
        // set up paths
        char sep = PROD_FileUtils.Sep;

        string thisRelativeScenePath = EditorSceneManager.GetActiveScene().path;                                // relative scene path

        // is scene saved?
        if ( thisRelativeScenePath.Length == 0 ) {
            Debug.Log ( "save your scene first." );
        }
        
        string thisSceneName = Path.GetFileName(thisRelativeScenePath);                                         // scene name
        string thisSceneVersionlessName = Regex.Replace (thisSceneName, @"_v\d{3}.unity", "");                  // scene name without version
        string thisRelativeSceneFolderPath = Path.GetDirectoryName(thisRelativeScenePath) + sep;                // relative scene folder path
        
        
        // check folder for all versions of this scene and create a list.
        List<string> filesList = new List<string>();                                                            // list to build all versions of our scene into
        
        DirectoryInfo dir = new DirectoryInfo(thisRelativeSceneFolderPath);
        FileInfo[] myFiles = dir.GetFiles("*.unity");
        
        
        // get a list of all versions of this scene
        foreach (var item in myFiles ) {
            if ( Regex.IsMatch (item.ToString(), thisSceneVersionlessName) ) {
                filesList.Add (item.ToString());
            }
        }
        
        
        // create new scene name and save scene
        string thisNewSceneName = PROD_VersionUp.NewVersion (thisSceneVersionlessName, ".unity", filesList);

        EditorSceneManager.SaveScene (EditorSceneManager.GetActiveScene(), thisRelativeSceneFolderPath + thisNewSceneName + ".unity", false);

    }

}
