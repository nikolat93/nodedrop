﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PROD_RecentSceneItemList : ScriptableObject
{
    public List<PROD_RecentSceneItem> recentSceneItemList;
}
