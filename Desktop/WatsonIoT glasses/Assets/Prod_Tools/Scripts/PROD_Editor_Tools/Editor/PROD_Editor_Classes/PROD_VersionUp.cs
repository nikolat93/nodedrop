﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Linq;
using System;
using ProdTools;


public class PROD_VersionUp {

    static List<string> newList = new List<string>();



    public static string NewVersion( string thisName, string thisExtension, List<string> thisList ) {

        // reset newList
        newList.Clear();

        // build a list from the array based on thisName
        foreach (string item in thisList) {
            if ( Regex.IsMatch( item, thisName + @"_v\d{3}" ) ) {
                newList.Add (item);
            }
        }

        // if list comes up empty with no matches, return with _v001 suffix
        if (newList.Count < 1) {
            thisName = Regex.Replace (thisName, thisExtension, "");
            return thisName + "_v001";
        }

        // return the new version name
        return thisName + "_v" + GetNewVersionNumberFromList(newList);

    }



    static string GetNewVersionNumberFromList ( List<string> thisList ) {

        // sort the list
        thisList.Sort ();

        // get the last item
        string lastItem = thisList.Last();

        // extract the version pattern
        string lastItemNum = Regex.Match (lastItem, @"_v\d{3}").ToString();

        // extract and iterate the number
        int iter = Convert.ToInt16 ( Regex.Replace (lastItemNum, @"_v", "") );
        iter++;
        string iterString = string.Format("{0:000}", iter);

        return iterString;

    }

}

