﻿using UnityEngine;
using System.Collections;
using ProdTools;

public class DistanceTriggerExample : MonoBehaviour
{
    public GameObject enableInRange;

    DistanceTrigger distanceTrigger;
    bool withinRange = false;


    void OnEnable ()
    {
        distanceTrigger = GetComponent<DistanceTrigger>();
    }


    void Update ()
    {
        withinRange = distanceTrigger.withinRange1;

        // Test Chingas for in range
        if (withinRange)
        {
            if (enableInRange != null)
                enableInRange.SetActive (true);
        }
    }
}