﻿using UnityEngine;
using System.Collections;
using ProdTools;

[RequireComponent (typeof (TransformDelta))]
public class TransformDeltaExample2 : MonoBehaviour
{
    public GameObject GO1;
    public GameObject GO2;

    public float speed = 1f;

    private Transform GO1Transform;
    private Transform GO2Transform;

    private TransformDelta thisTransformDelta;


    void OnEnable ()
    {
        GO1Transform = GO1.transform;
        GO2Transform = GO2.transform;

        thisTransformDelta = GetComponent<TransformDelta>();
    }


    void Update ()
    {
        GO1Transform.position = Vector3.Lerp(GO1Transform.position, GO2Transform.position, Time.deltaTime * thisTransformDelta.ThisDeltaPosition * speed);
        GO2Transform.position = Vector3.Lerp(GO2Transform.position, GO1Transform.position, Time.deltaTime * thisTransformDelta.ThisDeltaPosition * speed);
    }
}
