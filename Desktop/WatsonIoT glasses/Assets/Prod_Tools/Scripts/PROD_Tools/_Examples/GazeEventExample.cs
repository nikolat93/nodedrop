﻿using UnityEngine;
using System.Collections;
using ProdTools;

public class GazeEventExample : MonoBehaviour
{
    public GameObject cubeGO;
    private Renderer cubeRenderer;
    private Material cubeMaterial;
    private Color cubeColor;
    private Color cubeColorOriginal;
    private Color cubeColorWhite = new Color(1,1,1,1);
    private Color cubeColorRed = new Color(1,0,0,1);

    private GameObject thisGO;
    private GazeTrigger thisGazeTrigger;


    void OnEnable ()
    {
        thisGO = gameObject;
        thisGazeTrigger = thisGO.GetComponent<GazeTrigger>();
        cubeRenderer = cubeGO.GetComponent<Renderer>();
        cubeColorOriginal = cubeRenderer.material.color;
    }


    void OnDisable ()
    {
        cubeMaterial.color = cubeColorOriginal;
    }


    void Update ()
    {
        cubeMaterial = cubeRenderer.material;
        cubeColor = cubeMaterial.color;

        if (thisGazeTrigger.isInView && !thisGazeTrigger.isInFocus)
        {
            cubeMaterial.color = Color.Lerp(cubeColor, cubeColorWhite, Time.deltaTime * 1.0f);
        }
        else if (thisGazeTrigger.isInFocus)
        {
            cubeMaterial.color = Color.Lerp(cubeColor, cubeColorRed, Time.deltaTime * 5.0f);
        }
        else cubeMaterial.color = Color.Lerp(cubeColor, cubeColorOriginal, Time.deltaTime * 1.0f);
    }
}
