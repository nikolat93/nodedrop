﻿using UnityEngine;
using System.Collections;
using ProdTools;

[RequireComponent (typeof (TransformDelta))]
public class TransformDeltaExample : MonoBehaviour
{
    public float positionDelta;
    public float rotationDelta;

    TransformDelta thisTransformDelta;

    void OnEnable ()
    {
        thisTransformDelta = GetComponent<TransformDelta>();
    }


    void LateUpdate ()
    {
        positionDelta = thisTransformDelta.ThisDeltaPosition;
        rotationDelta = thisTransformDelta.ThisDeltaRotation;
    }
}
