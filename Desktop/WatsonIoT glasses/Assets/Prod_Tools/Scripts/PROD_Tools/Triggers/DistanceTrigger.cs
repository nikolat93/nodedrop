﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

namespace ProdTools
{
    public class DistanceTrigger : MonoBehaviour
    {
        #region vars
        [Tooltip("If no game object is set here, the world origin is used.")]
        public GameObject distanceToGO;
        public float range1 = 1f;
        public float range2 = 2f;

        public bool withinRange1 = false;
        public UnityEvent inRange1;
        public UnityEvent outOfRange1;

        public bool withinRange2 = false;
        public UnityEvent inRange2;
        public UnityEvent outOfRange2;

        GameObject tempGO;
        float distanceDifference;
        Transform pointATransform;
        Transform pointBTransform;

        public 
        #endregion


        void OnEnable ()
        {
            pointATransform = transform;
            if (distanceToGO != null)
            {
                pointBTransform = distanceToGO.transform;
            }
            else
            {
                tempGO = new GameObject(transform.name + "_DistanceTrigger_tempGO");
                pointBTransform = tempGO.transform;
            }
        }


        void OnDisable ()
        {
            Destroy(tempGO);
        }


        void LateUpdate ()
        {
            distanceDifference = Vector3.Distance (pointATransform.position, pointBTransform.position);

            withinRange1 = (distanceDifference <= range1);
            if(withinRange1)
                inRange1.Invoke();
            else
                outOfRange1.Invoke();

            withinRange2 = (distanceDifference <= range2);
            if(withinRange2)
                inRange2.Invoke();
            else
                outOfRange2.Invoke();
        }
    }
}