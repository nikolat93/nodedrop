﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

// simple gaze trigger for firing events without raycasting

namespace ProdTools
{
    public class GazeTrigger : MonoBehaviour
    {
        #region vars
        private Transform thisGO;
        public Transform gazerGO;


        /// <summary>
        ///  "viewToleranceMax" ranges from -1.0 to 1.0,
        /// with 1.0 equals "gazerGO" is looking perfectly toward "thisGO"
        /// and -1.0 looking perfectly away.
        /// These numbers help control the "isInView" boolean.
        /// </summary>
        [Range (-1f, 1f)]
        [Tooltip("Set this lower for a wider field of view. Use negative values to return isInView=true if this object is behind the gazer. -1 will always return isInView=true.")]
        public float viewToleranceMax = 0.719f;
        [Range (0.500f, 0.9999f)]
        [Tooltip("Setting at 0.5 = 90 degree focus. Useful settings are between .98 and .999 to simulate foviated focus.")]
        public float viewToleranceMin = 0.9990f;
        [Tooltip("Generally used to return true if something is in the periphery.")]
        public bool isInView = false;
        [Tooltip("Generally used to return true if something is in foviated focus.")]
        public bool isInFocus = false;
        [Range (1,60)]
        [Tooltip("How many times per second the calculation is made.")]
        public int iterationsPerSecond = 10;


        public UnityEvent inView;
        public UnityEvent notInView;

        public UnityEvent inFocus;
        public UnityEvent notInFocus;

        private Vector3 sightDir;
        private float sightDirDot;
        #endregion


        void OnEnable ()
        {
            thisGO = GetComponent<Transform>();
            StartCoroutine(GazeCheck());
        }


        void OnDisable ()
        {
            StopCoroutine(GazeCheck());
        }



        IEnumerator GazeCheck ()
        {
            while (true)
            {
                sightDir = thisGO.position - gazerGO.position;
                
                sightDirDot = Vector3.Dot (gazerGO.forward, sightDir.normalized);

                isInView = (sightDirDot > viewToleranceMax);

                if(isInView)
                    inView.Invoke();
                else
                    notInView.Invoke();

                isInFocus = (sightDirDot > viewToleranceMin);

                if(isInFocus)
                    inFocus.Invoke();
                else
                    notInFocus.Invoke();

                yield return new WaitForSeconds(1f/iterationsPerSecond);
            }
        }
    }
}