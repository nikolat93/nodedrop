﻿using UnityEngine;
using UnityEngine.Events;

namespace DaqriProd {

    [System.Serializable]
    public class KeyPressEvent {

        // Event Trigger to fire when GetButtonDown is called
        public UnityEvent onKeyDown;

        // This string will change to one of the mappedKey string's depending on the mappedKeyIndex selected in the inspector
        [HideInInspector]
        public string keyToPress;

        // This string field and int will be converted to a Popup menu from CustomKeyboardPressTriggerInspector's OnInspectorGUI()
        [HideInInspector]
        public string[] mappedKey = new string[] {"Fire1", "Fire2", "Fire3", "Jump", "Submit", "Cancel"};
        [HideInInspector]
        public int mappedKeyIndex;
    }
        
    public class KeyboardPressTrigger : MonoBehaviour {

        public KeyPressEvent keyPressEvent;

        // Update is called once per frame
        void Update () 
        {

            if(Input.GetButtonDown(keyPressEvent.keyToPress))
                keyPressEvent.onKeyDown.Invoke();
        }
    }
}