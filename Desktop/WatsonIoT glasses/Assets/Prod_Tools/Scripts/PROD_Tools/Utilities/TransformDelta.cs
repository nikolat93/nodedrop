﻿using UnityEngine;
using System.Collections;

namespace ProdTools
{
    public class TransformDelta : MonoBehaviour
    {
        public bool FactorPosition = true;
        public bool FactorRotation = true;

        private float thisDeltaPosition;
        private float thisDeltaRotation;

        private GameObject thisGameObject;
        private Transform thisTransform;

        private Vector3 thisCurrentPosition;
        private Vector3 thisOldPosition;

        private Vector3 thisCurrentForward;
        private Vector3 thisOldForward;


        void OnEnable ()
        {
            thisGameObject = gameObject;
            thisTransform = thisGameObject.transform;
            thisOldPosition = thisTransform.position;
            thisCurrentPosition = thisTransform.position;
        }


        public float ThisDeltaPosition
        {
            get
            {
                return thisDeltaPosition;
            }
            private set
            {
                thisDeltaPosition = value;
            }
        }


        public float ThisDeltaRotation
        {
            get
            {
                return 1f - thisDeltaRotation;
            }
            private set
            {
                thisDeltaRotation = value;
            }
        }


        void LateUpdate ()
        {
            if (FactorPosition)
            {
                thisCurrentPosition = thisTransform.position;
                ThisDeltaPosition = Vector3.Distance(thisOldPosition, thisCurrentPosition);
            }

            if (FactorRotation)
            {
                thisCurrentForward = thisTransform.forward;
                ThisDeltaRotation = Vector3.Dot(thisOldForward, thisCurrentForward);
            }

            thisOldPosition = thisCurrentPosition;
            thisOldForward = thisCurrentForward;
        }
    }
}