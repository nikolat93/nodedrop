﻿namespace UnityEngine
{
    using System;
    using System.Collections;
    
    // This can be called from anywhere to return if the orientation is vertical and gives gravity vector
    public static class GetOrientation
    {
        // Returns gravity direction according to device's gyro
        public static Vector3 Gravity {
            get {
                return new Vector3 (Input.gyro.gravity.x, Input.gyro.gravity.y, -1.0f * Input.gyro.gravity.z);  // Gyro gravity vector uses a different coordinate system.
            }
        }
        // Returns if the target is vertical
        public static bool IsTargetVertical (Transform target)
        {
            // get the up vector of the real world using the gyro gravity
            if (Camera.main == null) {
                return false;
            }
            Vector3 realWorldUp = Camera.main.transform.TransformDirection (Gravity * -1.0f);   // gravity vector is in local space of camera. convert to world coordinates
            
            // get the orientation of the target
            Vector3 targetForward = target.rotation * Vector3.forward;
            
            float dotProd = Vector3.Dot (realWorldUp, targetForward);
            return (dotProd > 0.5f);
        }
    
    }
}
