﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/*
 * This class can be instantiated and used to produce weighted randomizations
 * Just cast the 'Item' to whatever is needed on the calling code
 * 
 * 
 * */

public class RandomLookup : MonoBehaviour 
{
    List<ItemIndex> items = new List<ItemIndex>();
    int[] item_selection_pool;
    int pool_total = 0;

    // Pass in a list to build the pool for selection
    public void BuildPool(List<ItemIndex> itms)
    {
        for(int j=0; j<itms.Count; j++)
        {
            items.Add(itms[j]);
            pool_total+=itms[j].weight;
        }

        item_selection_pool = new int[pool_total];
        int i = 0;
        for (int m = 0; m < items.Count; m++) 
        {
            for (int p = 0; p < items[m].weight; p++) 
            {
                item_selection_pool[i] = m;
                i++;
            }
        }
    }

    public class ItemIndex
    {
        public int indx;
        public int weight;

        public ItemIndex(int i, int w)
        {
            indx = i;
            weight = w;
        }

    }

    // Call this to return a random selection from the pool based on weight
    public int GetRandomIndex()
    {
        int i = (item_selection_pool[Random.Range(0,pool_total)]);
        return i;
    }

}

