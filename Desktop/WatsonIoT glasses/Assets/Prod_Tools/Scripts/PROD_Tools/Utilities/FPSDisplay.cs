﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using ProdTools;

[RequireComponent(typeof(FPSCounter))]
public class FPSDisplay : MonoBehaviour
{
    public Text highestFPSLabel, averageFPSLabel, lowestFPSLabel;

    FPSCounter fpsCounter;

    static string[] stringsFrom00to99;
    List<string> numbers = new List<string>();

    void Awake ()
    {
        fpsCounter = GetComponent<FPSCounter>();

        for (int i = 0; i < 100; i++)
        {
            numbers.Add(i.ToString("D2"));
        }

        stringsFrom00to99 = numbers.ToArray();
    }


    void Update ()
    {
        highestFPSLabel.text = stringsFrom00to99[Mathf.Clamp(fpsCounter.HighestFPS, 0, 99)];
        averageFPSLabel.text = stringsFrom00to99[Mathf.Clamp(fpsCounter.AverageFPS, 0, 99)];
        lowestFPSLabel.text = stringsFrom00to99[Mathf.Clamp(fpsCounter.LowestFPS, 0, 99)];
    }
}
