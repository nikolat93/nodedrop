﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Jitter Filter Multiframe -
/// This is a low pass filter that samples past
/// n number of frames for an average smoothness.
/// </summary>

namespace ProdTools
{
    public class JitterFilterMultiframe : MonoBehaviour {

        public bool applyOnPosition = true;
        public bool applyOnRotation = true;

        [Range(0.0f, 1.0f)]
        public float posLowPassFilter = 0.5f;

        [Range(0.0f, 1.0f)]
        public float rotLowPassFilter = 0.5f;

        public int numberOfFrames = 5;

        GameObject thisGO;
        Vector3 newPosition;
        Quaternion newRotation;
        Vector3 oldPosition;
        Quaternion oldRotation;

        Vector3 tempVec3 = new Vector3(0,0,0);
        Quaternion tempQuat = Quaternion.identity;

        List<Vector3> positionFrames = new List<Vector3>();
        List<Quaternion> rotationFrames = new List<Quaternion>();


        void OnEnable ()
        {
            thisGO = gameObject;
            oldPosition = thisGO.transform.position;
            oldRotation = thisGO.transform.rotation;

            if (applyOnPosition)
            {
                positionFrames.Add(thisGO.transform.position);
            }
            if (applyOnRotation)
            {
                rotationFrames.Add(thisGO.transform.rotation);
            }
        }


        void LateUpdate ()
        {
            // add the latest position to the list
            if (applyOnPosition)
            {
                positionFrames.Add(thisGO.transform.position);

                if (positionFrames.Count > numberOfFrames)
                {
                    // remove the oldest items in the lists
                    positionFrames.RemoveAt(0);
                }

                // set new position
                newPosition = AveragePosition(positionFrames);
                thisGO.transform.position = newPosition;
            }
            else
            {
                thisGO.transform.position = oldPosition;
            }

            if (applyOnRotation)
            {
                rotationFrames.Add(thisGO.transform.rotation);

                if (rotationFrames.Count > numberOfFrames)
                {
                    // remove the oldest items in the lists
                    rotationFrames.RemoveAt(0);
                }

                // set new rotation
                newRotation = AverageRotation(rotationFrames);
                thisGO.transform.rotation = newRotation;
            }
            else
            {
                thisGO.transform.rotation = oldRotation;
            }
        }


        Vector3 AveragePosition ( List<Vector3> thisList )
        {
            for(int i=0; i<positionFrames.Count;i++)
            {
                tempVec3 = Vector3.Lerp(tempVec3, thisList[i], posLowPassFilter);
            }
            return tempVec3;
        }


        Quaternion AverageRotation ( List<Quaternion> thisList )
        {
            for(int i=0; i<rotationFrames.Count;i++)
            {
                tempQuat = Quaternion.Slerp(tempQuat, thisList[i], rotLowPassFilter);
            }
            return tempQuat;
        }

    }
}