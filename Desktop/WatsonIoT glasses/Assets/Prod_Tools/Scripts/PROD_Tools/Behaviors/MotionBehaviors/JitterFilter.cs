﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Jitter Filter -
/// A simple low pass filtering system for position and rotation
/// This will sample last frame and current frame for it's calculation
/// </summary>

namespace ProdTools
{
    public class JitterFilter : MonoBehaviour
    {
        Quaternion currentRotation, targetRotation;
        Vector3 currentPosition, targetPosition;

        GameObject thisGO;

        public bool applyOnRotation = true;
        public bool applyOnPosition = true;


        [Range(0.1f, 1.0f)]
        public float lowPassFilter = 0.5f;

        public float LowPassFilter
        {
            set { lowPassFilter = value; }
        }


        void OnEnable ()
        {
            thisGO = gameObject;
            currentRotation = targetRotation = thisGO.transform.rotation;
            currentPosition = targetPosition = thisGO.transform.position;
        }


        void LateUpdate ()
        {
            if (thisGO.transform.rotation != currentRotation)
            {
                targetRotation = thisGO.transform.rotation;
            }

            if (thisGO.transform.position != currentPosition)
            {
                targetPosition = thisGO.transform.position;
            }

            if (lowPassFilter < 1)
            {
                if (applyOnRotation)
                {
                    thisGO.transform.rotation = Quaternion.Slerp (currentRotation, targetRotation, lowPassFilter);
                }

                if (applyOnPosition)
                {
                    thisGO.transform.position = Vector3.Lerp (currentPosition, targetPosition, lowPassFilter);
                }
            }

            currentRotation = thisGO.transform.rotation;
            currentPosition = thisGO.transform.position;
        }

    }
}