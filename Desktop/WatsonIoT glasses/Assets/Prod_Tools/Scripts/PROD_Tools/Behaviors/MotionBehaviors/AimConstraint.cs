﻿using UnityEngine;
using System.Collections;

namespace ProdTools
{
    public class AimConstraint : MonoBehaviour
    {
        #region vars
        public Transform targetGO;
        public bool isAiming = true;
        public float speedTrack = 1.0f;
        public float speedRest = 1.0f;

        private Transform thisGO;
        private Quaternion thisOriginalRotation;
        private GameObject tempTransform;
        #endregion


        void OnEnable ()
        {
            thisGO = GetComponent<Transform>();
            thisOriginalRotation = thisGO.rotation;
            tempTransform = new GameObject();
            tempTransform.transform.position = thisGO.position;

            if (targetGO == null) targetGO = new GameObject().transform;
            targetGO.position = Vector3.zero;
            }


        void OnDisable ()
        {
            Destroy(tempTransform);
        }


        void FixedUpdate ()
        {
            tempTransform.transform.LookAt(targetGO);

            if (isAiming) {
                thisGO.localRotation = Quaternion.Lerp(thisGO.rotation, tempTransform.transform.rotation, Time.deltaTime * speedTrack);
            }
            else {
                thisGO.localRotation = Quaternion.Lerp(thisGO.rotation, thisOriginalRotation, Time.deltaTime * speedRest);
            }
        }
    }
}