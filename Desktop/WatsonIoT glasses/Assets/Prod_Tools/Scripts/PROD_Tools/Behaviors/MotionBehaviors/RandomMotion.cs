﻿using UnityEngine;
using System.Collections;

namespace ProdTools
{
    public class RandomMotion : MonoBehaviour
    {
        public float rangePos = 5f;
        [Range(0f,360f)]public float rangeRot = 45f;
        [Range(0f,1f)]public float xPosWeighting = 1f;
        public float xPosSpeed = 1f;
        [Range(0f,1f)]public float yPosWeighting = 1f;
        public float yPosSpeed = 1f;
        [Range(0f,1f)]public float zPosWeighting = 1f;
        public float zPosSpeed = 1f;
        [Range(0f,1f)]public float xRotWeighting = 1f;
        public float xRotSpeed = 1f;
        [Range(0f,1f)]public float yRotWeighting = 1f;
        public float yRotSpeed = 1f;
        [Range(0f,1f)]public float zRotWeighting = 1f;
        public float zRotSpeed = 1f;

        private Transform thisTransform;
        private Vector3 oldPosition;
        private Quaternion oldRotation;
        private Vector3 newPosition;
        private Quaternion newRotation;

        private Vector3 posSeed;
        private Vector3 rotSeed;
        private Vector3 posFactor;
        private Vector3 rotFactor;


        void OnEnable ()
        {
            thisTransform = transform;
            oldPosition = thisTransform.localPosition;
            oldRotation = thisTransform.localRotation;

            posSeed = new Vector3(RandomizeSeed(), RandomizeSeed(), RandomizeSeed());
            rotSeed = new Vector3(RandomizeSeed(), RandomizeSeed(), RandomizeSeed());

            StartCoroutine ( RandomAnim() );
        }

        IEnumerator RandomAnim ()
        {
            while (true)
            {
                posFactor = new Vector3(rangePos * xPosWeighting, rangePos * yPosWeighting, rangePos * zPosWeighting);

                rotFactor = new Vector3(rangeRot * xRotWeighting, rangeRot * yRotWeighting, rangeRot * zRotWeighting);

                newPosition = new Vector3(  (xPosWeighting>0) ? oldPosition.x + posFactor.x * (Mathf.PerlinNoise (Time.time * xPosSpeed, posSeed.x) - 0.5f) : oldPosition.x,
                                            (yPosWeighting>0) ? oldPosition.y + posFactor.y * (Mathf.PerlinNoise (Time.time * yPosSpeed, posSeed.y) - 0.5f) : oldPosition.y,
                                            (zPosWeighting>0) ? oldPosition.z + posFactor.z * (Mathf.PerlinNoise (Time.time * zPosSpeed, posSeed.z) - 0.5f) : oldPosition.z);
                
                newRotation = Quaternion.Euler(new Vector3( (xRotWeighting>0) ? oldRotation.eulerAngles.x + rotFactor.x * (Mathf.PerlinNoise (Time.time * xRotSpeed, rotSeed.x) - 0.5f) : oldRotation.eulerAngles.x,
                                                            (yRotWeighting>0) ? oldRotation.eulerAngles.y + rotFactor.y * (Mathf.PerlinNoise (Time.time * yRotSpeed, rotSeed.y) - 0.5f) : oldRotation.eulerAngles.y,
                                                            (zRotWeighting>0) ? oldRotation.eulerAngles.z + rotFactor.z * (Mathf.PerlinNoise (Time.time * zRotSpeed, rotSeed.z) - 0.5f) : oldRotation.eulerAngles.z));
                
                thisTransform.localPosition = newPosition;
                thisTransform.localRotation = newRotation;

                yield return null;
            }
        }

        void OnDisable ()
        {
            StopCoroutine ( RandomAnim() );
            thisTransform.localPosition = oldPosition;
            thisTransform.localRotation = oldRotation;
        }

        float RandomizeSeed ()
        {
            return Random.Range(-500f, 500f);
        }
    }
}