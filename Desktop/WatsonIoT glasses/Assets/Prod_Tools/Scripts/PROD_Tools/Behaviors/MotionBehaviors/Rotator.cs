﻿using UnityEngine;
using System.Collections;

namespace ProdTools
{
    public class Rotator : MonoBehaviour
    {
        public Vector3 rotationAngles = new Vector3(0,1,0);
        public float speed = 2.0f;

        Transform thisTransform;


        void Awake ()
        {
            thisTransform = transform;
        }


        void Update ()
        {
            thisTransform.Rotate (rotationAngles * speed);
        }
    }
}
