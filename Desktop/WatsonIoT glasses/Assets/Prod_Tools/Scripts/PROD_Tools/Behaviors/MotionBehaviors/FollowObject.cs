﻿using UnityEngine;
using System.Collections;

namespace ProdTools
{
    public class FollowObject : MonoBehaviour
    {
        #region vars
        public Transform objectToFollow;

        public bool maintainPositionOffset;
        public bool maintainRotationOffset;
        public bool followPosition = true;
        public bool followRotation = true;

        public float followPositionSpeed = 5f;
        public float followRotationSpeed = 5f;

        private Vector3 positionOffset;
        private Quaternion thisOffsetNewRotation;

        private Transform thisTransform;
        private Vector3 thisOriginalPosition;
        private Quaternion thisOriginalRotation;
        #endregion


        void OnEnable ()
        {
            thisTransform = transform;
            thisOriginalPosition = thisTransform.position;
            thisOriginalRotation = transform.rotation;
            positionOffset = thisOriginalPosition - objectToFollow.position;
            thisOffsetNewRotation = new Quaternion();
        }


        void LateUpdate ()
        {
            #region follow position
            if (followPosition)
            {
                if (maintainPositionOffset)
                {
                    thisTransform.position = Vector3.Lerp (thisTransform.position, objectToFollow.position + positionOffset, Time.deltaTime * followPositionSpeed);
                }
                else
                {
                    thisTransform.position = Vector3.Lerp (thisTransform.position, objectToFollow.position, Time.deltaTime * followPositionSpeed);
                }
            }
            else
            {
                thisTransform.position = Vector3.Lerp (thisTransform.position, thisOriginalPosition, Time.deltaTime * followPositionSpeed);
            }
            #endregion


            #region follow rotation
            if (followRotation)
            {
                // TODO: fix this rotation offset shit!
                thisOffsetNewRotation = Quaternion.Inverse(thisTransform.rotation) * objectToFollow.rotation;

                if (maintainRotationOffset)
                {
                    thisTransform.rotation = Quaternion.Lerp (thisTransform.rotation, thisOffsetNewRotation, Time.deltaTime * followRotationSpeed);
                }
                else
                {
                    thisTransform.rotation = Quaternion.Lerp (thisTransform.rotation, objectToFollow.rotation, Time.deltaTime * followRotationSpeed);
                }
            }
            else
            {
                thisTransform.rotation = Quaternion.Lerp (thisTransform.rotation, thisOriginalRotation, Time.deltaTime * followRotationSpeed);
            }
            #endregion
        }
    }
}