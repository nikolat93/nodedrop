﻿using UnityEngine;
using System.Collections;

namespace DaqriProd {
    
    public class ParentByString : MonoBehaviour {

        public string parentName;

        private GameObject parent;

        public void Parent () 
        {
            parent = GameObject.Find(parentName);

            transform.parent = parent.transform;
            transform.position = parent.transform.position;
            transform.rotation = parent.transform.rotation;
        }

        public void Unparent ()
        {
            transform.parent = null;
        }
    }
}