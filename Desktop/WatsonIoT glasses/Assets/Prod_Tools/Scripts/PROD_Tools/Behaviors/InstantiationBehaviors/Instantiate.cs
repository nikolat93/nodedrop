﻿using UnityEngine;
using System.Collections;

namespace ProdTools
{
    public class Instantiate : MonoBehaviour 
    {
        public GameObject[] toInstantiate;
        public Transform spawnPoint;
        public AudioSource audioSource;
        public AudioClip audioClip;
        public bool spawnWithDelay;
        public float delayTime = 0f;
        public float waitBetweenFires = 3f;

        private bool canFire = true;

        IEnumerator instantiateEvent;

        public void FireInstantiate () 
        {
            if(canFire)
            {
                if(instantiateEvent != null)
                    StopCoroutine (instantiateEvent);
                instantiateEvent = InstantiateEvent();
                StartCoroutine (instantiateEvent);
            }
        }

        IEnumerator InstantiateEvent ()
        {
            canFire = false;

            if(spawnWithDelay)
                yield return new WaitForSeconds (delayTime);

            for(int i=0; i<toInstantiate.Length; i++)
            {
                Instantiate(toInstantiate[i], spawnPoint.transform.position, spawnPoint.transform.rotation);
            }

            if( audioSource != null)
            {
                if(audioClip != null)
                    audioSource.clip = audioClip;
                else
                    Debug.Log("No audioClip assigned to " + gameObject.name);
                
                audioSource.Play();
            }
            else
            {
                Debug.Log("No AudioSource assigned to " + gameObject.name);
            }

            yield return new WaitForSeconds (waitBetweenFires);
            canFire = true;
        }

    }
}