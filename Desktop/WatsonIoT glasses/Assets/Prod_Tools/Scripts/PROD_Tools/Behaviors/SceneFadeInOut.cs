﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SceneFadeInOut : MonoBehaviour
{
    // Speed that the screen fades to and from black
    public float fadeSpeed = 1.5f;          

    // The Image we are fading in and out
    public Image fadeImage;

    // Whether or not the scene is still fading in
    private bool sceneStarting = true;      

    void Update ()
    {
        // If the scene is starting...
        if(sceneStarting)
            // ... call the StartScene function.
            StartScene();
    }

    void OnEnable ()
    {
        StartingValues();
    }

    void StartingValues ()
    {
        fadeImage.color = Color.black;
    }

    void FadeToClear ()
    {
        // Lerp the colour of the texture between itself and transparent.
        fadeImage.color = Color.Lerp(fadeImage.color, Color.clear, fadeSpeed * Time.deltaTime);
    }

    void FadeToBlack ()
    {
        // Lerp the colour of the texture between itself and black.
        fadeImage.color = Color.Lerp(fadeImage.color, Color.black, fadeSpeed * Time.deltaTime);
    }

    void StartScene ()
    {
        // Fade the texture to clear.
        FadeToClear();

        // If the texture is almost clear...
        if(fadeImage.color.a <= 0.05f)
        {
            // ... set the colour to clear and disable the GUITexture.
            fadeImage.color = Color.clear;
            fadeImage.enabled = false;

            // The scene is no longer starting.
            sceneStarting = false;
        }
    }

    public void EndScene ()
    {
        // Make sure the texture is enabled.
        fadeImage.enabled = true;

        // Start fading towards black.
        FadeToBlack();

        // If the screen is almost black...
        //if(fadeImage.color.a >= 0.95f)
            // ... reload the level.
            //SceneManager.LoadScene(0);
    }
}