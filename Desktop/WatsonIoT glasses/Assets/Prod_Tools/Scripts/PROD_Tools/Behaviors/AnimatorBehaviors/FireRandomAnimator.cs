﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace ProdTools
{
    public class FireRandomAnimator : MonoBehaviour {

        public List<AnimatorEvent> animatorEvent;

        public bool startOnEnable = false;

        // If set to true, we restart the FireRandom loop
        public bool restart = true;

        private AnimatorEvent thisEvent;

        // Cached coroutines
        IEnumerator fireRandom;
            
        void OnEnable ()
        {
            if(startOnEnable)
                FireRandom();
        }

        void FireRandom ()
        {
            if(fireRandom != null)
                StopCoroutine (fireRandom);
            fireRandom = _FireRandom();
                StartCoroutine (fireRandom);
        }

        IEnumerator _FireRandom ()  
        {
            thisEvent = animatorEvent [Random.Range (0, animatorEvent.Count)];

            yield return new WaitForSeconds(thisEvent.waitTime);

            foreach (Animator animatorsToFire in thisEvent.animatorController)
            {
                animatorsToFire.SetTrigger(thisEvent.parameter);
            }

            if(thisEvent.audioSource != null)
            {
                if(thisEvent.audioClip != null)
                {
                    thisEvent.audioSource.clip = thisEvent.audioClip;
                    thisEvent.audioSource.Play();
                }
                else
                {
                    Debug.Log("No AudioClip assigned to Animator Event on " + gameObject.name);
                }
            }
            else
            {
                Debug.Log("No AudioSource assigned to Animator Event on " + gameObject.name);
            }

            if(restart)
            {
                FireRandom();
            }
        }
    }
}