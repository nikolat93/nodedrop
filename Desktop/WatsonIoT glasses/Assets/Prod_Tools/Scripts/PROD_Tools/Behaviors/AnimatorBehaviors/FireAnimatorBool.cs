﻿using UnityEngine;
using System.Collections;

namespace ProdTools
{
    public class FireAnimatorBool: MonoBehaviour {

        // The animator we want to fire for this event
        public Animator animatorController;

        // The animator trigger to be set in the inspector
        public string parameter;

        // Bool value to set on the Animator
        public bool value;

        // Reference to the Audio Source and clip we want to play when the animator event fires
        public AudioSource audioSource;
        public AudioClip audioClip;

        // Wait time before the animator trigger can be fired again
        public float waitTime = 5f;

        // Whether or not we can fire the animator event
        private bool canFire = true;

        // Cached coroutines
        IEnumerator setAnimatorBool;

        public void SetAnimatorBool ()
        {
            if(canFire)
            {
                if(setAnimatorBool != null)
                    StopCoroutine (setAnimatorBool);
                setAnimatorBool = FireEvent();
                    StartCoroutine (setAnimatorBool);
            }
        }

        IEnumerator FireEvent ()
        {
            canFire = false;
            animatorController.SetBool(parameter, value);

            if(audioSource != null)
            {
                if(audioClip != null)
                    audioSource.clip = audioClip;
                else
                    Debug.Log("No Audio Clip assigned to " + gameObject.name);
                
                audioSource.Play();
            }
            else
            {
                Debug.Log("No Audio Source assigned to " + gameObject.name);
            }

            yield return new WaitForSeconds(waitTime);
            canFire = true;
        }
    }
}