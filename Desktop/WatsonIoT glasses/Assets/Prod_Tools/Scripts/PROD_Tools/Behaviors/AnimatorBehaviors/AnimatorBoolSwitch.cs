﻿using UnityEngine;
using System.Collections;

namespace ProdTools
{
    public class AnimatorBoolSwitch : MonoBehaviour {
        
        public Animator animatorController;
        public string animatorBool = "";

        [HideInInspector]
        public bool value;

        // If no Animator Controller has been assigned, look for one on the game object this component is attachd to
        // If no Animator Controller is found on the game object this component is attached to, throw up a warning on the console
        void Awake ()
        {
            if(animatorController == null)
                animatorController = gameObject.GetComponent<Animator>();

            if(animatorController == null)
                Debug.Log("No Animator component found on " + gameObject.name);
        }

        public void SetAnimatorBoolToTrue ()
        {
            if(animatorController)
                animatorController.SetBool(animatorBool, value = true);
        }

        public void SetAnimatorBoolToFalse ()
        {
            if(animatorController)
                animatorController.SetBool(animatorBool, value = false);
        }

        public void ToggleBool ()
        {
            value = !value;

            if(animatorController)
                animatorController.SetBool(animatorBool, value);
        }
    }
}