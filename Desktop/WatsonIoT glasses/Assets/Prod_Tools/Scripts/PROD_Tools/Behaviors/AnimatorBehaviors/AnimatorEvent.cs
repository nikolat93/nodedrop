﻿using UnityEngine;
using System.Collections;
namespace ProdTools
{
    /// <summary>
    /// FireAnimatorSequence and FireRandomAnimator inherit from this class to fire multiple Animator Events
    /// </summary>
    [System.Serializable]
    public class AnimatorEvent {

        public Animator[] animatorController;
        public string parameter;
        public bool value;
        public float waitTime;

        public AudioSource audioSource;
        public AudioClip audioClip;
    }
}