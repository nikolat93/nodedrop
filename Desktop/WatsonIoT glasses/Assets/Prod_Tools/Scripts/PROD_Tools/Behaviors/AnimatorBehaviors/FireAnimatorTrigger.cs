﻿using UnityEngine;
using System.Collections;

namespace ProdTools
{
    public class FireAnimatorTrigger : MonoBehaviour {

        // The animator we want to fire for this event
        public Animator animatorController;

        // The Animator trigger to be fired
        public string parameter;

        // Reference to the Audio Source and clip we want to play when the animator event fires
        public AudioSource audioSource;
        public AudioClip audioClip;

        // Wait time before the animator trigger can be fired again
        public float waitTime = 5f;

        // Whether or not we can fire the animator event
        private bool canFire = true;

        IEnumerator setAnimatorTrigger;
        
        public void SetAnimatorTrigger ()
        {
            if(canFire)
            {
                if(setAnimatorTrigger != null)
                    StopCoroutine (setAnimatorTrigger);
                setAnimatorTrigger = FireEvent();
                    StartCoroutine(setAnimatorTrigger);
            }
        }

        IEnumerator FireEvent ()
        {
            canFire = false;
            animatorController.SetTrigger(parameter);

            if(audioSource != null)
            {
                audioSource.clip = audioClip;
                audioSource.Play();
            }

            yield return new WaitForSeconds(waitTime);
            canFire = true;
        }
    }
}