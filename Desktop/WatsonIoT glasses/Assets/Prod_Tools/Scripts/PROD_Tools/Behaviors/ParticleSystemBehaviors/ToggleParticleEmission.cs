﻿using UnityEngine;
using System.Collections;

namespace ProdTools
{
    public class ToggleParticleEmission : MonoBehaviour {

        ParticleSystem[] systems;

        ParticleSystem systemParent;
        bool emissionValue;

        void Awake ()
        {
            systems = GetComponentsInChildren<ParticleSystem>();

            systemParent = GetComponent<ParticleSystem>();

            if(systemParent.emission.enabled == true)
                emissionValue = true;
            else
                emissionValue = false;
        }

        public void StartEmission ()
        {
            for (int i = 0; i < systems.Length; i++)
            {
                var emission = systems [i].emission;
                emission.enabled = true;
            }

            emissionValue = true;
        }

        public void StopEmission ()
        {
            for (int i = 0; i < systems.Length; i++)
            {
                var emission = systems [i].emission;
                emission.enabled = false;
            }

            emissionValue = false;
        }

        public void ToggleEmission ()
        {
            emissionValue = !emissionValue;

            for (int i = 0; i < systems.Length; i++)
            {
                var emission = systems [i].emission;
                emission.enabled = emissionValue;
            }
        }
    }
}