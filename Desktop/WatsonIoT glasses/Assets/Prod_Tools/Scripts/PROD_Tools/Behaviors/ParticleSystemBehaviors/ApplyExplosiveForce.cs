﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace ProdTools
{
    /// <summary>
    /// Applies an explosive force to nearby objects with rigidbodies
    /// </summary>
    public class ApplyExplosiveForce : MonoBehaviour
    {
        public float explosionRadius = 10;
        public float explosionForce = 4;

        public bool runOnEnable;

        void OnEnable ()
        {
            if(runOnEnable)
                StartCoroutine (ApplyForce());
        }

        public void ExplosiveForce ()
        {
            StartCoroutine(ApplyForce());
        }

        IEnumerator ApplyForce ()
        {
            // wait one frame because some explosions instantiate debris which should then
            // be pushed by physics force
            yield return null;
            
            var cols = Physics.OverlapSphere (transform.position, explosionRadius);
            var rigidbodies = new List<Rigidbody> ();
            
            for (int i = 0; i < cols.Length; i++)
            {
                if (cols [i].attachedRigidbody != null && !rigidbodies.Contains (cols [i].attachedRigidbody)) 
                {
                    rigidbodies.Add (cols [i].attachedRigidbody);
                }
            }

            for (int j = 0; j < rigidbodies.Count; j++) 
            {
                rigidbodies [j].AddExplosionForce (explosionForce, transform.position, explosionRadius, 1, ForceMode.Impulse);
            }
        }
    }
}