﻿using UnityEngine;
using System.Collections;

namespace ProdTools
{
    /// <summary>
    /// Waits for all particles to expire
    /// before destroying the gameObject
    /// </summary>
    public class DestroyParticlesAfterEmission: MonoBehaviour
    {
        float maxDuration;
        float maxLifetime;
        ParticleSystem[] systems;

        IEnumerator destroySystem;

        // Gets all of the particle systems and finds out the max duration and max lifetime of all systems to make sure all particles have stopped emitting before destroying the entire system
        void Awake ()
        {
            systems = GetComponentsInChildren<ParticleSystem>();

            // Get the maximum lifetime of any particles in this effect
            for (int i = 0; i < systems.Length; i++) 
            {
                maxLifetime = Mathf.Max (systems [i].startLifetime, maxLifetime);
            }
            // Get the maximum duration of any particles in this effect
            for (int i = 0; i < systems.Length; i++)
            {
                maxDuration = Mathf.Max (systems [i].duration, maxDuration);
            }
        }
            
        void OnEnable ()
        {
            DestroyParticles();
        }

        // Called OnEnable to destroy the particle system after it has stopped emitting
        void DestroyParticles ()
        {
            if(destroySystem != null)
                StopCoroutine(destroySystem);
            destroySystem = DestroySystem();
                StartCoroutine(destroySystem);
        }

        // The coroutine to begin shutting off and destroying the particle system
        IEnumerator DestroySystem ()
        {
            BroadcastMessage ("Extinguish", SendMessageOptions.DontRequireReceiver);

            // Wait for any remaining particles to expire
            yield return new WaitForSeconds (maxDuration + maxLifetime);

            Destroy (gameObject);
        }
    }
}