﻿using UnityEngine;
using System.Collections;

namespace ProdTools
{
    /// <summary>
    /// Allows a particle system to exist for a specified duration,
    /// then shuts off emission, and waits for all particles to expire
    /// before destroying the gameObject
    /// </summary>
    public class DestroyLoopingParticleSystem : MonoBehaviour
    {
        // The minimum and maximum duration of the particle system before it stops emitting
        public float minDuration = 0;
        public float maxDuration = 1;

        // The value of the particle that has the longest lifetime in the system
        float maxLifetime;
        // A failsafe to make sure we can't spam DestroyParticles();
        bool canExecute = true;
        // All of the particle systems in parented to the GameObject this script is attached to
        ParticleSystem[] systems;

        IEnumerator destroySystem;

        // Can be called from an EventTrigger or another Monobehaviour to shut off and destroy the particle system
        public void DestroyParticles ()
        {
            systems = GetComponentsInChildren<ParticleSystem> ();

            // Get the maximum lifetime of any particles in this effect
            for (int i = 0; i < systems.Length; i++) 
            {
                maxLifetime = Mathf.Max (systems [i].startLifetime, maxLifetime);
            }

            if(canExecute)
            {
                if(destroySystem != null)
                    StopCoroutine(destroySystem);
                destroySystem = DestroySystem();
                    StartCoroutine(destroySystem);

                canExecute = false;
            }
        }

        // The coroutine to begin shutting off and destroying the particle system
        IEnumerator DestroySystem ()
        {
            float stopTime = Time.time + Random.Range(minDuration, maxDuration);

            while (Time.time < stopTime)
            {
                yield return null;
            }

            // Turn off emission
            for (int j = 0; j < systems.Length; j++) 
            {
                var emission = systems [j].emission;
                emission.enabled = false;
            }

            BroadcastMessage ("Extinguish", SendMessageOptions.DontRequireReceiver);

            // Wait for any remaining particles to expire and then destroy the GameObject
            yield return new WaitForSeconds (maxLifetime);
            Destroy (gameObject);
        }
    }
}