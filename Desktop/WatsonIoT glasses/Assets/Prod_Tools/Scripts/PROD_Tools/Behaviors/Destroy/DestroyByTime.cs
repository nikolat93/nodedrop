﻿using UnityEngine;
using System.Collections;

namespace ProdTools
{
    public class DestroyByTime : MonoBehaviour
    {
        public float lifetime;

        void Start()
        {
            Destroy (gameObject, lifetime);
        }

        void OnApplicationQuit ()
        {
            Destroy (this.gameObject, lifetime);
        }
    }
}