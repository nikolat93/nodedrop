﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;

namespace DaqriProd
{

    [System.Serializable]
    public class Sequence {

        public float onEnterDelay = 0f;
        public UnityEvent onEnter;

        public float onExitDelay = 0f;
        public UnityEvent onExit;
    }

    public class Sequencer : MonoBehaviour {

        public List<Sequence> sequenceEvent;
        [Tooltip("Starts the sequence OnEnable if set to true")]
        public bool startOnEnable = false;

        void OnEnable ()
        {
            if(startOnEnable)
                StartCoroutine(_FireSequence());
        }

        public void StartSequencer ()
        {
            StartCoroutine(_FireSequence());
        }

        public void StopSequencer ()
        {
            StopCoroutine(_FireSequence());
        }

        IEnumerator _FireSequence ()
        {
            foreach (Sequence seqEvent in sequenceEvent)
            {
                if(seqEvent.onEnter != null)
                {
                    yield return new WaitForSeconds (seqEvent.onEnterDelay);
                    seqEvent.onEnter.Invoke();
                }

                if(seqEvent.onExit != null)
                {
                    yield return new WaitForSeconds(seqEvent.onExitDelay);
                    seqEvent.onExit.Invoke();
                }
            }
        }
    }
}