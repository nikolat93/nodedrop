﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace DaqriProd
{
    [System.Serializable]
    public  class AudioEvent 
    {
        public AudioSource audioSource;
        public AudioClip audioClip;

        public float exitTime;
        public bool waitForClip = true;

        [HideInInspector]
        public float audioClipLength;
    }

    public class AudioSequence : MonoBehaviour {

        public List<AudioEvent> audioEvent;

        public bool startOnEnable = false;

        void Awake ()
        {
            // Attempts to assign an AudioSource to each AudioEvent in the list if none has been assigned in the inspector
            foreach (AudioEvent audEvent in audioEvent)
            {
                if(audEvent.audioSource == null)
                    audEvent.audioSource = gameObject.GetComponent<AudioSource>();
            }
        }

        void OnEnable ()
        {
            // If true, the sequence automatically begins when the gameobject this component is attached to is enabled
            if(startOnEnable)
                StartCoroutine(FireEvent());
        }

        // Stops the coroutine when the gameobject this component is attached to is disabled
        void OnDisable ()
        {
            StopCoroutine(FireEvent());
        }

        // Starts the coroutine when called
        public void StartSequence ()
        {
            StartCoroutine(FireEvent());
        }

        // Stops the coroutine when called
        public void StopSequence ()
        {
            StopCoroutine(FireEvent());
        }

        // Stops the coroutine when the gameobject this component is attached to is destroyed
        void OnDestroy ()
        {
            StopCoroutine(FireEvent());
        }

        IEnumerator FireEvent ()
        {
            foreach (AudioEvent audEvent in audioEvent)
            {
                if(audEvent.audioSource != null)
                {
                    if(audEvent.audioClip != null)
                    {
                        audEvent.audioSource.clip = audEvent.audioClip;
                        audEvent.audioSource.Play();
                    }
                    else
                        Debug.Log("No Audio Clip assigned to Animator Event on " + gameObject.name);
                }
                else
                {
                    Debug.Log("No AudioSource assigned to Animator Event on " + gameObject.name);
                }

                // Used to make sure the audio clip has finished playing before moving on to the next event if waitForClip is set to true
                if(!audEvent.waitForClip)
                {
                    yield return new WaitForSeconds (audEvent.exitTime);
                }
                else
                {
                    if(audEvent.waitForClip && audEvent.audioClip != null)
                    {
                        audEvent.audioClipLength = audEvent.audioClip.length;

                        yield return new WaitForSeconds (audEvent.audioClipLength + audEvent.exitTime);
                    }
                }
            }
        }
    }
}
