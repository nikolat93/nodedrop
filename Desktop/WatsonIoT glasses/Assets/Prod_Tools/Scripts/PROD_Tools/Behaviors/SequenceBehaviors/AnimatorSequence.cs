﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace ProdTools
{
    public class FireAnimatorSequence : MonoBehaviour {

        public List<AnimatorEvent> animatorEvent;

        public bool startOnEnable = false;

        void OnEnable ()
        {
            if(startOnEnable)
                StartCoroutine(FireEvent());
        }

        void OnDisable ()
        {
            StopCoroutine(FireEvent());
        }

        public void StartSequence ()
        {
            StartCoroutine(FireEvent());
        }

        public void StopSequence ()
        {
            StopCoroutine(FireEvent());
        }

        IEnumerator FireEvent ()
        {
            foreach (AnimatorEvent animEvents in animatorEvent)
            {
                yield return new WaitForSeconds (animEvents.waitTime);

                if(animEvents.audioSource != null)
                {
                    if(animEvents.audioClip != null)
                        animEvents.audioSource.clip = animEvents.audioClip;
                    else
                        Debug.Log("No Audio Clip assigned to Animator Event on " + gameObject.name);

                    animEvents.audioSource.Play();
                }
                else
                {
                    Debug.Log("No AudioSource assigned to Animator Event on " + gameObject.name);
                }

                if(animEvents.animatorController != null)
                {
                    foreach (Animator animEvent in animEvents.animatorController)
                        animEvent.SetBool(animEvents.parameter, animEvents.value);
                }
                else
                {
                    Debug.Log("No AnimatorController assigned to Animator Event on " + gameObject.name);
                }
            }
        }
    }
}
