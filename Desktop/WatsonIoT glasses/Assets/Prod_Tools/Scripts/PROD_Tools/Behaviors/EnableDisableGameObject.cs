﻿using UnityEngine;
using System.Collections;

public class EnableDisableGameObject : MonoBehaviour {

    public GameObject [] objectsToToggle;

    public void EnableObjects ()
    {
        foreach(GameObject go in objectsToToggle)
            go.SetActive(true);
    }

    public void DisableObject ()
    {
        foreach(GameObject go in objectsToToggle)
            go.SetActive(false);
    }

    public void ToggleObjects () 
    {
        foreach(GameObject go in objectsToToggle)
        {
            if(go.activeSelf)
                go.SetActive(false);
            else
                go.SetActive(true);
        }
    }
}
