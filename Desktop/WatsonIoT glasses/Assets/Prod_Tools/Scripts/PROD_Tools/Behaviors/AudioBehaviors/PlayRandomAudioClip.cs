﻿using UnityEngine;
using System.Collections;

namespace ProdTools
{
    public class PlayRandomAudioClip: MonoBehaviour {

        public AudioClip[] audioClip;
        public AudioSource audioSource;

        // The amount of time to wait before starting the coroutine
        public float delay;
        // If true, the coroutine restarts after it is over
        public bool restart;
        // Start the Coroutine to play a random clip OnEnable
        public bool runOnEnable = true;

        // Cached coroutines
        IEnumerator playRandomClip;

        // Grab the audio source attached to this game object if none is assigned
        void Awake ()
        {
            if(audioSource == null)
                audioSource = gameObject.GetComponent<AudioSource> ();

            if(audioSource == null)
                Debug.Log("No AudioSource component found on " + gameObject.name);
        }

        void OnEnable ()
        {
            if(runOnEnable)
            {
                if(playRandomClip != null)
                    StopCoroutine(playRandomClip);
                playRandomClip = PlayRandomClip();
                    StartCoroutine(playRandomClip);
            }
        }

        // Can be called from an Event Trigger to start the coroutine
        public void PickRandomAudioClip ()
        {
            // Don't play another clip if one is already playing
            if(audioSource.isPlaying)
                return;
            
            if(playRandomClip != null)
                StopCoroutine(playRandomClip);
            playRandomClip = PlayRandomClip();
            StartCoroutine(playRandomClip);
        }

        IEnumerator PlayRandomClip()
        {
            yield return new WaitForSeconds(delay);
            if(audioSource)
            {
                audioSource.clip = audioClip[Random.Range(0, audioClip.Length)];
                audioSource.Play();
            }

            if (restart)
            {
                if(playRandomClip != null)
                    StopCoroutine(playRandomClip);
                playRandomClip = PlayRandomClip();
                    StartCoroutine(playRandomClip);
            }
        }
    }
}