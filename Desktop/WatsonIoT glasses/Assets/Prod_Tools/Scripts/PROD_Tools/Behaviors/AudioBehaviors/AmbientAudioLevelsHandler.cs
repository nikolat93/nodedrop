﻿using UnityEngine;
using System.Collections;

namespace ProdTools
{
    public class AmbientAudioLevelsHandler : MonoBehaviour {

        [Range(0, 1)] public float minVolume = 0.0f;
        [Range(0, 1)] public float maxVolume = 1.0f;

        AudioSource audioSource;
        AmbientAudioLevelsController ambAudCtrl;

        // Set up the references
        void Awake ()
        {
            ambAudCtrl = FindObjectOfType<AmbientAudioLevelsController>();
            audioSource = GetComponent<AudioSource>();

            if(audioSource != null)
            {
                audioSource.volume = maxVolume;
            }
            else
            {
                Debug.Log("No AudioSource component found on " + gameObject.name);
            }
        }

        // Used to adjust the volume up or down depending on the state
        void Update ()
        {
            // Bring the volume down to the minVolume assigned in the inspector
            if(ambAudCtrl.adjustLevels && audioSource.volume > minVolume)
            {
                audioSource.volume -= ambAudCtrl.adjustSpeed * Time.deltaTime;

                if(audioSource.volume <= minVolume)
                {
                    audioSource.volume = minVolume;
                    return;
                }
            }
            else
            {
                // Bring the volume up to the maxVolume assigned in the inspector
                if(!ambAudCtrl.adjustLevels && audioSource.volume < maxVolume)
                audioSource.volume += ambAudCtrl.adjustSpeed * Time.deltaTime;

                if(audioSource.volume >= maxVolume)
                {
                    audioSource.volume = maxVolume;
                    return;
                }
            }
        }
    }
}