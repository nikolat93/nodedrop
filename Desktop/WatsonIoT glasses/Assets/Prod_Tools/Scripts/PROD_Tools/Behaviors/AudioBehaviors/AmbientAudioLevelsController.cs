﻿using UnityEngine;
using System.Collections;

namespace ProdTools
{
    public class AmbientAudioLevelsController : MonoBehaviour {

        public float adjustSpeed = 0.5f;
        public float resetTime;

        public AudioSource[] ambientAudio;
        [HideInInspector]
        public bool adjustLevels;

        IEnumerator resetEvent;

        // If the coroutine is running, stop it. Then start the cached coroutine
        public void AdjustAudioLevel ()
        {
            if(resetEvent != null)
                StopCoroutine (resetEvent);
            resetEvent = ResetEvent();
                StartCoroutine(resetEvent);
        }

        // Set to boolean to true and reset it after the resetTime
        IEnumerator ResetEvent ()
        {
            adjustLevels = true;
            yield return new WaitForSeconds (resetTime);
            adjustLevels = false;
        }
    }
}