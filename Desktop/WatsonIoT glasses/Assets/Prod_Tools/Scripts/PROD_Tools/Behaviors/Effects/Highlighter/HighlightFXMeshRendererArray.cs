﻿using UnityEngine;
using System.Collections;

public class HighlightFXMeshRendererArray : MonoBehaviour
{
    public Renderer[] objectRendererArray;

    void Awake ()
    {
        for (int i = 0; i < objectRendererArray.Length; i++)
        {
            if ( !(objectRendererArray[i] is Renderer) ) objectRendererArray[i] = null;
        }
    }
}
