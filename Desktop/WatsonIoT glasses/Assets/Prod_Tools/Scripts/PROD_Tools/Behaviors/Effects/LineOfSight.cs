﻿using UnityEngine;
using System.Collections;

namespace ProdTools
{
    public class LineOfSight : MonoBehaviour
    {
        #region vars

        public GameObject target;
        public Material material;
        public Color colorStart = Color.yellow;
        public Color colorEnd = Color.red;
        public float thicknessStart = 0.005f;
        public float thicknessEnd = 0.001f;
        public float iterationsPerSecond = 60f;

        private Transform thisTransform;
        private LineRenderer lineRenderer;

        #endregion


        void OnEnable()
        {
            thisTransform = GetComponent<Transform>();
            
            lineRenderer = gameObject.AddComponent<LineRenderer>();
            //lineRenderer.material = new Material(Shader.Find("Particles/Additive"));
            lineRenderer.material = material;
            lineRenderer.SetColors(colorStart, colorEnd);
            lineRenderer.SetWidth(thicknessStart, thicknessEnd);
            lineRenderer.SetVertexCount(2);

            StartCoroutine( DrawLine() );
        }


        void OnDisable ()
        {
            StopCoroutine( DrawLine() );
        }


        IEnumerator DrawLine()
        {
            while (true)
            {
                if (target == null)
                    break;
                Vector3[] points = new Vector3[2];
                points[0] = thisTransform.position;
                points[1] = target.transform.position;

                lineRenderer.SetPositions(points);

                yield return new WaitForSeconds(1f/iterationsPerSecond);
            }
        }
    }
}