﻿using UnityEngine;
using System;
using System.Collections;

public class HighlightFXManager : MonoBehaviour {
    
    public GameObject[] rendererGroup;
    public static HighlightFXManager HighlightFXManagerInstance = null;

    private Camera dshCamera;
    private HighlightsFX highlighter;
    private HighlightFXMeshRendererArray mrArray;
    private bool highlightToggle = true;

    int index = 0;


    void Awake ()
    {
        // make singleton
        if (HighlightFXManagerInstance == null)
        {
            HighlightFXManagerInstance = this;
        }
        else if (HighlightFXManagerInstance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }


    void Start () 
    {
        dshCamera = GameObject.FindObjectOfType<Camera>();
        highlighter = dshCamera.gameObject.AddComponent<HighlightsFX>();
        mrArray = rendererGroup[0].GetComponent<HighlightFXMeshRendererArray>();
        highlighter.objectRenderer = mrArray.objectRendererArray;
    }


    void Update ()
    {
        highlighter.highlightToggle = highlightToggle;
    }


    public void NextIndex ()
    {
        index++;
        if (index > rendererGroup.Length-1) index = rendererGroup.Length-1;
        UpdateGroup();
    }

    public void NextIndexLoop ()
    {
        index++;
        if (index > rendererGroup.Length-1) index = 0;
        UpdateGroup();
    }


    public void PreviousIndex ()
    {
        index--;
        if (index < 0) index = 0;
        UpdateGroup();
    }

    public void PreviousIndexLoop ()
    {
        index--;
        if (index < 0) index = rendererGroup.Length-1;
        UpdateGroup();
    }

    void UpdateGroup ()
    {
        mrArray = rendererGroup[index].GetComponent<HighlightFXMeshRendererArray>();
        highlighter.objectRenderer = mrArray.objectRendererArray;
    }


    public void HighlightToggle ()
    {
        highlightToggle = !highlightToggle;
    }
}
