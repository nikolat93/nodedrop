﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;


public class Step : MonoBehaviour
{
    private const string TAG = "Step";
    // next step reference, this reference can be filled automatically by adding next steps, or reordering hierarchical
    public Step nextStep = null;
    // time after which change will be made when NextStep is called
    public float exitTime = 0f;
    // after this time, the step will automatically progress to another step
    public float nextStepTimeTransition = 0f;

    public List<GameObject> activateOnEnter;
    public List<GameObject> deactivateOnEnter;

    public UnityEvent OnEnter;
    public UnityEvent OnNextStep;

    // 1st level of depth children transforms, used to hide all step children
    private List<Transform> childrenTransforms = null;

    // These are to be used if we set the waitForAnimatorTrigger bool to true in the inspector if we need to wait for a trigger to fire before disabling this GameObject
    public bool waitForAnimatorTrigger = false;
    public Animator animatorController;
    public string parameter;

    void Start ()
    {
        if (childrenTransforms == null || childrenTransforms.Count == 0) {
            FillChildrenTransforms ();
        }
    }

    // this list contains hierarchy of objects it needs to activate to be activated in hierarchy
    List<GameObject> hierarchyToActivate;

    // deactivates first order children wen step is entered
    private bool deactivateStepChildrenOnEnter = true;

    // Step activates itself activating all parent hierarchy till its active
    public void EnterStep ()
    {
        Debug.Log ("Entering Step " + name);
        StepManager.Instance.EnteringState (this);

        if (OnEnter != null)
        {
            OnEnter.Invoke ();
        }

        // Default behavior, activate this object - and hierarchy of objects below
        gameObject.SetActive (true);

        // Make sure this step is active in hierarchy
        EnableObjectInHierarchy ();

        if (deactivateStepChildrenOnEnter) {
            // find 1st depth Step children and deactivate them
            Step[] stepChildren = GetComponentsInChildren  <Step> (true);
            foreach (Step step in stepChildren) {
                // check if step is a child of this object
                if (step.transform.parent == this.transform) {
                    step.transform.gameObject.SetActive (false);
                }
            }
        }

        foreach (GameObject activateMe in activateOnEnter) 
        {
            activateMe.SetActive (true);
        }

        foreach (GameObject deactivateMe in deactivateOnEnter) 
        {
            deactivateMe.SetActive (false);
        }
        if (nextStepTimeTransition > 0f) 
        {
            if (Application.isPlaying) {
                StartCoroutine (CallNextStepAfter (nextStepTimeTransition));
            }
            else
            {
                Debug.Log ("Step " + name + " has been setup to automatically switch to another step after " + nextStepTimeTransition + " sec skipping this in editor mode");
            }
        }
    }

    IEnumerator CallNextStepAfter (float timeDelay)
    {
        yield return new WaitForSeconds (timeDelay);
        NextStep ();
    }

    IEnumerator EnterNextStepAfter (float timeDelay)
    {
        yield return new WaitForSeconds (timeDelay);
        EnterNextStepAndNotifyStepManager ();
    }

    // This is most likely a temporary fix, but this coroutine will be called if the waitForAnimatorTrigger bool is set to true.
    // If it is, fire the animator trigger on the Animator dragged into the inspector then move on to the next step when done.
    IEnumerator WaitForAnimatorTrigger ()
    {
        if(animatorController != null)
            animatorController.SetTrigger(parameter);

        yield return new WaitForSeconds (1.5f);

        if (nextStep != null) 
        {
            HideStep ();
            nextStep.EnterStep ();
        }

        if (OnNextStep != null) 
            OnNextStep.Invoke ();
    }

    // deactivates 1st level depth children
    public void HideStep ()
    {
        if (childrenTransforms != null)
        {
            foreach (Transform child in childrenTransforms)
            {
                child.gameObject.SetActive (false);
            }
        }
        gameObject.SetActive (false);
    }

    public void NextStep ()
    {
        if (exitTime > 0f && Application.isPlaying) {
            StartCoroutine (EnterNextStepAfter (exitTime));
        } else {
            if (exitTime > 0f) {
                Debug.Log ("Step editor mode, there is exit time set to " + exitTime
                + " skipping in editor mode to next step");
            }
            EnterNextStepAndNotifyStepManager ();
        }
    }

    /// <summary>
    /// Enters the next step and notify step manager. This method is used by Step Manager, user should not use it.
    /// TODO hide from user.
    /// </summary>
    public void EnterNextStepAndNotifyStepManager (bool forceIgnoreAnimationTriggers = false)
    {
        StepManager.Instance.CallingNextStep (this, nextStep);
        if (nextStep != null) {
            if(!waitForAnimatorTrigger || forceIgnoreAnimationTriggers)
            {
                HideStep ();
                nextStep.EnterStep ();
            }
            else
            {
                StartCoroutine(WaitForAnimatorTrigger());
                return;
            }
        }
        if (OnNextStep != null) {
            OnNextStep.Invoke ();
        }
    }

    void Reset ()
    {
        StepManager.Instance.AddStep (gameObject);
    }

    void EnableObjectInHierarchy ()
    {
        if (!gameObject.activeInHierarchy) {
            if (hierarchyToActivate == null) {
                //find out the hierarchy which needs to be enabled
                hierarchyToActivate = new List<GameObject> ();
                while (!gameObject.activeInHierarchy) {
                    GameObject parent = transform.parent.gameObject;
                    parent.SetActive (true);
                    hierarchyToActivate.Add (parent);
                }
            } else {
                foreach (GameObject obj in hierarchyToActivate) {
                    obj.SetActive (true);
                }
            }
        }
    }

    public void FillActivateDeactivateLists ()
    {
        FillChildrenTransforms ();
        if (activateOnEnter == null) {
            activateOnEnter = new List<GameObject> ();
        } else {
            activateOnEnter.Clear ();
        }
        if (deactivateOnEnter == null) {
            deactivateOnEnter = new List<GameObject> ();
        } else {
            deactivateOnEnter.Clear ();
        }
        foreach (Transform child in childrenTransforms) {
            if (child.gameObject.activeInHierarchy) {
                activateOnEnter.Add (child.gameObject);
            } else {
                deactivateOnEnter.Add (child.gameObject);
            }
        }

    }

    void FillChildrenTransforms ()
    {
        Transform[] children = gameObject.GetComponentsInChildren <Transform> (true);
        if (childrenTransforms == null) {
            childrenTransforms = new List<Transform> ();
        } else {
            childrenTransforms.Clear ();
        }
        foreach (Transform child in children) {
            if (child.parent == transform) {
                childrenTransforms.Add (child);
            }
        }
    }
}

