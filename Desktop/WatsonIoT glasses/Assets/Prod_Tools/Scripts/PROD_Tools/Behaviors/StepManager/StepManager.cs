﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;
using System;

public class StepManager : MonoBehaviour
{
    private static StepManager instance;

    public Step startStep;
    public Step currentStep = null;

    // Connects newly added component to last step in list
    public bool connectNewStepToLastStep = true;

    public List<Step> stepsList;
    public List<Step> startStepProposalList;

    public UnityEvent lastInstructionCalled;

    public bool keyboardNavigation = true;
    public KeyCode nextStepKey = KeyCode.Z;
    public KeyCode prevStepKey = KeyCode.X;

    // Store visit history, for now it works as a stack
    private Stack<Step> stepCallHistory;

    public static StepManager Instance
    {
        get
        {
            return instance;
        }
    }

    void Awake ()
    {
        if (instance != null)
        {
            Destroy(this);
            return;
        }
        instance = this;
    }
        
    void Start ()
    {
        FirstStep ();
    }

    // Used to support keyboard input for moving back and forth between steps
    void Update ()
    {
        if (keyboardNavigation) {
            if (Input.GetKeyDown (nextStepKey)) {
                NextStep ();
            }
            if (Input.GetKeyDown (KeyCode.X)) {
                PrevStep ();
            }
        }
    }

    // Adds the step to the StepManager. If the option to connect to last step is checked, new step will be connected to last one
    public void AddStep (GameObject step)
    {
        if (stepsList == null)
        {
            stepsList = new List<Step> ();
        }

        if (step != null)
        {
            Step stepComponent = step.GetComponent <Step> ();
            if (stepComponent != null)
            {
                stepsList.Add (stepComponent);
                if (connectNewStepToLastStep)
                {
                    Step lastStep = FindLastStep();
                    lastStep.nextStep = stepComponent;
                }
            }
        }
    }

    // Removes missing componenets from the StepManager
    public void ValidateSteps ()
    {
        // search for all Step objects
        // this call returns also prefabs, so make sure each of the Step 
        // is in the hierarchy
        Step[] steps = Resources.FindObjectsOfTypeAll<Step> ();
        if (stepsList == null)
        {
            stepsList = new List<Step> ();
        }
        // add them to stepsList
        stepsList.Clear ();
        foreach (Step foundStep in steps)
        {
            // TODO find the way to determine if this is a prefab,
            if (foundStep.transform.parent != null)
            {
                stepsList.Add (foundStep);
            }
        }

        if (stepCallHistory != null) {
            stepCallHistory.Clear ();
        }

        DetermineFirstStep ();
        ClearCallHistory ();
    }

    void DetermineFirstStep ()
    {
        if (startStepProposalList == null) {
            startStepProposalList = new List<Step> ();
        }
        startStepProposalList.Clear ();

        if (startStep == null) {
            Debug.Log ("StepManager automatically picking start step");
            HashSet<Step> notReferencedSteps = new HashSet<Step> ();
            // add all steps to set
            foreach (Step step in stepsList) {
                notReferencedSteps.Add (step);
            }
            // remove referenced ones
            foreach (Step step in stepsList) {
                if(step.nextStep != null) {
                    notReferencedSteps.Remove (step.nextStep);
                }
            }
            foreach (Step step in notReferencedSteps) {
                startStepProposalList.Add (step);
            }
            if (startStepProposalList.Count > 0) {
                Debug.LogWarning ("StepManager detected few steps unreferenced by other steps nextStep mechanism, all objects " +
                "proposed as start objects will be put to StartStepProposalList for your convienience, automatically picking first");

                startStep = startStepProposalList [0];
            } else {
                Debug.LogWarning ("No unreferenced step by nextStep mechanism found. Possible no steps or loop in steps");
            }
            
        }
        if (currentStep == null) {
            currentStep = startStep;
        }
    }

    // This method is called from Step.EnterStep() method
    public void EnteringState (Step step)
    {
        HideAllSteps ();
        currentStep = step;
        if (stepCallHistory == null)
        {
            stepCallHistory = new Stack<Step> ();
        }
        stepCallHistory.Push (step);
    }

    public void CallingNextStep (Step currentStep, Step nextStep)
    {
        if (nextStep == null)
        {
            Debug.LogWarning ("StepManager the step named " + currentStep.name + " is null. Is it last step in the experience ?");
            if (lastInstructionCalled != null)
            {
                lastInstructionCalled.Invoke ();
            }
        }
    }

    /// <summary>
    /// Toggles next step ignoring exit and automatic transition time.
    /// </summary>
    public void NextStep (bool forceIgnoreAnimationTriggers = false)
    {
        if (currentStep != null)
        {
            currentStep.EnterNextStepAndNotifyStepManager (forceIgnoreAnimationTriggers);
        }
    }

    /// <summary>
    /// Goes back one step in called history.
    /// </summary>
    public void PrevStep ()
    {
        /// TODO maybe we need normal flow conencted by nextStep to be traversable with this mehtods?
        if (stepCallHistory != null)
        {
            if (stepCallHistory.Count > 1)
            {
                currentStep.HideStep ();
                stepCallHistory.Pop (); 
                Step prevStep = stepCallHistory.Pop ();
                prevStep.EnterStep ();
            }
            else
            {
                Debug.Log ("StepManager already at first step");
            }
        }
    }

    void ClearCallHistory ()
    {
        if (stepCallHistory == null) {
            stepCallHistory = new Stack<Step> ();
        } else {
            stepCallHistory.Clear ();
        }
    }

    void HideAllSteps ()
    {
        foreach (Step step in stepsList)
        {
            step.HideStep ();
        }
    }

    // Editor method to show first step
    public void FirstStep ()
    {
        HideAllSteps ();
        ClearCallHistory ();
        startStep.EnterStep ();
    }

    // Finds last connected step with nextStep
    Step FindLastStep ()
    {
        Step lastStep = startStep;
        if (lastStep != null)
        {
            while (lastStep.nextStep != null)
            {
                lastStep = lastStep.nextStep;
            }
        }
        return lastStep;
    }

    // Editor method to show last step
    public void LastStep ()
    {
        HideAllSteps ();
        // Follow each members from startStep until last step with no nextStep is found
        Step lastStep = FindLastStep ();
        lastStep.EnterStep ();
    }

    public void RemoveStep (Step step)
    {
        if (stepsList != null)
        {
            foreach (Step localStep in stepsList)
            {
                if (localStep == step)
                {
                    stepsList.Remove (localStep);
                    Debug.Log ("StepManager removing step " + localStep.name); 
                }
            }
        }
    }
}
