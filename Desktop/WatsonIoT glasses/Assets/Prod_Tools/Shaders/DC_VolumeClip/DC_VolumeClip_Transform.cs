﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class DC_VolumeClip_Transform : MonoBehaviour
{
    public Transform clipVolume;

    private Transform thisTransform;
    private Material[] mtls;

    void OnEnable ()
    {
        thisTransform = transform;
        mtls = thisTransform.GetComponent<Renderer>().sharedMaterials;
    }

    void Update()
    {
        if (!clipVolume) return;

        for (int i = 0; i < mtls.Length; i++)
        {
            mtls[i].SetMatrix("_TransformMatrix", clipVolume.worldToLocalMatrix);
        }
    }
}
