﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AnimateGauge : MonoBehaviour {

	public Image redRing;
	public Text temprature;

	void Update()
	{
		temprature.text = ((int)(300 * (redRing.fillAmount+ 0.1))).ToString();
	}
}
