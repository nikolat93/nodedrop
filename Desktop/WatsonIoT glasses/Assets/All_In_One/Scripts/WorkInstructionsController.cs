﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace DAQRI
{
    public class WorkInstructionsController : MonoBehaviour
    {
        private const string APPEAR_ANIMATION_TRIGGER = "show";


        [SerializeField]
        private Animator appearAnimator;

        [SerializeField]
        private Text instructionText;


        public void SetInstructionsText(string instructions)
        {
            instructionText.text = instructions;
        }

        public void SetInstructionsVisible(bool visible)
        {
            appearAnimator.SetBool(APPEAR_ANIMATION_TRIGGER, visible);
        }
    }
}
