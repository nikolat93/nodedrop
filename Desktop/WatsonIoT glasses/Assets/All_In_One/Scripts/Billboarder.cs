﻿using UnityEngine;
using System.Collections;

public class Billboarder : MonoBehaviour {

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

		Vector3 toCam = transform.position - Camera.main.transform.position;
		toCam.y = 0f;

		Quaternion lookAt = Quaternion.LookRotation(toCam, Vector3.up);
		gameObject.transform.rotation = lookAt;
	}

}