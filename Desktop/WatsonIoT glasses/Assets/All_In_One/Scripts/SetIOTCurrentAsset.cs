﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using IBM;

public class SetIOTCurrentAsset : MonoBehaviour
{
    [SerializeField]
    private IoTClient client;

    [SerializeField]
    private bool oneTime = true;

    private bool didSetAsset = false;


    public void SetGameObjectAsIOTAsset()
    {
        if (didSetAsset && oneTime)
        {
            return;
        }

        client.CurrentAsset = gameObject.name;
        didSetAsset = true;
    }
}
