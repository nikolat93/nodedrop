﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DAQRI;

public class TileVisibility : MonoBehaviour {

    public DataValueVisualization tile;

    public void ToggleTileVisibility () {
        if (tile == null) {
            return;
        }

        tile.IsUIVisible = !tile.IsUIVisible;
    }
}
