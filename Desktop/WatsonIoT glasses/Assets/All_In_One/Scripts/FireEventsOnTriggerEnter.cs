﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class FireEventsOnTriggerEnter : MonoBehaviour {
	public UnityEvent eventsToFire;

	void OnTriggerEnter(Collider other) 
	{
		if (eventsToFire != null)
			eventsToFire.Invoke();
	}	
}
