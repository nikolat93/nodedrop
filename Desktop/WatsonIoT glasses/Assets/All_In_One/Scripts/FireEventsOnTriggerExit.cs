﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class FireEventsOnTriggerExit : MonoBehaviour {
	public UnityEvent eventsToFire;

	void OnTriggerExit(Collider other) 
	{
		if (eventsToFire != null)
			eventsToFire.Invoke();
	}	
}
