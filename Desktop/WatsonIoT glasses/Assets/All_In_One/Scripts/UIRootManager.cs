﻿using UnityEngine;
using System.Collections;

public class UIRootManager: MonoBehaviour {

	public LineRenderer rubberBand;
	public GameObject pumpPart;
	public GameObject icon;
	public GameObject warning;
	public GameObject gauge;
	public GameObject stage4;

	[SerializeField]
	private Animator animator;

	const string k_EnableWarning = "EnableWarning";
	const string k_EnableGauge = "EnableGauge";
	const string k_Fixed = "IsFixed";
	const string k_EnableStage4 = "EnableStage4";

	public bool fix = false;

	private int rubberBandVertexCount = 2;

//	public bool isFixed
//	{
//		set 
//		{
//			animator.SetBool(k_EnableWarnig, value);
//		}
//	}

	void Start()
	{

	}

	void Update()
	{
		UpdateRubberBand();
	}


	void UpdateRubberBand()
	{
		if (rubberBand != null && pumpPart != null) 
		{
			rubberBand.SetPosition(0, pumpPart.transform.position);
		}
		if (icon != null) 
		{
			rubberBandVertexCount = 2;
			rubberBand.SetVertexCount(rubberBandVertexCount);
			rubberBand.SetPosition(rubberBandVertexCount-1, icon.transform.position);
		}
		if (warning != null && warning.activeSelf) 
		{
			rubberBandVertexCount++;
			rubberBand.SetVertexCount(rubberBandVertexCount);
			rubberBand.SetPosition(rubberBandVertexCount-1, warning.transform.position);
		}
		if (gauge != null && gauge.activeSelf) 
		{
			rubberBandVertexCount++;
			rubberBand.SetVertexCount(rubberBandVertexCount);
			rubberBand.SetPosition(rubberBandVertexCount-1, gauge.transform.position);
		}
	}
	//for test
	void OnEnable()
	{
		if(fix)
			animator.SetBool(k_Fixed, true);
		
	}



	public void EnableWarning()
	{
		if (animator == null)
			return;
		if (!fix)
			animator.SetBool(k_EnableWarning, true);
		else
			EnableGauge();
	}

	public void DisableWarning()
	{
		if (animator == null)
			return;
		if (!fix)
			animator.SetBool(k_EnableWarning, false);
		else
			DisableGauge();	
	}

	public void EnableGauge()
	{
		if (animator == null)
			return;
		animator.SetBool(k_EnableGauge, true);
	}

	public void DisableGauge()
	{
		if (animator == null)
			return;
		animator.SetBool(k_EnableGauge, false);
	}

	public void EnableStage4()
	{
		if (animator == null)
			return;
		animator.SetBool(k_EnableStage4, true);
	}

	public void DisableStage4()
	{
		if (animator == null)
			return;
		animator.SetBool(k_EnableStage4, false);
	}


//
//	void OnTriggerExit(Collider other) 
//	{
//		if (animator != null) 
//		{
//			if (!fix)
//				animator.SetBool(k_EnableWarning, false);
////			else
////				animator.SetBool(k_EnableGauge, false);
//		}
//	}

	public void SwitchGauge()
	{
		if (animator != null) 
		{
			animator.SetBool(k_EnableGauge, !animator.GetBool(k_EnableGauge));
		}
	}
		
}