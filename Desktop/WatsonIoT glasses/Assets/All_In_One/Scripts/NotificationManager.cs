﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DAQRI
{
    public class NotificationManager : MonoBehaviour
    {
        [SerializeField]
        private Notification notificationPrefab;

        private static NotificationManager instance = null;
        public static NotificationManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = GameObject.FindObjectOfType<NotificationManager>();
                }

                return instance;
            }
        }

        public void PresentNotification(string message)
        {
            Notification notification = Instantiate(notificationPrefab);
            notification.SetMessage(message);

            notification.transform.SetParent(DisplayManager.Instance.transform);

            Vector3 worldPosition = Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 1f, 2f));
            Vector3 localPosition = DisplayManager.Instance.transform.InverseTransformPoint(worldPosition);
            notification.transform.localPosition = localPosition;
            notification.transform.localRotation = Quaternion.identity;
        }
    }
}
