﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace DAQRI
{
    [RequireComponent(typeof(Animator))]
    public class Notification : MonoBehaviour
    {
        private const float DISPLAY_TIME = 5f;
        private const string ANIMATION_TRIGGER_NAME = "show";

        [SerializeField]
        private Text messageText;

        void Start()
        {
            GetComponent<Animator>().SetBool(ANIMATION_TRIGGER_NAME, true);
            StartCoroutine("Hide");
        }

        public void SetMessage(string message)
        {
            messageText.text = message;
        }

        private IEnumerator Hide()
        {
            yield return new WaitForSeconds(DISPLAY_TIME);
            GetComponent<Animator>().SetBool(ANIMATION_TRIGGER_NAME, false);
        }

        /// <summary>
        /// This is called by a Unity animation event when the dismiss animation finishes.
        /// </summary>
        private void NotificationDidDismiss()
        {
            Destroy(gameObject);
        }
    }
}
