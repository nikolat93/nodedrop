﻿
Shader "DC_Shaders/DC_DarwinCutaway"
{
	Properties
	{
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_MetallicGlossMap ("MetallicGloss", 2D) = "black" {}
		_GlossMapScale ("Smoothness", Range(0,1)) = 1.0
		[Normal]_BumpMap ("Bumpmap", 2D) = "bump" {}
		_OcclusionStrength ("Occlusion", Range(0,1)) = 1.0
		_EmissionColor ("Glow Color", Color) = (.5,.5,1,1)
		_EdgeGlowScale ("Edge Glow Size", Range(0,0.3)) = 0.01
	}
	SubShader
	{
		Tags { "RenderType" = "Opaque" "Queue" = "Geometry" }
		Cull Off
		LOD 200
		ZWrite On
		
		CGPROGRAM
		#pragma surface surf Standard addshadow vertex:vert
		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _MetallicGlossMap;
		sampler2D _BumpMap;

		struct Input
		{
			float2 uv_MainTex;
			float3 worldPos;
			float3 clipPos;
		};

		fixed4 _Color;
		fixed _GlossMapScale;
		fixed _OcclusionStrength;
		fixed4 _EmissionColor;
		fixed _EdgeGlowScale;
		float4x4 _TransformMatrix;

		void vert(inout appdata_full v, out Input o)
		{
			UNITY_INITIALIZE_OUTPUT(Input,o);
			o.clipPos = mul(_TransformMatrix, mul(unity_ObjectToWorld, v.vertex));
		}

		void surf (Input IN, inout SurfaceOutputStandard o)
		{
			fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
			fixed4 m = tex2D(_MetallicGlossMap, IN.uv_MainTex);

			if (length(IN.clipPos) < 0.5) discard;

			o.Albedo = lerp(c.rgb, float3(1,1,1), smoothstep(0.5 + _EdgeGlowScale, 0.5, length(IN.clipPos)));
			o.Metallic = m.r;
			o.Smoothness = m.a * _GlossMapScale;
			o.Occlusion = m.g + ((1-m.g) * (1-_OcclusionStrength));
			o.Normal = UnpackNormal (tex2D (_BumpMap, IN.uv_MainTex));
			o.Emission = lerp(float3(0,0,0), _EmissionColor, smoothstep(0.5 + _EdgeGlowScale, 0.5, length(IN.clipPos)));
		}
		ENDCG
	}
	FallBack "Diffuse"
}
