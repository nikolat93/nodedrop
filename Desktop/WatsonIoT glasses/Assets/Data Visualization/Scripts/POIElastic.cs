﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DAQRI;

public class POIElastic : MonoBehaviour {
    
    public Transform bottomAnchor;
    public RectTransform tile;
    public RectTransform normalIcon;
    public RectTransform warningIcon;
    public RectTransform criticalIcon;

    public LineRenderer bottomLineRenderer;
    public LineRenderer topLineRenderer;
    public float iconImageInset = 0.005f; // Use this if the image has a transparent border you need to account for

    [HideInInspector]
    public DataState state = DataState.Normal;


    public bool IsTopVisible {
        get { return topLineRenderer.gameObject.activeInHierarchy; }
        set { topLineRenderer.gameObject.SetActive (value); }
    }

    private Vector3 BottomAnchorPoint {
        get { return bottomAnchor.position; }
    }

    private Vector3 TileAnchor {
        get { return tile.position; }
    }

    private Vector3 IconBottomAnchor {
        get {
            Vector3 anchor = Vector3.zero;

            switch (state) {
            case (DataState.Normal):
                anchor = normalIcon.position;
                anchor.y -= YWorldHalfWidth (normalIcon);
                break;

            case (DataState.Warning):
                anchor = warningIcon.position;
                anchor.y -= YWorldHalfWidth (warningIcon);
                break;

            case (DataState.Critical):
                anchor = criticalIcon.position;
                anchor.y -= YWorldHalfWidth (criticalIcon);
                break;
            }

            return anchor;
        }
    }

    private Vector3 IconTopAnchor {
        get {
            Vector3 anchor = Vector3.zero;

            switch (state) {
            case (DataState.Normal):
                anchor = normalIcon.position;
                anchor.y += YWorldHalfWidth (normalIcon);
                break;

            case (DataState.Warning):
                anchor = warningIcon.position;
                anchor.y += YWorldHalfWidth (warningIcon);
                break;

            case (DataState.Critical):
                anchor = criticalIcon.position;
                anchor.y += YWorldHalfWidth (criticalIcon);
                break;
            }

            return anchor;
        }
    }


	void Start () {
        bottomLineRenderer.positionCount = 2;
        bottomLineRenderer.useWorldSpace = true;
        bottomLineRenderer.SetPosition (0, BottomAnchorPoint);
        bottomLineRenderer.SetPosition (1, IconBottomAnchor);

        topLineRenderer.positionCount = 2;
        topLineRenderer.useWorldSpace = true;
        topLineRenderer.SetPosition (0, IconTopAnchor);
        topLineRenderer.SetPosition (1, TileAnchor);
	}

	void LateUpdate () {
        bottomLineRenderer.SetPosition (0, BottomAnchorPoint);
        bottomLineRenderer.SetPosition (1, IconBottomAnchor);

        if (IsTopVisible) {
            topLineRenderer.SetPosition (0, IconTopAnchor);
            topLineRenderer.SetPosition (1, TileAnchor);
        }
	}

    private float YWorldHalfWidth (RectTransform rectTransform) {
        return (rectTransform.rect.height / 2f) * rectTransform.lossyScale.y - iconImageInset;
    }
}
