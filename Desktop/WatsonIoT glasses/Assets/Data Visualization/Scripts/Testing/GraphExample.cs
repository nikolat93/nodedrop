﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DAQRI;

public class GraphExample : MonoBehaviour {

    public List<LineGraph> graphs;
    bool upwards = true;

	void Start () {
        StartCoroutine ("Plot");
	}

    private IEnumerator Plot () {
        while (true) {
            yield return new WaitForSeconds(5f);

            foreach (LineGraph graph in graphs) {
                graph.yMin = (upwards ? 0f : 100f);
                graph.yMax = (upwards ? 100f : 200f);
                graph.numberOfYLabels = 5;
                List<float> values = new List<float> ();

                if (upwards) {
                    for (int i = (int)graph.yMin; i < (int)graph.yMax; i++) {
                        values.Add (i - UnityEngine.Random.value * 4f);
                    }
                } else {
                    for (int i = (int)graph.yMax; i > (int)graph.yMin; i--) {
                        values.Add (i - UnityEngine.Random.value * 4f);
                    }
                }

                graph.PlotValues (values);
            }

            upwards = !upwards;
        }
	}
}
