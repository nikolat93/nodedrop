﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DAQRI;

[ExecuteInEditMode]
public class DataTileGraphVisibility : MonoBehaviour {

    public RectTransform tile;
    public RectTransform graph;

    public bool showGraph = true;
    private bool showGraphLastValidation = true;


	void Update () {
        if (showGraph != showGraphLastValidation) {
            showGraphLastValidation = showGraph;

            graph.gameObject.SetActive (showGraph);

            int resizeFactor = (showGraph ? 1 : -1);
            Vector2 sizeDelta = tile.sizeDelta;
            sizeDelta.x = sizeDelta.x + resizeFactor * graph.rect.width;
            tile.sizeDelta = sizeDelta;
        }
	}
}
