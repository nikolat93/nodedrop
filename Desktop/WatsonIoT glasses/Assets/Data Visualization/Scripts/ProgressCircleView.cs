﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DAQRI {

    [ExecuteInEditMode]
    public class ProgressCircleView : AbstractProgressView {

        public UICircle emptyCircle;
        public UICircle filledCircle;


        protected override void RedrawFilledColor () {
            if (filledCircle == null) {
                return;
            }

            filledCircle.color = FilledColor;
        }

        protected override void RedrawEmptyColor () {
            if (emptyCircle == null) {
                return;
            }

            emptyCircle.color = EmptyColor;
        }

        protected override void RedrawFilledArea () {
            if (filledCircle == null) {
                return;
            }

            float percent = (currentValueForAnimation - MinValue) / (MaxValue - MinValue) * 100f;
            percent = Mathf.Clamp (percent, 0f, 100f);
            filledCircle.fillPercent = percent;
            filledCircle.SetVerticesDirty ();
        }
    }
}
