﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace DAQRI {

    [Serializable]
    [ExecuteInEditMode]
    public class DataStateHandler {

        public ImportantDataRanges warningRanges = new ImportantDataRanges ();
        public ImportantDataRanges criticalRanges = new ImportantDataRanges ();

        public UnityEvent OnDataEnteredNormalRange;


        private DataState state = DataState.Normal;
        internal DataState State {
            get { return state; }
            private set {
                if (state == value) {
                    return;
                }

                state = value;

                switch (state) {
                case (DataState.Normal):
                    OnDataEnteredNormalRange.Invoke ();
                    break;

                case (DataState.Warning):
                    warningRanges.OnDataEnteredRange.Invoke ();
                    break;

                case (DataState.Critical):
                    criticalRanges.OnDataEnteredRange.Invoke ();
                    break;
                }
            }
        }

        internal void UpdateWithValue (float value) {
            if (criticalRanges.ContainsValue (value)) {
                State = DataState.Critical;

            } else if (warningRanges.ContainsValue (value)) {
                State = DataState.Warning;

            } else {
                State = DataState.Normal;
            }
        }
    }
}

