﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

namespace DAQRI {

    [ExecuteInEditMode]
    public class DataValueVisualization : MonoBehaviour {

        public GameObject UIParent;
        public Text valueText;
        public Text unitsText;

        private float lastValidatedValue = 0f;

        [SerializeField]
        private float value = 0f;
        public float Value {
            get { return value; }
            set {
                this.value = value;
                SetValueText ();

                dataState.UpdateWithValue (this.value);
                OnValueChange.Invoke (this.value);
            }
        }

        [SerializeField]
        private int decimalPlaces = 0;
        public int DecimalPlaces {
            get { return decimalPlaces; }
            set {
                if (decimalPlaces == value) {
                    return;
                }

                decimalPlaces = value;

                ValidateDecimalValue ();
                SetValueText ();
            }
        }

        [SerializeField]
        private string units = string.Empty;
        public string Units {
            get { return units; }
            set {
                if (units == value) {
                    return;
                }

                units = value;
                SetUnitsText ();
            }
        }

        [Serializable]
        public class ValueEvent : UnityEvent<float> { }
        public ValueEvent OnValueChange;

        [SerializeField]
        private DataStateHandler dataState = new DataStateHandler ();

        public DataState State {
            get { return dataState.State; }
        }


        #region Validation

        protected void OnValidate () {
            if (Mathf.Abs (Value - lastValidatedValue) > float.Epsilon) {
                dataState.UpdateWithValue (Value);
                OnValueChange.Invoke (Value);
                lastValidatedValue = Value;
            }

            ValidateDecimalValue ();

            SetUnitsText ();
            SetValueText ();
        }

        private void ValidateDecimalValue () {
            if (decimalPlaces < 0) {
                decimalPlaces = 0;
            }
        }

        #endregion


        #region Views

        private void SetValueText () {
            if (valueText == null) {
                return;
            }

            valueText.text = value.ToString (string.Format ("n{0}", DecimalPlaces));
        }

        private void SetUnitsText () {
            if (unitsText == null) {
                return;
            }

            unitsText.text = units;
        }

        #endregion

        #region Public

        public bool IsUIVisible {
            get {
                return (UIParent == null ? false : UIParent.activeInHierarchy);
            }
            set {
                if (UIParent == null) {
                    return;
                }

                UIParent.SetActive (value);
            }
        }

        #endregion
    }
}
