﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace DAQRI {

    public class PointsOfInterest : MonoBehaviour {

        public DataValueVisualization tile;
        public GameObject normalIcon;
        public GameObject warningIcon;
        public GameObject criticalIcon;
        public POIElastic elastic;

        public void SetToNormalState () {
            normalIcon.SetActive (true);
            warningIcon.SetActive (false);
            criticalIcon.SetActive (false);
            elastic.state = DataState.Normal;
        }

        public void SetToWarningState () {
            normalIcon.SetActive (false);
            warningIcon.SetActive (true);
            criticalIcon.SetActive (false);
            elastic.state = DataState.Warning;
        }

        public void SetToCriticalState () {
            normalIcon.SetActive (false);
            warningIcon.SetActive (false);
            criticalIcon.SetActive (true);
            elastic.state = DataState.Critical;
        }

        public void ToggleTileVisibility () {
            if (tile == null) {
                return;
            }

            SetTileVisibility (!tile.IsUIVisible);
        }

        public void SetTileVisibility (bool isVisible) {
            if (tile == null) {
                return;
            }

			GameObject Watson = GameObject.Find ("WatsonIoT");
			Watson.GetComponent<HideTiles> ().HideAll ();
				
			tile.IsUIVisible = isVisible;
            elastic.IsTopVisible = isVisible;	
        }
    }
}
