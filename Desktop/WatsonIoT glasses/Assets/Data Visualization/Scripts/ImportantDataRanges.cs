﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace DAQRI {

    [Serializable]
    [ExecuteInEditMode]
    public class ImportantDataRanges {

        public bool hasLowerRange = false;
        public bool hasUpperRange = false;

        public float lowerRangeBound = 0f;
        public float upperRangeBound = 0f;

        public UnityEvent OnDataEnteredRange;

        /// <summary>
        /// Checks if a value is within any of the ranges.
        /// </summary>
        /// <returns><c>true</c>, if the value is in one of the ranges, <c>false</c> otherwise.</returns>
        /// <param name="value">Value.</param>
        public bool ContainsValue (float value) {
            return (
                (hasLowerRange && value <= lowerRangeBound) || 
                (hasUpperRange && value >= upperRangeBound)
            );
        }
    }
}
