﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace DAQRI {

    [ExecuteInEditMode]
    public class ProgressBarView : AbstractProgressView {

        public Image emptyRectImage;
        public Image filledRectImage;


        protected override void RedrawFilledColor () {
            if (filledRectImage == null) {
                return;
            }

            filledRectImage.color = FilledColor;
        }

        protected override void RedrawEmptyColor () {
            if (emptyRectImage == null) {
                return;
            }

            emptyRectImage.color = EmptyColor;
        }

        protected override void RedrawFilledArea () {
            if (filledRectImage == null) {
                return;
            }

            float fraction = (currentValueForAnimation - MinValue) / (MaxValue - MinValue);
            fraction = Mathf.Clamp (fraction, 0f, 1f);
            filledRectImage.fillAmount = fraction;
        }
    }
}
