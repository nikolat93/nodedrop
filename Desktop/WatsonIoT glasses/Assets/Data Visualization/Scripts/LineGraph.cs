﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DAQRI;

public class LineGraph : MonoBehaviour {

    public LineChartView chart;

    public int numberOfYLabels = 5;
    public float yMin = 0f;
    public float yMax = 100f;

    [SerializeField]
    private List<float> values = new List<float> ();


	void Start () {
        if (values.Count > 0) {
            PlotValues (new List<float> (values));
        }
	}

    public void PlotValues (List<float> valuesToPlot) {
        values.Clear ();

        SetOfSeries dataSet = new SetOfSeries ();
        dataSet.xLabels = new List<string> ();
        dataSet.series = new List<Series> ();

        Series series = new Series ();
        series.values = new List<float> ();
        dataSet.series.Add (series);

        dataSet.yTics = numberOfYLabels;

        int index = 0;
        foreach (float value in valuesToPlot) {
            values.Add (value);
            series.values.Add (value);
            dataSet.xLabels.Add (index.ToString ());

            index++;
        }

        dataSet.yMin = yMin;
        dataSet.yMax = yMax;

        chart.Plot (dataSet);
    }
}
