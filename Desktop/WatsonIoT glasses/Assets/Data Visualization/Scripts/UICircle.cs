﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

[ExecuteInEditMode]
public class UICircle : Graphic {

    [Serializable]
    public enum FillOrigin {
        Bottom,
        Top,
        Left,
        Right
    }

    
    [SerializeField]
    Texture m_Texture;

    [Range(0, 100)]
    public float fillPercent;

    public FillOrigin fillOrigin = FillOrigin.Bottom;

    public bool fillClockwise = true;

    public int thickness = 5;

    private int segments = 360;

    [Range(0, 360)]
    [SerializeField]
    private int degreesLessThanCircle = 0;

    public override Texture mainTexture {
        get {
            return m_Texture == null ? s_WhiteTexture : m_Texture;
        }
    }

    public Texture Texture {
        get { return m_Texture; }
        set {
            if (m_Texture == value) {
                return;
            }

            m_Texture = value;
            SetVerticesDirty();
            SetMaterialDirty();
        }
    }

    void Update() {
        thickness = (int)Mathf.Clamp(thickness, 0, rectTransform.rect.width / 2);
    }

    protected UIVertex[] SetVbo(Vector2[] vertices, Vector2[] uvs) {
        UIVertex[] vbo = new UIVertex[4];
        for (int i = 0; i < vertices.Length; i++) {
            var vert = UIVertex.simpleVert;
            vert.color = color;
            vert.position = vertices[i];
            vert.uv0 = uvs[i];
            vbo[i] = vert;
        }

        return vbo;
    }

    #pragma warning disable 0672
    protected override void OnPopulateMesh(Mesh toFill) {
        float outer = -rectTransform.pivot.x * rectTransform.rect.width;
        float inner = -rectTransform.pivot.x * rectTransform.rect.width + this.thickness; 
        toFill.Clear();

        var vbo = new VertexHelper(toFill);
        //UIVertex vert = UIVertex.simpleVert;
        Vector2 prevX = Vector2.zero;
        Vector2 prevY = Vector2.zero;
        Vector2 uv0 = new Vector2(0, 0);
        Vector2 uv1 = new Vector2(0, 1);
        Vector2 uv2 = new Vector2(1, 1);
        Vector2 uv3 = new Vector2(1, 0);
        Vector2 pos0;
        Vector2 pos1;
        Vector2 pos2;
        Vector2 pos3;

        float f = (this.fillPercent / 100f);
        float degrees = (360f - (float)degreesLessThanCircle) / segments;
        int fa = (int)((segments + 1) * f);
        float originOffsetDegrees = GetOffsetGetForFillOrigin ();
        int directionFactor = (fillClockwise ? -1 : 1);

        for (int i = 0; i < fa; i++) {
            float rad = Mathf.Deg2Rad * (i * degrees + originOffsetDegrees + (degreesLessThanCircle / 2)) * directionFactor;
            float c = Mathf.Cos(rad);
            float s = Mathf.Sin(rad);

            uv0 = new Vector2(0, 1);
            uv1 = new Vector2(1, 1);
            uv2 = new Vector2(1, 0);
            uv3 = new Vector2(0, 0);

            pos0 = prevX;
            pos1 = new Vector2(outer * c, outer * s);
            pos2 = new Vector2(inner * c, inner * s);
            pos3 = prevY;

            prevX = pos1;
            prevY = pos2;

            vbo.AddUIVertexQuad(SetVbo(new[] { pos0, pos1, pos2, pos3 }, new[] { uv0, uv1, uv2, uv3 }));
        }

        if (vbo.currentVertCount > 3) {
            vbo.FillMesh(toFill);
        }
    }

    private int GetOffsetGetForFillOrigin () {
        int offset = 0;

        switch (fillOrigin) {
        case FillOrigin.Left:
            offset = 0;
            break;

        case FillOrigin.Bottom:
            offset = (fillClockwise ? 270 : 90);
            break;

        case FillOrigin.Right:
            offset = 180;
            break;

        case FillOrigin.Top:
            offset = (fillClockwise ? 90 : 270);
            break;
        }

        return offset;
    }
}