﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum DataState {
    Normal,
    Warning,
    Critical
}
