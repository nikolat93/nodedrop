﻿using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Series
{
    public string label;
    public List<float> values;
}