﻿using UnityEngine;
using UnityEngine.UI;

public class LegendView : MonoBehaviour {

    public Image border;
    public Image image;
    public Text text;

    public void UpdateColor(Color color)
    {
        image.color = color;
    }

}
