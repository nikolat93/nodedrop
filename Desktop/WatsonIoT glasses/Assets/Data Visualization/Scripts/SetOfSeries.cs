﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


[Serializable]
public class SetOfSeries
{
    public List<string> xLabels;
    public List<Series> series;

    public float yMin;
    public float yMax;
    public int yTics;
    public string xAxisName;
    public string yAxisName;
    public string title;

    public static SetOfSeries GetChartData(string jsonString)
    {
        return JsonUtility.FromJson<SetOfSeries>(jsonString);
    }

}