using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LineChartView : BaseGraphView {

    public PointView pointViewPrefab;

    //TODO: Needs to be implemented
    protected override void ClearGraph()
    {
        
    }

    public override void PlotData()
    {
        // Cleanup old lines
        LineRenderer[] lines = graphPlotArea.GetComponentsInChildren<LineRenderer> ();
        foreach (LineRenderer line in lines) {
            Destroy (line.gameObject);
        }

        List<LineRenderer> lineRenderes = new List<LineRenderer>();

        int xSteps = dataset.xLabels.Count;
        float yMin = dataset.yMin;
        float yMax = dataset.yMax;
        int ySteps = dataset.yTics;

        for (int j = 0; j < dataset.series.Count; j++)
        {
            LineRenderer lr = new GameObject("Trend", typeof(RectTransform)).AddComponent<LineRenderer>();
            lr.useWorldSpace = false;

            AnimationCurve curve = new AnimationCurve();
            curve.AddKey(0, 3 * canvasScaleFactor);
            curve.AddKey(1, 3 * canvasScaleFactor);
            lr.widthCurve = curve;
            lr.widthMultiplier = 2f;
                
//            lr.material = new Material(Shader.Find("Unlit/Transparent"));
//            lr.material.renderQueue = 4000;
            lr.transform.SetParent(graphPlotArea, false);
            lr.GetComponent<RectTransform>().pivot = Vector2.zero;
            lr.GetComponent<RectTransform>().anchorMin = Vector2.zero;
            lr.GetComponent<RectTransform>().anchorMax = Vector2.zero;

            lr.GetComponent<RectTransform>().localPosition = Vector3.zero;

            // A simple 2 color gradient with a fixed alpha of 1.0f.
            float alpha = 1.0f;
            Gradient gradient = new Gradient();
            gradient.SetKeys(
                new GradientColorKey[] { new GradientColorKey(colorsForDatasets[j], 0.0f), new GradientColorKey(colorsForDatasets[j], 1.0f) },
                new GradientAlphaKey[] { new GradientAlphaKey(alpha, 1.0f), new GradientAlphaKey(alpha, 1.0f) }
                );
            lr.colorGradient = gradient;
            lineRenderes.Add(lr);

            for (int i = 0; i < dataset.xLabels.Count; i++)
            {
                float rawValue = dataset.series[j].values[i];
                Vector2 position;
                position.x = (Width / xSteps) * i;
                position.y = (rawValue - yMin) / (yMax - yMin) * (Height);

                if (pointViewPrefab != null) {
                    PointView dataPoint = Instantiate(pointViewPrefab);
                    dataPoint.transform.SetParent(graphPlotArea);
                    dataPoint.SetPosition(position);
                    dataPoint.text.text = rawValue.ToString();
                    dataPoint.text.color = colorsForDatasets[j];
                    Vector3 dataPointScale = dataPoint.transform.localScale;
                    dataPoint.transform.localScale = dataPointScale * canvasScaleFactor;
                }

                lr.positionCount = dataset.xLabels.Count;
                lr.SetPosition(i, position);
                lr.material.color = colorsForDatasets[j];
            }
        }

    }

    public static Vector3[] MakeSmoothCurve(Vector3[] arrayToCurve, float smoothness)
    {
        List<Vector3> points;
        List<Vector3> curvedPoints;
        int pointsLength = 0;
        int curvedLength = 0;

        if (smoothness < 1.0f) smoothness = 1.0f;

        pointsLength = arrayToCurve.Length;

        curvedLength = (pointsLength * Mathf.RoundToInt(smoothness)) - 1;
        curvedPoints = new List<Vector3>(curvedLength);

        float t = 0.0f;
        for (int pointInTimeOnCurve = 0; pointInTimeOnCurve < curvedLength + 1; pointInTimeOnCurve++)
        {
            t = Mathf.InverseLerp(0, curvedLength, pointInTimeOnCurve);

            points = new List<Vector3>(arrayToCurve);

            for (int j = pointsLength - 1; j > 0; j--)
            {
                for (int i = 0; i < j; i++)
                {
                    points[i] = (1 - t) * points[i] + t * points[i + 1];
                }
            }

            curvedPoints.Add(points[0]);
        }

        return (curvedPoints.ToArray());
    }


}
