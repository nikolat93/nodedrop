﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace DAQRI {

    [CustomPropertyDrawer (typeof (ImportantDataRanges))]
    public class ImportantDataRangesDrawer : PropertyDrawer {

        private const float LINE_SPACE = 4f;


        #region Drawing

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
            EditorGUI.BeginProperty (position, label, property);

            Rect titleLabelRect = new Rect (
                position.x,
                position.y,
                position.width,
                TitleLabelHeight
            );

            position.y = titleLabelRect.yMax;

            EditorGUI.LabelField (titleLabelRect, label.text);

            DrawRange (
                HasUpperRangeProperty (property),
                UpperBoundProperty (property),
                "Upper Range",
                ref position
            );

            DrawRange (
                HasLowerRangeProperty (property),
                LowerBoundProperty (property),
                "Lower Range",
                ref position
            );

            SerializedProperty eventProperty = OnDataEnteredRangeProperty (property);
            Rect eventPropertyRect = new Rect (
                position.x, 
                position.y + LINE_SPACE,
                position.width, 
                EditorGUI.GetPropertyHeight (eventProperty)
            );

            EditorGUI.PropertyField (eventPropertyRect, eventProperty);
            EditorGUI.EndProperty ();
        }

        private void DrawRange (SerializedProperty hasRangeProperty, SerializedProperty rangeBoundProperty, string name, ref Rect position) {
            Rect toggleLabelRect = new Rect (
                position.x, 
                position.y, 
                EditorGUIUtility.labelWidth, 
                RangeToggleHeight
            );

            Rect toggleRect = new Rect (
                EditorGUIUtility.labelWidth,
                position.y,
                35f,
                RangeToggleHeight
            );

            EditorGUI.indentLevel += 1;
            EditorGUI.LabelField (toggleLabelRect, name);
            EditorGUI.indentLevel -= 1;
            position.y = toggleLabelRect.yMax;

            hasRangeProperty.boolValue = EditorGUI.Toggle (toggleRect, hasRangeProperty.boolValue);
            if (hasRangeProperty.boolValue) {

                Rect fieldLabelRect = new Rect (
                    position.x,
                    toggleLabelRect.yMax,
                    EditorGUIUtility.labelWidth,
                    RangeFieldHeight
                );

                Rect fieldRect = new Rect (
                    EditorGUIUtility.labelWidth,
                    toggleLabelRect.yMax,
                    position.width - EditorGUIUtility.labelWidth,
                    RangeFieldHeight
                );

                EditorGUI.indentLevel += 2;
                EditorGUI.LabelField (fieldLabelRect, "Boundary");
                EditorGUI.indentLevel -= 2;

                rangeBoundProperty.floatValue = EditorGUI.FloatField (fieldRect, rangeBoundProperty.floatValue);
                position.y = fieldLabelRect.yMax;
            }
        }

        #endregion


        #region Properties

        private SerializedProperty OnDataEnteredRangeProperty (SerializedProperty importantDataRangesProperty) {
            return importantDataRangesProperty.FindPropertyRelative ("OnDataEnteredRange");
        }

        private SerializedProperty HasUpperRangeProperty (SerializedProperty importantDataRangesProperty) {
            return importantDataRangesProperty.FindPropertyRelative ("hasUpperRange");
        }

        private SerializedProperty HasLowerRangeProperty (SerializedProperty importantDataRangesProperty) {
            return importantDataRangesProperty.FindPropertyRelative ("hasLowerRange");
        }

        private SerializedProperty UpperBoundProperty (SerializedProperty importantDataRangesProperty) {
            return importantDataRangesProperty.FindPropertyRelative ("upperRangeBound");
        }

        private SerializedProperty LowerBoundProperty (SerializedProperty importantDataRangesProperty) {
            return importantDataRangesProperty.FindPropertyRelative ("lowerRangeBound");
        }

        #endregion


        #region Heights

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label) {
            return (
                TitleLabelHeight + 
                2 * RangeToggleHeight + 
                (HasUpperRangeProperty (property).boolValue ? RangeFieldHeight : 0f) +
                (HasLowerRangeProperty (property).boolValue ? RangeFieldHeight : 0f) +
                EditorGUI.GetPropertyHeight (OnDataEnteredRangeProperty (property))
            );
        }

        private float TitleLabelHeight {
            get { return EditorGUIUtility.singleLineHeight; }
        }

        private float RangeToggleHeight {
            get { return EditorGUIUtility.singleLineHeight; }
        }

        private float RangeFieldHeight {
            get { return EditorGUIUtility.singleLineHeight; }
        }

        #endregion
    }
}
