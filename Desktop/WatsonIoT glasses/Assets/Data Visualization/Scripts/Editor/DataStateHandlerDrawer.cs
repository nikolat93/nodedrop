﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace DAQRI {

    [CustomPropertyDrawer (typeof (DataStateHandler))]
    public class DataStateHandlerDrawer : PropertyDrawer {

        private const float LINE_SPACING = 4f;

        private bool isFoldoutOpen = false;


        #region Drawing

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
            EditorGUI.BeginProperty (position, label, property);

            Rect foldoutRect = new Rect (
                position.x,
                position.y,
                position.width,
                FoldoutLabelHeight
            );

            position.y += foldoutRect.height;
            isFoldoutOpen = EditorGUI.Foldout (foldoutRect, isFoldoutOpen, "Special Data Ranges");

            if (isFoldoutOpen) {
                DrawFoldoutContent (position, property);
            }

            EditorGUI.EndProperty ();
        }

        private void DrawFoldoutContent (Rect position, SerializedProperty property) {
            SerializedProperty warningRangeProperty = WarningRangeProperty (property);
            SerializedProperty criticalRangeProperty = CriticalRangeProperty (property);
            SerializedProperty normalRangeEventProperty = NormalRangeEventProperty (property);

            Rect criticalRangeRect = new Rect (
                position.x,
                position.y,
                position.width,
                EditorGUI.GetPropertyHeight (criticalRangeProperty)
            );

            Rect warningRangeRect = new Rect (
                position.x,
                criticalRangeRect.yMax,
                position.width,
                EditorGUI.GetPropertyHeight (warningRangeProperty)
            );

            Rect normalRangeLabelRect = new Rect (
                position.x,
                warningRangeRect.yMax,
                position.width,
                NormalRangeLabelHeight
            );

            Rect helpBoxRect = new Rect (
                position.x,
                normalRangeLabelRect.yMax,
                position.width,
                HelpBoxHeight
            );

            Rect normalRangeEventRect = new Rect (
                position.x,
                helpBoxRect.yMax + HelpBoxBottomSpacing,
                position.width,
                EditorGUI.GetPropertyHeight (normalRangeEventProperty)
            );

            EditorGUI.PropertyField (criticalRangeRect, criticalRangeProperty);
            EditorGUI.PropertyField (warningRangeRect, warningRangeProperty);
            EditorGUI.LabelField (normalRangeLabelRect, "Normal Range");
            EditorGUI.HelpBox (helpBoxRect, "The event will be invoked when the data exits the critical and warning ranges.", MessageType.Info);
            EditorGUI.PropertyField (normalRangeEventRect, normalRangeEventProperty);
        }

        #endregion


        #region Heights

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label) {
            float height = FoldoutLabelHeight;

            if (isFoldoutOpen) {
                height += (
                    EditorGUI.GetPropertyHeight (WarningRangeProperty (property)) + 
                    EditorGUI.GetPropertyHeight (CriticalRangeProperty (property)) +
                    NormalRangeLabelHeight +
                    HelpBoxHeight +
                    HelpBoxBottomSpacing +
                    EditorGUI.GetPropertyHeight (NormalRangeEventProperty (property))
                );
            }

            return height;
        }

        private float FoldoutLabelHeight {
            get { return EditorGUIUtility.singleLineHeight + LINE_SPACING; }
        }

        private float HelpBoxHeight {
            get { return EditorGUIUtility.singleLineHeight * 2.2f; }
        }

        private float HelpBoxBottomSpacing {
            get { return LINE_SPACING; }
        }

        private float NormalRangeLabelHeight {
            get { return EditorGUIUtility.singleLineHeight + LINE_SPACING; }
        }

        #endregion


        #region Properties

        private SerializedProperty WarningRangeProperty (SerializedProperty dataStateProperty) {
            return dataStateProperty.FindPropertyRelative ("warningRanges");
        }

        private SerializedProperty CriticalRangeProperty (SerializedProperty dataStateProperty) {
            return dataStateProperty.FindPropertyRelative ("criticalRanges");
        }

        private SerializedProperty NormalRangeEventProperty (SerializedProperty dataStateProperty) {
            return dataStateProperty.FindPropertyRelative ("OnDataEnteredNormalRange");
        }

        #endregion
    }
}
