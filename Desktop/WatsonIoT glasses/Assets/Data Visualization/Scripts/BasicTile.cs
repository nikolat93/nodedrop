﻿/****************************************************************************************************************************************
 * © 2016 Daqri International. All Rights Reserved.                                                                                     *
 *                                                                                                                                      *
 *     NOTICE:  All software code and related information contained herein is, and remains the property of DAQRI INTERNATIONAL and its  *
 * suppliers, if any.  The intellectual and technical concepts contained herein are proprietary to DAQRI INTERNATIONAL and its          *
 * suppliers and may be covered by U.S. and Foreign Patents, patents in process, and/or trade secret law, and the expression of         *
 * those concepts is protected by copyright law. Dissemination, reproduction, modification, public display, reverse engineering, or     *
 * decompiling of this material is strictly forbidden unless prior written permission is obtained from DAQRI INTERNATIONAL.             *
 *                                                                                                                                      *
 *                                                                                                                                      *
 *                                                                                                                                      *
 *     File Purpose:       <todo>                                                                                                       *
 *     Guide:              <todo>                                                                                                       *
 *                                                                                                                                      *
 ****************************************************************************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace DAQRI {
    
    [ExecuteInEditMode]
    public class BasicTile : MonoBehaviour {

        public Text body;
        public Text bodySecondary;


        [SerializeField]
        private bool useSecondaryBody = true;
        public bool UseSecondaryBody {
            get { return useSecondaryBody; }
            set {
                if (useSecondaryBody == value) {
                    return;
                }

                useSecondaryBody = value;
                LayoutBody ();
            }
        }

        [SerializeField]
        private float bodyVerticalOffset = 0f;
        public float BodyVerticalOffset {
            get { return bodyVerticalOffset; }
            set {
                if (Mathf.Abs (bodyVerticalOffset - value) < float.Epsilon) {
                    return;
                }

                bodyVerticalOffset = value;
                LayoutBody ();
            }
        }

        protected virtual void OnValidate () {
            LayoutBody ();
        }


        #region Layout

        private void LayoutBody () {
            if (body == null) {
                return;
            }

            float yPosition = 0f;
            RectTransform bodyRect = body.GetComponent <RectTransform> ();

            if (bodySecondary != null) {
                bodySecondary.gameObject.SetActive (UseSecondaryBody);
            }

            if (UseSecondaryBody && bodySecondary != null) {
                float totalHeight = bodyRect.rect.height + bodySecondary.GetComponent<RectTransform> ().rect.height;
                yPosition = totalHeight / 2f - bodyRect.pivot.y * bodyRect.rect.height + BodyVerticalOffset;

            } else {
                yPosition = BodyVerticalOffset;
            }

            Vector2 anchoredPosition = bodyRect.anchoredPosition;
            anchoredPosition.y = yPosition;
            bodyRect.anchoredPosition = anchoredPosition;
        }

        #endregion
    }
}
