﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum LABEL_TYPE
{
    NUMERIC, STRING    
}

public class BaseGraphView : MonoBehaviour {

    public RectTransform HorizontalAxis;
    public RectTransform VerticalAxis;
    public Text titleText;
    public Text xAxisLabel;
    public Text yAxisLabel;
    public RectTransform legendTransform;
    public LegendView legendViewPrefab;
    public Text categoryTextPrefab;
    public PointView xMarkingPrefab;
    public PointView yMarkingPrefab;
    protected SetOfSeries dataset;
    public List<Color> colorsForDatasets = new List<Color>();
    public LABEL_TYPE labelType;
    public RectTransform graphPlotArea;

    protected float canvasScaleFactor = 0.00195312f;

    virtual public float Width
    {
        get
        {
            return graphPlotArea.rect.width;
        }
    }

    virtual public float Height
    {
        get
        {
            return graphPlotArea.rect.height;
        }
    }

    virtual protected void ClearGraph() { }

    public void Plot(SetOfSeries dataset) 
    {
        canvasScaleFactor = GetComponent<RectTransform>().localScale.x;
        
        this.dataset = dataset;

        ClearGraph();

        PlotXYAxis();

        PlotLegends();

        PlotData();
    }

    protected void PlotLegends()
    {
        if (legendViewPrefab == null) {
            return;
        }

        for (int i = 0; i < dataset.series.Count; i++)
        {
            LegendView legend = Instantiate(legendViewPrefab);
            legend.transform.SetParent(legendTransform);
            legend.transform.localPosition = Vector3.zero;
            legend.text.text = dataset.series[i].label;

            Vector3 legendScale = legend.transform.localScale;
            legend.transform.localScale = legendScale * canvasScaleFactor;
            //transforms.Add(legend.transform);
            legend.UpdateColor(colorsForDatasets[i]);
        }
    }

    protected float xFactor = 0;

    private List<PointView> yAxisValueLabels = new List<PointView> ();
    public void PlotXYAxis()
    {
        // Remove old labels
        foreach (PointView point in yAxisValueLabels) {
            Destroy (point.gameObject);
        }
        yAxisValueLabels.Clear ();

        xAxisLabel.text = dataset.xAxisName;
        yAxisLabel.text = dataset.yAxisName;

        if (titleText != null) {
            titleText.text = dataset.title;
        }

        int xMin = 0;
        int xMax = (labelType == LABEL_TYPE.NUMERIC) ? dataset.xLabels.Count : dataset.xLabels.Count + 2;
        int xSteps = xMax - 1;
        float yMin = dataset.yMin, yMax = dataset.yMax;
        int ySteps = dataset.yTics;

        int xMarker = (xMax - xMin) / xSteps;
        //Debug.Log(xFactor);
        if (labelType == LABEL_TYPE.NUMERIC && xMarkingPrefab != null)
        {
            xFactor = (int)Width / (xSteps);
            for (int x = 0; x <= xSteps; x++)
            {
                PointView xMarking = Instantiate(xMarkingPrefab);
                xMarking.transform.SetParent(HorizontalAxis);

                float xPos = xFactor * x;
                xMarking.SetPosition(new Vector2(xPos, 0));
                SetFormattedText(xMarking.text, dataset.xLabels[x]);
            }
        }
        else if (xMarkingPrefab != null)
        {
            xFactor = (int)Width / (xSteps - 1);
            for (int x = 0; x < xSteps-1; x++)
            {
                PointView xMarking = Instantiate(xMarkingPrefab);
                xMarking.transform.SetParent(HorizontalAxis);
                
                float xPos = xFactor * x;
                xMarking.SetPosition(new Vector2(xPos, 0));
                Vector3 xMarkingScale = xMarking.transform.localScale;
                xMarking.transform.localScale = xMarkingScale * canvasScaleFactor;
                SetFormattedText(xMarking.text, dataset.xLabels[x]);
            }
        }

        if (ySteps > 0) {
            int yFactor = (int)(Height) / ySteps;
            float yMarker = (yMax - yMin) / ySteps;
            for (int y = 0; y <= ySteps; y++)
            {
                PointView yMarking = Instantiate(yMarkingPrefab);
                yMarking.transform.SetParent(VerticalAxis);
                float yPos = yFactor * y;
                yMarking.transform.localPosition = new Vector3(0, yPos, 0);
                yMarking.transform.localRotation = Quaternion.identity;
                Vector3 yMarkingScale = yMarking.transform.localScale;
                yMarking.transform.localScale = yMarkingScale * canvasScaleFactor;
                yMarking.text.text = (yMin + yMarker * y).ToString();
                yAxisValueLabels.Add (yMarking);
            }
        }
    }

    protected void SetFormattedText(Text text, string val)
    {
        if (val.Length > 5)
        {
            text.transform.localEulerAngles = new Vector3(0, 0, 0);
            text.transform.localPosition = new Vector3(16.0f, -18.0f, 0);
        }

        text.text = val;
    }

    public virtual void PlotData(){ }
}
