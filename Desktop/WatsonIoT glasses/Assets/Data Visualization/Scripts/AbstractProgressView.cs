﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace DAQRI {

    [ExecuteInEditMode]
    public abstract class AbstractProgressView : MonoBehaviour {

        protected abstract void RedrawFilledColor ();
        protected abstract void RedrawEmptyColor ();
        protected abstract void RedrawFilledArea ();


        public Text minValueLabel;
        public Text maxValueLabel;


        [SerializeField]
        private Color emptyColor;
        public Color EmptyColor {
            get { return emptyColor; }
            set {
                if (emptyColor.Equals (value)) {
                    return;
                }

                emptyColor = value;
                ValidateEmptyColor ();
            }
        }

        [SerializeField]
        private Color filledColor;
        public Color FilledColor {
            get { return filledColor; }
            set {
                if (filledColor.Equals (value)) {
                    return;
                }

                filledColor = value;
                ValidateFilledColor ();
            }
        }

        [SerializeField]
        private float minValue = 0;
        public float MinValue {
            get { return this.minValue; }
            set {
                if (Mathf.Abs (minValue - value) < float.Epsilon) {
                    return;
                }

                minValue = value;
                ValidateMaxValue ();
                RedrawFilledArea ();
            }
        }

        [SerializeField]
        private float maxValue = 100;
        public float MaxValue {
            get { return maxValue; }
            set {
                if (Mathf.Abs (maxValue - value) < float.Epsilon) {
                    return;
                }

                maxValue = value;
                ValidateMaxValue ();
                RedrawFilledArea ();
            }
        }

        public float animationSpeed = 4f;

        protected float currentValueForAnimation = 0f;
        private bool isAnimating = false;

        private float value = 0f;
        public float Value {
            get { return value; }
            set {
                this.value = value;

                if (Application.isPlaying && animationSpeed > 0f) {
                    isAnimating = true;

                } else {
                    currentValueForAnimation = this.value; // Don't animate if not playing, jump right to value
                    RedrawFilledArea ();
                }
            }
        }

        void Update () {
            if (isAnimating) {
                currentValueForAnimation = Mathf.Lerp(currentValueForAnimation, Value, Time.deltaTime * animationSpeed);

                if (Mathf.Abs(currentValueForAnimation - Value) < 0.001f) {
                    currentValueForAnimation = Value;
                    isAnimating = false;
                }

                RedrawFilledArea ();
            }
        }


        #region Validation State

        private Color lastValidatedEmptyColor;
        private bool DoesEmptyColorNeedValidation {
            get { return !EmptyColor.Equals (lastValidatedEmptyColor); }
        }

        private Color lastValidatedFilledColor;
        private bool DoesFilledColorNeedValidation {
            get { return !FilledColor.Equals (lastValidatedFilledColor); }
        }

        private float lastValidatedMinValue;
        private bool DoesMinValueNeedValidation {
            get { return Mathf.Abs (MinValue - lastValidatedMinValue) > float.Epsilon; }
        }

        private float lastValidatedMaxValue;
        private bool DoesMaxValueNeedValidation {
            get { return Mathf.Abs (MaxValue - lastValidatedMaxValue) > float.Epsilon; }
        }

        #endregion


        #region Performing Validation

        protected void OnValidate () {
            bool shouldRedrawBar = false;

            if (DoesMinValueNeedValidation) {
                ValidateMinValue ();
                shouldRedrawBar = true;
            }

            if (DoesMaxValueNeedValidation) {
                ValidateMaxValue ();
                shouldRedrawBar = true;
            }

            if (DoesEmptyColorNeedValidation) {
                ValidateEmptyColor ();
            }

            if (DoesFilledColorNeedValidation) {
                ValidateFilledColor ();
            }

            if (shouldRedrawBar) {
                RedrawFilledArea ();
            }
        }

        private void ValidateEmptyColor () {
            lastValidatedEmptyColor = emptyColor;

            RedrawEmptyColor ();
        }

        private void ValidateFilledColor () {
            lastValidatedFilledColor = filledColor;

            RedrawFilledColor ();
        }

        private void ValidateMinValue () {
            lastValidatedMinValue = minValue;

            if (minValueLabel != null) {
                minValueLabel.text = minValue.ToString ();
            }
        }

        private void ValidateMaxValue () {
            lastValidatedMaxValue = maxValue;

            if (maxValueLabel != null) {
                maxValueLabel.text = maxValue.ToString ();
            }
        }

        #endregion
    }
}
