﻿﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class PointView : MonoBehaviour {

    public Text text;
    public Image image;
    private RectTransform rect;

    public void SetPositionText(Vector2 position)
    {
        //text.text = "(" + position.x + "," + position.y + ")";
        text.text = "(" + (int)position.y + ")";
    }

    public void SetPosition(Vector2 position)
    {
        rect = GetComponent<RectTransform>();
        rect.localPosition = position;
        //Debug.Log(text.rectTransform.rect.x);
    }
}
