﻿using UnityEngine;
using System.Collections.Generic;
namespace IBM
{

    public class GraphData
    {
        private List<float> data = new List<float>();
        private float min, max;
        private int maxRows;
        private float dynamic_min;
        private float dynamic_max;
        public float Min { get { return dynamic_min; } }
        public float Max { get { return dynamic_max; } }

        public List<float> Data { get { return data; } }

        public GraphData(float min,float max,int maxRows)
        {
            this.min = min;
            this.max = max;
            this.dynamic_min = max + 1;
            this.dynamic_max = min - 1;
            this.maxRows = maxRows;
        }
        public bool GenerateRandom(float start)
        {
            float d = start;
            float d_r = 0;
            if(start >= min && start <= max)
            {
                while (data.Count < maxRows)
                {
                    d_r = d * 0.01f;
                    float newData = Random.Range(d-d_r,d+d_r);
                    if (AddNextData(newData))
                    {
                        d = newData;
                    }
                }
                return true;
            }
            return false;
        }
        public bool AddNextData(float newData)
        {
            if(newData >= min && newData <= max)
            {
                if(newData > dynamic_max)
                {
                    dynamic_max = newData;
                }
                if(newData < dynamic_min)
                {
                    dynamic_min = newData;
                }
                data.Add(newData);
                if(data.Count > maxRows)
                {
                    data.RemoveAt(0);
                }
                return true;
            }
            return false;
        }
    }
}