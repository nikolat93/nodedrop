﻿using UnityEngine;
using System.Collections;
using DAQRI;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Timers;
using System.Collections.Generic;
using MiniJSON;

namespace IBM
{
    public class IoTClient : MonoBehaviour
    {
        public DataValueVisualization temperature;
        public DataValueVisualization speed;
        public DataValueVisualization vibration;
        public DataValueVisualization pressureIn;
        public DataValueVisualization pressureOut;
        public DataValueVisualization tankLevel;

        public Text dateField;
        public Text techField;
        public Text descriptionField;
        public Text nextMaintenanceField;

        public LineGraph temperatureGraph;
        public LineGraph speedGraph;
        public LineGraph vibrationGraph;
//        public LineGraph pressureInGraph;
//        public LineGraph pressureOutGraph;
//        public LineGraph tankLevelGraph;

        public WorkInstructionsController workInstruction;
        private static readonly string BASEURL = "https://ar-connector.mybluemix.net";
        //private static readonly string BASEURL = "http://localhost:1337";
        private static readonly string API = "/api/v1/";

        GraphData temperatures = new GraphData(70, 300, 40);
        GraphData vibrations = new GraphData(0, 0.4f, 40);
        GraphData speeds = new GraphData(0, 1000, 40);
        GraphData pressureInData = new GraphData(0, 200, 40);
        GraphData pressureOutData = new GraphData(0, 200, 40);
        GraphData tankLevelData = new GraphData(0, 100, 40);

        private bool initialized = false;
        private ConnectResponse cr = null;

        private string ca;
        public string CurrentAsset
        {
            get { return this.ca; }
            set {
                this.ca = value;
                if (!string.IsNullOrEmpty(this.ca))
                {
                    StartCoroutine("SetCurrentAsset");
                }
            }
        }


        public bool Initilized
        {
            get { return this.initialized; }
        }
        private Credential stt_cred;
        private Credential tts_cred;
        public Credential SpeechToTextCred
        {
            get { return stt_cred; }
        } 
        public Credential TextToSpeechCred
        {
            get { return tts_cred; }
        }
        // Use this for initialization
        void Start()
        {
            
            temperatures.GenerateRandom(200);
            vibrations.GenerateRandom(0.20f);
            speeds.GenerateRandom(500);
            pressureInData.GenerateRandom(100);
            pressureOutData.GenerateRandom(100);
            tankLevelData.GenerateRandom(50);

            // initialize
            StartCoroutine(Connect());
            StartCoroutine(FetchData());
            StartCoroutine(FetchWorkorderData());
            StartCoroutine(Notifications());
        }


        IEnumerator Notifications()
        {
            while (true)
            {
                if (this.initialized)
                {
                    string url = BASEURL + API + "notifications?user=ROGERS";
                    UnityWebRequest req = UnityWebRequest.Get(url);
                    yield return req.Send();
                    if (req.isError)
                    {
                        
                    }
                    else
                    {
                        if (req.responseCode == 200)
                        {
                            IDictionary notification = Json.Deserialize(req.downloadHandler.text) as IDictionary;
                            if (notification["message"] != null)
                            {
                                Debug.Log("Message Received :"+ notification["message"]);
                                workInstruction.SetInstructionsText(notification["message"] as string);
                                workInstruction.SetInstructionsVisible(true);
                            }else if(notification["notification"] != null)
                            {
                                Debug.Log("Notification Received :" + notification["notification"]);
                                NotificationManager.Instance.PresentNotification(notification["notification"] as string);
                            }                      
                        }
                        else
                        {
                        }
                    }
                }
                else
                {
                    //May be we are off line
                    this.UpdateWorkorder("Devang", "Thu May 25,2017", "Change valve");
                }
                yield return new WaitForSeconds(3.0f);
            }
        }


        IEnumerator FetchWorkorderData()
        {
            while (true)
            {
                if (this.initialized)
                {
                    string url = BASEURL + API + "lastworkorder?deviceid=pump1";
                    UnityWebRequest req = UnityWebRequest.Get(url);
                    yield return req.Send();
                    if (req.isError)
                    {
                        //May be we are off line
                        this.UpdateWorkorder("Devang", "Thu May 25,2017", "Change valve");
                    }
                    else
                    {
                        if (req.responseCode == 200)
                        {
                            WorkorderData res = WorkorderData.CreateFromJSON(req.downloadHandler.text);
                            if(res.workorders.Length > 0)
                            {
                                this.UpdateWorkorder(res.workorders[0].owner, res.workorders[0].date, res.workorders[0].description);
                            }
                            break;
                        }
                        else
                        {
                        }
                    }
                }
                else
                {
                    //May be we are off line
                    this.UpdateWorkorder("Devang", "Thu May 25,2017", "Change valve");
                }
                yield return new WaitForSeconds(3.0f);
            }
        }

        IEnumerator FetchData()
        {
            while (true)
            {
                if (this.initialized)
                {
                    string url = BASEURL + API + "lastevent?devicetype=pump&deviceid=pump1&eventtype=pumpdata";
                    UnityWebRequest req = UnityWebRequest.Get(url);
                    yield return req.Send();
                    if (req.isError)
                    {
                        SimulateOfflineData();
                    }
                    else
                    {
                        if (req.responseCode == 200)
                        {
                            DeviceData res = DeviceData.CreateFromJSON(req.downloadHandler.text);
                            this.UpdateData(res.speed, res.vibration, res.temperature,res.pressurein,res.pressureout,res.tanklevel);
                        }
                        else
                        {
                        }
                    }
                }
                else
                {
                    SimulateOfflineData();
                }
                yield return new WaitForSeconds(3.0f);
            }
        }
        private void SimulateOfflineData()
        {
            this.UpdateData(this.speeds.Data[0], this.vibrations.Data[0], this.temperatures.Data[0],150,150,50);
        }
        private IEnumerator SetCurrentAsset()
        {
            string url = BASEURL + API + "action?user=ROGERS&currentasset=" + WWW.EscapeURL(this.CurrentAsset);
            UnityWebRequest req = UnityWebRequest.Get(url);
            yield return req.Send();
            if (req.isError)
            {
                
            }
            else
            {
                if (req.responseCode == 200)
                {
                    

                }
                else
                {
                    
                }
            }
        }
        public IEnumerator ExecuteCommand(string command,System.Action<string> OnSuccess)
        {
            string url = BASEURL + API + "command?text="+command;
            UnityWebRequest req = UnityWebRequest.Get(url);
            yield return req.Send();
            if (req.isError)
            {
                OnSuccess("");
            }
            else
            {
                if (req.responseCode == 200)
                {
                    CommandResponse res = CommandResponse.CreateFromJSON(req.downloadHandler.text);
                    if(res.commandtype == "message")
                    {
                        workInstruction.SetInstructionsText(res.message);
                        workInstruction.SetInstructionsVisible(true);
                        OnSuccess(res.text);
                    }
                    else if(res.commandtype == "action")
                    {
                        if(res.action == "hide")
                        {
                            workInstruction.SetInstructionsVisible(false);
                        }
                        OnSuccess(res.text);
                    }
                    else
                    {
                        OnSuccess(res.text);
                    }
                    
                }
                else
                {
                    OnSuccess("");
                }
            }
        }
        IEnumerator Connect()
        {
            while (true)
            {
                string url = BASEURL + API + "connect?clientid=helmet1&version=0.1&libraryversion=0.1";
                UnityWebRequest req = UnityWebRequest.Get(url);
                yield return req.Send();
                if (req.isError)
                {
                    initialized = false;
                }
                else
                {
                    if (req.responseCode == 200)
                    {
                        ConnectResponse res = ConnectResponse.CreateFromJSON(req.downloadHandler.text);
                        this.tts_cred = res.text_to_speech;
                        this.stt_cred = res.speech_to_text;
                        initialized = true;
                        break;
                    }
                    else
                    {
                        initialized = false;
                    }
                }
            }
            
        }

        void UpdateData(float s,float v,float t,int pin,int pout,int tl)
        {
            temperature.Value = t;
            vibration.Value = v;
            speed.Value = s;
            pressureIn.Value = pin;
            pressureOut.Value = pout;
            tankLevel.Value = tl;

            temperatures.AddNextData(t);
            vibrations.AddNextData(v);
            speeds.AddNextData(s);

            temperatureGraph.yMin = temperatures.Min;
            temperatureGraph.yMax = temperatures.Max; // Set these BEFORE calling PlotValues()
            temperatureGraph.PlotValues(temperatures.Data);

            vibrationGraph.yMin = vibrations.Min;
            vibrationGraph.yMax = vibrations.Max; // Set these BEFORE calling PlotValues()
            vibrationGraph.PlotValues(vibrations.Data);

            speedGraph.yMin = speeds.Min;
            speedGraph.yMax = speeds.Max; // Set these BEFORE calling PlotValues()
            speedGraph.PlotValues(speeds.Data);
        }

        void UpdateWorkorder(string owner,string workdate,string description)
        {
            dateField.text = workdate;
            techField.text = owner;
//            descriptionField.text = description;
            nextMaintenanceField.text = "Tue Aug 22 2017";
        }
        // Update is called once per frame
        void Update()
        {

        }
    }
}
