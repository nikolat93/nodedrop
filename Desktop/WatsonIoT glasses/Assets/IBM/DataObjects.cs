﻿using UnityEngine;
namespace IBM
{

    [System.Serializable]
    public class ConnectResponse
    {
        public string clientid;
        public Credential speech_to_text;
        public Credential text_to_speech;

        public static ConnectResponse CreateFromJSON(string jsonString)
        {
            return JsonUtility.FromJson<ConnectResponse>(jsonString);
        }
        
    }

    [System.Serializable]
    public class CommandResponse
    {
        public string commandtype; //'tts' , 'showmessage' or action
        public string text;
        public string message;
        public string action;
        public static CommandResponse CreateFromJSON(string jsonString)
        {
            return JsonUtility.FromJson<CommandResponse>(jsonString);
        }
    }

    [System.Serializable]
    public class DeviceData
    {
        public float speed;
        public float vibration;
        public float temperature;
        public int pressurein;
        public int pressureout;
        public int tanklevel;
        public static DeviceData CreateFromJSON(string jsonString)
        {
            return JsonUtility.FromJson<DeviceData>(jsonString);
        }
    }

    [System.Serializable]
    public class WorkorderData
    {
        public string assetnumber;
        public string assetdescription;
        public string assethealth;
        public Workorder[] workorders;

        public static WorkorderData CreateFromJSON(string jsonString)
        {
            return JsonUtility.FromJson<WorkorderData>(jsonString);
        }
    }

    [System.Serializable]
    public class Workorder
    {
        public string owner;
        public string date;
        public string description;
    }

    [System.Serializable]
    public class Credential
    {
        public string url;
        public string username;
        public string password;
    }
}
