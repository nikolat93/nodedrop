﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;
using IBM.Watson.DeveloperCloud.Logging;
using IBM.Watson.DeveloperCloud.Services.SpeechToText.v1;
using IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1;
using IBM.Watson.DeveloperCloud.Utilities;
using IBM.Watson.DeveloperCloud.DataTypes;

namespace IBM
{
    public class SpeechStreaming : MonoBehaviour
    {
        private int m_RecordingRoutine = 0;

        public AudioSource m_AudioSource;
        public IoTClient m_IoTClient;
        public UnityEvent OnMicrophoneStopped;
        public UnityEvent OnPanelOpenCommand;
        public UnityEvent OnPanelCloseCommand;

        private string m_MicrophoneID = null;
        private AudioClip m_Recording = null;
        private int m_RecordingBufferSize = 2;
        private int m_RecordingHZ = 22050;


        private SpeechToText m_SpeechToText ;
        private TextToSpeech m_TextToSpeech ;

        void Start()
        {
            LogSystem.InstallDefaultReactors();
            Debug.Log("Start();");

            StartCoroutine(Initialize());

            
        }

        IEnumerator Initialize()
        {
            while (true)
            {
                if (m_IoTClient.Initilized == true)
                {
                    //Credentials stt_credentials = new Credentials("a64915a1-0c31-435b-971e-94c207660e42", "JVBgTnzuva6R", "https://stream.watsonplatform.net/speech-to-text/api");
                    //Credentials tts_credentials = new Credentials("9f847a3c-1b71-47d7-b25e-ae318fc05ff5", "w6IfIXPttGeH", "https://stream.watsonplatform.net/text-to-speech/api");
                    Credentials stt_credentials = new Credentials(m_IoTClient.SpeechToTextCred.username, m_IoTClient.SpeechToTextCred.password, m_IoTClient.SpeechToTextCred.url);
                    Credentials tts_credentials = new Credentials(m_IoTClient.TextToSpeechCred.username, m_IoTClient.TextToSpeechCred.password, m_IoTClient.TextToSpeechCred.url);

                    m_SpeechToText = new SpeechToText(stt_credentials);
                    m_TextToSpeech = new TextToSpeech(tts_credentials);
                    SendMicrophoneOffEvent();
                    m_TextToSpeech.Voice = VoiceType.en_US_Lisa;
                    m_SpeechToText.GetCustomizations(OnGetCustomizations);
                    break;
                }
                else
                {
                    
                }
                yield return new WaitForSeconds(3.0f);
            }
        }

        private void OnGetCustomizations(Watson.DeveloperCloud.Services.SpeechToText.v1.Customizations customizations, string customData)
        {
            if(customizations.customizations.Length > 0)
            {
                m_SpeechToText.CustomModel = customizations.customizations[0].customization_id;
            }
            
        }

        public bool Active
        {
            get { return m_SpeechToText.IsListening; }
            set
            {
                if (value && !m_SpeechToText.IsListening)
                {
                    m_SpeechToText.DetectSilence = true;
                    m_SpeechToText.EnableWordConfidence = false;
                    m_SpeechToText.EnableTimestamps = false;
                    m_SpeechToText.SilenceThreshold = 0.03f;
                    m_SpeechToText.MaxAlternatives = 1;
                    m_SpeechToText.EnableContinousRecognition = true;
                    m_SpeechToText.EnableInterimResults = true;
                    m_SpeechToText.OnError = OnError;
                    //m_SpeechToText.InactivityTimeout = 3600;
                    m_SpeechToText.StartListening(OnRecognize);
                }
                else if (!value && m_SpeechToText.IsListening)
                {
                    m_SpeechToText.StopListening();
                }
            }
        }

        private void StartRecording()
        {
            if (m_RecordingRoutine == 0)
            {
                UnityObjectUtil.StartDestroyQueue();
                m_RecordingRoutine = Runnable.Run(RecordingHandler());
            }
        }

        private void StopRecording()
        {
            if (m_RecordingRoutine != 0)
            {
                Microphone.End(m_MicrophoneID);
                Runnable.Stop(m_RecordingRoutine);
                m_RecordingRoutine = 0;
            }
        }

        private void OnError(string error)
        {
            Active = false;
            StopRecording();
            Debug.Log("Error! "+error);
            SendMicrophoneOffEvent();
            //StartCoroutine(RestartAfterError());
        }

        private IEnumerator RestartAfterError()
        {
            yield return new WaitForSeconds(2);
            Active = true;
            StartRecording();
        }

        private IEnumerator RecordingHandler()
        {
            m_Recording = Microphone.Start(m_MicrophoneID, true, m_RecordingBufferSize, m_RecordingHZ);
            yield return null;      // let m_RecordingRoutine get set..

            if (m_Recording == null)
            {
                StopRecording();
                yield break;
            }

            bool bFirstBlock = true;
            int midPoint = m_Recording.samples / 2;
            float[] samples = null;

            while (m_RecordingRoutine != 0 && m_Recording != null)
            {
                int writePos = Microphone.GetPosition(m_MicrophoneID);
                if (writePos > m_Recording.samples || !Microphone.IsRecording(m_MicrophoneID))
                {
                    Debug.Log("Microphone disconnected.");

                    StopRecording();
                    yield break;
                }

                if ((bFirstBlock && writePos >= midPoint)
                    || (!bFirstBlock && writePos < midPoint))
                {
                    // front block is recorded, make a RecordClip and pass it onto our callback.
                    samples = new float[midPoint];
                    m_Recording.GetData(samples, bFirstBlock ? 0 : midPoint);

                    AudioData record = new AudioData();
                    record.MaxLevel = Mathf.Max(samples);
                    record.Clip = AudioClip.Create("Recording", midPoint, m_Recording.channels, m_RecordingHZ, false);
                    record.Clip.SetData(samples, 0);

                    m_SpeechToText.OnListen(record);

                    bFirstBlock = !bFirstBlock;
                }
                else
                {
                    // calculate the number of samples remaining until we ready for a block of audio, 
                    // and wait that amount of time it will take to record.
                    int remaining = bFirstBlock ? (midPoint - writePos) : (m_Recording.samples - writePos);
                    float timeRemaining = (float)remaining / (float)m_RecordingHZ;

                    yield return new WaitForSeconds(timeRemaining);
                }

            }

            yield break;
        }

        private void OnRecognize(SpeechRecognitionEvent result)
        {
            if (result != null && result.results.Length > 0)
            {
                foreach (var res in result.results)
                {
                    foreach (var alt in res.alternatives)
                    {
                        string text = alt.transcript;
                        
                        if (res.final == true)
                        {
                            Debug.Log("Got Text:" + text);
                            ExecuteCommand(text);
                        }
                    }
                }
            }
        }

        private void ExecuteCommand(string text)
        {
            StartCoroutine(m_IoTClient.ExecuteCommand(text, result => {
                if (result != null && result.Trim().Length != 0)
                {
                    Speak(result);
                }

            }));
        }
        private void Speak(string text)
        {
            m_TextToSpeech.ToSpeech(text, ToSpeechCallback);
        }

        private void ToSpeechCallback(AudioClip clip,string customdata)
        {
            m_AudioSource.clip = clip;
            m_AudioSource.Play();
        }

        public void TurnOnMicrophone () {
            if (Active)
            {
                return;
            }
            Active = true;
            StartRecording();
        }

        public void TurnOffMicrophone () {
            if (!Active)
            {
                return;
            }
            Active = false;
            StopRecording();
        }

        // Call this from somewhere
        private void SendMicrophoneOffEvent () {
            OnMicrophoneStopped.Invoke ();
        }

        // Call this to open panels
        private void SendPanelOpenEvent () {
            OnPanelOpenCommand.Invoke ();
        }

        // Call this to close panels
        private void SendPanelCloseEvent () {
            OnPanelCloseCommand.Invoke ();
        }
    }
}