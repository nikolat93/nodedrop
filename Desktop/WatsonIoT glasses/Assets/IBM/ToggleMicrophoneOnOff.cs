﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ToggleMicrophoneOnOff : MonoBehaviour
{

    public GameObject microphoneOn;
    public GameObject microphoneOff;
    private bool isOn = false;

    public UnityEvent OnMicrophoneOn;
    public UnityEvent OnMicrophoneOff;

    public void ToggleOnOff()
    {
        SetMicrophoneOn(!isOn);
    }

    public void SetMicrophoneOn(bool isMicrophoneOn)
    {
        SetMicrophoneOn(isMicrophoneOn, true);
    }

    public void SetMicrophoneOn(bool isMicrophoneOn, bool sendEvents)
    {
        isOn = isMicrophoneOn;

        microphoneOn.SetActive(isOn);
        microphoneOff.SetActive(!isOn);

        if (!sendEvents)
        {
            return;
        }

        if (isOn)
        {
            OnMicrophoneOn.Invoke();
        }
        else
        {
            OnMicrophoneOff.Invoke();
        }
    }
}
