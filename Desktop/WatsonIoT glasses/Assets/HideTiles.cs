﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideTiles : MonoBehaviour {

	public void HideAll () {
		if (GameObject.Find ("Speed") != null) {
			GameObject SpeedUI = GameObject.Find ("Speed").transform.GetChild (1).transform.GetChild (0).gameObject;
			GameObject SpeedElastic = GameObject.Find ("Speed").transform.GetChild (2).transform.GetChild (1).gameObject;

			SpeedUI.SetActive (false);
			SpeedElastic.SetActive (false);
		}

		if (GameObject.Find ("Vibration") != null) {
			GameObject VibrationUI = GameObject.Find ("Vibration").transform.GetChild (1).transform.GetChild (0).gameObject;
			GameObject VibrationElastic = GameObject.Find ("Vibration").transform.GetChild (2).transform.GetChild (1).gameObject;

			VibrationUI.SetActive (false);
			VibrationElastic.SetActive (false);
		}

		if (GameObject.Find ("Bearing_Temp") != null) {
			GameObject Bearing_TempUI = GameObject.Find ("Bearing_Temp").transform.GetChild (1).transform.GetChild (0).gameObject;
			GameObject Bearing_TempElastic = GameObject.Find ("Bearing_Temp").transform.GetChild (2).transform.GetChild (1).gameObject;

			Bearing_TempUI.SetActive (false);
			Bearing_TempElastic.SetActive (false);
		}

		if (GameObject.Find ("Pressure In") != null) {
			GameObject PressureInUI = GameObject.Find ("Pressure In").transform.GetChild (1).transform.GetChild (0).gameObject;
			GameObject PressureInElastic = GameObject.Find ("Pressure In").transform.GetChild (2).transform.GetChild (1).gameObject;

			PressureInUI.SetActive (false);
			PressureInElastic.SetActive (false);
		}

		if (GameObject.Find ("Pressure Out") != null) {
			GameObject PressureOutUI = GameObject.Find ("Pressure Out").transform.GetChild (1).transform.GetChild (0).gameObject;
			GameObject PressureOutElastic = GameObject.Find ("Pressure Out").transform.GetChild (2).transform.GetChild (1).gameObject;

			PressureOutUI.SetActive (false);
			PressureOutElastic.SetActive (false);
		}

		if (GameObject.Find ("Tank Level") != null) {
			GameObject TankLevelUI = GameObject.Find ("Tank Level").transform.GetChild (1).transform.GetChild (0).gameObject;
			GameObject TankLevelElastic = GameObject.Find ("Tank Level").transform.GetChild (2).transform.GetChild (1).gameObject;

			TankLevelUI.SetActive (false);
			TankLevelElastic.SetActive (false);
		}
	}
}
